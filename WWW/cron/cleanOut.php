<?php
$expiretime=2880; //expire time in minutes, 7 days = 7*24*60
$tmpFolder='temp/';
$fileTypes="*.*";

foreach (glob($tmpFolder . $fileTypes) as $Filename) {
	// Read file creation time
	$FileCreationTime = filemtime($Filename);
	// Calculate file age in seconds
	$FileAge = time() - $FileCreationTime;
	// Is the file older than the given time span?
	if ($FileAge > ($expiretime)){
		// Now do something with the olders files...
		echo "The file $Filename is older than $expiretime minutes (".$FileAge.") (".$FileCreationTime.")<br/>";

		//delete files:
		unlink($Filename);
	}

}
?>