function validateForm() {
    //event.preventDefault();

    // Remove all messages and borders
    $('.error-message').remove();
    $('input[name=client_name]').css("border", "");
    $('[name=insurance_company]').css("border", "");
    $('[name=advisorID]').css("border", "");
    $('[name=staffID]').css("border", "");
    $('#client_dob_wrapper').css("border", "");
    $('input[name=client_address]').css("border", "");
    $('input[name=client_city]').css("border", "");
    $('[name=client_state]').css("border", "");
    $('input[name=client_postcode]').css("border", "");
    $('[name=client_country]').css("border", "");
    $('input[name=client_email]').css("border", "");
    // Get values
    var status= true;
    var a = document.getElementById("client_name_value").value;
    var b = document.getElementById("insurance_company_value").value;
    var f = document.getElementById("date-picker-1").value;
    var i = document.getElementById("client_address_value").value;
    var j = document.getElementById("client_city_value").value;
    var k = document.getElementById("client_state_value").value;
    var l = document.getElementById("client_postcode_value").value;
    var m = document.getElementById("client_country_value").value;
    var n = document.getElementById("client_phone_value").value;
    var o = document.getElementById("client_mobile_value").value;
    var p = document.getElementById("client_workphone_value").value;
    // var q = document.getElementById("client_email_value").value;

    // Compare values for blanks
    if (a == "") { $('#client_name').append('<p class="bg-danger error-message">Please ensure you enter a name</p>'); status=false;}
    if (b == "0") { $('#insurance_company').append('<p class="bg-danger error-message">Please ensure you select an insurance company</p>'); status=false;}

    // if (c == "0") { $('#advisorID').append('<p class="bg-danger error-message">Please ensure you select an advisor</p>'); }
    // if (d == "0") { $('#staffID').append('<p class="bg-danger error-message">Please ensure you select a staff member</p>'); }
    if (f == "") { $('#client_DOB').append('<p class="bg-danger error-message">Please ensure you enter a date of birth</p>'); status=false;}
    // if (g == "") { $('#client_sex').append('<p class="bg-danger error-message">Please ensure you select a gender</p>'); status=false;}
    if (i == "" || j == "" || k == "0" || l == "") { $('#client_address').append('<p class="bg-danger error-message">Please ensure you have entered all required address fields</p>'); status=false;}
    if (n == "" && o == "" && p == "") { $('#client_phone').append('<p class="bg-danger error-message">Please ensure you enter one phone number</p>'); status=false;}
    // if (q == "") { $('#client_email').append('<p class="bg-danger error-message">Please ensure you enter a email</p>'); status=false;}
    // Set borders if blank
    if (a == "") { $('input[name=client_name]').css("border", "2px solid red"); status=false;}
    if (b == "0") { $('[name=insurance_company]').css("border", "2px solid red"); status=false;}
    // if (c == "0") { $('[name=advisorID]').css("border", "2px solid red"); }
    // if (d == "0") { $('[name=staffID]').css("border", "2px solid red"); }
    if (f == "") { $('#client_dob_wrapper').css("border", "2px solid red"); status=false;}
    if (i == "") { $('input[name=client_address]').css("border", "2px solid red"); status=false;}
    if (j == "") { $('input[name=client_city]').css("border", "2px solid red");status=false; }
    if (k == "0") { $('[name=client_state]').css("border", "2px solid red"); status=false;}
    if (l == "") { $('input[name=client_postcode]').css("border", "2px solid red");status=false; }
    // if (m == "0") { $('[name=client_country]').css("border", "2px solid red"); status=false;}
    // if (q == "") { $('input[name=client_email]').css("border", "2px solid red"); status=false;}
    
    if (status==false) {
        return false;
    } else {
        $('#referal-form').submit();
    }
} 

function validateFormClient() {
    //event.preventDefault();

    // Remove all messages and borders
    $('.error-message').remove();
    $('input[name=client_name]').css("border", "");
    $('[name=insurance_company]').css("border", "");
    $('#client_dob_wrapper').css("border", "");
    $('input[name=client_address]').css("border", "");
    $('input[name=client_city]').css("border", "");
    $('[name=client_state]').css("border", "");
    $('input[name=client_postcode]').css("border", "");
    $('[name=client_country]').css("border", "");
    $('input[name=client_email]').css("border", "");
    var status= true;

    // Get values
    // var a = document.forms["referal-form"]["client_name"].value;
    // var b = document.forms["referal-form"]["insurance_company"].value;
    // var f = document.forms["referal-form"]["client_DOB"].value;
    // var g = document.forms["referal-form"]["client_sex"].value;
    // var i = document.forms["referal-form"]["client_address"].value;
    // var j = document.forms["referal-form"]["client_city"].value;
    // var k = document.forms["referal-form"]["client_state"].value;
    // var l = document.forms["referal-form"]["client_postcode"].value;
    // var m = document.forms["referal-form"]["client_country"].value;
    // var n = document.forms["referal-form"]["client_phone"].value;
    // var o = document.forms["referal-form"]["client_mobile"].value;
    // var p = document.forms["referal-form"]["client_workphone"].value;

    var a = document.getElementById("client_name_value").value;
    var b = document.getElementById("insurance_company_value").value;
    var f = document.getElementById("date-picker-1").value;
    var i = document.getElementById("client_address_value").value;
    var j = document.getElementById("client_city_value").value;
    var k = document.getElementById("client_state_value").value;
    var l = document.getElementById("client_postcode_value").value;
    // var m = document.getElementById("client_country_value").value;
    var n = document.getElementById("client_phone_value").value;
    var o = document.getElementById("client_mobile_value").value;
    var p = document.getElementById("client_workphone_value").value;
    // var q = document.getElementById("client_email_value").value;

    // Compare values for blanks
    if (a == "") { $('#client_name').append('<p class="bg-danger error-message">Please ensure you enter a name</p>'); status=false;}
    if (b == "0") { $('#insurance_company').append('<p class="bg-danger error-message">Please ensure you select an insurance company</p>'); status=false;}
    if (f == "") { $('#client_DOB').append('<p class="bg-danger error-message">Please ensure you enter a date of bith</p>'); status=false;}
    // if (g == "") { $('#client_sex').append('<p class="bg-danger error-message">Please ensure you select a gender</p>'); status=false;}
    if (i == "" || j == "" || k == "0" || l == "") { $('#client_address').append('<p class="bg-danger error-message">Please ensure you have entered all required address fields</p>'); status=false;}
    if (n == "" && o == "" && p == "") { $('#client_phone').append('<p class="bg-danger error-message">Please ensure you enter one phone number</p>'); status=false;}
    // if (q == "") { $('#client_email').append('<p class="bg-danger error-message">Please ensure you enter a email</p>'); status=false;}

    // Set borders if blank
    if (a == "") { $('input[name=client_name]').css("border", "2px solid red"); status=false;}
    if (b == "0") { $('[name=insurance_company]').css("border", "2px solid red"); status=false;}
    if (f == "") { $('#client_dob_wrapper').css("border", "2px solid red"); status=false;}
    if (i == "") { $('input[name=client_address]').css("border", "2px solid red"); status=false;}
    if (j == "") { $('input[name=client_city]').css("border", "2px solid red"); status=false;}
    if (k == "0") { $('[name=client_state]').css("border", "2px solid red"); status=false;}
    if (l == "") { $('input[name=client_postcode]').css("border", "2px solid red"); status=false;}
    // if (m == "0") { $('[name=client_country]').css("border", "2px solid red"); status=false;}
    // if (q == "") { $('input[name=client_email]').css("border", "2px solid red"); status=false;}

    if (status==false) {
        return false;
    } else {
        $('#referal-form').submit();
    }
}

function validateFormAdmin() {
    //event.preventDefault();

    // Remove all messages and borders
    $('.error-message').remove();
    $('input[name=client_name]').css("border", "");
    $('[name=insurance_company]').css("border", "");
    $('[name=advisorID]').css("border", "");
    $('[name=staffID]').css("border", "");
    $('#client_dob_wrapper').css("border", "");
    $('input[name=client_address]').css("border", "");
    $('input[name=client_city]').css("border", "");
    $('[name=client_state]').css("border", "");
    $('input[name=client_postcode]').css("border", "");
    $('[name=client_country]').css("border", "");
    $('input[name=client_email]').css("border", "");
    // Get values
    var status= true;
    var a = document.getElementById("client_name_value").value;
    var b = document.getElementById("insurance_company_value").value;
    var c = document.getElementById("advisorID_value").value;
    var d = document.getElementById("staffID_value").value;
    var f = document.getElementById("date-picker-1").value;
    var i = document.getElementById("client_address_value").value;
    var j = document.getElementById("client_city_value").value;
    var k = document.getElementById("client_state_value").value;
    var l = document.getElementById("client_postcode_value").value;
    // var m = document.getElementById("client_country_value").value;
    var n = document.getElementById("client_phone_value").value;
    var o = document.getElementById("client_mobile_value").value;
    var p = document.getElementById("client_workphone_value").value;
    // var q = document.getElementById("client_email_value").value;

    // Compare values for blanks
    if (a == "") { $('#client_name').append('<p class="bg-danger error-message">Please ensure you enter a name</p>'); status=false;}
    if (b == "0") { $('#insurance_company').append('<p class="bg-danger error-message">Please ensure you select an insurance company</p>'); status=false;}

    if (c == "0") { $('#advisorID').append('<p class="bg-danger error-message">Please ensure you select an advisor</p>'); }
    if (d == "0") { $('#staffID').append('<p class="bg-danger error-message">Please ensure you select a staff member</p>'); }
    if (f == "") { $('#client_DOB').append('<p class="bg-danger error-message">Please ensure you enter a date of birth</p>'); status=false;}
    // if (g == "") { $('#client_sex').append('<p class="bg-danger error-message">Please ensure you select a gender</p>'); status=false;}
    if (i == "" || j == "" || k == "0" || l == "") { $('#client_address').append('<p class="bg-danger error-message">Please ensure you have entered all required address fields</p>'); status=false;}
    if (n == "" && o == "" && p == "") { $('#client_phone').append('<p class="bg-danger error-message">Please ensure you enter one phone number</p>'); status=false;}
    // if (q == "") { $('#client_email').append('<p class="bg-danger error-message">Please ensure you enter a email</p>'); status=false;}
    // Set borders if blank
    if (a == "") { $('input[name=client_name]').css("border", "2px solid red"); status=false;}
    if (b == "0") { $('[name=insurance_company]').css("border", "2px solid red"); status=false;}
    if (c == "0") { $('[name=advisorID]').css("border", "2px solid red"); }
    if (d == "0") { $('[name=staffID]').css("border", "2px solid red"); }
    if (f == "") { $('#client_dob_wrapper').css("border", "2px solid red"); status=false;}
    if (i == "") { $('input[name=client_address]').css("border", "2px solid red"); status=false;}
    if (j == "") { $('input[name=client_city]').css("border", "2px solid red");status=false; }
    if (k == "0") { $('[name=client_state]').css("border", "2px solid red"); status=false;}
    if (l == "") { $('input[name=client_postcode]').css("border", "2px solid red");status=false; }
    // if (m == "0") { $('[name=client_country]').css("border", "2px solid red"); status=false;}
    // if (q == "") { $('input[name=client_email]').css("border", "2px solid red"); status=false;}
    
    if (status==false) {
        return false;
    } else {
        $('#referal-form').submit();
    }
} 