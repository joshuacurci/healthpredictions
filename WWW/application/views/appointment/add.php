<h1 class="page-header col-xs-11">Add New Client Appointment</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/appointment/newclientapp/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
    <input type="hidden" name="clientID" value="<? echo $clientID ?>"/>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Type:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="app_type">
        <option selected value="">Please select your appointment type</option>
        <option value="D">Doctor</option>
        <option value="P">Pathologist</option>
        <option value="S">Specialist</option>
        <option value="N">Nurse</option>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="app_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Time:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Appointment Time eg. 02:00pm" name="app_time" >
      </div>
      <div style="clear:both"></div>
    </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Date:</label>
      <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="app_date"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Location:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Location" name="app_place" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Service:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Service" name="app_service" >
      </div>
      <div style="clear:both"></div>
    </div>


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientID ?>" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->