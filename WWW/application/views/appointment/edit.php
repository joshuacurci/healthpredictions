<h1 class="page-header col-xs-11">Add New Client Appointment</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/appointment/saveclientapp/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
    <input type="hidden" name="clientID" value="<? echo $appData[0]['clientID'] ?>"/>
    <input type="hidden" name="appID" value="<? echo $appData[0]['appID'] ?>"/>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Type:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="app_type">
        <option value="">Please select your appointment type</option>
        <option <? if($appData[0]['app_type'] == 'D' || $appData[0]['app_type'] == '0') { echo 'selected';} ?> value="D">Doctor</option>
        <option <? if($appData[0]['app_type'] == 'P' || $appData[0]['app_type'] == '1') { echo 'selected';} ?> value="P">Pathologist</option>
        <option <? if($appData[0]['app_type'] == 'S' || $appData[0]['app_type'] == '2') { echo 'selected';} ?> value="S">Specialist</option>
        <option <? if($appData[0]['app_type'] == 'N' || $appData[0]['app_type'] == '3') { echo 'selected';} ?> value="N">Nurse</option>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="app_name" value="<? echo $appData[0]['app_name'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Time:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Appointment Time eg. 02:00pm" name="app_time"  value="<? echo $appData[0]['app_time'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Appointment Date:</label>
      <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="app_date" value="<? echo date('d-m-Y',$appData[0]['app_date']); ?>"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Location:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Location" name="app_place"  value="<? echo $appData[0]['app_place'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Service:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Service" name="app_service"  value="<? echo $appData[0]['app_service'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>	
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->