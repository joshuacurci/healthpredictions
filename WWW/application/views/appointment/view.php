<h1 class="page-header col-xs-11">Appointment Information</h1><i class="fa fa-book col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
  <a href="<? echo base_url(); ?>index.php/pathology/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/pathology/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Appointment</a>
  <a href="<? echo base_url(); ?>index.php/pathology/edit/<? echo $appData[0]['appID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Appointment</a>
</div> 

<?
$servername = $this->db->hostname;
$username = $this->db->username;
$password = $this->db->password;
$dbname = $this->db->database;
            // Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM tbl_client WHERE clientID = ".$appData[0]['clientID']."";
$result = $conn->query($sql);
$num_rec = $result->num_rows;
$clientData = $result->fetch_assoc(); 

$sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData['assigned_advisor']."";
$result2 = $conn->query($sql2);
$num_rec2 = $result2->num_rows;
$advisorData = $result2->fetch_assoc(); 

?>
<!-- Client Details -->
<div class="col-md-12 ">
<div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $appData[0]['dateadded']);?></div>
<div class="col-xs-6">
    <div class="col-sm-12 top-buttons">
      <div class=""><h2>Client Details</h2></div> 
      
    </div> 
    <div class="col-md-12">
      <table class="view-table">
        <tr>
          <td class="view-title"><strong>Name:</strong></td>
          <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/"><?php echo $clientData['client_name']; ?></a></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Sex:</strong></td>
          <td>
            <? if ($clientData['client_sex'] == 'M') { echo 'Male'; }  ?>
            <? if ($clientData['client_sex'] == 'F') { echo 'Female'; }  ?>
          </td>
        </tr>

        <tr>
          <td class="view-title"><strong>Phone:</strong></td>
          <td><?php echo $clientData['client_phone']; ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Mobile:</strong></td>
          <td><?php echo $clientData['client_mobile']; ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Insurance Company:</strong></td>
          <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
           $conn = new mysqli($servername, $username, $password, $dbname);
           if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
          }

          $sql = "SELECT * FROM tbl_insurance WHERE insID = ".$clientData['insurance_company']."";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
           while($row = $result->fetch_assoc()) { 
            echo "<a href='//".$_SERVER['SERVER_NAME']."/insurance/view/".$row['insID']."/' target='_blank'>".$row['ins_name']."</a>"; 
          } } else { } ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Insurance Customer Reference Number:</strong></td>
          <td><?php echo $clientData['insurance_number']; ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Assigned Advisor:</strong></td>
          <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
           $conn = new mysqli($servername, $username, $password, $dbname);
           if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
          }

          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData['assigned_advisor']."";
          $result = $conn->query($sql);
          if ($result->num_rows > 0) {
           while($row = $result->fetch_assoc()) { 
            echo "<a href='//".$_SERVER['SERVER_NAME']."/advisor/view/".$row['advisorID']."/' target='_blank'>".$row['advisor_name']."</a>";
          } } else { } ?></td>
        </tr>


      </table>
    </div>
  </div>


  <!-- Appointment Details -->

  <div class="col-md-6 ">

    <div class="col-sm-12 top-buttons">
      <div class=""><h2>Appointment Details</h2></div> 
      <!-- <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $appData[0]['dateadded']);?></div> -->
    </div> 
    <div class="col-md-12">
      <table class="view-table">
        <tr>
          <td class="view-title"><strong>Appointment Date:</strong></td>
          <td><? echo date('d/m/Y',$appData[0]['app_date']); ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Appointment Time:</strong></td>
          <td> <? echo $appData[0]['app_time']; ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Service Booked:</strong></td>
          <td><?php echo $appData[0]['app_service']; ?></td>
        </tr>

        <tr>
          <td class="view-title"><strong>Address of Service:</strong></td>
          <td><?php echo $appData[0]['app_place']; ?></td>
        </tr>


      </table>
    </div>
  </div>
</div>
<!-- Last </div> will be in the footer -->