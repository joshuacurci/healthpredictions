<h1 class="page-header col-xs-11">Workload Report</h1> <i class="fa fa-briefcase col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<div class="col-sm-12">
  <div class="col-sm-4 ">
    <div class="search-box">
      <h3>Search Specific Date</h3>
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/workloadreport/search_app_date/"onsubmit="return IsEmpty()" name="workloadForm" >
        <div class="col-sm-12 search-box-item">

          <div class="search-field">
            <div class="search-title">Name:</div>
            <div>
              <input type="text" class="form-control" id="capital-text" placeholder="Name" name="name" >
            </div>
            <div class="search-title">Search Date:</div>
            <div class="input-group">

              <input id="date-picker-1" type="text" class="date-picker form-control" name="app_date"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Date</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>


  <div class="col-sm-8 ">
    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/workloadreport/search_between_date/">
        <div class="col-xs-12" style="padding-left: 0px;">
          <div class="col-xs-6" style="padding-left: 0px;">
            <h3>Search Between Dates </h3> 
          </div>
          <div class="col-xs-6 text-right" style="padding-right: 0px;">
          <span style="font-size: 14px;" class="text-right"></span>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="search-title">Name:</div>
          <div class="col-sm-6 searchdate_name">
            <input type="text" class="form-control" id="capital-text" placeholder="Name" name="name" >
          </div>
        </div>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Start Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-2" type="text" class="date-picker form-control" name="startdate"/>
              <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Last Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-3" type="text" class="date-picker form-control" name="lastdate"/>
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Between Dates</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>
  <div style="clear:both"></div>
</div>

<!-- SEARCH RESULTS  -->
<? 
if (isset($searchresults)) { 

  ?>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#result">Result</a></li>
    <li><a data-toggle="tab" href="#resultdetail">Result Detail</a></li>
  </ul>
  <div class="col-sm-12">
    <div class="tab-content">
      <div id="result" class="tab-pane fade in active">
        <h3>Search Results</h3>
        <table class="table table-striped table-responsive">
          <thead>
            <tr>
              <td>
                <strong>Name</strong>
              </td>
              <td>
                <strong>Appointment Count</strong>
              </td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
              <? if($searchresults['name'] == '') { ?>
                Name Field Empty
              <? 
              } else {
                 echo $searchresults['name'];
              } ?>
              </td>
              <td><? echo COUNT($countData); ?></td>
            </tr>
          </tbody>
        </table>
      </div>

      <div id="resultdetail" class="tab-pane fade in">
        <h3>Result Detail</h3>
        <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
            <tr>
              <th>Type</th>
              <th>Name</th>
              <th>Time</th>
              <th>Date</th>
              <th>Address</th>
              <th>Service</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">
            <? foreach ($appData as $appData) { ?>
              <tr>
                <td>
                  <? if ($appData['app_type'] == '0' || $appData['app_type'] == 'D') { echo "Doctor"; } 
                  else if ($appData['app_type'] == '1' || $appData['app_type'] == 'P') { echo "Pathologist"; } 
                  else if ($appData['app_type'] == '2' || $appData['app_type'] == 'S') { echo "Specialist"; } 
                  else if ($appData['app_type'] == '3' || $appData['app_type'] == 'N') { echo "Nurse"; } ?>
                </td>
                <td><? echo $appData['app_name']; ?></td>
                <td><? echo $appData['app_time']; ?></td>
                <td><? echo date('d/m/Y',$appData['app_date']); ?></td>
                <td><? echo $appData['app_place']; ?></td>
                <td><? echo $appData['app_service']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/edit/<? echo $appData['appID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/delete/<? echo $appData['appID']; ?>/<? echo $appData['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
              </tr>
              <? } ?>
            </tbody>
          </table>
            </div>
        </div>
      </div>
    </div>

    <? } else { ?>

      <div class="col-sm-12 ">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#pathologist">Pathologist</a></li>
          <li><a data-toggle="tab" href="#genprac">GP</a></li>
          <li><a data-toggle="tab" href="#specialist">Specialist</a></li>
          <li><a data-toggle="tab" href="#nurse">Nurse</a></li>
        </ul>
        <div class="col-sm-12">
          <div class="tab-content">
            <div id="pathologist" class="tab-pane fade in active">
              <div class="col-sm-12 top-buttons"> 
              <div style="overflow-x:auto;"> 
                <table class="table table-striped table-responsive" id="advisor-data">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Service</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Phone</th>
                      <th>Fax</th>
                      <th>Email</th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="myTable">
                    <? foreach ($pathologyData as $pathologyData) { ?>
                      <tr>
                        <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/pathology/view/<? echo $pathologyData['pathID']; ?>/" ><? echo $pathologyData['path_name']; ?></a></td>
                        <td style="width: 150px"><? echo $pathologyData['path_service']; ?></td>
                        <td><? echo $pathologyData['path_address']; ?></td>
                        <td><? echo $pathologyData['path_city']; ?></td>
                        <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

            // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                        if ($conn->connect_error) {
                          die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$pathologyData['path_state']."";
                        $result = $conn->query($sql);

                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) { ?>
                            <td><? echo $row['state_name']; ?></td>
                            <? }
                          } else { ?>
                            <td>No Assigned State</td>
                            <? } ?>
                            <td><? echo $pathologyData['path_phone']; ?></td>
                            <td><? echo $pathologyData['path_fax']; ?></td>
                            <td><? echo $pathologyData['path_email']; ?></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/view/<? echo $pathologyData['pathID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/edit/<? echo $pathologyData['pathID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/delete/<? echo $pathologyData['pathID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                          </tr>
                          <? } ?>
                        </tbody>
                      </table>
                          </div>
                    </div>
                  </div>

                  <!-- GP -->
                  <div id="genprac" class="tab-pane fade">
                    <div class="col-sm-12 top-buttons">  
                    <div style="overflow-x:auto;">
                      <table class="table table-striped table-responsive" id="advisor-data">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Phone</th>
                            <th>Fax</th>
                            <th>Email</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="myTable">
                          <? foreach ($gpData as $gpData) { ?>
                            <tr>
                              <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" ><? echo $gpData['GP_name']; ?></a></td>
                              <td><? echo $gpData['GP_address']; ?></td>
                              <td><? echo $gpData['GP_city']; ?></td>
                              <?
                              $servername = $this->db->hostname;
                              $username = $this->db->username;
                              $password = $this->db->password;
                              $dbname = $this->db->database;

            // Create connection
                              $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                              if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                              }

                              $sql = "SELECT * FROM tbl_au_states WHERE id = ".$gpData['GP_state']."";
                              $result = $conn->query($sql);

                              $num_rec = $result->num_rows;

                              if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) { ?>
                                  <td><? echo $row['state_name']; ?></td>
                                  <? }
                                } else { ?>
                                  <td>No Assigned State</td>
                                  <? } ?>
                                  <td><? echo $gpData['GP_phone']; ?></td>
                                  <td><? echo $gpData['GP_fax']; ?></td>
                                  <td><? echo $gpData['GP_email']; ?></td>
                                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/edit/<? echo $gpData['GPID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/delete/<? echo $gpData['GPID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                                </tr>
                                <? } ?>
                              </tbody>
                            </table>
                                </div>

                          </div>
                        </div>
                        <div id="specialist" class="tab-pane fade">
                          <div class="col-sm-12 top-buttons">  
                          <div style="overflow-x:auto;">
                            <table class="table table-striped table-responsive" id="advisor-data">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Address</th>
                                  <th>City</th>
                                  <th>State</th>
                                  <th>Phone</th>
                                  <th>Fax</th>
                                  <th>Email</th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody id="myTable">
                                <? foreach ($specialistData as $specialistData) { ?>
                                  <tr>
                                    <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" ><? echo $specialistData['spec_name']; ?></a></td>
                                    <td><? echo $specialistData['spec_address']; ?></td>
                                    <td><? echo $specialistData['spec_city']; ?></td>
                                    <?
                                    $servername = $this->db->hostname;
                                    $username = $this->db->username;
                                    $password = $this->db->password;
                                    $dbname = $this->db->database;

            // Create connection
                                    $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                                    if ($conn->connect_error) {
                                      die("Connection failed: " . $conn->connect_error);
                                    }

                                    $sql = "SELECT * FROM tbl_au_states WHERE id = ".$specialistData['spec_state']."";
                                    $result = $conn->query($sql);

                                    $num_rec = $result->num_rows;

                                    if ($result->num_rows > 0) {
                                      while($row = $result->fetch_assoc()) { ?>
                                        <td><? echo $row['state_name']; ?></td>
                                        <? }
                                      } else { ?>
                                        <td>No Assigned State</td>
                                        <? } ?>
                                        <td><? echo $specialistData['spec_phone']; ?></td>
                                        <td><? echo $specialistData['spec_fax']; ?></td>
                                        <td><? echo $specialistData['spec_email']; ?></td>
                                        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/edit/<? echo $specialistData['specID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/delete/<? echo $specialistData['specID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                                      </tr>
                                      <? } ?>
                                    </tbody>
                                  </table>
                                      </div>
                                </div>
                              </div>
                              <div id="nurse" class="tab-pane fade">
                                <div class="col-sm-12 top-buttons">  
                                <div style="overflow-x:auto;">
                                  <table class="table table-striped table-responsive" id="advisor-data">
                                    <thead>
                                      <tr>
                                        <th>Name</th>
                                        <th>Service</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>State</th>
                                        <th>Phone</th>
                                        <th>Fax</th>
                                        <th>Email</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody id="myTable">
                                      <? foreach ($nurseData as $nurseData) { ?>
                                        <tr>
                                          <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/nurse/view/<? echo $nurseData['nurseID']; ?>/" ><? echo $nurseData['nurse_name']; ?></a></td>
                                          <td style="width: 150px"><? echo $nurseData['nurse_service']; ?></td>
                                          <td><? echo $nurseData['nurse_address']; ?></td>
                                          <td><? echo $nurseData['nurse_city']; ?></td>
                                          <?
                                          $servername = $this->db->hostname;
                                          $username = $this->db->username;
                                          $password = $this->db->password;
                                          $dbname = $this->db->database;

            // Create connection
                                          $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                                          if ($conn->connect_error) {
                                            die("Connection failed: " . $conn->connect_error);
                                          }

                                          $sql = "SELECT * FROM tbl_au_states WHERE id = ".$nurseData['nurse_state']."";
                                          $result = $conn->query($sql);

                                          $num_rec = $result->num_rows;

                                          if ($result->num_rows > 0) {
                                            while($row = $result->fetch_assoc()) { ?>
                                              <td><? echo $row['state_name']; ?></td>
                                              <? }
                                            } else { ?>
                                              <td>No Assigned State</td>
                                              <? } ?>
                                              <td><? echo $nurseData['nurse_phone']; ?></td>
                                              <td><? echo $nurseData['nurse_fax']; ?></td>
                                              <td><? echo $nurseData['nurse_email']; ?></td>
                                              <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/view/<? echo $nurseData['nurseID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                                              <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/edit/<? echo $nurseData['nurseID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                              <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/delete/<? echo $nurseData['nurseID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                                            </tr>
                                            <? } ?>
                                          </tbody>
                                        </table>
                                            </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <? } ?>