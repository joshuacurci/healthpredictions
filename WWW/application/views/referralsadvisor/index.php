<h1 class="page-header col-xs-11">Referrals by Advisor</h1> <i class="fa fa-file-text-o col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12">
  <div class="col-sm-4 ">
    <div class="search-box">
      <h3>Search Specific Date</h3>
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referralsadvisor/search_app_date/">
        <div class="col-sm-12 search-box-item">
        <div class="search-title">Advisor:</div>
          <div class="search-field">
            <select class="form-control" id="inputorg1" name="advisorID">
              <option value="">All</option>
              <? foreach ($advData as $advData) { ?>
               <option value="<? echo $advData['advisorID'] ?>"><? echo $advData['advisor_name'] ?></option>
               <? } ?>
             </select>
           </div>
          <div class="search-title">Search Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-1" type="text" class="date-picker form-control" name="referral_date"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Date</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>


  <div class="col-sm-8 ">
    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referralsadvisor/search_between_date/">
        <div class="col-xs-12" style="padding-left: 0px;">
          <div class="col-xs-6" style="padding-left: 0px;">
            <h3>Search Between Dates </h3> 
          </div>
          <div class="col-xs-6 text-right" style="padding-right: 0px;">
            <span style="font-size: 14px;" class="text-right">Important: Select Dates to Get a Result</span>
          </div>
        </div>
         <div class="col-sm-12" style="padding: 0px;">
          <div class="search-title col-sm-12">Advisor:</div>
          <div class="search-field col-sm-6">
            <select class="form-control" id="inputorg1" name="advisorID">
              <option value="">All</option>
              <? foreach ($advData2 as $advData) { ?>
               <option value="<? echo $advData['advisorID'] ?>"><? echo $advData['advisor_name'] ?></option>
               <? } ?>
             </select>
           </div>
         </div>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Start Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-2" type="text" class="date-picker form-control" name="startdate"/>
              <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Last Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-3" type="text" class="date-picker form-control" name="lastdate"/>
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Between Dates</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>
  <div style="clear:both"></div>
</div>

<div class="col-sm-12 ">

  <div class="col-sm-12 top-buttons">  
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Organisation</th>
          <th>Address</th>
          <th>Email </th>
          <th>Email 2</th>
          <th>DOB</th>
          <th>Insurance Company</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($referralClient as $referralClient) { ?>

          <tr>
            <td><? echo $referralClient['client_name']; ?></td>
            <td><? echo $referralClient['client_organization']; ?></td>
            <td><? echo $referralClient['client_address']; ?>, <? echo $referralClient['client_city']; ?></td>
            <?  ?>
            <td><? echo $referralClient['client_email']; ?></td>
            <td><? echo $referralClient['client_email2']; ?></td>
            <td><? echo $referralClient['client_DOB']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;
            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_insurance WHERE insID = ".$referralClient['insurance_company']."";
            $result = $conn->query($sql);
            $num_rec = $result->num_rows;
            $insData = $result->fetch_assoc(); 
            ?>
            <td><? echo $insData['ins_name']; ?></td>
            <td></td>
           
          <? } ?>
        </tbody>
      </table>
          </div>
    </div>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
    </div>

<!-- Last </div> will be in the footer -->