<h1 class="page-header">Dashboard</h1>

  <div class="col-sm-12 top-buttons">
    <a href="<? echo base_url(); ?>index.php/referral/lodge1/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Lodge a Referral</a>
  </div>


<div class="col-xs-12 main-data-content">
<h3>Your Information</h3>
  <div class="col-xs-4 advisor-block"> <!-- Address -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $advisorData[0]['advisor_name']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Company:</strong></td>
        <td><?php echo $advisorData[0]['advisor_company']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Groups:</strong></td>
        <td><?php echo $advisorData[0]['advisor_groups']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td>
          <?php echo $advisorData[0]['advisor_address']; ?><br/>
          <?php echo $advisorData[0]['advisor_address2']; ?><br/>
          <?php echo $advisorData[0]['advisor_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData[0]['advisor_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $advisorData[0]['advisor_postcode']; ?><br/>
          </td>
      </tr>

      <?php if($advisorData[0]['advisor_postaladdress'] != ''){ ?>
      <tr>
        <td class="view-title"><strong>Postal Address:</strong></td>
        <td>
          <?php echo $advisorData[0]['advisor_postaladdress']; ?><br/>
          <?php echo $advisorData[0]['advisor_postaladdress2']; ?><br/>
          <?php echo $advisorData[0]['advisor_postalcity']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData[0]['advisor_postalstate']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $advisorData[0]['advisor_postalpostcode']; ?><br/>
          </td>
          <? } ?>
      </tr>
    </table>
  </div>

  <div class="col-xs-4 advisor-block"> <!-- Emails -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Main Email:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Joint Emails:</strong></td>
        <td>
          <?php echo $advisorData[0]['advisor_email2']; ?><br/>
          <?php echo $advisorData[0]['advisor_email3']; ?><br/>
          <?php echo $advisorData[0]['advisor_email4']; ?><br/>
          <?php echo $advisorData[0]['advisor_email5']; ?><br/>
          <?php echo $advisorData[0]['advisor_email6']; ?>
        </td>
      </tr>
      </table>
  </div>

  <div class="col-xs-4 advisor-block"> <!-- Emails -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Phone:</strong></td>
        <td><?php echo $advisorData[0]['advisor_phone']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Work Phone:</strong></td>
        <td><?php echo $advisorData[0]['advisor_workphone']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Mobile:</strong></td>
        <td><?php echo $advisorData[0]['advisor_mobile']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Fax:</strong></td>
        <td><?php echo $advisorData[0]['advisor_fax']; ?></td>
      </tr>
    </table>
  </div>

  <div style="clear:both"></div>

  <hr>
<h3>Submitted Referral</h3>
  <div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referral/advisorsearch/">
      <div class="col-sm-10 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-2" style="text-align: center;">
        <button type="submit" class="btn btn-primary" style="width:100%">Search</button>
      </div>
      <div style="clear:both"></div>
  </form>
</div>
<div style="clear:both"></div>

  <div class="col-sm-12 top-buttons">  
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Sex</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="">
        <? foreach ($clientData as $clientData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $clientData['referralID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
            <td style="width: 150px">
            <? if ($clientData['client_sex'] == 'M') { echo 'Male'; }  ?>
            <? if ($clientData['client_sex'] == 'F') { echo 'Female'; }  ?>
            </td>
            <td><? echo $clientData['client_address']; ?></td>
            <td><? echo $clientData['client_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData['client_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $clientData['client_phone']; ?></td>
                <td><? echo $clientData['client_fax']; ?></td>
                <td><? echo $clientData['client_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $clientData['referralID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/edit/<? echo $clientData['referralID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
              </tr>
              <? } ?>
            </tbody>
          </table>
              </div>
        </div>

</div>

<!-- Last </div> will be in the footer -->