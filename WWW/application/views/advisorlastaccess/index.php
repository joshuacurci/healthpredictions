<h1 class="page-header col-xs-11">Advisor Last Access</h1> <i class="fa fa-low-vision col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
$siteID = $_SESSION['siteID'];
?>

<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/advisorlastaccess/search_last_access/">
      <div class="col-sm-1 search-box-item">
        <button type="submit" name="1month" class="btn btn-primary">1 month</button>
      </div>

      <div class="col-sm-1 search-box-item">

        <button type="submit" name="3month" class="btn btn-primary">3 months</button>

      </div>

      <div class="col-sm-1 search-box-item">

        <button type="submit" name="6month" class="btn btn-primary">6 months</button>

      </div>
      <div style="clear:both"></div>
    </form>
  </div>
  <div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
?>
  <div class="col-sm-12">
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Advisor Name</th>
          <th>Advisor Buisness Name</th>
          <th>Address</th>
          <th>City</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($advisorData as $IndAdvisor) { ?>
          <tr>
            <td><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $IndAdvisor['advisorID']; ?>/" ><? echo $IndAdvisor['advisor_name']; ?></a></td>
            <td><? echo $IndAdvisor['advisor_company']; ?></td>
            <td><? echo $IndAdvisor['advisor_address']; ?></td>
            <td><? echo $IndAdvisor['advisor_city']; ?></td>
            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $IndAdvisor['advisorID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/edit/<? echo $IndAdvisor['advisorID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/delete/<? echo $IndAdvisor['advisorID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
          </tr>
          <? } ?>
        </tbody>
      </table>
        </div>
    </div>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
    </div>
    <? } ?>


<!-- Last </div> will be in the footer -->