<h1 class="page-header col-xs-11">Insurance Information</h1> <i class="fa fa-building col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/insurance/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Insurance</a>
  <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
  <a href="<? echo base_url(); ?>index.php/insurance/edit/<? echo $insuranceData[0]['insID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Insurance</a>
  <?php } ?>
</div>  

<div class="col-xs-6 top-buttons">
  <div class="name-header"><i class="fa fa-building" aria-hidden="true"></i> <?php echo $insuranceData[0]['ins_name']; ?></div>
<div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $insuranceData[0]['dateadded']);?></div> 
</div> 



<div class="col-xs-12 main-data-content">
  <div class="col-xs-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $insuranceData[0]['ins_name']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Responsible Person:</strong></td>
        <td><?php echo $insuranceData[0]['ins_responsibleperson']; ?></td>
      </tr>
    </table>
  </div>

  <div class="col-xs-4">
  <table class="view-table">
    <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td>
          <?php echo $insuranceData[0]['ins_address']; ?><br/>
          <?php echo $insuranceData[0]['ins_address2']; ?><br/>
          <?php echo $insuranceData[0]['ins_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$insuranceData[0]['ins_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $insuranceData[0]['ins_postcode']; ?><br/>
            <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_country WHERE id = ".$insuranceData[0]['ins_country']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['country_name']; ?>
                    <? }
                  } else { } ?>
        </td>
      </tr>
      

    </table>
  </div>


  <div class="col-xs-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Phone Number:</strong></td>
        <td><?php echo $insuranceData[0]['ins_phone']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Fax Number:</strong></td>
        <td><?php echo $insuranceData[0]['ins_fax']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email Address 1:</strong></td>
        <td><?php echo $insuranceData[0]['ins_email']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email Address 2:</strong></td>
        <td><?php echo $insuranceData[0]['ins_email2']; ?></td>
      </tr>
        </table>
  </div>

  <div style="clear:both"></div>
  <hr/>


  <div class="col-xs-6">
    <strong>Instructions: </strong><br/>
    <?php echo $insuranceData[0]['ins_notes']; ?>
  </div>

  <div class="col-xs-6">
    <strong>Instructions: </strong><br/>
    <?php echo $insuranceData[0]['ins_ins4results']; ?>
  </div>
  <div style="clear:both"></div>

  <div class="col-xs-6">
    <strong>Underwriting Contract: </strong><br/>
    <?php echo $insuranceData[0]['ins_underwritingcontract']; ?>
  </div>

      </div>      

<!-- Last </div> will be in the footer -->