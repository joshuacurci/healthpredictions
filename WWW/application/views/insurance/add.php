<h1 class="page-header col-xs-11">Add New Insurance</h1> <i class="fa fa-building col-xs-1" aria-hidden="true"></i>

<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/insurance/newinsurance/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="ins_name" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="ins_address" />
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 2" name="ins_address2" >
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="ins_city" >
        <select class="form-control address-half address-feilds" id="inputorg1" name="ins_state">
          <option selected value="0">Please select your state</option>
          <? foreach ($insuranceState as $insuranceState) { ?>
           <option value="<? echo $insuranceState['id'] ?>"><? echo $insuranceState['state_code'] ?></option>
           <? } ?>
         </select>
         <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="ins_postcode" >
         <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="ins_country">
          <option value="0">Please select your country</option>
          <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $countrydata) { ?>
            <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
            <? } ?>
          </select>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="ins_phone" />
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="ins_fax"/>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 1" name="ins_email" />
        </div>
        <div style="clear:both"></div>
      </div>
      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 2" name="ins_email2" />
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Person Responsible:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Person Responsible" name="ins_responsibleperson" />
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Account Contact:</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Name" name="ins_contactname" >
        </div>
        <div class="col-sm-3">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Email" name="ins_contactemail" >
        </div>
        <div class="col-sm-3">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Phone Number" name="ins_contactphone" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Add Underwriting Contract:</label>
        <div class="col-sm-9">
          <textarea class="form-control" id="inputrecNum1" placeholder="Underwriting Contract" name="ins_underwritingcontract" ></textarea>
        </div>
        <div style="clear:both"></div>
      </div>  

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Add Notes:</label>
        <div class="col-sm-9">
        <textarea class="form-control" placeholder="Notes" name="ins_notes" ></textarea>
        </div>
        <div style="clear:both"></div>
      </div>  

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Add Intructions For Results:</label>
        <div class="col-sm-9">
          <textarea class="form-control" placeholder="Add Intructions For Results" name="ins_ins4results"></textarea>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
        </div>
      </div>

    </form>

  </div>      

<!-- Last </div> will be in the footer -->