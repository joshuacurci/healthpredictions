<h1 class="page-header col-xs-11">Insurance</h1> <i class="fa fa-building col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/insurance/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Insurance</a>
</div>  
<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/insurance/search/">
      <div class="col-sm-4 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Insurance Name" name="ins_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['ins_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By City" name="ins_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['ins_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-3 search-box-item">
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="ins_state">
          <option value="">Please Select State</option>
          <option value="">All</option>
          <? foreach ($insuranceState as $insuranceState) { ?>
           <option value="<? echo $insuranceState['id'] ?>"><? echo $insuranceState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

     <div class="col-sm-2">
      <button type="submit" class="btn btn-primary">Search</button>
    </div>
    <div style="clear:both"></div>
    <div class="advanced-search-link"><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/insurance/search_all">Advanced search</a></div>
  </form>
</div>
<div style="clear:both"></div>
<div class="col-sm-12 ">

  <div class="col-sm-12 top-buttons"> 
  <div style="overflow-x:auto;"> 
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Responsible Person</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($insuranceData as $insuranceData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/insurance/view/<? echo $insuranceData['insID']; ?>/" ><? echo $insuranceData['ins_name']; ?></a></td>
            <td style="width: 150px"><? echo $insuranceData['ins_responsibleperson']; ?></td>
            <td><? echo $insuranceData['ins_address']; ?></td>
            <td><? echo $insuranceData['ins_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$insuranceData['ins_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $insuranceData['ins_phone']; ?></td>
                <td><? echo $insuranceData['ins_fax']; ?></td>
                <td><? echo $insuranceData['ins_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/insurance/view/<? echo $insuranceData['insID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/insurance/edit/<? echo $insuranceData['insID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/insurance/delete/<? echo $insuranceData['insID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                <?php } ?>
              </tr>
              <? } ?>
            </tbody>
          </table>
                </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>

