<h1 class="page-header col-xs-11">Pathologist Information</h1><i class="fa fa-heartbeat col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/pathology/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Pathologist</a>
  <a href="<? echo base_url(); ?>index.php/pathology/edit/<? echo $pathologyData[0]['pathID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Pathologist</a>
</div> 

<div class="col-xs-6 top-buttons">
  <div class="name-header"><i class="fa fa-heartbeat" aria-hidden="true"></i> <?php echo $pathologyData[0]['path_name']; ?></div> 
<div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $pathologyData[0]['dateadded']);?></div>
</div> 



<div class="col-xs-12 main-data-content">
  <div class="col-md-4">
    <dl class="dl-horizontal">
      <dt>
        Name:<br/>
        Pathology Service:<br/>
        Pathology Company:<br/>
        Practice Manager:<br/>
        Email:<br/>
        Fax:<br/>
      </dt>
      <dd>
        <?php echo $pathologyData[0]['path_name']; ?><br/>        
        <?php echo $pathologyData[0]['path_service']; ?><br/>
        <?php echo $pathologyData[0]['path_company']; ?><br/>
        <?php echo $pathologyData[0]['path_responsibleperson']; ?><br/>
        <?php echo $pathologyData[0]['path_email']; ?><br/>
         <?php echo $pathologyData[0]['path_fax']; ?><br/>
      </dd>
    </dl>
  </div>
  <div class="col-md-4">
    <dl class="dl-horizontal">
      <dt>
        Acc. Number:<br/>
        Acc. Contact Name:<br/>
        Acc. Contact Email:<br/>
        Acc. Contact Phone #:<br/>
        Phone Number:<br/>
        Mobile Number:<br/>
        
      </dt>
      <dd>
        <?php echo $pathologyData[0]['path_accnumber']; ?><br/>
        <?php echo $pathologyData[0]['path_contactname']; ?><br/>
        <?php echo $pathologyData[0]['path_contactemail']; ?><br/>
        <?php echo $pathologyData[0]['path_contactphone']; ?><br/>
        <?php echo $pathologyData[0]['path_phone']; ?><br/>
        <?php echo $pathologyData[0]['path_mobile']; ?><br/>
       
    </dl>
  </div>

  <div class="col-md-4">
    <dl class="dl-horizontal">
      <dt>
        Address:
      </dt>
      <dd>
        <?php echo $pathologyData[0]['path_address']; ?><br/>
        <?php echo $pathologyData[0]['path_address2']; ?><br/>
        <?php echo $pathologyData[0]['path_city']; ?> ,<?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$pathologyData[0]['path_state']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <? echo $row['state_name']; ?>
            <? }
          } else { ?>

            <? } ?>,
            <?php echo $pathologyData[0]['path_postcode']; ?><br/>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_country WHERE id = ".$pathologyData[0]['path_country']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['country_name']; ?>
                <? }
              } else { ?>
                <? } ?>
              </dd>
            </dl>
          </div>
          <div style="clear:both"></div>
          <hr/>


          <div class="col-xs-12">
            <strong>Instructions: </strong><br/>
            <?php echo $pathologyData[0]['path_instructions']; ?>
          </div>
          
        </div>   

<!-- Last </div> will be in the footer -->