<h1 class="page-header col-xs-11">Search Pathology</h1><i class="fa fa-heartbeat col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
  <? 
  if (isset($searchresults)) { 
    echo '<h3>Search Results</h3>'; 
  } else {
    ?>
    <div class="search-div-searches">
      <div class="search-box">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/pathology/search_result/">
          <div class="col-sm-12 ">
          <center><h3>Pathology Advanced Search</h3>
             <p>Fill in one or more criteria to search for a Pathologist.</p></center>
           </div>
           <br/><br/>
           <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Name:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Name" name="path_name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Service:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Service" name="path_service" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Service Company:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Service Company" name="path_company" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 1:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Address Line 1" name="path_address" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 2:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum2" placeholder="Address Line 2" name="path_address2" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">City/Suburb:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="City/Suburb" name="path_city" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg1" name="path_state">
                <option selected value="">Please select your state/territory</option>
                <? foreach ($pathologyState as $gpState) { ?>
                 <option value="<? echo $gpState['id'] ?>"><? echo $gpState['state_code'] ?></option>
                 <? } ?>
               </select>
             </div>
             <div style="clear:both"></div>
           </div>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Postcode:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="path_postcode" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Country:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg2" name="path_country">
                <option value="">Please select your country</option>
                <? foreach ($country as $countrydata) { ?>
                  <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Phone Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="path_phone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Mobile Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="path_mobile" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Fax Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="path_fax" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="path_email" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Account Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Account Number" name="path_accnumber" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Account Contact Name:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Account Contact Name" name="path_contactname" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Account Contact Email:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Account Contact Email" name="path_contactemail" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Account Contact Phone Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Account Contact Phone Number" name="path_contactphone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Practice Manager:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Practice Manager" name="path_responsibleperson" />
              </div>
              <div style="clear:both"></div>
            </div> 

            <div style="clear:both"></div>

            <div class="col-sm-12">
              <button type="submit" class="btn btn-primary">Search</button>
            </div>
            <div style="clear:both"></div>
          </form>
        </div>
      </div>
      <?php } ?>

      <? if(isset($searchresults)) { echo '<div style="display: block;">';
    } else { echo '<div style="display: none;">'; }
    ?>
    <a href="<? echo base_url(); ?>index.php/pathology/search_all/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <div class="col-sm-12 ">
      <div class="col-sm-12 top-buttons">
      <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
            <tr>
              <th>Name</th>
              <th>Service</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Phone</th>
              <th>Fax</th>
              <th>Email</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">
            <? foreach ($pathologyData as $pathologyData) { ?>
              <tr>
                <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/pathology/view/<? echo $pathologyData['pathID']; ?>/" /><? echo $pathologyData['path_name']; ?></a></td>
                <td style="width: 150px"><? echo $pathologyData['path_service']; ?></td>
                <td><? echo $pathologyData['path_address']; ?></td>
                <td><? echo $pathologyData['path_city']; ?></td>
                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_au_states WHERE id = ".$pathologyData['path_state']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['state_name']; ?></td>
                    <? }
                  } else { ?>
                    <td>No Assigned State</td>
                    <? } ?>
                    <td><? echo $pathologyData['path_phone']; ?></td>
                    <td><? echo $pathologyData['path_fax']; ?></td>
                    <td><? echo $pathologyData['path_email']; ?></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/view/<? echo $pathologyData['pathID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/edit/<? echo $pathologyData['pathID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/pathology/delete/<? echo $pathologyData['pathID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                  </tr>
                  <? } ?>
                </tbody>
              </table>
                  </div>
            </div>

            <div class="col-md-12 text-center">
              <ul class="pagination pagination-lg" id="myPager"></ul>
            </div>
          </div>

<!-- Last </div> will be in the footer -->