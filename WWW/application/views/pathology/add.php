<h1 class="page-header col-xs-11">Add New Pathologist</h1> <i class="fa fa-heartbeat col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/pathology/newpathology/">
      <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="path_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Service:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Service" name="path_service" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Service Company:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Service Company" name="path_company" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="path_address" >
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 2" name="path_address2" >
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="path_city" >
        <select class="form-control address-half address-feilds" id="inputorg1" name="path_state">
        <option selected value="0">Please select your state</option>
         <? foreach ($pathologyState as $pathologyState) { ?>
           <option value="<? echo $pathologyState['id'] ?>"><? echo $pathologyState['state_code'] ?></option>
           <? } ?>
         </select>
         <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="path_postcode" >
         <div class="clearboth"></div>
          <select class="form-control address-feilds" id="inputorg2" name="path_country">
          <option value="0">Please select your country</option>
          <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $countrydata) { ?>
            <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
           <? } ?>
         </select>
      </div>
      <div style="clear:both"></div>
    </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone" name="path_phone" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile" name="path_mobile" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax" name="path_fax" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email" name="path_email" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Account Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Account Number" name="path_accnumber" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Account Contact:</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Name" name="path_contactname" >
      </div>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Email" name="path_contactemail" >
      </div>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Phone Number" name="path_contactphone" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Practice Manager:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Practice Manager" name="path_responsibleperson" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Add Instructions:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Instructions" name="path_instructions" ></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->