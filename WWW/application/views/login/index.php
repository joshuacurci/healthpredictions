<?php
  if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
    echo '<span style="z-index: 5000; position: absolute; top: 0; right: 1.5em; padding: 0.3rem; font-size: 16px; background: green; color: #fff; border-radius: 0 0 5px 5px;">Local</span>';
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Welcome to the Health Predictions Portal</title>

  <style>
    :root {
      --main-color: #<? echo $sitedetails[0]['site_maincolour'] ?>;
      --main-color-text: #<? echo $sitedetails[0]['site_maincolour_text'] ?>;
      --second-color: #<? echo $sitedetails[0]['site_seccolour'] ?>;
      --second-color-text: #<? echo $sitedetails[0]['site_seccolour_text'] ?>;
      --thierd-color: #<? echo $sitedetails[0]['site_thrcolour'] ?>;
      --thierd-color-text: #<? echo $sitedetails[0]['site_thrcolour_text'] ?>;
    }
  </style>

  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/main.css">
    <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
  <script>tinymce.init({selector:'textarea'});</script>
</head>

<body>
<div id="container" style="width:100%; height:100%;">
	<div class="login-wrapper">
		<div class="login-image">
		<img src="//<?php echo $_SERVER['SERVER_NAME']; ?>/images/<? echo $sitedetails[0]['site_logo'] ?>" />
	</div>
	<div class="login-fields">
		<?php 
          $attributes = array("id" => "loginform", "name" => "loginform");
          echo form_open("login/login", $attributes);?>
		  <div class="form-group">
		    <input type="text" class="form-control email-address" id="exampleInputEmail1" name="txt_username" placeholder="Username" value=''>
		    <input type="password" class="form-control" id="exampleInputPassword1" name="txt_password" placeholder="Password" value=''>
		  </div>
		  <input id="btn_login" name="btn_login" type="submit" class="btn btn-default btn-login" value="Sign In" />
          <?php echo form_close(); ?>
          <?php echo $this->session->flashdata('msg'); ?>
      <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/index.php/login/forgotpassword/">Forgot your password?</a>
	</div>
	</div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/bootstrap.min.js"></script>
    <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
            jQuery(".date-picker").datepicker({
                  format: "dd-mm-yyyy",
                  todayBtn: "linked",
                  autoclose: true,
                  todayHighlight: true
              });

           $("#capital-text").keyup(function(evt){
              // to capitalize all words  
              var cp_value= ucwords($(this).val(),true) ;
              $(this).val(cp_value );
            });

			
     </script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/pagenation-script.js"></script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/capital.js"></script>
      
</body>
</html>

