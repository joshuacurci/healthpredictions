<h1 class="page-header col-xs-11">Search Advisor</h1><i class="fa fa-life-ring col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
  <? 
  if (isset($searchresults)) { 
    echo '<h3>Search Results</h3>'; 
  } else {
    ?>
    <div class="search-div-searches">
      <div class="search-box">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/advisor/search_result/">
          <div class="col-sm-12 ">
            <center><h3>Advisor Advanced Search</h3>
             <p>Fill in one or more criteria to search for a Client.</p></center>
           </div>
           <br/><br/>
           <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Name:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Name" name="advisor_name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Company:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Organisation" name="advisor_company" />
            </div>
            <div style="clear:both"></div>
          </div>  

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Insurance Company:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg2" name="insuranceID">
                <option value="">Please an Insurance Company</option>
                <? foreach ($insuranceCompany as $inCom) { ?>
                  <option value="<? echo $inCom['insID'] ?>"><? echo $inCom['ins_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
            </div>   

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Groups:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Groups" name="advisor_groups" />
            </div>
            <div style="clear:both"></div>
          </div>     

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 1:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Address Line 1" name="advisor_address" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 2:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum2" placeholder="Address Line 2" name="advisor_address2" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">City/Suburb:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="City/Suburb" name="advisor_city" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg1" name="advisor_state">
                <option selected value="">Please select your state/territory</option>
                <? foreach ($advisorState as $advisorState) { ?>
                 <option value="<? echo $advisorState['id'] ?>"><? echo $advisorState['state_code'] ?></option>
                 <? } ?>
               </select>
             </div>
             <div style="clear:both"></div>
           </div>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Postcode:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="advisor_postcode" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Country:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg2" name="advisor_country">
                <option value="">Please select your country</option>
                <? foreach ($country as $countrydata) { ?>
                  <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Phone Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="advisor_phone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Work Phone:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Phone" name="advisor_workphone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Mobile Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="advisor_mobile" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Fax Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="advisor_fax" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="advisor_email" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">2nd Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="advisor_email2" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">3rd Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Third Email Address" name="advisor_email3" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">4th Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Fourth Email Address" name="advisor_email4" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">5th Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Fifth Email Address" name="advisor_email5" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">6th Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Sixth Email Address" name="advisor_email6" />
              </div>
              <div style="clear:both"></div>
            </div>

            <!-- Commented for now, needs to be a date range -->

          <!-- <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Date of Birth:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Date of Birth" name="advisor_DOB" />
            </div>
            <div style="clear:both"></div>
          </div> -->

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Points enabled:</label>
            <div class="col-sm-7">
             <select class="form-control" id="inputorg1" name="advisor_enable_points">
               <option value="">Points enabled?</option>
               <option value="Y">Yes</option>
               <option value="N">No</option>

             </select>
           </div>
           <div style="clear:both"></div>
         </div>

         <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Total Points:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Total Points" name="advisor_points" />
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">First time use?:</label>
          <div class="col-sm-7">
           <select class="form-control" id="inputorg1" name="advisor_first_time">
             <option value="">First time use?</option>
             <option value="Y">Yes</option>
             <option value="N">No</option>
           </select>
         </div>
         <div style="clear:both"></div>
       </div>

        <div class="col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Included in mailing list?:</label>
          <div class="col-sm-7">
           <select class="form-control" id="inputorg1" name="advisor_mailing_list">
             <option value="">Included in mailing list?</option>
             <option value="Y">Yes</option>
             <option value="N">No</option>
           </select>
         </div>
         <div style="clear:both"></div>
       </div>

        <div class="col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Active/Inactive:</label>
          <div class="col-sm-7">
           <select class="form-control" id="inputorg1" name="advisor_status">
             <option value="">Please select Active or Inactive</option>
             <option value="A">Active</option>
             <option value="I">Inactive</option>
           </select>
         </div>
         <div style="clear:both"></div>
       </div>

       <div style="clear:both"></div>

       <div class="col-sm-12">
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>
    </form>
  </div>
</div>
<?php } ?>

<? if(isset($searchresults)) { echo '<div style="display: block;">';
} else { echo '<div style="display: none;">'; }
?>
<a href="<? echo base_url(); ?>index.php/advisor/search_all/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
<div class="col-sm-12 ">
  <div class="col-sm-12 top-buttons">
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Company</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($advisorData as $advisorData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $advisorData['advisorID']; ?>/" /><? echo $advisorData['advisor_name']; ?></a></td>
            <td style="width: 150px"><? echo $advisorData['advisor_company']; ?></td>
            <td><? echo $advisorData['advisor_address']; ?></td>
            <td><? echo $advisorData['advisor_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData['advisor_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $advisorData['advisor_phone']; ?></td>
                <td><? echo $advisorData['advisor_fax']; ?></td>
                <td><? echo $advisorData['advisor_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $advisorData['advisorID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/edit/<? echo $advisorData['advisorID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/delete/<? echo $advisorData['advisorID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
              </tr>
              <? } ?>
            </tbody>
          </table>
              </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>
      </div>

<!-- Last </div> will be in the footer -->