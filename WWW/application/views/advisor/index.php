<h1 class="page-header col-xs-11">Advisors</h1> <i class="fa fa-life-ring col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
$siteID = $_SESSION['siteID'];

?>

<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/advisor/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Advisor</a>
</div>

<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/advisor/search/">
      <div class="col-sm-4 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Advisor Name" name="GP_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['GP_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By City" name="GP_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['GP_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-3 search-box-item">
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="GP_state">
          <option value="">Please Select State</option>
          <option value="">All</option>
          <? foreach ($advisorState as $advisorState) { ?>
           <option value="<? echo $advisorState['id'] ?>"><? echo $advisorState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

      <div class="col-sm-2">
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>

      <div class="advanced-search-link"><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisor/search_all">Advanced search</a></div>
  </form>
</div>
<div style="clear:both"></div>

<div class="col-sm-12">
<div style="overflow-x:auto;">
<table class="table table-striped table-responsive" id="advisor-data">
  <thead>
    <tr>
      <th>Advisor Name</th>
      <th>Advisor Buisness Name</th>
      <th>Address</th>
      <th>City</th>
      <th>Point</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody id="myTable">
  <? foreach ($advisorData as $IndAdvisor) { ?>
    <tr>
      <td><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $IndAdvisor['advisorID']; ?>/" ><? echo $IndAdvisor['advisor_name']; ?></a></td>
      <td><? echo $IndAdvisor['advisor_company']; ?></td>
      <td><? echo $IndAdvisor['advisor_address']; ?></td>
      <td><? echo $IndAdvisor['advisor_city']; ?></td>
      <td><? echo $IndAdvisor['advisor_points'];?></td>
      
      <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $IndAdvisor['advisorID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
      <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/edit/<? echo $IndAdvisor['advisorID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
      <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/advisor/delete/<? echo $IndAdvisor['advisorID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
    </tr>
  <? } ?>
  </tbody>
</table>
  </div>
</div>

<div class="col-md-12 text-center">
  <ul class="pagination pagination-lg" id="myPager"></ul>
</div>
       

<!-- Last </div> will be in the footer -->