<h1 class="page-header col-xs-11">Advisor Information</h1> <i class="fa fa-life-ring col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/advisor/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Advisor</a>
  <a href="<? echo base_url(); ?>index.php/advisor/edit/<? echo $advisorData[0]['advisorID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Advisor</a>
  <a href="<? echo base_url(); ?>index.php/advisor/point_management/<? echo $advisorData[0]['advisorID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Manage Points</a>
</div> 

<div class="col-xs-6 top-buttons">
  <div class="name-header"><i class="fa fa-life-ring" aria-hidden="true"></i> <?php echo $advisorData[0]['advisor_name']; ?></div> 
  <div class="sub-header">
    <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$advisorData[0]['siteID']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['site_name']; ?>
                <? }
              } else { } ?>
  </div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $advisorData[0]['dateadded']);?></div>
</div> 

<div class="col-xs-12 main-data-content">

  <div class="col-xs-4 advisor-block"> <!-- Address -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $advisorData[0]['advisor_name']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Buisness Name:</strong></td>
        <td><?php echo $advisorData[0]['advisor_company']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Group:</strong></td>
        <td><?php echo $advisorData[0]['advisor_groups']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td>
          <?php echo $advisorData[0]['advisor_address']; ?><br/>
          <?php echo $advisorData[0]['advisor_address2']; ?><br/>
          <?php echo $advisorData[0]['advisor_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData[0]['advisor_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $advisorData[0]['advisor_postcode']; ?><br/>
            <!-- <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_country WHERE country_code = '".$advisorData[0]['advisor_country']."'";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['country_name']; ?>
                    <? }
                  } else { } ?> -->
          </td>
      </tr>

      <?php if($advisorData[0]['advisor_postaladdress'] != ''){ ?>
      <tr>
        <td class="view-title"><strong>Postal Address:</strong></td>
        <td>
          <?php echo $advisorData[0]['advisor_postaladdress']; ?><br/>
          <?php echo $advisorData[0]['advisor_postaladdress2']; ?><br/>
          <?php echo $advisorData[0]['advisor_postalcity']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData[0]['advisor_postalstate']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $advisorData[0]['advisor_postalpostcode']; ?><br/>
          </td>
          <? } ?>
      </tr>
    </table>
  </div>

  <div class="col-xs-4 advisor-block"> <!-- Emails -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Email 1:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Email 2:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email2']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Email 3:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email3']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Email 4:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email4']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Email 5:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email5']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Email 6:</strong></td>
        <td><?php echo $advisorData[0]['advisor_email6']; ?></td>
      </tr>
      </table>
  </div>

  <div class="col-xs-4 advisor-block"> <!-- Emails -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Phone:</strong></td>
        <td><?php echo $advisorData[0]['advisor_phone']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Work Phone:</strong></td>
        <td><?php echo $advisorData[0]['advisor_workphone']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Mobile:</strong></td>
        <td><?php echo $advisorData[0]['advisor_mobile']; ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Fax:</strong></td>
        <td><?php echo $advisorData[0]['advisor_fax']; ?></td>
      </tr>
    </table>
  </div>

  <div style="clear:both"></div>
  <hr/>

  <div class="col-xs-4 advisor-block"> <!-- Questions -->
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Points Enabled:</strong></td>
        <td><?php if($advisorData[0]['advisor_enable_points'] == 'Y') {echo "Yes";} else {echo "No";} ?></td>
      </tr>
      <?php if($advisorData[0]['advisor_enable_points'] == 'Y') { ?>
      <tr>
        <td class="view-title"><strong>Point Total:</strong></td>
        <td><?php echo $advisorData[0]['advisor_points']; ?></td>
      </tr>
      <? } else {} ?>
      <tr>
        <td class="view-title"><strong>Is this first time use?:</strong></td>
        <td><?php if($advisorData[0]['advisor_first_time'] == 'Y') {echo "Yes";} else {echo "No";} ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Include this person in the mailing list?:</strong></td>
        <td><?php if($advisorData[0]['advisor_mailing_list'] == 'Y') {echo "Yes";} else {echo "No";} ?></td>
      </tr>
      <tr>
        <td class="view-title"><strong>Copy results to advisor?:</strong></td>
        <td><?php if($advisorData[0]['advisor_copy_results'] == 'Y') {echo "Yes";} else {echo "No";} ?></td>
      </tr>
      </table> 
  </div>

  <div class="col-xs-4 advisor-block"> <!-- Questions -->
    <table class="view-table">
 <!-- User Login -->
      <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
          $conn = new mysqli($servername, $username, $password, $dbname);
          if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
              $sql = "SELECT * FROM tbl_users WHERE userID = ".$advisorData[0]['userID']."";
          $result = $conn->query($sql);
          $num_rec = $result->num_rows;
          if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>

                <tr>
                  <td class="view-title"><strong>Username:</strong></td>
                  <td><?php echo $row['username']; ?></td>
                </tr>
                <tr>
                  <td class="view-title"><strong>User Status:</strong></td>
                  <td><?php if($row['usr_status'] == 'A') { 
                    echo 'Active'; 
                  } else if ($row['usr_status'] == 'I'){
                    echo 'Inactive';
                  } else {
                    echo 'Status Not Selected';
                  }
                  ?></td>
                </tr>
                <tr>
                  <td class="view-title"><strong>Last Login:</strong></td>
                  <td><?php if($row['lastlogin'] == ""){ echo "Not yet logged in";}else{ echo $row['lastlogin'];} ?></td>
                </tr>

              <? } } else { ?>

                <tr>
                  <td class="view-title"><strong>Username:</strong></td>
                  <td>N/A</td>
                </tr>
                <tr>
                  <td class="view-title"><strong>User Status:</strong></td>
                  <td>N/A</td>
                </tr>
                <tr>
                  <td class="view-title"><strong>Last Login:</strong></td>
                  <td>N/A</td>
                </tr>

        <? } ?>
    </table> 
  </div>

  <div class="col-xs-4 advisor-block">
  <? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$advisorData[0]['siteID']."";
                $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { 
                      echo '<img class="logoPrimary" src="//'. $_SERVER['SERVER_NAME'] .'/uploads/logos/'. $row['site_logo'] .'" style="width:100%; max-width:280px; text-align:right;">'; 
                  } } else { } ?>
  </div>

  <div style="clear:both"></div>
  <hr/>

  <div class="col-xs-12">
    <strong>Advisor Comments: </strong><br/>
    <?php echo $advisorData[0]['advisor_comments']; ?>
  </div>

</div>      

<!-- Last </div> will be in the footer -->