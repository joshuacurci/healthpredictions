<h1 class="page-header col-xs-11"><?php echo $info[0]['advisor_name'];?></h1> <h2 class="pb-2 mt-4 mb-2" style="color: #8BBEF4">Current Points: <?php echo $info[0]['advisor_points']?></h2>

<div class="col-xs-12 top-buttons">
<form action="<?echo base_url();?>advisor/addPoints" method="POST">
Reason: <input type="text" name="Reason">
Points: <input type="text" name="Points">
<input style="display:none;" type="text" name="advisorID" value="<?php echo $this->uri->segment('3')?>">
<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Points</button>
</form>
<form action="<?echo base_url();?>advisor/redeemPoints" method="POST">
Reason: <input type="text" name="Reason">
Points: <input type="text" name="Points" >
<input style="display:none;" type="text" name="advisorID" value="<?php echo $this->uri->segment('3')?>">
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Redeem Points</button>
 </form>

<div style="padding-top: 30px" >
   
   
 

    <table class="table table-striped table-responsive" id="advisor-data">
    <thead>
    <tr>
        <th>Point Name</th>
        <th>Type</th>
        <th>Date</th>
        <th>Point Change</th>
        <th></th>
        <th></th>
    </tr>    
    </thead>
     <?php
     
     foreach($points as $row){
         echo "<tr>";
         echo "<td>".$row['pointName']."</td>";
         if($row['app_type'] == 'R')
            $type="Redeem";
            elseif($row['app_type'] == 'A')
            $type="Add";
            elseif($row['app_type'] == 'D')
            $type="Doctor";
            elseif($row['app_type'] == 'P')
            $type="Pathologist";
            elseif($row['app_type'] == 'S')
            $type="Specialist";
            elseif($row['app_type'] == 'N')
            $type="Nurse"; 
            elseif($row['app_type'] == 'GP')
            $type="GP"; 
            elseif($row['app_type'] == '')
            $type="Empty"; 
            else{
                $type="Pls Update Type";
            }
         
         echo "<td>".$type."</td>";
         echo "<td>".$row['point_date']."</td>";
         echo "<td>".$row['pointChange']."</td>";
         echo "</tr>";
     }
     ?>
     </table>
     </div>
 