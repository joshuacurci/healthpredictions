<h1 class="page-header col-xs-11">Edit - <? echo $advisorData[0]['advisor_name'] ?></h1> <i class="fa fa-life-ring col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/advisor/save/">
    <input type="hidden" name="advisorID" value="<? echo $advisorData[0]['advisorID'] ?>"/>
    <input type="hidden" name="siteIDorig" value="<? echo $advisorData[0]['siteID'] ?>"/>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Name" name="advisor_name" value="<? echo $advisorData[0]['advisor_name'] ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Advisor Buisness Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Advisor Buisness Name" name="advisor_company" value="<? echo $advisorData[0]['advisor_company'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>
<!-- 
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Insurance Company:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="insuranceID">
                <option selected value="0">Please select an Insurance Company</option>
              <? foreach ($insuranceCompany as $inCom) { ?>
                <option <? if($inCom['insID'] == $advisorData[0]['insuranceID']) {echo 'selected';} ?> value="<? echo $inCom['insID']; ?>"><? echo $inCom['ins_name']; ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>
 -->
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Health Predictions/JV Company:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="siteID">
                <option selected value="0">Please select a Health Predictions/JV Company</option>
              <? foreach ($siteDetails as $siteData) { ?>
                <option <? if($siteData['siteID'] == $advisorData[0]['siteID']) {echo 'selected';} ?> value="<? echo $siteData['siteID'] ?>"><? echo $siteData['site_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Advisor Group:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Advisor Group" name="advisor_groups" value="<? echo $advisorData[0]['advisor_groups'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 1" name="advisor_address" value="<? echo $advisorData[0]['advisor_address'] ?>">
            <div class="clearboth"></div>
            <input type="text" class="form-control address-feilds" id="inputrecNum3" placeholder="Address Line 2" name="advisor_address2" value="<? echo $advisorData[0]['advisor_address2'] ?>">
            <div class="clearboth"></div>
            <input type="text" class="form-control address-feilds address-largehalf" id="inputrecNum4" placeholder="City/Suburb" name="advisor_city" value="<? echo $advisorData[0]['advisor_city'] ?>">
            <select class="form-control address-half address-feilds" id="inputorg1" name="advisor_state">
          <? foreach ($advisorState as $advisorStateChanged) { ?>
           <option <? if($advisorStateChanged['id'] == $advisorData[0]['advisor_state']) {echo 'selected';} ?> value="<? echo $advisorStateChanged['id'] ?>"><? echo $advisorStateChanged['state_code'] ?></option>
           <? } ?>
            </select>
            <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="advisor_postcode" value="<? echo $advisorData[0]['advisor_postcode'] ?>">
            <div class="clearboth"></div>
          </div>
                <div style="clear:both"></div>
        </div>

         <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Postal Address:</label>
          <div class="col-sm-9">
            <label class="">
              <input type="checkbox" name="different_postal_address" id="same_as_above" value="Y" <? if ($advisorData[0]['advisor_postaladdress'] == '') {echo 'checked';} ?> > Same as above
            </label>

            <div id="postal_address_box" <? if ($advisorData[0]['advisor_postaladdress'] == '') {echo 'style="display:none;"';} ?>>
            <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 1" name="advisor_postaladdress" value="<? echo $advisorData[0]['advisor_postaladdress'] ?>">
            <div class="clearboth"></div>
            <input type="text" class="form-control address-feilds" id="inputrecNum3" placeholder="Address Line 2" name="advisor_postaladdress2" value="<? echo $advisorData[0]['advisor_postaladdress2'] ?>">
            <div class="clearboth"></div>
            <input type="text" class="form-control address-feilds address-largehalf" id="inputrecNum4" placeholder="City/Suburb" name="advisor_postalcity" value="<? echo $advisorData[0]['advisor_postalcity'] ?>">
            <select class="form-control address-half address-feilds" id="inputorg1" name="advisor_postalstate">
                <option selected value="0">Please select your state/territory</option>
              <? foreach ($advisorState as $advisorStateNew) { ?>
                <option <? if($advisorStateNew['id'] == $advisorData[0]['advisor_postalstate']) {echo 'selected';} ?> value="<? echo $advisorStateNew['id'] ?>"><? echo $advisorStateNew['state_code'] ?></option>
              <? } ?>
            </select>
            <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="advisor_postalpostcode" value="<? echo $advisorData[0]['advisor_postalpostcode'] ?>">
            <div class="clearboth"></div>
            </div>

          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="advisor_phone" value="<? echo $advisorData[0]['advisor_phone'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="advisor_mobile" value="<? echo $advisorData[0]['advisor_mobile'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Work Number:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Number" name="advisor_workphone" value="<? echo $advisorData[0]['advisor_workphone'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="advisor_fax" value="<? echo $advisorData[0]['advisor_fax'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 1" name="advisor_email" value="<? echo $advisorData[0]['advisor_email'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 2" name="advisor_email2" value="<? echo $advisorData[0]['advisor_email2'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 3:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 3" name="advisor_email3" value="<? echo $advisorData[0]['advisor_email3'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 4:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 4" name="advisor_email4" value="<? echo $advisorData[0]['advisor_email4'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 5:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 5" name="advisor_email5" value="<? echo $advisorData[0]['advisor_email5'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 6:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 6" name="advisor_email6" value="<? echo $advisorData[0]['advisor_email6'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Points Enabled:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="advisor_enable_points" id="inlineRadio1" value="Y" <? if ($advisorData[0]['advisor_enable_points'] == 'Y') { echo "checked"; } ?>> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="advisor_enable_points" id="inlineRadio2" value="N" <? if ($advisorData[0]['advisor_enable_points'] == 'N') { echo "checked"; } ?>> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Point Total:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Point Total" name="advisor_points" value="<? echo $advisorData[0]['advisor_points'] ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this first time use?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="advisor_first_time" id="inlineRadio1" <? if ($advisorData[0]['advisor_first_time'] == 'Y') { echo "checked"; } ?> value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="advisor_first_time" id="inlineRadio2" <? if ($advisorData[0]['advisor_first_time'] == 'N') { echo "checked"; } ?> value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Include this person in the mailing list?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="advisor_mailing_list" id="inlineRadio1" <? if ($advisorData[0]['advisor_mailing_list'] == 'Y') { echo "checked"; } ?> value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="advisor_mailing_list" id="inlineRadio2" <? if ($advisorData[0]['advisor_mailing_list'] == 'N') { echo "checked"; } ?> value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Copy results to advisor?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="advisor_copy_results" id="inlineRadio1" <? if ($advisorData[0]['advisor_copy_results'] == 'Y') { echo "checked"; } ?> value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="advisor_copy_results" id="inlineRadio2" <? if ($advisorData[0]['advisor_copy_results'] == 'N') { echo "checked"; } ?> value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this user Active or Inactive?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="advisor_status" id="inlineRadio1" <? if ($advisorData[0]['advisor_status'] == 'A') { echo "checked"; } ?> value="A"> Active
            </label>
            <label class="radio-inline">
              <input type="radio" name="advisor_status" id="inlineRadio2" <? if ($advisorData[0]['advisor_status'] == 'I') { echo "checked"; } ?> value="I"> Inactive
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Notes:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert note here..." name="advisor_comments">
              <? echo $advisorData[0]['advisor_comments'] ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>        

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
          </div>
        </div>

  </form>

</div>  

<script>
  $('#same_as_above').change(function(){
    $( "#postal_address_box" ).toggle();
});
</script>      

<!-- Last </div> will be in the footer -->