<h1 class="page-header">Dashboard</h1>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Quick search</h3>
          </div>
          <div class="panel-body">
           <form class="form-inline home-search" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/home/dashSearch/">
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail3">Search type</label>
                  <select class="form-control" name="searchType">
                      <option>Search type</option>
                      <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                      <option value="GP">GP</option>
                      <option value="Pathology">Pathology</option>
                      <option value="Nurse">Nurse</option>
                      <option value="Specialist">Specialist</option>
                      <option value="Insurance">Insurance</option>
                      <option value="Advisor">Advisor</option>
                      <option value="Client">Client</option>
                      <option value="Staff">Staff</option>
                      <?php } ?>
                      <?php if ($_SESSION['usertype'] == 'F' || $_SESSION['usertype'] == 'G' || $_SESSION['usertype'] == 'H') { ?>
                      <option value="GP">GP</option>
                      <option value="Nurse">Nurse</option>
                      <option value="Insurance">Insurance</option>
                      <option value="Client">Client</option>
                      <?php } ?>
                  </select>
              </div>
              <div class="form-group">
                <label class="sr-only" for="exampleInputPassword3">Name</label>
                <input type="text" class="form-control" id="exampleInputPassword3" name="searchedName" placeholder="Name"/>
              </div>
              <button type="submit" class="btn btn-primary">Search</button>
          </form>
          </div>
        </div>         
        <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
        <div class="row">
          <div class="col-xs-6 col-md-3"><div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Referrals</h3>
              </div>
              <div class="panel-body dash-results">
                  <h1>
                    <?php if($unacnowkalgedReferrals < 10) { ?>
                      <span class="text-success"><? echo $unacnowkalgedReferrals; ?></span>
                    <? } else if($unacnowkalgedReferrals < 20) { ?>
                      <span class="text-warning"><? echo $unacnowkalgedReferrals; ?></span>
                    <? } else { ?>
                      <span class="text-danger"><? echo $unacnowkalgedReferrals; ?></span>
                    <? } ?>
                  </h1>
                  <h4>UNACKNOWLEDGED</h4>
              </div>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">No. Referrals - Staff</h3>
              </div>
              <div class="panel-body">
                  <table class='dash-table'>
                    <tr class="top-row">
                      <td class='startvalue'></td>
                      <td class='sectiondifferent'>Current<br/>Week</td>
                      <td>Last<br/>Week</td>
                    </tr>

                    <?php 
                    $i = 1;
                    $staffTotal = 0;
                    $staffLwTotal = 0;

                    foreach ($staffWeek as $staffData) {
                      if ($i < 11){?>
                        <tr>
                          <td class='startvalue'><? echo $staffData['name'] ?></td>
                          <td class='sectiondifferent'><? echo $staffData['number'] ?></td>
                          <td><? echo $staffData['lastweekNum'] ?></td>
                        </tr>
                      <?}
                      $staffTotal = $staffTotal + $staffData['number'];
                      $staffLwTotal = $staffLwTotal + $staffData['lastweekNum'];
                      $i++;
                    }
                    ?>
                    <tr class='bottom-row'>
                      <td class='startvalue'>TOTAL</td>
                      <td class='sectiondifferent'><? echo $staffTotal; ?></td>
                      <td><? echo $staffLwTotal; ?></td>
                    </tr>
                  </table>
              </div>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">No. Referrals - Week</h3>
              </div>
              <div class="panel-body">
                <table class='dash-table'>
                  <tr class="top-row">
                    <td class='startvalue'></td>
                    <td class='sectiondifferent'>Current<br/>Week</td>
                    <td>Last<br/>Week</td>
                  </tr>
                  <?php 
                  $i = 1;
                  $staffTotal = 0;
                  $staffLwTotal = 0;

                  foreach ($jvWeek as $staffData) {
                    if ($i < 11){?>
                      <tr>
                        <td class='startvalue'><? echo $staffData['name'] ?></td>
                        <td class='sectiondifferent'><? echo $staffData['number'] ?></td>
                        <td><? echo $staffData['lastweekNum'] ?></td>
                      </tr>
                    <?}
                    $staffTotal = $staffTotal + $staffData['number'];
                    $staffLwTotal = $staffLwTotal + $staffData['lastweekNum'];
                    $i++;
                  }
                  ?>
                  <tr class='bottom-row'>
                    <td class='startvalue'>TOTAL</td>
                    <td class='sectiondifferent'><? echo $staffTotal; ?></td>
                    <td><? echo $staffLwTotal; ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <div class="col-xs-6 col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">No. Referrals - Annual</h3>
              </div>
              <div class="panel-body">
                <table class='dash-table'>
                  <tr class="top-row">
                    <td class='startvalue'></td>
                    <td class='sectiondifferent'>This<br/>Year</td>
                    <td>Last<br/>Year</td>
                  </tr>
                  <?php 
                  $i = 1;
                  $staffTotal = 0;
                  $staffLwTotal = 0;

                  foreach ($jvYear as $staffData) {
                    if ($i < 11){?>
                      <tr>
                        <td class='startvalue'><? echo $staffData['name'] ?></td>
                        <td class='sectiondifferent'><? echo $staffData['number'] ?></td>
                        <td><? echo $staffData['lastweekNum'] ?></td>
                      </tr>
                    <?}
                    $staffTotal = $staffTotal + $staffData['number'];
                    $staffLwTotal = $staffLwTotal + $staffData['lastweekNum'];
                    $i++;
                  }
                  ?>
                  <tr class='bottom-row'>
                    <td class='startvalue'>TOTAL</td>
                    <td class='sectiondifferent'><? echo $staffTotal; ?></td>
                    <td><? echo $staffLwTotal; ?></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
      </div>
                <?php } ?>   
      <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>        
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Quick Links</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
              <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/clientdiary/index"><i class="fa fa-book" aria-hidden="true"></i> Client Diary</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/referralsadvisor/index"><i class="fa fa-file-text-o" aria-hidden="true"></i> Referrals by Advisor</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/referralbycreation/index"><i class="fa fa-calendar" aria-hidden="true"></i> Referrals by Creation</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/groupbyinsurance/index"><i class="fa fa-building-o" aria-hidden="true"></i> Referrals group by insurance</a>

              </div>
              <div class="col-md-6">

                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/workloadreport/index"><i class="fa fa-briefcase" aria-hidden="true"></i> Workload Reports</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/clientsbytest/index"><i class="fa fa-object-group" aria-hidden="true"></i> Clients by tests</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisorlastaccess/index"><i class="fa fa-low-vision" aria-hidden="true"></i> Advisor last access</a>   
               </div>
            </div>
          </div>
          
      </div>
      <?php } ?>
      <?php if ($_SESSION['usertype'] == 'F' || $_SESSION['usertype'] == 'G' || $_SESSION['usertype'] == 'H') { ?>
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Quick Links</h3>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-6">
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/index"><i class="fa fa-users" aria-hidden="true"></i> Clients</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/index"><i class="fa fa-medkit" aria-hidden="true"></i> GPs</a>
              </div>
              <div class="col-md-6">
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/index"><i class="fa fa-plus-square" aria-hidden="true"></i> Nurses</a><br/>
                <a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/insurance/index"><i class="fa fa-building" aria-hidden="true"></i> Insurance</a><br/>
               </div>
            </div>
          </div>
          
      </div>
      <?php } ?>

<!-- Last </div> will be in the footer -->