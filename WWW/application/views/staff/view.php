<h1 class="page-header col-xs-11">Staff Information</h1> <i class="fa fa-h-square col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/staff/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Staff Member</a>
  <a href="<? echo base_url(); ?>index.php/staff/edit/<? echo $staffData[0]['staffID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Staff Member</a>
</div> 

<div class="col-xs-6 top-buttons">
  <div class="name-header"><i class="fa fa-heartbeat" aria-hidden="true"></i> <?php echo $staffData[0]['staff_name']; ?></div>  
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $staffData[0]['dateadded']);?></div>
</div> 



<div class="col-xs-12 main-data-content">
  <div class="col-md-6">
    <dl class="dl-horizontal">
      <dt>
        Name:<br/>
        Email:<br/>
        Staff Service:<br/>
        Staff Location:<br/>
        Responsible Person:<br/>
      </dt>
      <dd>
        <?php echo $staffData[0]['staff_name']; ?><br/>
        <?php echo $staffData[0]['staff_email']; ?><br/>
        <?php 
        if($staffData[0]['staff_service']=="D")
        {
          echo "Supervisor";
        }
        if($staffData[0]['staff_service']=="E")
        {
          echo "Case File Manager";
        }
        if($staffData[0]['staff_service']=="F")
        {
          echo "Nurses/GP (Staff)";
        }
        if($staffData[0]['staff_service']=="G")
        {
          echo "Nurses/GP (Contractors)";
        }
        if($staffData[0]['staff_service']=="H")
        {
          echo "Nurses/GP (Contractors - Own Apointments)";
        }
        if($staffData[0]['staff_service']=="I")
        {
          echo "Advisors";
        }
         ?><br/>
        <?php echo $staffData[0]['staff_location']; ?><br/>
        <?php echo $staffData[0]['staff_responsibleperson']; ?><br/>
      </dd>
    </dl>
  </div>

  <div class="col-md-6">
    <dl class="dl-horizontal">
      <dt>
        Address:
      </dt>
      <dd>
        <?php echo $staffData[0]['staff_address']; ?><br/>
        <?php echo $staffData[0]['staff_address2']; ?><br/>
        <?php echo $staffData[0]['staff_city']; ?> ,<?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$staffData[0]['staff_state']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <? echo $row['state_name']; ?>
            <? }
          } else { ?>
            
          <? } ?>,
          <?php echo $staffData[0]['staff_postcode']; ?><br/>
          <?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_country WHERE id = ".$staffData[0]['staff_country']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <? echo $row['country_name']; ?>
            <? }
          } else { ?>
            <? } ?>
      </dd>
    </dl>
   </div>

   <div style="clear:both"></div>

   <hr/>

   <div class="col-xs-12">
    <strong>Notes: </strong><br/>
    <?php echo $staffData[0]['staff_notes']; ?>
  </div>
          
</div>   

<!-- Last </div> will be in the footer -->