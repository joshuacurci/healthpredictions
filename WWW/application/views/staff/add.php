<h1 class="page-header col-xs-11">Add Staff</h1> <i class="fa fa-h-square col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
<?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/staff/newstaff/">
   <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="capital-text" placeholder="Name" name="staff_name"/>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Service:</label>
      <div class="col-sm-9">
        <select class="form-control" name="staff_service">
         <option value="">Please Select</option>
         <? foreach ($typeData as $usertypes) { 
           if ($usertypes['typeID']>=4) {?>
          <option value="<? echo $usertypes['type_letter'] ?>"><? echo $usertypes['type_name'] ?></option>
          <? } } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Service Location:</label>
    <div class="col-sm-9">
    <input type="text" class="form-control" id="inputrecNum1" placeholder="Staff Service Location" name="staff_location" />
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="staff_address" />
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 2" name="staff_address2" />
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="staff_city" />
      <select class="form-control address-half address-feilds" id="inputorg1" name="staff_state">
      <option value="0">Please select your state</option>
       <? foreach ($staffState as $staffState) { ?>
         <option value="<? echo $staffState['id'] ?>"><? echo $staffState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="staff_postcode" />
       <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="staff_country">
          <option value="0">Please select your country</option>
          <option selected value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $countrydata) { ?>
            <option  value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
           <? } ?>
         </select> 
    </div>
    <div style="clear:both"></div>
  </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone" name="staff_phone" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile" name="staff_mobile" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax" name="staff_fax" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email" name="staff_email" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Person Responsible:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Person Responsible" name="staff_responsibleperson" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Add Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="staff_notes" /></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->