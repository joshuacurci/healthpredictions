<?php
  if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
    echo '<span style="z-index: 5000; position: fixed; top: 0; right: 1.5em; padding: 0.3rem; font-size: 16px; background: green; color: #fff; border-radius: 0 0 5px 5px;">Local</span>';
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><? echo $title; ?></title>

  <style>
    :root {
      --main-color: #<? echo $sitedetails[0]['site_maincolour'] ?>;
      --main-color-text: #<? echo $sitedetails[0]['site_maincolour_text'] ?>;
      --second-color: #<? echo $sitedetails[0]['site_seccolour'] ?>;
      --second-color-text: #<? echo $sitedetails[0]['site_seccolour_text'] ?>;
      --thierd-color: #<? echo $sitedetails[0]['site_thrcolour'] ?>;
      --thierd-color-text: #<? echo $sitedetails[0]['site_thrcolour_text'] ?>;
    }
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-datepicker.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/main.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/45e81b2b58.css" media="all">
  <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
  <script>tinymce.init({selector:'textarea'});</script>

  <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

  <!--<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="all">
  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>

</head>

<body>
  <div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">
        <!-- Main Sidebar -->
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 sidebar sidebar-offcanvas" id="sidebar">
          <img class="logoPrimary" src="//<?php echo $_SERVER['SERVER_NAME']; ?>/uploads/logos/<? echo $sitedetails[0]['site_logo'] ?>" style="width:100%; max-width:280px;">
          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '1') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisorhome/index"><i class="fa fa-dashboard" aria-hidden="true"></i>  Dashboard</a></li>
            <li <? if ($menuitem == '8') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/index"><i class="fa fa-users" aria-hidden="true"></i>  Clients</a></li>
          </ul>
          <hr>

          <div class="side-contact">
            <b>How to contact us:</b><br/>
        <b>Address:</b><br/>
        <?php echo $sitedetails[0]['site_address']; ?><br/>
          <?php echo $sitedetails[0]['site_address2']; ?><br/>
          <?php echo $sitedetails[0]['site_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$sitedetails[0]['site_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $sitedetails[0]['site_postcode']; ?><br/>
            <!-- <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_country WHERE country_code = '".$sitedetails[0]['site_country']."'";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['country_name']; ?>
                    <? }
                  } else { } ?> -->

      <strong>Phone Number:</strong> <?php echo $sitedetails[0]['site_phone']; ?><br/>

      <strong>Fax Number:</strong> <?php echo $sitedetails[0]['site_fax']; ?><br/>

      <strong>Email:</strong> <a href="mailto:<?php echo $sitedetails[0]['site_email']; ?>"><?php echo $sitedetails[0]['site_email']; ?></a><br/>

      <strong>Website:</strong> <a href="<?php echo $sitedetails[0]['site_website']; ?>" target="_blank"><?php echo $sitedetails[0]['site_website']; ?></a><br/>
          </div>

          <!-- 
          Commented out for now.
          <hr>
          <ul class="nav nav-sidebar">
            <li><a href=""><i class="fa fa-chain" aria-hidden="true"></i>  Access Link</a></li>
            <li><a href=""><i class="fa fa-list" aria-hidden="true"></i>  Advisor</a></li>
          </ul> -->

        </div><!--/.sidebar-offcanvas-->

        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-lg-10 col-md-offset-3 col-lg-offset-2 main">