<h1 class="page-header col-xs-11">Specialist Clinic Information</h1><i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/specialists/clinic_add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Specialist Clinic</a>
  <a href="<? echo base_url(); ?>index.php/specialists/clinic_edit/<? echo $clinicData[0]['clinicID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Specialist Clinic</a>

</div> 

<div class="name-header"><i class="fa fa-medkit" aria-hidden="true"></i> <?php echo $clinicData[0]['clinic_name']; ?></div> 

<div class="col-xs-12 main-data-content">
  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $clinicData[0]['clinic_name']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Speciality:</strong></td>
        <td><?php echo $clinicData[0]['clinic_profession']; ?></td>
      </tr>

<!--       <tr>
        <td class="view-title"><strong>Personal Insurance:</strong></td>
        <td><?php echo $clinicData[0]['clinic_pi_insurance']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Personal Insurance Number:</strong></td>
        <td><?php echo $clinicData[0]['clinic_pi_insurance_num']; ?></td>
      </tr> -->

      <tr>
        <td class="view-title"><strong>Practice Manager:</strong></td>
        <td><?php echo $clinicData[0]['clinic_responsibleperson']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Active/Inactive:</strong></td>
        <td><? if($clinicData[0]['clinic_active'] == "1") {echo 'Active';} ?><? if($clinicData[0]['clinic_active'] == "0") {echo 'Inactive';} ?></td>
      </tr>
    </table>
  </div>

  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Phone:</strong></td>
        <td><?php echo $clinicData[0]['clinic_phone']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Mobile:</strong></td>
        <td><?php echo $clinicData[0]['clinic_mobile']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Fax:</strong></td>
        <td><?php echo $clinicData[0]['clinic_fax']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email 1:</strong></td>
        <td><?php echo $clinicData[0]['clinic_email']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email 2:</strong></td>
        <td><?php echo $clinicData[0]['clinic_email2']; ?></td>
      </tr>

    </table>
  </div>

  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td>
          <?php echo $clinicData[0]['clinic_address']; ?><br/>
          <?php echo $clinicData[0]['clinic_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clinicData[0]['clinic_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $clinicData[0]['clinic_postcode']; ?><br/>
            <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_country WHERE id = ".$clinicData[0]['clinic_country']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['country_name']; ?>
                    <? }
                  } else { } ?>
        </td>
      </tr>

    </table>
  </div>

  <div style="clear:both"></div>
  <hr/>
  <div class="col-xs-6">
    <strong>Instructions: </strong><br/>
    <?php echo $clinicData[0]['clinic_instructions']; ?>
  </div>
  <div class="col-xs-6">
    <strong>Notes: </strong><br/>
    <?php echo $clinicData[0]['clinic_notes']; ?>
  </div>

</div>      

<!-- Last </div> will be in the footer -->