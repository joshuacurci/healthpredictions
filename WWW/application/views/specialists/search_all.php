<h1 class="page-header col-xs-11">Search Specialist</h1><i class="fa fa-plus-square col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
  <? 
  if (isset($searchresults)) { 
    echo '<h3>Search Results</h3>'; 
  } else {
    ?>
    <div class="search-div-searches">
      <div class="search-box">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/specialists/search_result/">
          <div class="col-sm-12 ">
            <center><h3>Specialist Advanced Search</h3>
             <p>Fill in one or more criteria to search for a Specialist.</p></center>
           </div>
           <br/><br/>
           <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Name:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Name" name="spec_name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Profession:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Profession" name="spec_profession" />
            </div>
            <div style="clear:both"></div>
          </div>   

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Type:</label>
            <div class="col-sm-7">
             <select class="form-control" id="GP_type" name="spec_type">
               <option value="">Please select Specialist Type</option>
               <option value="0">Clinic</option>
               <option value="1">Individual</option>
             </select>
           </div>
           <div style="clear:both"></div>
         </div>     

         <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 1:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="capital-text" placeholder="Address Line 1" name="spec_address" />
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 2:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="inputrecNum2" placeholder="Address Line 2" name="spec_address2" />
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">City/Suburb:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="capital-text" placeholder="City/Suburb" name="spec_city" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
          <div class="col-sm-7">
            <select class="form-control" id="inputorg1" name="spec_state">
              <option selected value="">Please select your state/territory</option>
              <? foreach ($specialistState as $specialistState) { ?>
               <option value="<? echo $specialistState['id'] ?>"><? echo $specialistState['state_code'] ?></option>
               <? } ?>
             </select>
           </div>
           <div style="clear:both"></div>
         </div>

         <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Postcode:</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="spec_postcode" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="col-md-4 form-group">
          <label for="inputrecNum1" class="col-sm-5 control-label">Country:</label>
          <div class="col-sm-7">
            <select class="form-control" id="inputorg2" name="spec_country">
              <option value="">Please select your country</option>
              <? foreach ($country as $countrydata) { ?>
                <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
                <? } ?>
              </select>
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Phone Number:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="spec_phone" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Mobile Number:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="spec_mobile" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Fax Number:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="spec_fax" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Email Address:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="spec_email" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">2nd Email Address:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="spec_email2" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Person Responsible:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Person Responsible" name="spec_responsibleperson" />
            </div>
            <div style="clear:both"></div>
          </div> 

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Active/Inactive:</label>
            <div class="col-sm-7">
             <select class="form-control" id="inputorg1" name="spec_active">
               <option value="">Please select if Active or Inactive</option>
               <option value="1">Active</option>
               <option value="0">Inactive</option>

             </select>
           </div>
           <div style="clear:both"></div>
         </div>

         <div style="clear:both"></div>

         <div class="col-sm-12">
          <button type="submit" class="btn btn-primary">Search</button>
        </div>
        <div style="clear:both"></div>
      </form>
    </div>
  </div>
  <?php } ?>

  <? if(isset($searchresults)) { echo '<div style="display: block;">';
} else { echo '<div style="display: none;">'; }
?>
<a href="<? echo base_url(); ?>index.php/specialists/search_all/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
<div class="col-sm-12 ">
  <div class="col-sm-12 top-buttons">
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Profession</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($specialistData as $specialistData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" /><? echo $specialistData['spec_name']; ?></a></td>
            <td style="width: 150px"><? echo $specialistData['spec_profession']; ?></td>
            <td><? echo $specialistData['spec_address']; ?></td>
            <td><? echo $specialistData['spec_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$specialistData['spec_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $specialistData['spec_phone']; ?></td>
                <td><? echo $specialistData['spec_fax']; ?></td>
                <td><? echo $specialistData['spec_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/edit/<? echo $specialistData['specID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/delete/<? echo $specialistData['specID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
              </tr>
              <? } ?>
            </tbody>
          </table>
              </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>
      </div>

<!-- Last </div> will be in the footer -->