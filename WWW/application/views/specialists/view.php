<h1 class="page-header col-xs-11">Individual Specialist Information</h1><i class="fa fa-user-md col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/specialists/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Individual Specialist</a>
  <a href="<? echo base_url(); ?>index.php/specialists/edit/<? echo $specialistData[0]['specID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Individual Specialist</a>

</div>  
<div class="col-xs-6 top-buttons">
  <div class="name-header"><i class="fa fa-user-md" aria-hidden="true"></i> <?php echo $specialistData[0]['spec_name']; ?> <br /></div>
  <div class="sub-header"><?php echo $specialistData[0]['spec_profession']; ?></div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $specialistData[0]['dateadded']);?></div>
</div>

<div class="col-xs-12 main-data-content">
  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $specialistData[0]['spec_name']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Speciality:</strong></td>
        <td><?php echo $specialistData[0]['spec_profession']; ?></td>
      </tr>

      <tr>
              <td class="view-title"><strong>Practice Manager:</strong></td>
              <td><?php echo $specialistData[0]['spec_responsibleperson']; ?></td>
            </tr>
            <tr>
              <td class="view-title"><strong>Active/Inactive:</strong></td>
              <td>
                <?php if ($specialistData[0]['spec_active'] == 0) { 
                  echo "Inactive"; 
                } else if ($specialistData[0]['spec_active'] == 1) { 
                  echo "Active"; 
                } else {
                  echo "Not selected";
                } ?>
              </td>
            </tr>

            <!-- Start of Clinic -->

      <tr>
  <td class="view-title"><strong>Clinic:</strong></td>
  <!-- CLinic 1 -->
  <?
  $servername = $this->db->hostname;
  $username = $this->db->username;
  $password = $this->db->password;
  $dbname = $this->db->database;

        // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic']."";
  $result = $conn->query($sql);

  $num_rec = $result->num_rows;

  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) { ?>
      <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
      <? }
    } else { ?>      <td>No Assigned Clinic</td>
      <? } ?>
    </tr>
    <!-- Clinic 2 -->
    <?php if ($specialistData[0]['spec_clinic2'] != 0) { ?>
      <tr>
        <td class="view-title"><strong></strong></td>
        <? $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

    // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic2']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
            <? }
          } else { ?>  <td>No Assigned Clinic</td><? } ?>

        </tr>
        <? } ?>
        <!-- Clinic 3 -->
        <?php if ($specialistData[0]['spec_clinic3'] != 0) { ?>
          <tr>
            <td class="view-title"><strong></strong></td>
            <? $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

  // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic3']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                <? }
              } else { ?> <td>No Assigned Clinic</td> <? } ?>
            </tr>
            <? } ?>
            <!-- Clinic 4 -->
            <?php if ($specialistData[0]['spec_clinic4'] != 0) { ?>
              <tr>
                <td class="view-title"><strong></strong></td>
                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

  // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic4']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                    <? }
                  } else { ?> <td>No Assigned Clinic</td> <? } ?>


                </tr>
                <? } ?>

                <!-- Clinic 5 -->
                <?php if ($specialistData[0]['spec_clinic5'] != 0) { ?>
                  <tr>
                    <td class="view-title"><strong></strong></td>

                    <? $servername = $this->db->hostname;
                    $username = $this->db->username;
                    $password = $this->db->password;
                    $dbname = $this->db->database;

  // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                    if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic5']."";
                    $result = $conn->query($sql);

                    $num_rec = $result->num_rows;

                    if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) { ?>
                        <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                        <? }
                      } else { ?> <td>No Assigned Clinic</td> <? } ?>

                    </tr>
                    <? } ?>
                    <!-- Clinic 6 -->
                    <?php if ($specialistData[0]['spec_clinic6'] != 0) { ?>
                      <tr>
                        <td class="view-title"><strong></strong></td>

                        <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

  // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                        if ($conn->connect_error) {
                          die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic6']."";
                        $result = $conn->query($sql);

                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) { ?>
                            <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                            <? }
                          } else { ?> <td>No Assigned Clinic</td><? } ?>

                        </tr>
                        <? } ?>
                        <!-- Clinic 7 -->
                        <?php if ($specialistData[0]['spec_clinic7'] != 0) { ?>
                          <tr>
                            <td class="view-title"><strong></strong></td>
                            <?
                            $servername = $this->db->hostname;
                            $username = $this->db->username;
                            $password = $this->db->password;
                            $dbname = $this->db->database;

  // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                            if ($conn->connect_error) {
                              die("Connection failed: " . $conn->connect_error);
                            }

                            $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic7']."";
                            $result = $conn->query($sql);

                            $num_rec = $result->num_rows;

                            if ($result->num_rows > 0) {
                              while($row = $result->fetch_assoc()) { ?>
                                <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                                <? }
                              } else { ?>                                          <td>No Assigned Clinic</td>
                                <? } ?>
                              </tr>
                              <? } ?>
                              <!-- Clinic 8 -->
                              <?php if ($specialistData[0]['spec_clinic8'] != 0) { ?>
                                <tr>
                                  <td class="view-title"><strong></strong></td>


                                  <?
                                  $servername = $this->db->hostname;
                                  $username = $this->db->username;
                                  $password = $this->db->password;
                                  $dbname = $this->db->database;

  // Create connection
                                  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                                  if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                  }

                                  $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = ".$specialistData[0]['spec_clinic8']."";
                                  $result = $conn->query($sql);

                                  $num_rec = $result->num_rows;

                                  if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                      <td><? echo $row['clinic_name']; ?> (<? echo $row['clinic_city']; ?>)</td>
                                      <? }
                                    } else { ?>                                                <td>No Assigned Clinic</td>
                                      <? }
                                    } ?>
                                  </tr>

          </table>
        </div>
        <div class="col-md-4">
          <table class="view-table">
            
            <tr>
              <td class="view-title"><strong>Last Day Active:</strong></td>
              <td><?php echo $specialistData[0]['spec_date_of_active']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Last day Inactive:</strong></td>
              <td><?php echo $specialistData[0]['spec_date_of_inactive']; ?></td>
            </tr>
            <tr>
              <td class="view-title"><strong>Email 1:</strong></td>
              <td><?php echo $specialistData[0]['spec_email']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Email 2:</strong></td>
              <td><?php echo $specialistData[0]['spec_email2']; ?></td>
            </tr>
            <tr>
              <td class="view-title"><strong>Phone:</strong></td>
              <td><?php echo $specialistData[0]['spec_phone']; ?></td>
            </tr>
          </table>
        </div>

        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Mobile:</strong></td>
              <td><?php echo $specialistData[0]['spec_mobile']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Fax:</strong></td>
              <td><?php echo $specialistData[0]['spec_fax']; ?></td>
            </tr>
            <tr>
              <td class="view-title"><strong>Address:</strong></td>
              <td>
                <?php echo $specialistData[0]['spec_address']; ?><br/>
                <?php echo $specialistData[0]['spec_address2']; ?><br/>
                <?php echo $specialistData[0]['spec_city']; ?>, <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_au_states WHERE id = ".$specialistData[0]['spec_state']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['state_name']; ?>
                    <? }
                  } else { } ?>
                  , <?php echo $specialistData[0]['spec_postcode']; ?><br/>
                  <?
                  $servername = $this->db->hostname;
                  $username = $this->db->username;
                  $password = $this->db->password;
                  $dbname = $this->db->database;

            // Create connection
                  $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                  if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                  }

                  $sql = "SELECT * FROM tbl_country WHERE id = ".$specialistData[0]['spec_country']."";
                  $result = $conn->query($sql);

                  $num_rec = $result->num_rows;

                  if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { ?>
                      <? echo $row['country_name']; ?>
                      <? }
                    } else { } ?>
                  </td>
                </tr>





              </table>
            </div>

            <div style="clear:both"></div>
            <hr/>

            <div class="col-xs-12">
              <strong>Instructions: </strong><br/>
              <?php echo $specialistData[0]['spec_instructions']; ?>
            </div>

          </div>      

<!-- Last </div> will be in the footer -->