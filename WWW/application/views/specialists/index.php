<h1 class="page-header col-xs-11">Specialists</h1> <i class="fa fa-user-md col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/specialists/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Individual Specialist</a>
  <a href="<? echo base_url(); ?>index.php/specialists/clinic_add/" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Specialist Clinic</a>
</div>  
<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/specialists/search/">
      <div class="col-sm-3 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Individual Specialist's Name" name="spec_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['spec_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Specialist's Speciality" name="spec_profession" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['spec_profession'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By Individual Specialist City" name="spec_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['spec_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-2 search-box-item">
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="spec_state">
          <option selected value="">Select Individual Specialists State</option>
          <? foreach ($specialistState as $specialistState) { ?>
            <option value="<? echo $specialistState['id'] ?>"><? echo $specialistState['state_code'] ?></option>
            <? } ?>
          </select>
        </div>
      </div>

          <div class="col-sm-1">
      <button type="submit" class="btn btn-primary">Search</button>
    </div>
    <div style="clear:both"></div>
    <div class="advanced-search-link"><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/specialists/search_all">Advanced search</a></div>
  </form>
  </div>
  <div style="clear:both"></div>
  <div class="col-sm-12 project-info">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#specialists">Individual Specialist</a></li>
      <li><a data-toggle="tab" href="#clinic">Clinic List</a></li>
    </ul>
    <div class="col-sm-12 ">
     <div class="tab-content">
      <!-- List of Specialists -->
      <div id="specialists" class="tab-pane fade in active">
      <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Phone</th>
              <th>Fax</th>
              <th>Email</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">
            <? foreach ($specialistData as $specialistData) { ?>
              <tr>
                <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" ><? echo $specialistData['spec_name']; ?></a></td>
                <td><? echo $specialistData['spec_address']; ?></td>
                <td><? echo $specialistData['spec_city']; ?></td>
                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_au_states WHERE id = ".$specialistData['spec_state']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['state_name']; ?></td>
                    <? }
                  } else { ?>
                    <td>No Assigned State</td>
                    <? } ?>
                    <td><? echo $specialistData['spec_phone']; ?></td>
                    <td><? echo $specialistData['spec_fax']; ?></td>
                    <td><? echo $specialistData['spec_email']; ?></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/view/<? echo $specialistData['specID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/edit/<? echo $specialistData['specID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/delete/<? echo $specialistData['specID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                  </tr>
                  <? } ?>
                </tbody>
              </table>
                  </div>
            </div>
            <!-- List of Specialists Clinic -->
            <div id="clinic" class="tab-pane fade">
            <div style="overflow-x:auto;">
              <table class="table table-striped table-responsive" id="advisor-data">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Phone</th>
                    <th>Fax</th>
                    <th>Email</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="myTable">
                  <? foreach ($specialistClinic as $specialistData) { ?>
                    <tr>
                    <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/specialists/clinic_view/<? echo $specialistData['clinicID']; ?>/" ><? echo $specialistData['clinic_name']; ?></a></td>
                      <td><? echo $specialistData['clinic_address']; ?></td>
                      <td><? echo $specialistData['clinic_city']; ?></td>
                      <?
                      $servername = $this->db->hostname;
                      $username = $this->db->username;
                      $password = $this->db->password;
                      $dbname = $this->db->database;

            // Create connection
                      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                      if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                      }

                      $sql = "SELECT * FROM tbl_au_states WHERE id = ".$specialistData['clinic_state']."";
                      $result = $conn->query($sql);

                      $num_rec = $result->num_rows;

                      if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { ?>
                          <td><? echo $row['state_name']; ?></td>
                          <? }
                        } else { ?>
                          <td>No Assigned State</td>
                          <? } ?>
                          <td><? echo $specialistData['clinic_phone']; ?></td>
                          <td><? echo $specialistData['clinic_fax']; ?></td>
                          <td><? echo $specialistData['clinic_email']; ?></td>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/clinic_view/<? echo $specialistData['clinicID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/clinic_edit/<? echo $specialistData['clinicID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/specialists/clinic_delete/<? echo $specialistData['clinicID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                        </tr>
                        <? } ?>
                      </tbody>
                    </table>
                        </div>
                  </div>
                </div>
                <div class="col-md-12 text-center">
                  <ul class="pagination pagination-lg" id="myPager"></ul>
                </div>


<!-- Last </div> will be in the footer -->