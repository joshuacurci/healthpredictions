<h1 class="page-header col-xs-11">Add New Individual Specialist</h1> <i class="fa fa-user-md col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/specialists/newspecialist/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="spec_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Speciality:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Speciality" name="spec_profession" >
      </div>
      <div style="clear:both"></div>
    </div>

    
    <!-- Start of clinic -->
    <div class="form-group ">
     <label for="inputrecNum1" class="col-sm-3 control-label">Assign Clinic:</label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic" class="btn btn-success add_clinic_button" onclick="moreClinic()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   
   <div class="form-group gp_clinic_select_div gp_clinic_select2">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic2">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic2" class="btn btn-success add_clinic_button2" onclick="moreClinic2()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group gp_clinic_select_div gp_clinic_select3">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic3">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic3" class="btn btn-success add_clinic_button3" onclick="moreClinic3()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group gp_clinic_select_div gp_clinic_select4">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic4">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic4" class="btn btn-success add_clinic_button4" onclick="moreClinic4()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group gp_clinic_select_div gp_clinic_select5">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic5">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic5" class="btn btn-success add_clinic_button5" onclick="moreClinic5()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group gp_clinic_select_div gp_clinic_select6">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic6">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic6" class="btn btn-success add_clinic_button6" onclick="moreClinic6()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group gp_clinic_select_div gp_clinic_select7">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic7">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic7" class="btn btn-success add_clinic_button7" onclick="moreClinic7()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group gp_clinic_select_div gp_clinic_select8">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="spec_clinic8">
         <option value="0">Select clinic to assign</option>
         <? foreach ($specClinic as $clinicdata) { ?>
          <option><? echo $clinicdata['clinic_name'] ?> (<? echo $clinicdata['clinic_city'] ?>)</option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <p>Sorry, you have reach the maximum number of Clinic to input</p>
     </div>
     <div style="clear:both"></div>
   </div>

   <!--end of clinic -->

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Specialist Address Line 1" name="spec_address" >
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Specialist Address Line 2" name="spec_address2">
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="spec_city" >
        <select class="form-control address-half address-feilds" id="inputorg1" name="spec_state">
          <option selected value="0">Please select your state/territory</option>
          <? foreach ($specialistState as $specialistState) { ?>
           <option value="<? echo $specialistState['id'] ?>"><? echo $specialistState['state_code'] ?></option>
           <? } ?>
         </select>
         <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="spec_postcode" >
         <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="spec_country">
          <option value="0">Please select your country</option>
          <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $countrydata) { ?>
            <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
            <? } ?>
          </select>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="spec_phone" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="spec_mobile" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Work Phone Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Number" name="spec_workphone" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="spec_fax" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 1" name="spec_email" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 2" name="spec_email2" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Practice Manager:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Practice Manager" name="spec_responsibleperson" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Active/Inactive:</label>
        <div class="col-sm-9">
         <select class="form-control" id="spec_type" name="spec_active">
           <option value="">Please select Specialist Type</option>
           <option value="1">Inactive</option>
           <option value="0">Active</option>
         </select>
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
       <label for="inputrecNum1" class="col-sm-3 control-label">Last Active Date:</label>
       <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="spec_date_of_active"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Last Inactive Date:</label>
      <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="spec_date_of_inactive"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Add Instructions:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Instructions" name="spec_instructions" ></textarea>
      </div>
      <div style="clear:both"></div>
    </div> 

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Add Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Instructions" name="spec_notes" ></textarea>
      </div>
      <div style="clear:both"></div>
    </div> 

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->