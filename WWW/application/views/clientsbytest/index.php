<h1 class="page-header col-xs-11">Clients by tests</h1> <i class="fa fa-object-group col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12">
  <div class="col-sm-12 ">
    <? 
    if (!isset($searchresults)) { 
      ?>

      <div class="search-box">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" onsubmit="return testEmpty()" name="testForm" action="<? echo base_url(); ?>index.php/clientsbytest/search_by_test/" >

          <div class="col-sm-8 ">
            <h3>Optional Date Range</h3>
            <p>Clients that are archived will not be displayed</p>
            <div class="col-sm-6 search-box-item">
              <div class="search-title">Start Date:</div>
              <div class="search-field">
                <div class="input-group">
                  <input id="date-picker-1" type="text" class="date-picker form-control" name="startdate"/>
                  <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
                </div>
              </div>
            </div>

            <div class="col-sm-6 search-box-item">
              <div class="search-title">Last Date:</div>
              <div class="search-field">
                <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="lastdate"/>
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
                </div>
              </div>
              <br/>
              <br/>
            </div>
          </div>
          <div style="clear:both"></div>

          <div class="col-sm-4">
            <h3>Search by Test</h3>
            <b>Medicals</b><br/>
            <input type="hidden" name="checkedid[]"/> 
            <? foreach($testTypes as $finalTest) { ?>
              <? if ($finalTest['groupID'] == 1) { ?>
                <input type="checkbox" id="clientbytest_checkbox" value="<? echo $finalTest['testtypeID']; ?>" name="checkedid[]">
                <? echo $finalTest['type_name']; ?><br/>
                <? } ?>
                <? } ?>
              </div>
              <div class="col-sm-4">
                <b>Pathology</b><br/>
                <? foreach($testTypes as $finalTest) { ?>
                  <? if ($finalTest['groupID'] == 2) { ?>
                    <input type="checkbox" id="clientbytest_checkbox" value="<? echo $finalTest['testtypeID']; ?>" name="checkedid[]">
                    <? echo $finalTest['type_name']; ?><br/>
                    <? } ?>
                    <? } ?>
                  </div>
                  <div class="col-sm-4">
                    <b>Other</b><br/>
                    <? foreach($testTypes as $finalTest) { ?>
                      <? if ($finalTest['groupID'] == 3) { ?>
                        <input type="checkbox" id="clientbytest_checkbox" value="<? echo $finalTest['testtypeID']; ?>" name="checkedid[]">
                        <? echo $finalTest['type_name']; ?><br/>
                        <? } ?>
                        <? } ?>
                      </div>
                      <div style="clear:both"></div>
                      <br/><br/>


                      <div class="form-group search-box-button">
                        <div class="col-sm-12">
                          <br/>
                          <button type="submit" id="clientbytest_submit" class="btn btn-primary">Search</button>
                        </div>
                        <div style="clear:both"></div>
                      </div>
                    </div>


                  </form>
                </div>
              </div>
              <div style="clear:both"></div>
            </div>
            <?
          } else {
            ?>

            <div class="col-sm-12 top-buttons">  

              <div class="col-sm-12 top-buttons">  
              <div style="overflow-x:auto;">
                <table class="table table-striped table-responsive" id="advisor-data">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Sex</th>
                      <th>Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Phone</th>
                      <th>Fax</th>
                      <th>Email</th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="myTable">
                    <?
                    foreach ($clientData as $clientData) { ?>
                      <tr>
                        <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
                        <td style="width: 150px">
                          <? if ($clientData['client_sex'] == 'M') { echo 'Male'; }  ?>
                          <? if ($clientData['client_sex'] == 'F') { echo 'Female'; }  ?>
                        </td>
                        <td><? echo $clientData['client_address']; ?></td>
                        <td><? echo $clientData['client_city']; ?></td>
                        <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

            // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                        if ($conn->connect_error) {
                          die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData['client_state']."";
                        $result = $conn->query($sql);

                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) { ?>
                            <td><? echo $row['state_name']; ?></td>
                            <? }
                          } else { ?>
                            <td>No Assigned State</td>
                            <? } ?>
                            <td><? echo $clientData['client_phone']; ?></td>
                            <td><? echo $clientData['client_fax']; ?></td>
                            <td><? echo $clientData['client_email']; ?></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientData['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientData['clientID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                          </tr>
                          <? } ?>
                        </tbody>
                      </table>
                          </div>
                    </div>

                    <div class="col-md-12 text-center">
                      <ul class="pagination pagination-lg" id="myPager"></ul>
                    </div>
                    <? }  ?>
                  </div>
                  </div>
<!-- Last </div> will be in the footer -->