<h1 class="page-header col-xs-11">Add New Client Appointment</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/clientcontact/saveclientcontact/">
    <input type="hidden" name="historyID" value="<? echo $contactData[0]['historyID'] ?>"/>
    <input type="hidden" name="clientID" value="<? echo $clientID ?>"/>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Staff Member:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="history_staffID">
        <option selected value="0">Please select your name</option>
        <? foreach ($staffList as $staffList) { ?>
         <option <? if($contactData[0]['history_staffID'] == $staffList['staffID']) { echo 'selected';} ?> value="<? echo $staffList['staffID'] ?>"><? echo $staffList['staff_name'] ?></option>
        <? } ?>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="history_action" ><? echo $contactData[0]['history_action'] ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>    


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientID ?>" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->