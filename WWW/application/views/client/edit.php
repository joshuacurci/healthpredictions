<h1 class="page-header col-xs-11">Edit Client</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/save/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
    <input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Client Name" name="client_name" value="<?php echo $clientData[0]['client_name']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Insurance Company:</label>
      <div class="col-sm-9">
        <select class="form-control" id="inputorg1" name="insurance_company">
          <option selected value="0">Please select client insurance company</option>
          <? foreach ($insuranceComp as $insuranceData) { ?>
           <option <?php if ($insuranceData['insID'] == $clientData[0]['insurance_company']) { echo "selected";  } ?> value="<? echo $insuranceData['insID'] ?>"><? echo $insuranceData['ins_name'] ?></option>
           <? } ?>
         </select>
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Application/Policy Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Application/Policy Number" name="insurance_number" value="<?php echo $clientData[0]['insurance_number']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>


    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Assigned Advisor:</label>
      <div class="col-sm-9">
      <?php if ($_SESSION['usertype'] != 'I') { ?>
        <select class="form-control" id="inputorg1" name="assigned_advisor">
          <option selected value="0">Please select assigned advisor</option>
          <? foreach ($advisorDetails as $advisorData) { ?>
           <option <?php if ($advisorData['advisorID'] == $clientData[0]['assigned_advisor']) { echo "selected";  } ?> value="<? echo $advisorData['advisorID'] ?>"><? echo $advisorData['advisor_name'] ?></option>
           <? } ?>
         </select>
         <?php }
         else { ?>
          <? foreach ($advisorDetails as $advisorData) { ?>
            <?php if ($advisorData['advisorID'] == $clientData[0]['assigned_advisor']) {  ?>
              <input type="hidden" class="form-control" name="assigned_advisor" value="<?php echo $advisorData['advisorID']; ?>">
            <input type="text" class="form-control" name="assigned_advisor_name" value="<?php echo $advisorData['advisor_name']; ?>" disabled>
         <?php }}}?>
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Assigned Staff Member:</label>
      <div class="col-sm-9">
        <select class="form-control" id="inputorg1" name="assigned_staff">
          <option selected value="0">Please select assigned staff member</option>
          <? foreach ($staffDetails as $staffData) { ?>
           <option <?php if ($staffData['staffID'] == $clientData[0]['assigned_staff']) { echo "selected";  } ?> value="<? echo $staffData['staffID'] ?>"><? echo $staffData['staff_name'] ?></option>
           <? } ?>
         </select>
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Date Of Birth:</label>
      <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="client_DOB" value="<?php echo $clientData[0]['client_DOB']; ?>"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Sex:</label>
      <div class="col-sm-9">
       <input  <?php if($clientData[0]['client_sex'] == 'M') { echo 'checked'; }?> type="radio" name="client_sex" value="M">&nbsp Male &nbsp&nbsp
       <input <?php if($clientData[0]['client_sex'] == 'F') { echo 'checked'; }?> type="radio" name="client_sex" value="F">&nbsp Female
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Company/Organization:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Company/Organization" name="client_organization" value="<?php echo $clientData[0]['client_organization']; ?>"/>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="client_address" value="<?php echo $clientData[0]['client_address']; ?>"/>
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 2" name="client_address2" value="<?php echo $clientData[0]['client_address2']; ?>"/>
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="client_city" value="<?php echo $clientData[0]['client_city']; ?>"/>
      <select class="form-control address-half address-feilds" id="inputorg1" name="client_state">
        <option selected value="0">Please select your state</option>
        <? foreach ($clientState as $clientState) { ?>
         <option <?php if ($clientState['id'] == $clientData[0]['client_state']) { echo "selected";  } ?> value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="client_postcode" value="<?php echo $clientData[0]['client_postcode']; ?>"/>
       <div class="clearboth"></div>
       <select class="form-control address-feilds" id="inputorg2" name="client_country">
        <option value="0">Please select your country</option>
        <option value="13">Australia</option>
        <option value="158">New Zealand</option>
        <option disabled="disabled">----</option>
        <? foreach ($country as $countrydata) { ?>
          <option <?php if ($countrydata['id'] == $clientData[0]['client_country']) { echo "selected";  } ?> value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="client_phone" value="<?php echo $clientData[0]['client_phone']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="client_mobile" value="<?php echo $clientData[0]['client_mobile']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Work Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Number" name="client_workphone" value="<?php echo $clientData[0]['client_workphone']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="client_fax" value="<?php echo $clientData[0]['client_fax']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="client_email" value="<?php echo $clientData[0]['client_email']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Second Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="client_email2" value="<?php echo $clientData[0]['client_email2']; ?>"/>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Advisor Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="client_notes" ><?php echo $clientData[0]['client_notes']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->