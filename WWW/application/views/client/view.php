<script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/client-view.js"></script>

<h1 class="page-header col-xs-11">Client Information</h1><i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-8 top-buttons">
  <? if ($_SESSION['usertype'] == 'I') { ?>
    <a href="<?php echo base_url();?>index.php/client/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <? } else { ?>
    <a href="<?php echo base_url();?>index.php/client/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
    <a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Client Information</a>
    <a href="<? echo base_url(); ?>index.php/client/client_complete/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true" ></span> Complete Client</a>
    <a href="<? echo base_url(); ?>index.php/client/pre_generate/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/" class="btn btn-warning"><span class="glyphicon glyphicon-new-window" aria-hidden="true"></span> Notification Forms</a>
  <? } } ?>
</div> 

<div class="col-md-4 top-buttons">
  <div class="name-header"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $clientData[0]['client_name']; ?></div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $clientData[0]['dateadded']);?></div>
  <div style="text-align:right"><b>ID:</b> <?php echo sprintf('%08d', $clientData[0]['clientID']); ?>-<?php echo sprintf('%010d', $clientData[0]['refferalID']); ?></div>
</div>  



<div class="col-xs-12 main-data-content">
        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Name:</strong></td>
              <td><?php echo $clientData[0]['client_name']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Company/organization:</strong></td>
              <td><?php echo $clientData[0]['client_organization']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Date of Birth:</strong></td>
              <td><?php echo $clientData[0]['client_DOB']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Sex:</strong></td>
              <td>
                <? if ($clientData[0]['client_sex'] == 'M') { echo 'Male'; }  ?>
                <? if ($clientData[0]['client_sex'] == 'F') { echo 'Female'; }  ?>
              </td>
            </tr>

            <tr>
              <td class="view-title"><strong>Insurance Company:</strong></td>
              <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_insurance WHERE insID = ".$clientData[0]['insurance_company']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        if ($_SESSION['usertype'] == 'I') {
                          echo $row['ins_name']; 
                        } else {
                          echo "<a href='//".$_SERVER['SERVER_NAME']."/insurance/view/".$row['insID']."/' target='_blank'>".$row['ins_name']."</a>"; 
                        }
                       } } else { } ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Application/Policy Number:</strong></td>
              <td><?php echo $clientData[0]['insurance_number']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Assigned Advisor:</strong></td>
              <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData[0]['assigned_advisor']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        
                        if ($_SESSION['usertype'] == 'I') {
                          echo $row['advisor_name']; 
                        } else {
                          echo "<a href='//".$_SERVER['SERVER_NAME']."/advisor/view/".$row['advisorID']."/' target='_blank'>".$row['advisor_name']."</a>"; 
                        }
                       } } else { } ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Assigned Staff Member:</strong></td>
              <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_staff WHERE staffID = ".$clientData[0]['assigned_staff']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        
                        if ($_SESSION['usertype'] == 'I') {
                          echo $row['staff_name']; 
                        } else {
                          echo "<a href='//".$_SERVER['SERVER_NAME']."/staff/view/".$row['staffID']."/' target='_blank'>".$row['staff_name']."</a>"; 
                        }
                       } } else { } ?></td>
            </tr>
            <tr>
              <td class="view-title"><strong>Client Status:</strong></td>
              <?php if ($clientData[0]['client_archived']=="Y") { ?>
              <td>Completed</td>
              <?php } 
                    else if ($clientData[0]['client_deleted']=="Y") { ?>
              <td>Deleted</td>
                    <?php } 
                    else if ($clientData[0]['client_active']=="Y"){ ?>
              <td>Active</td>
                    <?php } 
                    else 
                    { ?>
              <td>Not Active</td>
                    <?php } ?>
            </tr>
            
          </table>
        </div>

        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Address:</strong></td>
              <td>
                <?php echo $clientData[0]['client_address']; ?><br/>
                <?php echo $clientData[0]['client_address2']; ?><br/>
                <?php echo $clientData[0]['client_city']; ?>, <?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData[0]['client_state']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <? echo $row['state_name']; ?>
            <? }
          } else { } ?>, <?php echo $clientData[0]['client_postcode']; ?><br/>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_country WHERE id = ".$clientData[0]['client_country']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['country_name']; ?>
                <? }
              } else { ?>
                
                <? } ?>
              </td>
            </tr>
          </table>

          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Email:</strong></td>
              <td><?php echo $clientData[0]['client_email']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Secondary Email:</strong></td>
              <td><?php echo $clientData[0]['client_email2']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Phone:</strong></td>
              <td><?php echo $clientData[0]['client_phone']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Work Phone:</strong></td>
              <td><?php echo $clientData[0]['client_workphone']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Mobile:</strong></td>
              <td><?php echo $clientData[0]['client_mobile']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Fax:</strong></td>
              <td><?php echo $clientData[0]['client_fax']; ?></td>
            </tr>

          </table>
        </div>

        <div class="col-md-4">
            <div class="client-view-logo" style="text-align:right">
              <? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$clientData[0]['siteID']."";
                $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { 
                      echo '<img class="logoPrimary" src="//'. $_SERVER['SERVER_NAME'] .'/uploads/logos/'. $row['site_logo'] .'" style="width:100%; max-width:280px; text-align:right;">'; 
                  } } else { } ?>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr>
        <strong>Advisor Notes: </strong><br/>
        <?php echo $clientData[0]['client_notes']; ?>
    </div>

<div style="clear:both"></div>
<hr>
<? if ($_SESSION['usertype'] != 'I') { ?>
<div class="col-xs-12 main-data-content">
  <table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>Print</th>
        <th>Email</th>
        <th>Fax</th>
        <th>Phone</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><strong>Pathology Appointment Notification</strong></td>
        <td><?php if($not1[0]['print_date'] != "") { echo date("d/m/Y",$not1[0]['print_date']);} ?></td>
        <td><?php if($not1[0]['email_date'] != "") { echo date("d/m/Y",$not1[0]['email_date']);} ?></td>
        <td><?php if($not1[0]['fax_date'] != "") { echo date("d/m/Y",$not1[0]['fax_date']);} ?></td>
        <td><?php if($not1[0]['phone_date'] != "") { echo date("d/m/Y",$not1[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Doctor Appointment Notification</strong></td>
        <td><?php if($not2[0]['print_date'] != "") { echo date("d/m/Y",$not2[0]['print_date']);} ?></td>
        <td><?php if($not2[0]['email_date'] != "") { echo date("d/m/Y",$not2[0]['email_date']);} ?></td>
        <td><?php if($not2[0]['fax_date'] != "") { echo date("d/m/Y",$not2[0]['fax_date']);} ?></td>
        <td><?php if($not2[0]['phone_date'] != "") { echo date("d/m/Y",$not2[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Client Consent Form</strong></td>
        <td><?php if($not3[0]['print_date'] != "") { echo date("d/m/Y",$not3[0]['print_date']);} ?></td>
        <td><?php if($not3[0]['email_date'] != "") { echo date("d/m/Y",$not3[0]['email_date']);} ?></td>
        <td><?php if($not3[0]['fax_date'] != "") { echo date("d/m/Y",$not3[0]['fax_date']);} ?></td>
        <td><?php if($not3[0]['phone_date'] != "") { echo date("d/m/Y",$not3[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Client Appointment Notification/Instructions</strong></td>
        <td><?php if($not4[0]['print_date'] != "") { echo date("d/m/Y",$not4[0]['print_date']);} ?></td>
        <td><?php if($not4[0]['email_date'] != "") { echo date("d/m/Y",$not4[0]['email_date']);} ?></td>
        <td><?php if($not4[0]['fax_date'] != "") { echo date("d/m/Y",$not4[0]['fax_date']);} ?></td>
        <td><?php if($not4[0]['phone_date'] != "") { echo date("d/m/Y",$not4[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Advisor Appointment Notification</strong></td>
        <td><?php if($not5[0]['print_date'] != "") { echo date("d/m/Y",$not5[0]['print_date']);} ?></td>
        <td><?php if($not5[0]['email_date'] != "") { echo date("d/m/Y",$not5[0]['email_date']);} ?></td>
        <td><?php if($not5[0]['fax_date'] != "") { echo date("d/m/Y",$not5[0]['fax_date']);} ?></td>
        <td><?php if($not5[0]['phone_date'] != "") { echo date("d/m/Y",$not5[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Advisor Completion Notification</strong></td>
        <td><?php if($not6[0]['print_date'] != "") { echo date("d/m/Y",$not6[0]['print_date']);} ?></td>
        <td><?php if($not6[0]['email_date'] != "") { echo date("d/m/Y",$not6[0]['email_date']);} ?></td>
        <td><?php if($not6[0]['fax_date'] != "") { echo date("d/m/Y",$not6[0]['fax_date']);} ?></td>
        <td><?php if($not6[0]['phone_date'] != "") { echo date("d/m/Y",$not6[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Referral Acknowledgment </strong></td>
        <td><?php if($not7[0]['print_date'] != "") { echo date("d/m/Y",$not7[0]['print_date']);} ?></td>
        <td><?php if($not7[0]['email_date'] != "") { echo date("d/m/Y",$not7[0]['email_date']);} ?></td>
        <td><?php if($not7[0]['fax_date'] != "") { echo date("d/m/Y",$not7[0]['fax_date']);} ?></td>
        <td><?php if($not7[0]['phone_date'] != "") { echo date("d/m/Y",$not7[0]['phone_date']);} ?></td>
      </tr>
      <tr>
        <td><strong>Insurance Completion Notification</strong></td>
        <td><?php if($not8[0]['print_date'] != "") { echo date("d/m/Y",$not8[0]['print_date']);} ?></td>
        <td><?php if($not8[0]['email_date'] != "") { echo date("d/m/Y",$not8[0]['email_date']);} ?></td>
        <td><?php if($not8[0]['fax_date'] != "") { echo date("d/m/Y",$not8[0]['fax_date']);} ?></td>
        <td><?php if($not8[0]['phone_date'] != "") { echo date("d/m/Y",$not8[0]['phone_date']);} ?></td>
      </tr>
    </tbody>
  </table>
  <? if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'C') { ?><div class="rightaligned"><a href="<? echo base_url(); ?>index.php/client/notifyedit/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Notification Dates</a></div><? } ?>
</div>

<div style="clear:both"></div>
<hr>
<? } ?>

<div class="col-xs-12 main-data-content">
  <div>
    <h3>Tests Ordered</h3>
        <div style="clear:both;"></div>
        <br/>
        <?  $indTicked =[];
        for($i=0; $i<count($testTicked); $i++)
        {
          array_push($indTicked, $testTicked[$i]['testtypeID']);
        } ?>
        <div class="col-sm-4">
        <b>Medicals</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 1) { ?>
          
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>
        <div class="col-sm-4">
        <b>Pathology</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 2) { ?>
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>
        <div class="col-sm-4">
        <b>Other</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 3) { ?>
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>

        <div style="clear:both"></div>
        
        <? if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B') { ?>
        <div class="rightaligned"><a href="<? echo base_url(); ?>index.php/client/edittests/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Tests Ordered</a></div>
        <? } ?>

        <br/><br/>

        <div class="notes-wrapper">
        <strong>Test Notes: </strong><br/>
          <div>
            <?php echo $testNotes[0]['testNote']; ?>
          </div>
        </div>
  </div>

<hr>

<? if ($_SESSION['usertype'] != 'I') { ?>
      <div>
      <h3>Client Appointments</h3>
      
      <? if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['usertype'] == 'C' || $_SESSION['usertype'] == 'D' || $_SESSION['usertype'] == 'E') { ?>
        <a onclick='displaynewappointment()' class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new appointment</a>
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/appointment/newclientapp/" id="myTableAppointmentForm" style="display:none;">
        <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
        <input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID']; ?>"/>
        <table style='width:100%;' class='new-test-table'>
        <tr>
        <td>Type</td>
        <td>Name</td>
        <td>Time</td>
        <td>Date</td>
        <td>Address</td>
        <td>Service</td>
        <td colspan='2'></td>
        </tr>
        <tr>
        <td><select class='form-control' id='inputorg1' name='app_type'><option selected value=''>Appointment type</option><option value='GP'>GP</option><option value='P'>Pathologist</option><option value='S'>Specialist</option><option value='N'>Nurse</option></select></td>
        <td><input type='text' class='form-control' id='capital-text' placeholder='Name' name='app_name' ></td>
        <td><input type='text' class='form-control' id='capital-text' placeholder='Appointment Time eg. 02:00pm' name='app_time' ></td>
        <td><input id='date-picker-1' type='text' class='date-picker form-control' name='app_date'/><label for='date-picker-1' class='input-group-addon btn'><span class='glyphicon glyphicon-calendar'></span></label></td>
        <td><input type='text' class='form-control' id='capital-text' placeholder='Location' name='app_place' ></td>
        <td><textarea class='form-control' id='capital-text' placeholder='Service' name='app_service' ></textarea></td>
        <td colspan='2'><button type='submit' class='btn btn-success'><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new appointment</button></td>
          </tr></table>
        </form>      
        <? } ?>  
        <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
              <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Time</th>
                <th>Date</th>
                <th>Address</th>
                <th>Service</th>
                <th></th>
                <th></th>
              </tr>
          </thead>
          <tbody id="myTableAppointment">
            <? foreach ($clientApointment as $clientApointment) { ?>
              <tr>
                <td>
                  <? if ($clientApointment['app_type'] == '0' || $clientApointment['app_type'] == 'GP') { echo "GP"; } 
                  else if ($clientApointment['app_type'] == '1' || $clientApointment['app_type'] == 'P') { echo "Pathologist"; } 
                  else if ($clientApointment['app_type'] == '2' || $clientApointment['app_type'] == 'S') { echo "Specialist"; } 
                  else if ($clientApointment['app_type'] == '3' || $clientApointment['app_type'] == 'N') { echo "Nurse"; } ?>
                </td>
                <td><? echo $clientApointment['app_name']; ?></td>
                <td><? echo $clientApointment['app_time']; ?></td>
                <td><? echo date('d/m/Y',$clientApointment['app_date']); ?></td>
                <td><? echo $clientApointment['app_place']; ?></td>
                <td><? echo $clientApointment['app_service']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/edit/<? echo $clientApointment['appID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/delete/<? echo $clientApointment['appID']; ?>/<? echo $clientApointment['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
              </tr>
            <? } ?>
          </tbody>
        </table>
            </div>
      </div>
<? }
else
{ ?>
      <div>
      <h3>Client Appointments</h3>
      <div style="overflow-x:auto;">
<table class="table table-striped table-responsive" id="advisor-data">
          <thead>
              <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Time</th>
                <th>Date</th>
                <th>Address</th>
                <th>Service</th>
              </tr>
          </thead>
          <tbody id="myTableAppointment">
            <? foreach ($clientApointment as $clientApointment) { ?>
              <tr>
                <td>
                  <? if ($clientApointment['app_type'] == '0' || $clientApointment['app_type'] == 'D') { echo "Doctor"; } 
                  else if ($clientApointment['app_type'] == '1' || $clientApointment['app_type'] == 'P') { echo "Pathologist"; } 
                  else if ($clientApointment['app_type'] == '2' || $clientApointment['app_type'] == 'S') { echo "Specialist"; } 
                  else if ($clientApointment['app_type'] == '3' || $clientApointment['app_type'] == 'N') { echo "Nurse"; } ?>
                </td>
                <td><? echo $clientApointment['app_name']; ?></td>
                <td><? echo $clientApointment['app_time']; ?></td>
                <td><? echo date('d/m/Y',$clientApointment['app_date']); ?></td>
                <td><? echo $clientApointment['app_place']; ?></td>
                <td><? echo $clientApointment['app_service']; ?></td>
              </tr>
            <? } ?>
          </tbody>
        </table>
            </div>
        </div>
<?php } ?>
<hr>
      <div>
      <h3>Client Contact History</h3>
      <?php if ($_SESSION['usertype'] != 'D' && $_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H' && $_SESSION['usertype'] != 'I') { ?>
        <a onclick='displaynewcontact()' class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new client contact</a>
       <?php } ?>
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/clientcontact/newclienthistory/" id="myTableContactForm" style="display:none; padding: 10px 25px;">
        <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
        <input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID']; ?>"/>
        <input type="hidden" name="history_staffID" value="<? echo $_SESSION['userID']; ?>"/>
        
        <div class="form-group">
          <label for="inputrecNum1" class="control-label">Notes:</label>
          <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="history_action" ></textarea>
        </div> 
        
        <button type='submit' class='btn btn-success'><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new client contact</button>

        </form>      
        <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
              <tr>
                <th style="width:10%">Date</th>
                <th style="width:10%">Time</th>
                <th style="width:30%">Action</th>
                <th style="width:15%">Staff</th>
                <th></th>
                <th></th>
              </tr>
          </thead>
          <tbody id="myTable">
            <? foreach ($clientHistory as $clientHistory) { ?>
              <tr>
                <td><? echo date('d/m/Y',$clientHistory['history_timestamp']); ?></td>
                <td><? echo date('h:i A',$clientHistory['history_timestamp']); ?></td>
                <td>
                  <? if($clientHistory['history_lastedited'] != '') {
                    echo '<i>(Edited '.date('d/m/Y h:i A',$clientHistory['history_lastedited']).')</i>';
                  } ?>
                  <? echo $clientHistory['history_action']; ?>
                </td>
                <td> <? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_users WHERE userID = ".$clientHistory['history_staffID']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        echo $row['username'];
                       } } else { } ?>
                </td>
                <?php if ($_SESSION['usertype'] != 'D' && $_SESSION['usertype'] != 'E' && $_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H' && $_SESSION['usertype'] != 'I') { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/clientcontact/edit/<? echo $clientHistory['historyID']; ?>/<? echo $clientHistory['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <?php } ?>
                <td class="tableButtons"><? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'){ ?><a href="<? echo base_url(); ?>index.php/clientcontact/delete/<? echo $clientHistory['historyID']; ?>/<? echo $clientHistory['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a><? } ?></td>
              </tr>
            <? } ?>
          </tbody>
        </table>
                </div>
      </div>
    </div>

<!-- Last </div> will be in the footer -->
<script>
function refresh()
{
  setTimeout(function(){ location.reload(); },2000);
}
</script>