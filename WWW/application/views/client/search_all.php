<h1 class="page-header col-xs-11">Search Client</h1><i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
  <? 
  if (isset($searchresults)) { 
    echo '<h3>Search Results</h3>'; 
  } else {
    ?>
    <div class="search-div-searches">
      <div class="search-box">
        <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search_result/">
          <div class="col-sm-12 ">
            <center><h3>Client Advanced Search</h3>
             <p>Fill in one or more criteria to search for a Client.</p></center>
           </div>
           <br/><br/>
           <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Name:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Name" name="client_name" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Organisation:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Organisation" name="client_organization" />
            </div>
            <div style="clear:both"></div>
          </div>     

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 1:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="Address Line 1" name="client_address" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 2:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum2" placeholder="Address Line 2" name="client_address2" />
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">City/Suburb:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="capital-text" placeholder="City/Suburb" name="client_city" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg1" name="client_state">
                <option selected value="">Please select your state/territory</option>
                <? foreach ($clientState as $clientState) { ?>
                 <option value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
                 <? } ?>
               </select>
             </div>
             <div style="clear:both"></div>
           </div>

           <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Postcode:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="client_postcode" >
            </div>
            <div style="clear:both"></div>
          </div>

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Country:</label>
            <div class="col-sm-7">
              <select class="form-control" id="inputorg2" name="client_country">
                <option value="">Please select your country</option>
                <? foreach ($country as $countrydata) { ?>
                  <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Phone Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="client_phone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Work Phone:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Phone" name="client_workphone" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Mobile Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="client_mobile" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Fax Number:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="client_fax" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="client_email" />
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="col-md-4 form-group">
              <label for="inputrecNum1" class="col-sm-5 control-label">2nd Email Address:</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="client_email2" />
              </div>
              <div style="clear:both"></div>
            </div>

            <!-- Commented for now, needs to be a date range -->

          <!-- <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Date of Birth:</label>
            <div class="col-sm-7">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Date of Birth" name="client_DOB" />
            </div>
            <div style="clear:both"></div>
          </div> -->

          <div class="col-md-4 form-group">
            <label for="inputrecNum1" class="col-sm-5 control-label">Sex:</label>
            <div class="col-sm-7">
             <select class="form-control" id="inputorg1" name="client_sex">
               <option value="">Please select Client sex</option>
               <option value="M">Male</option>
               <option value="F">Female</option>

             </select>
           </div>
           <div style="clear:both"></div>
         </div>

         <div style="clear:both"></div>

         <div class="col-sm-12">
          <button type="submit" class="btn btn-primary">Search</button>
        </div>
        <div style="clear:both"></div>
      </form>
    </div>
  </div>
  <?php } ?>

  <? if(isset($searchresults)) { echo '<div style="display: block;">';
} else { echo '<div style="display: none;">'; }
?>
<a href="<? echo base_url(); ?>index.php/client/search_all/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
<div class="col-sm-12 ">
  <div class="col-sm-12 top-buttons">
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Organisation</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($clientData as $clientData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" /><? echo $clientData['client_name']; ?></a></td>
            <td style="width: 150px"><? echo $clientData['client_organization']; ?></td>
            <td><? echo $clientData['client_address']; ?></td>
            <td><? echo $clientData['client_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData['client_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $clientData['client_phone']; ?></td>
                <td><? echo $clientData['client_fax']; ?></td>
                <td><? echo $clientData['client_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientData['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientData['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                <?php } ?>
              </tr>
              <? } ?>
            </tbody>
          </table>
                </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>
      </div>

<!-- Last </div> will be in the footer -->