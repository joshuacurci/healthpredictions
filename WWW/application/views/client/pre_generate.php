<h1 class="page-header col-xs-11">Client Information</h1><i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-8 top-buttons">
	<a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
	<a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientData[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Client Information</a>
</div> 

<div class="col-md-4 top-buttons">
	<div class="name-header"><i class="fa fa-users" aria-hidden="true"></i> <?php echo $clientData[0]['client_name']; ?></div>
	<div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $clientData[0]['dateadded']);?></div>
</div>  
<form target="_blank" enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/generate/generate_form/" onsubmit="return IsEmpty()" name="appForm" >
	<div class="col-xs-12">

		<input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID']; ?>" />
		<input type="hidden" name="insuranceID" value="<? echo $clientData[0]['insurance_company']; ?>" />

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-1 control-label">Appointments:</label>
			<div class="col-sm-12">
			<p>	<i>Please select at least one appointment.<b> Only ticked item/s will be included.</b></i></p>
			</div>
			<div class="col-sm-12">
			<div style="overflow-x:auto;">
				<table class="table table-striped table-responsive" id="advisor-data">
					<thead>
						<tr>
							<th></th>
							<th>Type</th>
							<th>Staff Name</th>
							<th>Time</th>
							<th>Date</th>
							<th>Location</th>
							<th>Service</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<? foreach ($clientApointment as $clientApointment) { ?>
						<tbody id="myTable">

							<tr>
								<td>
									<input type="checkbox" id="appID_form" name="appID[]" value="<? echo $clientApointment['appID']; ?>" checked/>
								</td>
								<td>
									<? if ($clientApointment['app_type'] == '0' || $clientApointment['app_type'] == 'GP') { echo "GP"; } 
									else if ($clientApointment['app_type'] == '1' || $clientApointment['app_type'] == 'P') { echo "Pathologist"; } 
									else if ($clientApointment['app_type'] == '2' || $clientApointment['app_type'] == 'S') { echo "Specialist"; } 
									else if ($clientApointment['app_type'] == '3' || $clientApointment['app_type'] == 'N') { echo "Nurse"; } ?> 
								</td>
								<td><? echo $clientApointment['app_name']; ?> </td>
								<td><? echo $clientApointment['app_time']; ?> </td>
								<td><? echo date('d/m/Y',$clientApointment['app_date']); ?> </td>
								<td><? echo $clientApointment['app_place']; ?> </td>
								<td><? echo $clientApointment['app_service']; ?></td>
								<td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/edit/<? echo $clientApointment['appID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
								<td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/delete/<? echo $clientApointment['appID']; ?>/<? echo $clientApointment['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>

							</tr>
							
						</tbody>
						<? } ?>
					</table>
					</div>
			</div>
		</div>

		<div class="col-xs-12 main-data-content">
		<p><i><b>Please select a report to generate:</b></i></p>
		<div style="overflow-x:auto;">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Report</th>
								<th>View Report Generator</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><strong>Referral Acknowledgment </strong></td>
								<td>
									<button type="submit" name="referral_acknowledgement" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
									<button type="submit" name="referral_acknowledgement_tab" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate in new tab
									</button>
									<button type="submit" name="referral_acknowledgement_email" class="btn btn-primary">
										<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email to advisor
									</button>
								</td>
								
							</tr>
							<tr>
								<td><strong>Notification to Nurse of Referral/Appointment </strong></td>
								<td>
									<!-- <a href="<? echo base_url(); ?>index.php/client/path_notification_view/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									</a> -->
									<button type="submit" name="notif_to_path" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
										
								</td>
								
							</tr>
							<!-- <tr>
								<td><strong>Notification to Doctor of Appointment and Consent ID </strong></td>
								<td>
									//<a href="" class="btn btn-success" target="_blank">
									//	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									//</a>
									<button type="submit" name="notif_to_doctor" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
								</td>
								
							</tr>
							<tr>
								<td><strong>Notification to Client of Appointment, Client Instructions and Consent ID </strong></td>
								<td>
									//<a href="<? //echo base_url(); ?>index.php/client/client_notification_view/<? //echo $clientData[0]['clientID']; ?>/<? //echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">
									//	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									//</a>

									<button type="submit" name="notif_to_client" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
								</td>
								
							</tr> -->
							<tr>
								<td><strong>Notification to Advisor of Appointments </strong></td>
								<td>
									<!-- <a href="<? echo base_url(); ?>index.php/client/advisor_notification_view/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">		
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									</a> -->
									<button type="submit" name="notif_to_advisor" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
									<button type="submit" name="notif_to_advisor_email" class="btn btn-primary">
										<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email to advisor
									</button>
								</td>
								
							</tr>
							<tr>
								<td><strong>Notification to Advisor of Completion and Summary Form </strong></td>
								<td>
									<!-- <a href="<? echo base_url(); ?>index.php/client/advisor_notification_of_completion_view/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									</a> -->
									<button type="submit" name="notif_to_adv_completion" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
									<button type="submit" name="notif_to_adv_completion_email" class="btn btn-primary">
										<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email to advisor
									</button>
								</td>
								
							</tr>
							<!-- <tr>
								<td><strong>Notification to Insurance Company of Completion</strong></td>
								<td>
									//<a href="<? //echo base_url(); ?>index.php/client/noti_to_ins_company_completion_view/<? //echo $clientData[0]['clientID']; ?>/<? //echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">
									//	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									//</a>
									<button type="submit" name="notif_to_ins_company" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Generate
									</button>
								</td>
								
							</tr>
							<tr>
								<td><strong>Client Consent and Identification</strong></td>
								<td>
									//<a href="<? //echo base_url(); ?>index.php/client/client_consent_view/<? //echo $clientData[0]['clientID']; ?>/<? //echo $clientData[0]['insurance_company']; ?>/" class="btn btn-success" target="_blank">			
									//	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View
									//</a>
									<button type="submit" name="client_consent_id" class="btn btn-primary">
										<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
									</button>
								</td>
							</tr> -->
						</tbody>
					</table>
					</div>
					<!-- <div class="rightaligned"><a href="<? //echo base_url(); ?>index.php/client/notifyedit/<? //echo $clientData[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Notification Dates</a></div> -->


			<div style="clear:both"></div>

		
		<hr>
</form>
<!-- Last </div> will be in the footer -->