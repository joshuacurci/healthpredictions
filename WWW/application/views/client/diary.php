<h1 class="page-header col-xs-11">Client Diary</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
</div>  
<div class="col-sm-12">


  <div class="col-sm-4 ">
    <div class="search-box">
      <h3>Search Specific Date</h3>
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search_app_date/">
        <div class="col-sm-12 search-box-item">
          <div class="search-title">Search:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-1" type="text" class="date-picker form-control" name="app_date"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>


  <div class="col-sm-8 ">
    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
        <h3>Search Between Dates</h3>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Search Specific Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-1" type="text" class="date-picker form-control" name="client_DOB"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Search Specific Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-1" type="text" class="date-picker form-control" name="client_DOB"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>
  <div style="clear:both"></div>
</div>

<? 
if (isset($searchresults)) { 
  ?>
  <div class="col-sm-12 ">

    <div class="col-sm-12 top-buttons">  
    <div style="overflow-x:auto;">
      <table class="table table-striped table-responsive" id="advisor-data">
        <thead>
          <tr>
            <th>Time</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Mobile</th>
            <th>Service Booked</th>
            <th>Pathology/Dr</th>
            <th>Address of Service</th>
            <th>Advisor</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="myTable">
          <? foreach ($appData as $appData) { ?>

            <tr>
              <td><? echo date('d/m/Y',$appData['app_date']); ?>, <? echo $appData['app_time']; ?></td>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;
            // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_client WHERE clientID = ".$appData['clientID']."";
              $result = $conn->query($sql);
              $num_rec = $result->num_rows;
              $clientData = $result->fetch_assoc(); 

              $sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData['assigned_advisor']."";
              $result2 = $conn->query($sql2);
              $num_rec2 = $result2->num_rows;
              $advisorData = $result2->fetch_assoc(); 

              ?>
              <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $appData['clientID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
              <td><? echo $clientData['client_phone']; ?></td>
              <td><? echo $clientData['client_mobile']; ?></td>
              <?  ?>
              <td><? echo $appData['app_service']; ?></td>
              <td><? echo $appData['app_name']; ?></td>
              <td><? echo $appData['app_place']; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/advisor/view/<? echo $advisorData['advisorID']; ?>/" ><? echo $advisorData['advisor_name']; ?></a></td>
              <td></td>
              <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/edit/<? echo $appData['appID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
              <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/appointment/delete_app/<? echo $appData['clientID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>




            </tr>
            <? } ?>
          </tbody>
        </table>
            </div>
      </div>

      <div class="col-md-12 text-center">
        <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>
      <? } ?>

<!-- Last </div> will be in the footer -->