<h1 class="page-header col-xs-11">Client</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <? if ($_SESSION['usertype'] == 'I') { ?>
    <a href="<? echo base_url(); ?>index.php/referral/lodge1/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Lodge a Referral</a>
  <? } else { ?>
    <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
    <a href="<? echo base_url(); ?>index.php/client/archived/" class="btn btn-warning"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Archived Clients </a>
  <? } ?>
</div>  
<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
    <div class="col-sm-12 ">
      <h3>Search</h3>
      </div>
      <div class="col-sm-4 search-box-item">
        <div class="search-title">Name:</div>
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-4 search-box-item">
        <div class="search-title">City:</div>
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By City" name="client_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-4 search-box-item">
      <div class="search-title">State:</div>
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="client_state">
          <option value="">All</option>
          <? foreach ($clientState as $clientState) { ?>
           <option value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>


     <div class="form-group search-box-button">
      <div class="col-sm-12">
        <br/>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>
    </div>
  </form>
</div>
<div style="clear:both"></div>
<div class="col-sm-12 ">

  <div class="col-sm-12 top-buttons">  
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Sex</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>JV</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <? if ($_SESSION['usertype'] != 'I') { ?><th></th><? } ?>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($clientData as $clientData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
            <td style="width: 150px">
            <? if ($clientData['client_sex'] == 'M') { echo 'Male'; }  ?>
            <? if ($clientData['client_sex'] == 'F') { echo 'Female'; }  ?>
            </td>
            <td><? echo $clientData['client_address']; ?></td>
            <td><? echo $clientData['client_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData['client_state']."";
            $result = $conn->query($sql);

            $sql2 = "SELECT * FROM tbl_sites WHERE siteID = ".$clientData['siteID']."";
            $result2 = $conn->query($sql2);
              if ($result2->num_rows > 0) {
                while($row2 = $result2->fetch_assoc()) { 
                  $jv=$row2['site_name'];
              } } else { }

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $clientData['client_phone']; ?></td>
                <td><? echo $jv; ?></td>
                <td><? echo $clientData['client_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientData['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <? if ($_SESSION['usertype'] != 'I') { ?><td class="tableButtons"><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientData['clientID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td><? } ?>
                <?php } ?>
              </tr>
              <? } ?>
            </tbody>
          </table>
          </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>


<!-- Last </div> will be in the footer -->