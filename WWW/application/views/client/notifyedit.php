<h1 class="page-header col-xs-11">Edit Client Notifications for <? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientID."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        echo $row['client_name']; 
                       } } else { } ?></h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/savenotify/">
    <input type="hidden" name="clientID" value="<? echo $clientID ?>"/>

    <div class="col-xs-12 main-data-content">
  <table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>Print</th>
        <th>Email</th>
        <th>Fax</th>
        <th>Phone</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><strong>Pathology Appointment Notification</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="not1[print_date]" value="<?php echo $not1[0]['print_date']; ?>"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-2" type="text" class="date-picker form-control" name="not1[email_date]" value="<?php echo $not1[0]['email_date']; ?>"/>
            <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-3" type="text" class="date-picker form-control" name="not1[fax_date]" value="<?php echo $not1[0]['fax_date']; ?>"/>
            <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-4" type="text" class="date-picker form-control" name="not1[phone_date]" value="<?php echo $not1[0]['phone_date']; ?>"/>
            <label for="date-picker-4" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Doctor Appointment Notification</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-5" type="text" class="date-picker form-control" name="not2[print_date]" value="<?php echo $not2[0]['print_date']; ?>"/>
            <label for="date-picker-5" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-6" type="text" class="date-picker form-control" name="not2[email_date]" value="<?php echo $not2[0]['email_date']; ?>"/>
            <label for="date-picker-6" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-7" type="text" class="date-picker form-control" name="not2[fax_date]" value="<?php echo $not2[0]['fax_date']; ?>"/>
            <label for="date-picker-7" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-8" type="text" class="date-picker form-control" name="not2[phone_date]" value="<?php echo $not2[0]['phone_date']; ?>"/>
            <label for="date-picker-8" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Client Consent Form</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-9" type="text" class="date-picker form-control" name="not3[print_date]" value="<?php echo $not3[0]['print_date']; ?>"/>
            <label for="date-picker-9" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-10" type="text" class="date-picker form-control" name="not3[email_date]" value="<?php echo $not3[0]['email_date']; ?>"/>
            <label for="date-picker-10" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-11" type="text" class="date-picker form-control" name="not3[fax_date]" value="<?php echo $not3[0]['fax_date']; ?>"/>
            <label for="date-picker-11" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-12" type="text" class="date-picker form-control" name="not3[phone_date]" value="<?php echo $not3[0]['phone_date']; ?>"/>
            <label for="date-picker-12" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Client Appointment Notification/Instructions</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-13" type="text" class="date-picker form-control" name="not4[print_date]" value="<?php echo $not4[0]['print_date']; ?>"/>
            <label for="date-picker-13" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-14" type="text" class="date-picker form-control" name="not4[email_date]" value="<?php echo $not4[0]['email_date']; ?>"/>
            <label for="date-picker-14" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-15" type="text" class="date-picker form-control" name="not4[fax_date]" value="<?php echo $not4[0]['fax_date']; ?>"/>
            <label for="date-picker-15" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-16" type="text" class="date-picker form-control" name="not4[phone_date]" value="<?php echo $not4[0]['phone_date']; ?>"/>
            <label for="date-picker-16" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Advisor Appointment Notification</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-17" type="text" class="date-picker form-control" name="not5[print_date]" value="<?php echo $not5[0]['print_date']; ?>"/>
            <label for="date-picker-17" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-18" type="text" class="date-picker form-control" name="not5[email_date]" value="<?php echo $not5[0]['email_date']; ?>"/>
            <label for="date-picker-18" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-19" type="text" class="date-picker form-control" name="not5[fax_date]" value="<?php echo $not5[0]['fax_date']; ?>"/>
            <label for="date-picker-19" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-20" type="text" class="date-picker form-control" name="not5[phone_date]" value="<?php echo $not5[0]['phone_date']; ?>"/>
            <label for="date-picker-20" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Advisor Completion Notification</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-21" type="text" class="date-picker form-control" name="not6[print_date]" value="<?php echo $not6[0]['print_date']; ?>"/>
            <label for="date-picker-21" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-22" type="text" class="date-picker form-control" name="not6[email_date]" value="<?php echo $not6[0]['email_date']; ?>"/>
            <label for="date-picker-22" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-23" type="text" class="date-picker form-control" name="not6[fax_date]" value="<?php echo $not6[0]['fax_date']; ?>"/>
            <label for="date-picker-23" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-24" type="text" class="date-picker form-control" name="not6[phone_date]" value="<?php echo $not6[0]['phone_date']; ?>"/>
            <label for="date-picker-24" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Referral Acknowledgment </strong></td>
        <td><div class="notify-date">
            <input id="date-picker-25" type="text" class="date-picker form-control" name="not7[print_date]" value="<?php echo $not7[0]['print_date']; ?>"/>
            <label for="date-picker-25" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-26" type="text" class="date-picker form-control" name="not7[email_date]" value="<?php echo $not7[0]['email_date']; ?>"/>
            <label for="date-picker-26" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-27" type="text" class="date-picker form-control" name="not7[fax_date]" value="<?php echo $not7[0]['fax_date']; ?>"/>
            <label for="date-picker-27" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-28" type="text" class="date-picker form-control" name="not7[phone_date]" value="<?php echo $not7[0]['phone_date']; ?>"/>
            <label for="date-picker-28" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
      <tr>
        <td><strong>Insurance Completion Notification</strong></td>
        <td><div class="notify-date">
            <input id="date-picker-29" type="text" class="date-picker form-control" name="not8[print_date]" value="<?php echo $not8[0]['print_date']; ?>"/>
            <label for="date-picker-29" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-30" type="text" class="date-picker form-control" name="not8[email_date]" value="<?php echo $not8[0]['email_date']; ?>"/>
            <label for="date-picker-30" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-31" type="text" class="date-picker form-control" name="not8[fax_date]" value="<?php echo $not8[0]['fax_date']; ?>"/>
            <label for="date-picker-31" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
        <td><div class="notify-date">
            <input id="date-picker-32" type="text" class="date-picker form-control" name="not8[phone_date]" value="<?php echo $not8[0]['phone_date']; ?>"/>
            <label for="date-picker-32" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div></td>
      </tr>
    </tbody>
  </table>

  <div class="rightaligned">
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientID; ?>/" class="btn btn-info">Cancel</a>
  </div>

  </form>
</div>

<!-- Last </div> will be in the footer -->
