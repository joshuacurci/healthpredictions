<?php 
$apparray=[];
foreach($clientApointment as $app)
{
	array_push($apparray,$app['appID']);
}
$ids=implode("-", $apparray);
?>
<h1 class="page-header col-xs-11">Notification to Insurance Company of Completion</h1>
<div style="clear:both"></div>


<div class="col-xs-8 top-buttons">
  <a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>

  <a href="<? echo base_url(); ?>index.php/client/noti_to_ins_company_completion/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/<? echo $ids; ?>/" class="btn btn-success" target="_blank"><span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span> View PDF</a>
    <a href="<? echo base_url(); ?>index.php/client/noti_to_ins_company_completion_dl/<? echo $clientData[0]['clientID']; ?>/<? echo $clientData[0]['insurance_company']; ?>/<? echo $ids; ?>/" class="btn btn-primary" target="_blank"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download PDF</a>
</div> 

<div class="col-md-4 top-buttons">
  <div class="name-header"><?php echo $clientData[0]['client_name']; ?></div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $clientData[0]['dateadded']);?></div>
</div>  
