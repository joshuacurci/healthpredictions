<div class="xs-col-12" text-align="center">
	<div style="text-align:center; line-height: .8;">
		<h2>Client Instructions</h2>
	</div>
</div>

<ul>
	<li>
		Phone 1800 003 224 for all enquiries.
	</li>
	<li>
		Where possible, appointments are scheduled for one visit but in some circumstances two visits may be required.
	</li>
	<li>
		The appointment takes between half an hour and one and a half hours, depending on the procedures required.
	</li>
	<li>
		If blood tests are ordered, the client should fast for 10 hours before the appointment time. Avoid strenuous exercise the day before the test and if possible avoid alcohol for a few days before the test.
	</li>
	<li>
		If blood tests are ordered they may include a HIV test.
	</li>
	<li>
		All results are forwarded to the referring Insurance Company or Adviser, usually within five working days, all follow up of results is by the Insurance Company.
	</li>
	<li>
		Take some photo identification to the appointment/s (drivers license or passport)
	</li>
	<li>
		All accounts are sent to the Insurance Company.


	</li>
</ul>