<p style="font-weight: bold; font-style: italic;">Point Details</p>
<div class="" text-align="center">
	<div style="text-align:left; line-height: 1.5;">
		<table>
			<tr>
				<!-- Advisor Info -->
				<td>
					<table>
						<tr>
							<td colspan="1">
								<strong>
									Name:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_name']; ?>
							</td>
						</tr>
                        <tr>
							<td colspan="1">
								<strong>
									Points:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_points']; ?>
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>
									Company:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_company']; ?>
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>
									Groups:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_groups']; ?>
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>
									Address:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_address']; ?>
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>
									City:
								</strong>
							</td>
							<td>
								<?php echo $advisorData[0]['advisor_city']; ?>
							</td>
						</tr>
						<tr>
							<td colspan="1">
								<strong>
									State:
								</strong>
							</td>
							<?
							$servername = $this->db->hostname;
							$username = $this->db->username;
							$password = $this->db->password;
							$dbname = $this->db->database;

            // Create connection
							$conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
							if ($conn->connect_error) {
								die("Connection failed: " . $conn->connect_error);
							}

							$sql = "SELECT * FROM tbl_au_states WHERE id = ".$advisorData[0]['advisor_state']."";
							$result = $conn->query($sql);

							$num_rec = $result->num_rows;

							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) { ?>
									<td colspan="2"><? echo $row['state_name']; ?></td>
									<? 
								}
							} else { ?>
								<td colspan="2">No Assigned State</td>
								<? } ?>
							</tr>
						</table>
					</td>
					<!-- advisor Info -->
					<td>
						<table>
							<tr>
								<td colspan="1">
									<strong>
										Postcode:
									</strong>
								</td>
								<td>
									<?php echo $advisorData[0]['advisor_postcode']; ?>
								</td>
							</tr>
							<tr>
								<td colspan="1">
									<strong>
										Country:
									</strong>
								</td>
								<?
								$servername = $this->db->hostname;
								$username = $this->db->username;
								$password = $this->db->password;
								$dbname = $this->db->database;

            // Create connection
								$conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
								if ($conn->connect_error) {
									die("Connection failed: " . $conn->connect_error);
								}

								$sql = "SELECT * FROM tbl_country WHERE id = ".$advisorData[0]['advisor_country']."";
								$result = $conn->query($sql);

								$num_rec = $result->num_rows;

								if ($result->num_rows > 0) {
									while($row = $result->fetch_assoc()) { ?>
										<td colspan="2"><? echo $row['country_name']; ?></td>
										<? 
									}
								} else { ?>
									<td colspan="2">No Assigned State</td>
									<? } ?>
								</tr>
								<tr>
									<td colspan="1">
										<strong>
											Phone:
										</strong>
									</td>
									<td>
										<?php echo $advisorData[0]['advisor_phone']; ?>
									</td>
								</tr>
								<tr>
									<td colspan="1">
										<strong>
											Work Phone:
										</strong>
									</td>
									<td>
										<?php echo $advisorData[0]['advisor_workphone']; ?>
									</td>
								</tr>
								<tr>
									<td colspan="1">
										<strong>
											Mobile:
										</strong>
									</td>
									<td>
										<?php echo $advisorData[0]['advisor_mobile']; ?>
									</td>
								</tr>
								<tr>
									<td colspan="1">
										<strong>
											Fax:
										</strong>
									</td>
									<td>
										<?php echo $advisorData[0]['advisor_fax']; ?>
									</td>
								</tr>
								<tr>
									<td colspan="1">
										<strong>
											Email:
										</strong>
									</td>
									<td>
										<?php echo $advisorData[0]['advisor_email']; ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

                <style>
	.time_date_table > td {
		border: 1px solid black;
		text-align: center;
		line-height: 2;
	}
</style>	
<div>
	<p style="font-weight: bold; font-style: italic;">Bonus Point Redemptions</p>
	<table width="600px" class="time_date_table">
		<tr>
			<td>
				Point
			</td>
			<td>
				Name
			</td>
			<td>
				Value
			</td>
		</tr>
		<tr>
			<td>
            70 Points
			</td>
			<td>
            Moive money
			</td>
			<td>
            value $70
			</td>
		</tr>
        <tr>
			<td>
            100 Points
			</td>
			<td>
            Gift Vocher
			</td>
			<td>
            value $100
			</td>
		</tr>
        <tr>
			<td>
            150 Points
			</td>
			<td>
            Gift Vocher
			</td>
			<td>
            value $150
			</td>
		</tr>
        <tr>
			<td>
            200 Points
			</td>
			<td>
            Gift Vocher
			</td>
			<td>
            value $200
			</td>
		</tr>
        <tr>
			<td>
            500 Points
			</td>
			<td>
            Gift Vocher
			</td>
			<td>
            value $500
			</td>
		</tr>
	</table>
</div>
<p>** Available gift vouchers - Village/Hoyts, Coles/Myer, Bunning & Visa gift cards</p>
<br>
<p>If, for any reason you do not wish to claim your points, phone your case file manager and the loyalty points may be offered as a personal development or educational grant to you, or as a cash donation to the Salvation Army in your name.</p>
                <br>
			</div>
		</div>



		
