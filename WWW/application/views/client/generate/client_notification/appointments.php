
<style>
	.time_date_table > td {
		border: 1px solid black;
		text-align: center;
		line-height: 2;
		padding: 10px !important;
	}
</style>	
<div class="col-xs-12">
	<p style="font-weight: bold; font-style: italic;">Appointments</p>
	<table width="100%" height="100px" class="time_date_table">
		<tr>
			<td style="border: 1px solid #000">
				<strong>
					Type
				</strong>
			</td>
			<td style="border: 1px solid #000">
				<strong>
					Name
				</strong>
			</td>
			<td style="border: 1px solid #000">
				<strong>
					Time
				</strong>
			</td>
			<td style="border: 1px solid #000">
				<strong>
					Date
				</strong>
			</td>
			<td style="border: 1px solid #000">
				<strong>
					Address
				</strong>
			</td>
			<td style="border: 1px solid #000">
				<strong>
					Service
				</strong>
			</td>
		</tr>
		<? if (isset($clientApointment)){
			foreach ($clientApointment as $clientApointment) { ?>
			<tr>
				<td style="border: 1px solid #000">
					<? if ($clientApointment['app_type'] == '0' || $clientApointment['app_type'] == 'D') { echo "Doctor"; } 
					else if ($clientApointment['app_type'] == '1' || $clientApointment['app_type'] == 'P') { echo "Pathologist"; } 
					else if ($clientApointment['app_type'] == '2' || $clientApointment['app_type'] == 'S') { echo "Specialist"; } 
					else if ($clientApointment['app_type'] == '3' || $clientApointment['app_type'] == 'N') { echo "Nurse"; } ?>
				</td>
				<td style="border: 1px solid #000">
					<? echo $clientApointment['app_name']; ?>
				</td>
				<td style="border: 1px solid #000">
					<? echo $clientApointment['app_time']; ?>
				</td>
				<td style="border: 1px solid #000">
					<? echo date('d/m/Y',$clientApointment['app_date']); ?>
				</td>
				<td style="border: 1px solid #000">
					<? echo $clientApointment['app_place']; ?>
				</td>
				<td style="border: 1px solid #000">
					<? echo $clientApointment['app_service']; ?>
				</td>
			</tr>

			<? } } else { ?>
				<tr>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
			</tr>
			<tr>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
			</tr>
			<? } ?>
			<tr>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
				<td style="border: 1px solid #000">
				</td>
			</tr>
		</table>
		<br/>
	</div>

