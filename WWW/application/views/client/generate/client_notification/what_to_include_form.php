<?php 
$apparray=[];
foreach($clientApointment as $app)
{
	array_push($apparray,$app['appID']);
}
$ids=implode(",", $apparray);
?>
<br/>
<h1 class="page-header col-xs-11">Select What to include in the report</h1>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/client_notification_ins_save/">
		<input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
		<input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID'] ?>"/>
		<input type="hidden" name="appID" value="<?php echo $ids ?>"/>
		<input type="hidden" name="insurance_company" value="<? echo $clientData[0]['insurance_company'] ?>"/>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">HIV Information :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['HIV'] == 'Y') { echo 'checked'; }?> type="radio" name="HIV" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['HIV'] == 'N') { echo 'checked'; }?> type="radio" name="HIV" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Instructions After Taking Blood Tests :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['instruct_taking_blood'] == 'Y') { echo 'checked'; }?> type="radio" name="instruct_taking_blood" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['instruct_taking_blood'] == 'N') { echo 'checked'; }?> type="radio" name="instruct_taking_blood" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Client Consent Identification :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['client_consent_id'] == 'Y') { echo 'checked'; }?> type="radio" name="client_consent_id" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['client_consent_id'] == 'N') { echo 'checked'; }?> type="radio" name="client_consent_id" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Client Instructions :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['client_instructions'] == 'Y') { echo 'checked'; }?> type="radio" name="client_instructions" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['client_instructions'] == 'N') { echo 'checked'; }?> type="radio" name="client_instructions" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>


		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Pathology tests Explained :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['path_tests_explained'] == 'Y') { echo 'checked'; }?> type="radio" name="path_tests_explained" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['path_tests_explained'] == 'N') { echo 'checked'; }?> type="radio" name="path_tests_explained" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Client Authority to Release Test Results :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['client_authority_results'] == 'Y') { echo 'checked'; }?> type="radio" name="client_authority_results" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['client_authority_results'] == 'N') { echo 'checked'; }?> type="radio" name="client_authority_results" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="submit" name="submit" target="_blank" class="btn btn-info">Submit Only</button>
				<button type="submit" name="submit_view" target="_blank" class="btn btn-success">Submit And View PDF</button>
				<button type="submit" name="submit_download" target="_blank" class="btn btn-primary">Submit And Download PDF</button>
				<a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
			</div>
		</div>

	</form>

</div>      

<!-- Last </div> will be in the footer -->