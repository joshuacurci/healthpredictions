<br/>
<h1 class="page-header col-xs-11">Select What to include in the report</h1>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
	<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/client_referral_noti_save/">
		<input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>
		<input type="hidden" name="clientID" value="<? echo $clientData[0]['clientID'] ?>"/>
		<input type="hidden" name="appID" value="<? echo $clientApointment[0]['appID'] ?>"/>
		<input type="hidden" name="insurance_company" value="<? echo $clientData[0]['insurance_company'] ?>"/>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">HIV Information :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['HIV'] == 'Y') { echo 'checked'; }?> type="radio" name="HIV" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['HIV'] == 'N') { echo 'checked'; }?> type="radio" name="HIV" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<label for="inputrecNum1" class="col-sm-3 control-label">Instructions After Taking Blood Tests :</label>
			<div class="col-sm-9">
				<input <?php if($clientNoti_reportData[0]['instruct_taking_blood'] == 'Y') { echo 'checked'; }?> type="radio" name="instruct_taking_blood" value="Y">&nbsp Yes &nbsp&nbsp
				<input <?php if($clientNoti_reportData[0]['instruct_taking_blood'] == 'N') { echo 'checked'; }?> type="radio" name="instruct_taking_blood" value="N">&nbsp No
			</div>
			<div style="clear:both"></div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<button type="submit" name="submit_view" target="_blank" class="btn btn-success">View PDF</button>
				<button type="submit" name="submit_download" target="_blank" class="btn btn-primary">Download PDF</button>
				<button type="submit" name="submit_email" target="_blank" class="btn btn-primary">Email PDF</button>
				<a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
			</div>
		</div>

	</form>

</div>      

<!-- Last </div> will be in the footer -->