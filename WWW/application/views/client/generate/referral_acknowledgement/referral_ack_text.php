<p>
This document acknowledges receipt of the above referral. Our insurance medial case file managers will endeavour to fulfill your requested time and place. 
If this cannot be achieved we may with the client's consent modify the appointment time and place. 
We now take full responsibility for expeditiously arranging the services, where possible on a one-stop basis.
</p>