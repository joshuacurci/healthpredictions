<div class="xs-col-12" text-align="center">
	<div style="text-align:center; line-height: 1.2; text-transform: uppercase;">
		<h2>Bombora Medical<br/>Life Insurance Medical Service</h2>
	</div>
</div>
<p style="line-height: .5;">
	1 Wallen Road
</p>
<p style="line-height: .5;">
	Hawthorn VIC 3122
</p>
<p style="line-height: .5;">
	Ph: (03) 9819 9700
</p>
<p style="line-height: .5;">
	Fax: (03) 9819 4699
	<br/>
<br/>
<br/>
</p>

<div>

<p>

	Dear Advisor,
</p>
<p>
	Bombora Medical operates its services through a sophisticated computer-based system which, if you require, allows referral access through <a href="http://www.bomboramedical.com.au">http://www.bomboramedical.com.au</a>. 
</p>
<p>
	The following facilities can be added to suit your needs: 
</p>
<ul style="list-style-image: url('<? echo base_url(); ?>/images/arrow.gif');">
	<li>	 	
		Direct access from your computer to our database on all clients relevant to your services. This service will save your administration a substantial amount of time since it will allow you, with one click, to pick up the clients that you have referred to Bombora Medical with their status.
	</li>
	<li>	 	
		Hyperlinks from your website to our referral site. This service is of particular value with groups and will allow a direct, one click, hyperlink from your group website to either Bombora Medical website or our referral page.
	</li>	 	
	<li>
		A Bombora Medical icon can be placed on your windows desktop screen, allowing one-click access to our website or referral page.
	</li>
</ul>
<p>
	If you require any of these, contact us or write your name and phone number on the bottom of this page and fax it back.
<br/>
<br/>
</p>
</div>
<p>
	Yours faithfully 
</p>
<p style="text-decoration: underline; line-height: 2;">
	MAURICE ROSENBAUM 
</p>
<p>
	MEDICAL DIRECTOR - Bombora Medical Pty Ltd.
</p>