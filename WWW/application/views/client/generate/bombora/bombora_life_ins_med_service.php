<div class="xs-col-12" text-align="center">
	<div style="text-align:center; line-height: .8;">
		<h2>Bombora Medical Life Insurance Medical Service</h2>
	</div>
</div>
<p>
	<strong>Bombora Medical Life Insurance Services</strong> offers the following -
</p>
<ul>
	<li>
		<strong>Pathology Services:</strong>
		<br/>
		Bombora Medical offers a full range of pathology services throughout Australia in urban and rural areas.
	</li>
	<li>
		<strong>Medical services:</strong>
		<br/>
		Bombora Medical offers a full range of medical services including specialist medical assessment, general practitioner medical assessment, paramedical assessment as well as special tests, particularly electrocardiograms, stress tests, echocardiograms, blood pressure monitoring, lung function tests and Holter monitoring. Pathology services are coordinated with medical services as required.
	</li>
	<li>
		<strong>Geographical spread:</strong>
		<br/>
		The service operates in all capital cities in Australia and most rural centres including most small population towns. A full range of services is assured in all large towns centres, but in some smaller towns some services cannot be provided locally.
	</li>
	<li>
		<strong>Home or workplace visits: </strong>
		<br/>
		Bombora Medical offers home or workplace pathology visits as required in most of the geographical area of our service. Home or workplace paramedical and general practitioner visits can usually be arranged, but specialist home or workplace visits can only be arranged under special circumstances.
	</li>
	<li>
		<strong>Accreditation: </strong>
		<br/>
		Bombora Medical Services are accepted by all Life Insurance Companies.
	</li>
	<li>
		<strong>Control of bookings:</strong>
		<br/>
		Referral to the service is by telephone, fax, e-mail or throughour website and our office fully controls all bookings and follows up to ensure that appointments are kept. All data is collated, reviewed and passed on as a single package to the referring insurance company without delay.
	</li>
	<li>
		<strong>Medical supervision:</strong>
		<br/>
		Because the Bombora Medical service has been added to a pre-existing, multi-state cardiac diagnostic service, all services are medically supervised.

	</li>
	<li>
		<strong>Reflex testing:</strong>
		<br/>
		If required, reflex testing can be arranged.
	</li>
</ul>
<p>
	Further ancillary services:
</p>
<ul>
	<li>
		<strong>Medical practitioner report follow-up:</strong>
		<br/>
		Bombora Medical will, if required, follow-up and obtain treating medical practitioner reports.
	</li>
	<li>
		<strong>Medical fitness assessment: </strong>
		<br/>
		In capital cities and some rural centres we are able to offer retirement and fitness assessment.
	</li>
	<li>
		<strong>Medical discussion and oral review of specific cases: </strong>
		<br/>
		This service is available by telephone at any time.
	</li>
	<li>
		<strong>Electronic bookings & foreign language client instructions:</strong>
		<br/>
		These services are available at <a href="http://www.bomboramedical.com.au">http://www.bomboramedical.com.au</a> and if you require, we would be glad to tailor our IT service to your requirements.

	</li>
</ul>
<h3>	
	Bombora Medical offers home or workplace visits for pathology, paramedical and medical examination in all urban and many rural areas throughout Australia.
</h3>

