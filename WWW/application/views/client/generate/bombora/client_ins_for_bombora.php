<div style="text-align:center">
	<h2>
		CLIENT INSTRUCTIONS FOR BOMBORA MEDICAL SERVICES
	</h2>
</div>
<p>
	<strong>
		INTRODUCTION
	</strong>
</p>
<p>
	Your insurance company requires you to have various tests prior to accepting your insurance cover, and these tests are to be performed by Bombora Medical. This testing is routine throughout the insurance industry, but the level of testing depends on various factors, including your age, value and type of the policy, etc.
</p>
<p>
	<strong>
		APPOINTMENT TIMES 
	</strong>
</p>
<p>
	Bombora Medical appointments are almost always on time. If a medical examination is required you should arrive 15 minutes before the appointment in order to fill out the appropriate documents.
</p>
<p>
	<strong>
		IDENTIFICATION
	</strong>
</p>
<p>
	Always take photo identification (a driver's license is best) to the examination.
</p>
<p>
	<strong>
		INSTRUCTIONS PRIOR TO TAKING BLOOD 
	</strong>
</p>
<p>
	Most insurance applications require that a sample of blood be taken from you for various tests, most commonly for cholesterol, sugar, electrolyte, liver function and immunological tests for Hepatitis and HIV. The blood will be taken by a qualified individual and will involve a needle prick followed by the application of a small dressing. Pressure will be placed on the puncture site immediately after the blood is taken and you will be asked to limit your arm movement for the remainder of the day to prevent bruising. Before the blood is taken you should note the following:
</p>
<p>
	<ul>
		<li>
			<strong>Fasting: </strong>You should fast for at least eight hours prior to the service. During this time you may take your regular medication and drink as much water or weak tea (no sugar or milk) as you require.
		</li>
		<li>
			<strong>Alcohol: </strong> If liver function tests are being performed (this is the case in most individuals) it is best to abstain from drinking alcohol for a few days prior to the test.
		</li>
		<li>
			<strong>Sex: </strong>If prostate specific antibody (PSA) is ordered (this request is unusual), you should abstain from sexual intercourse for two days.
		</li>
	</ul>
</p>
	<strong>
		QUESTIONNAIRE & PHYSICAL EXAMINATION 
	</strong>
<p>
	Your assessment may include a questionnaire and physical examination by a nurse or doctor and may include various supplementary tests, such as an electrocardiogram and an exercise stress test. The examinations will be done by qualified individuals and the service will take between thirty minutes and one hour, usually in the home or workplace. Female clients should not be examined if alone in the premises.
</p>
<p>
	<strong>
		CONFIDENTIALITY AND RELEASE OF INFORMATION 
	</strong>
</p>
<p>
	The examination and tests are ordered and paid for by the insurance company and all documentation is their property but still subject to your legal privacy and disclosure entitlement over that information.
	You will be asked to sign a consent document allowing release of the information to your medical practitioner and/or insurance advisor. This is subject to your approval and you may refuse this request, but if the documentation is signed, the information may be released.
</p>
<p>
	<strong>
		SUPPLEMENTARY DOCUMENTATION 
	</strong>
</p>
<p>
	We have enclosed for your information the following documents:
	<ol>
		<li>
			A copy of the Bombora Medical information consent and identification form which summarises your rights and requirement of disclosure, privacy information about HIV testing, consent to disclosure (if you approve) and instructions following blood taking, you will be required to sign all or part of this document and provide photo identification at the time of examination.

		</li>
		<li>
			Pathology tests explained.
		</li>
	</ol>
</p>
<p>
	<strong>
		FURTHER INFORMATION 
	</strong>
</p>
<p>
	If you wish to discuss any of these matters further, telephone Bombora Medical central booking office on (03) 9819 9700
</p>