
<style>
	.time_date_table > td {
		border: 1px solid black;
		text-align: center;
		line-height: 2;
	}
</style>
<div class="" style="text-align:left; ">
	<p>Visit time requested:</p>
	<table width="400px" class="time_date_table">
		<tr>
			<td>
				Day
			</td>
			<td>
				Time
			</td>
		</tr>
		<tr>
			<td>

			</td>
			<td>

			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
			</td>
		</tr>
	</table>
</div>
<table width="100%">
	<tr>
		<td>
			<p style="font-weight: bold; font-style: italic;">Services Specified</p>
			<ul>
				<? $indTicked =[];
        for($i=0; $i<count($testTicked); $i++)
        {
          array_push($indTicked, $testTicked[$i]['testtypeID']);
        } ?>
				<? foreach($testTypes as $finalTest) { ?>
					<? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
						<li>
							<? echo $finalTest['type_name']; ?>
						</li>
						<? } 
					} ?>
				</ul>
			</td>
			<td>
			</td>
		</tr>
	</table>
	<p>
		Please call mobile to confirm place for tests. Please email a copy of the results to our office.  
	</p>
	<p>
This document acknowledges receipt of the above referral. Our insurance medial case file managers will endeavour to fulfill your requested time and place. 
If this cannot be achieved we may with the client's consent modify the appointment time and place. 
We now take full responsibility for expeditiously arranging the services, where possible on a one-stop basis.
	</p>