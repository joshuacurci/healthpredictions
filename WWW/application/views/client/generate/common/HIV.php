<p>
	<strong>
		HIV (Aids)
	</strong>
</p>
<p>
	AIDS is caused by the human immuno-deficiency virus (HIV). It causes disease mainly by damage to the body's immune system and this in turn, frequently leads to infections, organ damage and cancer. There is no cure for this condition and it is ultimately fatal except in some rare cases in which progressive disease does not occur, treatment however, is capable of prolonging and improving the quality of life. 
</p>
<p>
	The passage of blood or other fluids from one person to another can pass on HIV (as well as Hepatitis B & C infection). Infected persons may pass the infection to others through sexual activity (especially unprotected sexual intercourse) or through shared syringes or needles, donated blood serum or other blood components, donated organs or an infected mother may pass HIV to her baby. No other modes of transmission have yet been identified. 
</p>
<p>
	Following infection with HIV, there may be a glandular fever or flu-like illness, usually occurring after several weeks and about this time the body produces antibodies which can be detected by the proposed blood tests. Following this, there may be no further symptoms of infection for anything up to 12 years, but ultimately the serious manifestations almost always develop. There is a period of up to three months following HIV infection during which time the infection cannot be detected by blood tests but can still be passed on to others. 
</p>
<p>
	AIDS and HIV are notifiable to government health authorities. The infection may exclude some people from their occupation. It is an offence to knowingly transmit infection to others or to put others at risk of infection. There is a reasonable possibility of ill-informed discrimination against infected persons. Your proposal for insurance is unlikely to be accepted if the test is positive. 
</p>
<p>
	Acceptance of your insurance proposal will indicated that the HIV test was negative and you will be notified in person in the event that the HIV test is positive. If you wish to have further information you should see your doctor or an expert in sexually transmitted disease.
</p>
<br/>