<div class="xs-col-12" text-align="center">
	<div style="text-align:center; line-height: .8;">
		<h2>Medical Appointment Request / Confirmation</h2>
	</div>
</div>
<br/>
<div>
	<div style="text-align: center">
		<h1>Client Information</h1>
	</div>
	<p>
		<strong>
			Client Duty of Disclosure 
		</strong>
	</p>
	<p>Before you enter into a contract with an insurer, you have a duty to disclose to the insurance company every matter that you know or could reasonably be expected to know, that is relevant to the insurers decision whether to accept the risk of the insurance, and if so, to decide on the terms of acceptance. This duty to disclose also applies before you extend, vary or reinstate a contract of life insurance. In addition the duty of disclosure continues until a policy has been issued. </p>
	<p>
		The duty of disclosure is, however, waived in relation to disclosure of the following matters; those that diminish the risk undertaken by the insurer, those that are common knowledge, those that the insurer knows, or in the ordinary course of business ought to know and any disclose which is waived by the insurer.
	</p>
	<p>
		If you fail to comply with the duty of disclosure and the insurer would not have entered into the contract on any terms if the failure had not occurred, the insurer may avoid the contract within three years of entering into it. If the non-disclosure is fraudulent the insurer may avoid the contract at any time. An insurer who is entitled to avoid a contract of life insurance may, within three years of entering into the contract, elect not to avoid it, but to reduce the sum insured for in accordance with a formula that takes into account the premium that would have been payable if the disclosure of all relevant information to the insurer had been complete.
	</p>
	<p>
		The insurance proposed for, under this application, is considered to be life insurance policies for the purpose of the Insurance Contract Act 1984. 
	</p>
	<p>
		<strong>
			Privacy 
		</strong>
	</p>
	<p>
		Bombora Medical understands the importance, and the legal requirement of safeguarding the privacy of clients in relation to all information collected from the client, most particularly information about health. It is further recognised that the information is sensitive. 
	</p>
	<p>
		The information that Bombora Medical collects will be used by the life insurance company for the purpose of enabling the life insurance company to decide whether to offer insurance cover or change the terms of a policy, and the insurance company also has obligations to openly deal with the client and forward sensitive and important information in accordance with the law. 
	</p>
	<p>
		Information collected by the performance of blood tests and/or by examination by doctors or nurses and may include material relevant to the family. 
	</p>
	<p>
		Should you wish to view documents you may do so at any time by contacting the requesting insurance company and as a matter of policy, Bombora Medical Life Insurance Services may or may not send blood test results to the doctor nominated on this form. 
	</p>
	<p>
		In some situations it is necessary to perform blood tests in addition to those initially requested by the insurance company. This is known as reflex testing and may require further blood taking or may be achieved by testing on existing blood samples that have already been collected. 
	</p>
	<p>
		<strong>
			HIV (Aids)
		</strong>
	</p>
	<p>
		AIDS is caused by the human immuno-deficiency virus (HIV). It causes disease mainly by damage to the body's immune system and this in turn, frequently leads to infections, organ damage and cancer. There is no cure for this condition and it is ultimately fatal except in some rare cases in which progressive disease does not occur, treatment however, is capable of prolonging and improving the quality of life. 
	</p>
	<p>
		The passage of blood or other fluids from one person to another can pass on HIV (as well as Hepatitis B & C infection). Infected persons may pass the infection to others through sexual activity (especially unprotected sexual intercourse) or through shared syringes or needles, donated blood serum or other blood components, donated organs or an infected mother may pass HIV to her baby. No other modes of transmission have yet been identified. 
	</p>
	<p>
		Following infection with HIV, there may be a glandular fever or flu-like illness, usually occurring after several weeks and about this time the body produces antibodies which can be detected by the proposed blood tests. Following this, there may be no further symptoms of infection for anything up to 12 years, but ultimately the serious manifestations almost always develop. There is a period of up to three months following HIV infection during which time the infection cannot be detected by blood tests but can still be passed on to others. 
	</p>
	<p>
		AIDS and HIV are notifiable to government health authorities. The infection may exclude some people from their occupation. It is an offence to knowingly transmit infection to others or to put others at risk of infection. There is a reasonable possibility of ill-informed discrimination against infected persons. Your proposal for insurance is unlikely to be accepted if the test is positive. 
	</p>
	<p>
		Acceptance of your insurance proposal will indicated that the HIV test was negative and you will be notified in person in the event that the HIV test is positive. If you wish to have further information you should see your doctor or an expert in sexually transmitted disease.
	</p>
</div>
<div>
	<h3>
		Instructions after taking blood 
	</h3>
	<p>
		Because taking blood has required puncture of a vein, secondary bleeding may occur. 
	</p>
	<p>
		For the rest of the day you should: 
	</p>
	<ul>
		<li>
			Not lift or carry anything heavy,
		</li>
		<li>
			Not wear tight or restricting clothing above the puncture site,
		</li>
		<li>
			Avoid strenuous activity
		</li>
	</ul>
	<p>
		A small amount of bruising is normal and sometimes unavoidable. Bruising should be minimised by the above instructions and if the bruising appears excessive, seek medical assistance. 
	</p>
</div>
