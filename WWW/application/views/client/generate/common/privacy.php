<p>
	<strong>
		Privacy 
	</strong>
</p>
<p>
	Bombora Medical understands the importance, and the legal requirement of safeguarding the privacy of clients in relation to all information collected from the client, most particularly information about health. It is further recognised that the information is sensitive. 
</p>
<p>
	The information that Bombora Medical collects will be used by the life insurance company for the purpose of enabling the life insurance company to decide whether to offer insurance cover or change the terms of a policy, and the insurance company also has obligations to openly deal with the client and forward sensitive and important information in accordance with the law. 
</p>
<p>
	Information collected by the performance of blood tests and/or by examination by doctors or nurses and may include material relevant to the family. 
</p>
<p>
	Should you wish to view documents you may do so at any time by contacting the requesting insurance company and as a matter of policy, Bombora Medical Life Insurance Services may or may not send blood test results to the doctor nominated on this form. 
</p>
<p>
	In some situations it is necessary to perform blood tests in addition to those initially requested by the insurance company. This is known as reflex testing and may require further blood taking or may be achieved by testing on existing blood samples that have already been collected. 
</p>
<br/>