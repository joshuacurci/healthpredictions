<div>
	<div style="text-align: center">
		<h2>
			PATHOLOGY TESTS EXPLAINED
		</h2>
	</div>
	<p>
		<strong>
			MBA 20 - The MBA20 tests contains the following -
		</strong>
	</p>
	<ul style="list-style-type: none; line-height: 1.5;">
		<li>
			<strong>Sodium.</strong> The sodium level may indicate dehydration, high blood pressure, or occasionally other conditions. 
		</li>
		<li>
			<strong>Potassium.</strong> The potassium level may indicate high blood pressure or kidney disease. 
		</li>
		<li>
			<strong>Chloride and Bicarbonate.</strong> This minor test is to determine whether acid/alkali (pH) balance is normal.
		</li>
		<li>
			<strong>Urea, Creatinine & eGFR.</strong> These tests may indicate kidney disease. 
		</li>
		<li>
			<strong>Bilirubin (Total), Total protein & Albumin, Alkaline Phosphatase, AST, Gamma GT, ALT.</strong> These tests may indicate liver and bone disease.
		</li>
		<li>
			<strong>Calcium, corrected calcium & Phosphate.</strong> These tests may indicate kidney and bone disease. 
		</li>
		<li>
			<strong>Uric acid. </strong> This test may indicate gout.
		</li>
	</ul>
	<p>
		<strong>
			LIPID PROFILE 
		</strong>
	</p>
		<p>
			The lipid profile is a group of tests to determine risk of coronary heart disease, cerebral artery disease and vascular disease, the tests include total cholesterol, HDL-cholesterol, LDL-cholesterol, and triglycerides. Sometimes the report includes additional calculated values such as the cholesterol/HDL ratio or a risk score based on lipid profile results.
			HDL Cholesterol. Also known as HDL, HDL-c or 'good' cholesterol (High-density lipoprotein cholesterol). LDL Cholesterol. Also know as LDL, LDL-c, 'bad' cholesterol, (Low-density lipoprotein cholesterol).
		</p>
		<p>
			<strong>
				GLUCOSE STUDIES (Blood sugar and glucose tolerance test). 
			</strong>
		</p>
		<p>
			These are performed on blood and urine to screen for diabetes and pre-diabetes.
		</p>
		<p>
			<strong>
				FBC (FULL BLOOD COUNT) 
			</strong>
		</p>
		<p>
			The full blood count (FBC), is sometimes referred to as a full blood examination (FBE) or complete blood count (CBC) and assesses the status of a number of different features of the blood, including the following -
		</p>
		<ul style="list-style-type: none; line-height: 1.5;">
			<li>
				<strong>Haemoglobin (Hb).</strong>Haemoglobin is the iron-containing compound of the red blood cells, and is responsible for transporting oxygen around the body. Measuring the concentration of haemoglobin in the blood screens for anaemia. 
			</li>
			<li>
				<strong>Red cell count (RCC), Packed cell volume (PCV) haematocrit (Hct), mean cell volume, mean corpuscular volume (MCV), mean cell haemoglobin (MCH) and mean cell haemoglobin concentration (MCHC)</strong> are all an estimation of the number and characteristics of the red blood cells. 
			</li>
			<li>
				<strong>White cell (leucocyte) count, Leucocyte (white cell) differential count. </strong> This estimates the number and types of white blood cells per unit of blood and provides a screening test for inflammation blood disorders and allergies. 
				Platelet count. Is an estimation of the number of platelets per litre of blood and is a screening test for the tendency to bleed.
			</li>
		</ul>
		<p>
			<strong>URINE TESTING. </strong>Chemical urine testing and microscopic testing (MSU or urine M & C). This is to screen for metabolic and kidney disorders and for urinary tract infections.
		</p>
	</div>
