<style>
	.time_date_table > td {
		border: 1px solid black;
		text-align: center;
		line-height: 2;
	}
</style>
<div class="" style="text-align:left; ">
	<table width="400px">
		<tr>
			<td width="150px">
				<strong>
					Appointment Time:
				</strong>
			</td>
			<td>
				Appointment time placeholder
			</td>
		</tr>
		<tr>
			<td width="150px">
				<strong>
					Appointment Date:
				</strong>
			</td>
			<td>
				Appointment date placeholder
			</td>
		</tr>
		<tr>
			<td width="150px">
				<strong>
					Appointment Place:
				</strong>
			</td>
			<td>
				Appointment place placeholder
			</td>
		</tr>
	</table>
	<p>or</p>
	<table width="400px" class="time_date_table">
		<tr>
			<td>
				Preference
			</td>
			<td>
				Day
			</td>
			<td>
				Time
			</td>
		</tr>
		<tr>
			<td>
				1
			</td>
			<td>

			</td>
			<td>

			</td>
		</tr>
		<tr>
			<td>
				2
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td>
				3
			</td>
			<td>
			</td>
			<td>
			</td>
		</tr>
	</table>
</div>
<h4>
	PATHOLOGY SERVICE TO MAKE APPOINTMENT IF NOT MADE BY BOMBORA MEDICAL
</h4>
<table width="100%">
	<tr>
		<td>
			<p style="font-weight: bold; font-style: italic;">Services Specified</p>
			<ul>
				<? $indTicked =[];
        for($i=0; $i<count($testTicked); $i++)
        {
          array_push($indTicked, $testTicked[$i]['testtypeID']);
        } ?>
				<? foreach($testTypes as $finalTest) { ?>
					<? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
						<li>
							<? echo $finalTest['type_name']; ?>
						</li>
						<? } ?> 
						<? } ?>
					</ul>
				</td>
				<td>
				</td>
			</tr>
		</table>
		<p>
			Please call mobile to confirm place for tests. Please email a copy of the results to our office.  
		</p>