<div style="text-align:center">
	<h2>
		CLIENT AUTHORITY TO RELEASE TEST RESULTS
	</h2>
</div>
<div>
</div><style>
.form-field {
	border: 1px solid #000000;
	width: 100%;
	height: 50px;
}
.form-field-label {
	width: 200px;
}
.form-field-tr {
	padding: 2px;
}
input {
	border: 1px solid #000000;
	padding: 5px 10px;
}
</style>
<p>
	<strong>
		If you require a copy of your examination and test results to be forwarded to your Financial Advisor and/or General Practitioner please tick one or both of the options below and fill in the information required. 
	</strong>
</p>
<p>
	I, the undersigned, authorise a copy of all my examinations and test results to be released by Health Predictions to my Financial Advisor named below.
</p>
<p>
	<ul>
		<li>
			<p  style="text-decoration: underline; line-height: 1; font-size: 10px;">
				NOTE: Should you not wish your Financial Advisor to receive a copy of your results, please indicate this and initial where applicable.
			</p>
			<p>
				<table class="form-field-tr" style="line-height: 2;">
					<tr>
						<td class="form-field-label">
							Financial advisor:
						</td>
						<td>
							<!-- <input size="60" type="text" value="<?php echo $clientData[0]['client_name']; ?>" class="form-field" name="client_name"/> -->
							PLACEHOLDER
						</td>
					</tr>
					<tr>
						<td class="form-field-label">				
							Client's Name:
						</td>
						<td>
							<?php echo $clientData[0]['client_name']; ?>
						</td>
					</tr>
					<tr>
						<td class="form-field-label">
							Client's Signature:
						</td>
						<td>
							<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_organization']; ?>" name="client_fulladdress"/>
						</td>
					</tr>
					<tr>
						<td class="form-field-label">
							Personal ID (Drivers licence):
						</td>
						<td>
							<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
						</td>
					</tr>
					<tr>
						<td class="form-field-label">
							Dated:
						</td>
						<td>
							<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
						</td>
					</tr>
				</table>
			</li>
			<li>
				<p>
					I, the undersigned, request that a copy of all my examinations and test results to be released by Bombora Medical to my General Practitioner named below.
				</p>
				<p>
					<table class="form-field-tr" style="line-height: 2;">
						<tr>
							<td class="form-field-label">
								General Practitioner:
							</td>
							<td>
								<input size="60" type="text" value="<?php echo $clientData[0]['client_name']; ?>" class="form-field" name="client_name"/>
							</td>
						</tr>
						<tr>
							<td class="form-field-label">
								Address:
							</td>
							<td>
								<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_fulladdress']; ?>" name="client_fulladdress"/>
							</td>
						</tr>
						<tr>
							<td class="form-field-label">
								Client's Name:
							</td>
							<td>
								<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_organization']; ?>" name="client_fulladdress"/>
							</td>
						</tr>
						<tr>
							<td class="form-field-label">
								Client's Signature:
							</td>
							<td>
								<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
							</td>
						</tr>
						<tr>
							<td class="form-field-label">
								Personal ID: (Drivers licence):
							</td>
							<td>
								<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
							</td>
						</tr>
						<tr>
							<td class="form-field-label">	
								Dated:
							</td>
							<td>
								<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
							</td>
						</tr>
					</table>
				</li>
			</ul>
		</p>
		<div>
			<p>
				In the case of a positive HIV, Hep B or C test, I wish to be advised by (tick): 
				<select>
				<option value="ins_company">The insurance company medical officer</option>
					<option value="saab">Saab</option>
					<option value="mercedes">Mercedes</option>
					<option value="audi">Audi</option>
				</select>
			</p>

		</div>

