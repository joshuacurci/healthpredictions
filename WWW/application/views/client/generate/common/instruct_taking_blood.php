<h3>
	Instructions after taking blood 
</h3>
<p>
	Because taking blood has required puncture of a vein, secondary bleeding may occur. 
</p>
<p>
	For the rest of the day you should: 
</p>
<ul>
	<li>
		Not lift or carry anything heavy,
	</li>
	<li>
		Not wear tight or restricting clothing above the puncture site,
	</li>
	<li>
		Avoid strenuous activity
	</li>
</ul>
<p>
	A small amount of bruising is normal and sometimes unavoidable. Bruising should be minimised by the above instructions and if the bruising appears excessive, seek medical assistance. 
</p>