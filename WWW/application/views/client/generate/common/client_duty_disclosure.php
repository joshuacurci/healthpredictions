
<div style="text-align: center">
	<h1>Client Information</h1>
</div>
<p>
	<strong>
		Client Duty of Disclosure 
	</strong>
</p>
<p>Before you enter into a contract with an insurer, you have a duty to disclose to the insurance company every matter that you know or could reasonably be expected to know, that is relevant to the insurers decision whether to accept the risk of the insurance, and if so, to decide on the terms of acceptance. This duty to disclose also applies before you extend, vary or reinstate a contract of life insurance. In addition the duty of disclosure continues until a policy has been issued. </p>
<p>
	The duty of disclosure is, however, waived in relation to disclosure of the following matters; those that diminish the risk undertaken by the insurer, those that are common knowledge, those that the insurer knows, or in the ordinary course of business ought to know and any disclose which is waived by the insurer.
</p>
<p>
	If you fail to comply with the duty of disclosure and the insurer would not have entered into the contract on any terms if the failure had not occurred, the insurer may avoid the contract within three years of entering into it. If the non-disclosure is fraudulent the insurer may avoid the contract at any time. An insurer who is entitled to avoid a contract of life insurance may, within three years of entering into the contract, elect not to avoid it, but to reduce the sum insured for in accordance with a formula that takes into account the premium that would have been payable if the disclosure of all relevant information to the insurer had been complete.
</p>
<p>
	The insurance proposed for, under this application, is considered to be life insurance policies for the purpose of the Insurance Contract Act 1984. 
</p>
<br/>