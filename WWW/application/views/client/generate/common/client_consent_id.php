<style>
	.form-field {
		border: 1px solid #000000;
		width: 100%;
		height: 50px;
	}
	.form-field-label {
		width: 200px;
	}
	.form-field-tr {
		padding: 2px;
	}
	input {
		border: 1px solid #000000;
		padding: 5px 10px;
	}
</style>
<div style="text-align: center">
	<h2>
		Client Consent and Identification
	</h2>
</div>
<p>
	<strong>
		I, the undersigned, certify that I am the individual named above as client.
	</strong>
</p>
<ol>
	<li>
		I authorise the result of all examinations and/or tests be forwarded to the insurance company, advisor who referred me to Bombora Medical for the tests and/or the medical practitioner designated on this document.
	</li>
	<li>
		I understand that the tests may include an H.I.V. aids test. 
	</li>
	<li>
		I authorise any other secondary tests that may be required to be performed on the blood sample/s collected today. 
	</li>
</ol>

<div>

</div>

<table class="form-field-tr" style="line-height: 2;">
	<tr>
		<td class="form-field-label">
			<strong>
				Client Name
			</strong>
		</td>
		<td>
			<input size="60" type="text" value="<?php echo $clientData[0]['client_name']; ?>" class="form-field" name="client_name"/>
		</td>
	</tr>
	<tr>
		<td class="form-field-label" class="form-field-label">
			<strong>
				Client Address
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_fulladdress']; ?>" name="client_fulladdress"/>
		</td>
	</tr>
	<tr>
		<td class="form-field-label" class="form-field-label">
			<strong>
				Insurance Company Name
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_organization']; ?>" name="client_fulladdress"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				General Practitioner Name
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_name']; ?>" name="client_genprac_name"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				Client General Practioner
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="<?php echo $clientData[0]['client_genprac_address']; ?>" name="client_genprac_address"/>
		</td>
	</tr>
	<tr>
	<td>
			<strong>
				Address
			</strong>
	</td>
	<td>
	</td>
	</tr>
	<tr>
		<td>
			<strong>
				Witness Signature
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="" name="client_witness_signature"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				Witness Print Name
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="" name="client_witness_print_name"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				Client Signature
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="" name="client_witness_print_name"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				Date Signed
			</strong>
		</td>
		<td>
			<input size="60" type="text" class="form-field" value="" name="client_witness_print_name"/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>
				Client Name
			</strong>
		</td>
		<td>
		</td>
	</tr>
</table>