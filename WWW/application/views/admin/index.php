<h1 class="page-header col-xs-11">Admin</h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-md-12">
<a href="<? echo base_url(); ?>index.php/admin/userindex/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Users</a>
</div>

<div class="col-xs-6">
  <h3>Sites</h3>
  <a href="<? echo base_url(); ?>index.php/admin/addsite/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Site</a>
  <table class="table table-striped table-responsive">
    <thead>
      <tr>
        <th>Name</th>
        <th>Domain</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody id="myTable">
    <? foreach ($siteData as $stData) { ?>
      <tr>
        <td><a href="<? echo base_url(); ?>index.php/admin/viewsite/<? echo $stData['siteID']; ?>/" ><? echo $stData['site_name']; ?></a></td>
        <td><? echo $stData['site_domain']; ?></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/viewsite/<? echo $stData['siteID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/editsite/<? echo $stData['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/deletesite/<? echo $stData['siteID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
      </tr>
    <? } ?>
    </tbody>
  </table>
</div>

       

<!-- Last </div> will be in the footer -->