<h1 class="page-header col-xs-11">View Site - <? echo $siteData[0]['site_name']; ?></h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/admin/addsite/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Site</a>
  <a href="<? echo base_url(); ?>index.php/admin/editsite/<? echo $siteData[0]['siteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Site</a>
</div>  

<div class="col-xs-12 main-data-content">
  <table class="view-table">
      <tr>
        <td class="view-title"><strong>Domain:</strong></td>
        <td><?php echo $siteData[0]['site_domain']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td>
        <?php echo $siteData[0]['site_address']; ?><br/>
          <?php echo $siteData[0]['site_address2']; ?><br/>
          <?php echo $siteData[0]['site_city']; ?>, <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$siteData[0]['site_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['state_name']; ?>
                <? }
              } else { } ?>
            , <?php echo $siteData[0]['site_postcode']; ?><br/>
            <!-- <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_country WHERE country_code = '".$siteData[0]['site_country']."'";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <? echo $row['country_name']; ?>
                    <? }
                  } else { } ?> -->
        </td>
      </tr>

      <tr>
        <td class="view-title"><strong>Phone Number:</strong></td>
        <td><?php echo $siteData[0]['site_phone']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Fax Number:</strong></td>
        <td><?php echo $siteData[0]['site_fax']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email:</strong></td>
        <td><?php echo $siteData[0]['site_email']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Website:</strong></td>
        <td><?php echo $siteData[0]['site_website']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Logo:</strong></td>
        <td><img src="<? echo base_url(); ?>uploads/logos/<?php echo $siteData[0]['site_logo']; ?>" width="200px"/></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Primary Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_maincolour']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_maincolour']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Primary Text Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_maincolour_text']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_maincolour_text']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Secondary Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_seccolour']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_seccolour']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Secondary Text Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_seccolour_text']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_seccolour_text']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Tertiary Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_thrcolour']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_thrcolour']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Tertiary Text Colour:</strong></td>
        <td>#<?php echo $siteData[0]['site_thrcolour_text']; ?> <div style="width:15px; height:15px; background:#<?php echo $siteData[0]['site_thrcolour_text']; ?>; border:1px solid #000; display:inline-block"></div></td>
      </tr>

  </table>
</div>


       

<!-- Last </div> will be in the footer -->