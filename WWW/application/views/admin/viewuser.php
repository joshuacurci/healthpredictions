<h1 class="page-header col-xs-11">View User - <? echo $userData[0]['name']; ?></h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/admin/userindex/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/admin/adduser/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New User</a>
  <a href="<? echo base_url(); ?>index.php/admin/edituser/<? echo $userData[0]['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit User</a>
</div>  

<div class="col-xs-12 main-data-content">
  <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $userData[0]['name']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Username:</strong></td>
        <td><?php echo $userData[0]['username']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email 1:</strong></td>
        <td><?php echo $userData[0]['email']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email 2:</strong></td>
        <td><?php echo $userData[0]['email2']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Phone Number:</strong></td>
        <td><?php echo $userData[0]['usr_phone']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Mobile Number:</strong></td>
        <td><?php echo $userData[0]['usr_mobile']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>User Type:</strong></td>
        <td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

                      // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
                      // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_usertypes WHERE type_letter = '".$userData[0]['type_letter']."'";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { 
                echo $row['type_name'];
              }
            } else { } ?>
        </td>
      </tr>

      <tr>
        <td class="view-title"><strong>Status:</strong></td>
        <td>
          <? if ($userData[0]['usr_status'] == 'A') {
            echo "Active";
          } else {
            echo "Disabled";
          } ?>
        </td>
      </tr>

      <tr>
                  <td class="view-title"><strong>Last logged in:</strong></td>
                  <td><?php if($userData[0]['lastlogin'] == ""){ echo "Not yet logged in";}else{ echo $userData[0]['lastlogin'];} ?></td>
                </tr>

      <tr>
                  <td class="view-title"><strong>Date Created:</strong></td>
                  <td><?php if($userData[0]['dateadded'] == ""){ echo "Not yet created";}else{ echo date('Y-m-d h:m:s',$userData[0]['dateadded']);} ?></td>
                </tr>

  </table>
</div>


       

<!-- Last </div> will be in the footer -->