<h1 class="page-header col-xs-11">User List</h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/admin/usersearch/">
    <div class="col-sm-12 ">
      <h3>Search</h3>
      </div>
      <div class="col-sm-6 search-box-item">
        <div class="search-title">Name:</div>
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Name" name="user_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['user_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-6 search-box-item">
        <div class="search-title">Email:</div>
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Name" name="email_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['email_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-12 search-box-item">
        <div class="search-title">User Status:</div>
        <div class="search-field">
        <select class="form-control" name="usr_status">
        <option <? if(isset($searchresults)) {if($searchresults['usr_status'] == "A") {echo "selected";}} ?> value="ALL">All</option>
          <option <? if(isset($searchresults)) {if($searchresults['usr_status'] == "A") {echo "selected";}} ?> value="A">Active</option>
          <option <? if(isset($searchresults)) {if($searchresults['usr_status'] == "D") {echo "selected";}} ?> value="D">Disabled</option>
        </select>
        </div>
      </div>

     <div class="form-group search-box-button">
      <div class="col-sm-12">
        <br/>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>
    </div>
  </form>
</div>
<div style="clear:both"></div>

<div class="col-xs-12">
  <a href="<? echo base_url(); ?>index.php/admin/adduser/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New User</a>
  <div style="overflow-x:auto;">
  <table class="table table-striped table-responsive" id="advisor-data">
    <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>User Type</th>
        <th>Last Login</th>
        <th>Date Created</th>
        <th></th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody id="myTable">
    <? foreach ($userData as $usrData) { ?>
      <tr>
        <td><a href="<? echo base_url(); ?>index.php/admin/viewuser/<? echo $usrData['userID']; ?>/" ><? echo $usrData['name']; ?></a></td>
        <td><? echo $usrData['email']; ?></td>
        <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database; $conn = new mysqli($servername, $username, $password, $dbname);
            $sql = "SELECT * FROM tbl_usertypes WHERE type_letter = '".$usrData['type_letter']."'";
            $result = $conn->query($sql);
            $num_rec = $result->num_rows;
            if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { echo $row['type_name']; } } else { echo 'error'; } ?>
              </td>
        <td><? echo $usrData['lastlogin']; ?></td>
        <td><? echo date('Y-m-d h:m:s',$usrData['dateadded']);; ?></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/viewuser/<? echo $usrData['userID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/edituser/<? echo $usrData['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
        <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/admin/deleteuser/<? echo $usrData['userID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
      </tr>
    <? } ?>
    </tbody>
  </table>
    </div>
  <div class="col-md-12 text-center">
    <ul class="pagination pagination-lg" id="myPager"></ul>
  </div>
</div>
      

<!-- Last </div> will be in the footer -->