<h1 class="page-header col-xs-11">Add User</h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
</div>  

<div class="col-xs-12 main-data-content">
<?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/admin/newuser/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID']; ?>">

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Username:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Username" name="username" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Password:</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="capital-text" placeholder="Password" name="password" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Email Address 1" name="email" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Email Address 2" name="email2" >
      </div>
      <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Phone Number" name="usr_phone" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Mobile Number" name="usr_mobile" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">User Type:</label>
      <div class="col-sm-9">
        <select class="form-control" name="type_letter">
         <option value="">Please Select</option>
         <? foreach ($typeData as $usertypes) { ?>
          <option value="<? echo $usertypes['type_letter'] ?>"><? echo $usertypes['type_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>
</div>


       

<!-- Last </div> will be in the footer -->