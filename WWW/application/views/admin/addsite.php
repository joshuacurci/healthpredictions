<script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/jscolor.js"></script>

<h1 class="page-header col-xs-11">Add Site</h1> <i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
</div>  

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/admin/newsite/">

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Site Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Site Name" name="site_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Site Domain:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Site Domain" name="site_domain" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Site Address:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="site_address" />
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 2" name="site_address2" />
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="site_city" />
      <select class="form-control address-half address-feilds" id="inputorg1" name="site_state">
      <option value="0">Please select your state</option>
       <? foreach ($staffState as $staffState) { ?>
         <option value="<? echo $staffState['id'] ?>"><? echo $staffState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="site_postcode" />
       <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="site_country">
          <option value="0">Please select your country</option>
          <option selected value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $countrydata) { ?>
            <option  value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
           <? } ?>
         </select> 
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="site_phone" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="site_fax" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email" name="site_email" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Website:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Website" name="site_website" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Site Logo:</label>
      <div class="col-sm-9">
        <input type="file" id="exampleInputFile" name="site_logo[]">
        <p class="help-block">Width should be no larger than 350px.</p>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Primary Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_maincolour">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Primary Text Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_maincolour_text">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Secondary Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_seccolour">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Secondary Text Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_seccolour_text">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Tertiary Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_thrcolour">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Tertiary Text Colour:</label>
      <div class="col-sm-9">
        <input class="jscolor form-control" value="000000" name="site_thrcolour_text">
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a href="<? echo base_url(); ?>index.php/admin/index/" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>
</div>


       

<!-- Last </div> will be in the footer -->