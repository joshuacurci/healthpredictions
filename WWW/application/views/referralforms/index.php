<h1 class="page-header col-xs-11">Edit Client</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
<a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-danger">Go back</a>
<br/>
<br/>
</div>

<div style="clear:both"></div>
<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referralforms/save/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">HIV information:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text" placeholder="Client Name" name="HIV"> <?php echo $referralformsData[0]['HIV']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Instructions After Taking Blood:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text2" placeholder="Client Name" name="instruct_taking_blood"> <?php echo $referralformsData[0]['instruct_taking_blood']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Client Consent Identification:
      <br/>
      <br/>
      <span style="color: red; font-weight: normal;">This content will be placed above the Client Signed Information </span>
      </label>
      
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text3" placeholder="Client Name" name="client_consent_ID"> <?php echo $referralformsData[0]['client_consent_ID']; ?></textarea>

      </div>
      <div style="clear:both"></div>

    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Pathology Tests Explained:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text4" placeholder="Client Name" name="path_tests_explained"> <?php echo $referralformsData[0]['path_tests_explained']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Client Authority to Release Results:
      <br/>
      <br/>
      <span style="color: red; font-weight: normal;">This content will be placed above the Client Signed Information </span>
      </label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text5" placeholder="Client Name" name="client_auth_to_release"> <?php echo $referralformsData[0]['client_auth_to_release']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Bombora Medical Offers:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text6" placeholder="Client Name" name="bombora_medical_offers"> <?php echo $referralformsData[0]['bombora_medical_offers']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <hr/>

    <div class="form-group">
      <label for="inputrecNum" class="col-sm-3 control-label">Client Instructions for Bombora Medical:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="capital-text7" placeholder="Client Name" name="client_instruct_for_bombora"> <?php echo $referralformsData[0]['client_instruct_for_bombora']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary" onclick="return confirm('Important! All your changes will be saved. Are you sure you want to continue?');" data-toggle="tooltip" data-placement="top">Submit</button>
        <a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-info" onclick="return confirm('Important! Are you sure you want to cancel all of your changes?');" data-toggle="tooltip" data-placement="top">Cancel</a>
      </div>
    </div>
  </form>
</div>      


<!-- Last </div> will be in the footer -->