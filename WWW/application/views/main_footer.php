
<footer class="footer-section">
<i>&copy; <? echo date('Y'); ?> SWiM Communications.</i> For support please email us at <a href="mailto:support@swim.com.au">support@swim.com.au</a> or <a href="https://www.swim.com.au/supportticket/" target="_blank">submit support ticket</a>
</footer>
</div>
</div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/bootstrap.min.js"></script>
    <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
            jQuery(".date-picker").datepicker({
                  format: "dd-mm-yyyy",
                  todayBtn: "linked",
                  autoclose: true,
                  todayHighlight: true
              });

           $("#capital-text").keyup(function(evt){
              // to capitalize all words  
              var cp_value= ucwords($(this).val(),true) ;
              $(this).val(cp_value );
            });

			
     </script>
     <!-- <script src="//<?php //echo $_SERVER['SERVER_NAME']; ?>/js/pagenation-script.js"></script> -->
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/new-page.js"></script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/menu.js"></script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/capital.js"></script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/form-submit.js"></script>
     <script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/active-inactive.js"></script>
     <script src="<?php echo base_url(); ?>js/clinic-individual.js"></script>
      
</body>
</html>