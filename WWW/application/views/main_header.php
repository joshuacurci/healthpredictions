<?php
  if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1') {
    echo '<span style="z-index: 5000; position: fixed; top: 0; right: 1.5em; padding: 0.3rem; font-size: 16px; background: green; color: #fff; border-radius: 0 0 5px 5px;">Local</span>';
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><? echo $title; ?></title>

  <style>
    :root {
      --main-color: #<? echo $sitedetails[0]['site_maincolour'] ?>;
      --main-color-text: #<? echo $sitedetails[0]['site_maincolour_text'] ?>;
      --second-color: #<? echo $sitedetails[0]['site_seccolour'] ?>;
      --second-color-text: #<? echo $sitedetails[0]['site_seccolour_text'] ?>;
      --thierd-color: #<? echo $sitedetails[0]['site_thrcolour'] ?>;
      --thierd-color-text: #<? echo $sitedetails[0]['site_thrcolour_text'] ?>;
    }
  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

	<link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-datepicker.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="//<?php echo $_SERVER['SERVER_NAME']; ?>/css/main.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/45e81b2b58.css" media="all">
  <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
  <script>tinymce.init({selector:'textarea'});</script>

  <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>

  <!--<link rel="stylesheet" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" media="all">
  <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>-->

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>

</head>

<body>
  <div class="container-fluid">
    <div class="row row-offcanvas row-offcanvas-left">
        <!-- Main Sidebar -->
        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2 sidebar sidebar-offcanvas" id="sidebar">
          <img class="logoPrimary" src="//<?php echo $_SERVER['SERVER_NAME']; ?>/uploads/logos/<? echo $sitedetails[0]['site_logo'] ?>" style="width:100%; max-width:280px;">
          <!-- Dashboard START -->
          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '1') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/home/index"><i class="fa fa-dashboard" aria-hidden="true"></i>  Dashboard</a></li>
          </ul>
          <!-- Dashboard END -->
          <hr> 
          <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
          <!-- Searches START -->
          <ul class="nav nav-sidebar">            
            <li <? if ($menuitem == '10') { echo 'class="active dropdown opendrop collapsed"';} else {echo 'class="dropdown opendrop collapsed"';} ?> data-toggle="collapse" data-parent="#p1" href="#pv1">
              <a class="nav-sub-container"><i class="fa fa-search" aria-hidden="true"></i> Searches</a>
            </li>
              <ul class="nav nav-pills nav-stacked collapse submenuclass" id="pv1">
                <li <? if ($menuitem_toggle == '20') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/search_all"><i class="fa fa-medkit" aria-hidden="true"></i> GP</a></li>
                <li <? if ($menuitem_toggle == '14') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/pathology/search_all"><i class="fa fa-heartbeat" aria-hidden="true"></i> Pathology</a></li>
                <li <? if ($menuitem_toggle == '15') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/search_all"><i class="fa fa-plus-square" aria-hidden="true"></i> Nurse</a></li>
                <li <? if ($menuitem_toggle == '16') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/specialists/search_all"><i class="fa fa-user-md" aria-hidden="true"></i> Specialists</a></li>
                <li <? if ($menuitem_toggle == '17') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/insurance/search_all"><i class="fa fa-building" aria-hidden="true"></i> Insurance</a></li>
                <li <? if ($menuitem_toggle == '18') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisor/search_all"><i class="fa fa-life-ring" aria-hidden="true"></i> Advisor</a></li>
                <li <? if ($menuitem_toggle == '19') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/search_all"><i class="fa fa-users" aria-hidden="true"></i> Client</a></li>
                <li <? if ($menuitem_toggle == '20') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/staff/search_all"><i class="fa fa-h-square" aria-hidden="true"></i> Staff</a></li>
              </ul>
          </ul>
          <!-- Searches END -->
          <hr>
          <?php } ?>
          <?php if ( $_SESSION['usertype'] == 'F' || $_SESSION['usertype'] == 'G' || $_SESSION['usertype'] == 'H') { ?>
            <ul class="nav nav-sidebar">            
            <li <? if ($menuitem == '10') { echo 'class="active dropdown opendrop collapsed"';} else {echo 'class="dropdown opendrop collapsed"';} ?> data-toggle="collapse" data-parent="#p1" href="#pv1">
              <a class="nav-sub-container"><i class="fa fa-search" aria-hidden="true"></i> Searches</a>
            </li>
              <ul class="nav nav-pills nav-stacked collapse submenuclass" id="pv1">
                <li <? if ($menuitem_toggle == '20') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/search_all"><i class="fa fa-medkit" aria-hidden="true"></i> GP</a></li>
                <li <? if ($menuitem_toggle == '15') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/search_all"><i class="fa fa-plus-square" aria-hidden="true"></i> Nurse</a></li>
                <li <? if ($menuitem_toggle == '17') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/insurance/search_all"><i class="fa fa-building" aria-hidden="true"></i> Insurance</a></li>
                <li <? if ($menuitem_toggle == '19') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/search_all"><i class="fa fa-users" aria-hidden="true"></i> Client</a></li>
              </ul>
          </ul>
          <hr>
          <?php } ?>
          <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
          <!-- Main Section START -->
          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '30') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/referral/index/"><i class="fa fa-users" aria-hidden="true"></i>  Referrals</a></li>
            <li <? if ($menuitem == '8') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/index"><i class="fa fa-users" aria-hidden="true"></i>  Clients</a></li>
            <li <? if ($menuitem == '5') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/specialists/index"><i class="fa fa-user-md" aria-hidden="true"></i>  Specialists</a></li>
            <li <? if ($menuitem == '2') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/index"><i class="fa fa-medkit" aria-hidden="true"></i>  GPs</a></li>
            <li <? if ($menuitem == '4') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/index"><i class="fa fa-plus-square" aria-hidden="true"></i>  Nurses</a></li>
            <li <? if ($menuitem == '3') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/pathology/index"><i class="fa fa-heartbeat" aria-hidden="true"></i>  Pathology</a></li>
          </ul>
          <!-- Main Section END -->
          <hr>
          <?php }  ?>
          <?php if ( $_SESSION['usertype'] == 'F' || $_SESSION['usertype'] == 'G' || $_SESSION['usertype'] == 'H') { ?>
            <ul class="nav nav-sidebar">
                      <li <? if ($menuitem == '8') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/client/index"><i class="fa fa-users" aria-hidden="true"></i>  Clients</a></li>
                      <li <? if ($menuitem == '2') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/index"><i class="fa fa-medkit" aria-hidden="true"></i>  GPs</a></li>
                      <li <? if ($menuitem == '4') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/index"><i class="fa fa-plus-square" aria-hidden="true"></i>  Nurses</a></li>
                      </ul>
                      <hr>
            <?php } ?>
          <!-- Insurance + Advisors START -->

          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '6') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/insurance/index"><i class="fa fa-building" aria-hidden="true"></i>  Insurance</a></li>
            <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
            <li <? if ($menuitem == '7') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisor/index"><i class="fa fa-life-ring" aria-hidden="true"></i>  Advisors</a></li>
            <hr>
            <?php } ?>
          </ul>
          <!-- Insurance + Advisors END -->
          <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
          <!-- Reports START -->
          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '11') { echo 'class="active dropdown opendrop collapsed"';} ?> data-toggle="collapse" data-parent="#p2" href="#pv2"><a class="nav-sub-container"><i class="fa fa-list" aria-hidden="true"></i>  Reports</a></li>
            <ul class="nav nav-pills nav-stacked collapse submenuclass <? if ($menuitem == '11') { echo 'in';} ?>" id="pv2">
                <li <? if ($menuitem_toggle == '21') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/clientdiary/index"><i class="fa fa-book" aria-hidden="true"></i> Client Diary</a></li>
                <li <? if ($menuitem_toggle == '22') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/referralsadvisor/index"><i class="fa fa-file-text-o" aria-hidden="true"></i> Referrals by Advisor</a></li>
                <li <? if ($menuitem_toggle == '23') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/referralbycreation/index"><i class="fa fa-calendar" aria-hidden="true"></i> Referrals by Creation</a></li>
                <li <? if ($menuitem_toggle == '24') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/groupbyinsurance/index"><i class="fa fa-building-o" aria-hidden="true"></i> Referrals group by insurance</a></li>
                <li <? if ($menuitem_toggle == '25') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/workloadreport/index"><i class="fa fa-briefcase" aria-hidden="true"></i> Workload Reports</a></li>
                <li <? if ($menuitem_toggle == '26') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/clientsbytest/index"><i class="fa fa-object-group" aria-hidden="true"></i> Clients by tests</a></li>
                <li <? if ($menuitem_toggle == '27') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/advisorlastaccess/index"><i class="fa fa-low-vision" aria-hidden="true"></i> Advisor last access</a></li>
                <!-- <li <? //if ($menuitem_toggle == '28') { echo 'class="active"';} ?>><a href="//<?php //echo $_SERVER['SERVER_NAME']; ?>/staff/services_stats"><i class="fa fa-line-chart" aria-hidden="true"></i> Services stats</a></li>  -->
              </ul>
          </ul>
          <!-- Reports END -->
          <hr>
          <!-- Staff + Admin START -->
          <ul class="nav nav-sidebar">
            <li <? if ($menuitem == '9') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/staff/index"><i class="fa fa-h-square" aria-hidden="true"></i>  Staff</a></li>
            <?php if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B') { ?>
              <li <? if ($menuitem == '12') { echo 'class="active"';} ?>><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/admin/index"><i class="fa fa-lock" aria-hidden="true"></i>  Admin</a></li>
            <?php } ?>
          </ul>
          <!-- Staff + Admin END -->
            <?php } ?>
          <!-- 
          Commented out for now.
          <hr>
          <ul class="nav nav-sidebar">
            <li><a href=""><i class="fa fa-chain" aria-hidden="true"></i>  Access Link</a></li>
            <li><a href=""><i class="fa fa-list" aria-hidden="true"></i>  Advisor</a></li>
          </ul> -->

        </div><!--/.sidebar-offcanvas-->

        <div class="col-sm-9 col-sm-offset-3 col-md-9 col-lg-10 col-md-offset-3 col-lg-offset-2 main">