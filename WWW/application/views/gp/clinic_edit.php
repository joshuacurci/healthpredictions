<h1 class="page-header col-xs-11">Edit General Practitioner Clinic</h1><i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/genprac/save_clinic/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <input type="hidden" name="clinicID" value="<? echo $clinicData[0]['clinicID']; ?>" >

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Clinic Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Clinic Name" name="clinic_name" value="<?php echo $clinicData[0]['clinic_name'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>


     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Clinic Address" name="clinic_address" value="<?php echo $clinicData[0]['clinic_address'] ?>" >
        <div class="clearboth"></div>
        
        <!-- <input type="text" class="form-control address-feilds" id="inputrecNum3" placeholder="Address Line 2" name="clinic_address2" value="<?php echo $clinicData[0]['clinic_address2'] ?>"> -->
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="clinic_city" value="<?php echo $clinicData[0]['clinic_city'] ?>">
        <select class="form-control address-half address-feilds" id="inputorg1" name="clinic_state"><option value="0">Please select your State/Territory</option>
          <? foreach ($gpState as $gpState) { ?>
           <option <? if($gpState['id'] == $clinicData[0]['clinic_state']) {echo 'selected';} ?> value="<? echo $gpState['id'] ?>"><? echo $gpState['state_code'] ?></option>
           <? } ?>
         </select>
         <input type="text" class="form-control address-half2 address-feilds" placeholder="Postcode" name="clinic_postcode" value="<?php echo $clinicData[0]['clinic_postcode'] ?>">
         <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="clinic_country">
          <option value="0">Please select your country</option>
          <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $gpCountry) { ?>
           <option <? if($gpCountry['id'] == $clinicData[0]['clinic_country']) {echo 'selected';} ?> value="<? echo $gpCountry['id'] ?>"><? echo $gpCountry['country_name'] ?></option>
           <? } ?>
         </select>
      </div>
      <div style="clear:both"></div>
    </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone" name="clinic_phone" value="<?php echo $clinicData[0]['clinic_phone'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile" name="clinic_mobile" value="<?php echo $clinicData[0]['clinic_mobile'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax" name="clinic_fax" value="<?php echo $clinicData[0]['clinic_fax'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email" name="clinic_email" value="<?php echo $clinicData[0]['clinic_email'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="2nd Email" name="clinic_email2" value="<?php echo $clinicData[0]['clinic_email2'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

<!--     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance" name="clinic_pi_insurance" value="<?php echo $clinicData[0]['clinic_pi_insurance'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance Number" name="clinic_pi_insurance_num" value="<?php echo $clinicData[0]['clinic_pi_insurance_num'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div> -->

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Practice Manager:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Clinic Practice Manager" name="clinic_responsibleperson" value="<?php echo $clinicData[0]['clinic_responsibleperson'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Instructions:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Instructions" name="clinic_instructions"><?php echo $clinicData[0]['clinic_instructions'] ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Notes" name="clinic_notes"><?php echo $clinicData[0]['clinic_notes'] ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->