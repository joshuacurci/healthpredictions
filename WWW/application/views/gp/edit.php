<h1 class="page-header col-xs-11">Edit Individual General Practitioner</h1><i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/genprac/save/<">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <input type="hidden" name="GPID" value="<? echo $gpData[0]['GPID']; ?>" >

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Practitioner Name" name="GP_name" value="<?php echo $gpData[0]['GP_name'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <!-- Start of Clinic -->

    <div class="form-group ">
     <label for="inputrecNum1" class="col-sm-3 control-label">Assign Clinic:</label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option <? if($clinicdata['clinicID'] == $gpData[0]['GP_clinic']) {echo 'selected';} ?> value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic" class="btn btn-success <?php if ($gpData[0]['GP_clinic2'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button" onclick="moreClinic()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   
   <div class="form-group <?php if ($gpData[0]['GP_clinic2'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select2">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic2">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option <? if($clinicdata['clinicID'] == $gpData[0]['GP_clinic2']) {echo 'selected';} ?> value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic2" class="btn btn-success <?php if ($gpData[0]['GP_clinic3'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button2" onclick="moreClinic2()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group <?php if ($gpData[0]['GP_clinic3'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select3">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic3">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic3" class="btn btn-success <?php if ($gpData[0]['GP_clinic4'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button3" onclick="moreClinic3()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group <?php if ($gpData[0]['GP_clinic4'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select4">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic4">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic4" class="btn btn-success <?php if ($gpData[0]['GP_clinic5'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button4" onclick="moreClinic4()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group <?php if ($gpData[0]['GP_clinic5'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select5">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic5">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic5" class="btn btn-success <?php if ($gpData[0]['GP_clinic6'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button5" onclick="moreClinic5()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group <?php if ($gpData[0]['GP_clinic6'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select6">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic6">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic6" class="btn btn-success <?php if ($gpData[0]['GP_clinic7'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button6" onclick="moreClinic6()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group <?php if ($gpData[0]['GP_clinic7'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select7">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic7">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <button id="gp_moreclinic7" class="btn btn-success <?php if ($gpData[0]['GP_clinic8'] != 0) { echo'gp_clinic_select_div'; } ?> add_clinic_button7" onclick="moreClinic7()">Assign More Clinic</button>
     </div>
     <div style="clear:both"></div>
   </div>


   <div class="form-group <?php if ($gpData[0]['GP_clinic8'] == 0) { echo'gp_clinic_select_div'; } ?> gp_clinic_select8">
     <label for="inputrecNum1" class="col-sm-3 control-label"></label>
     <div class="col-sm-7">
       <select class="form-control" id="gp_clinic_select" name="GP_clinic8">
         <option value="0">Select clinic to assign</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div class="col-sm-2">
       <p>Sorry, you have reach the maximum number of Clinic to input</p>
     </div>
     <div style="clear:both"></div>
   </div>

   <!--end of clinic -->


   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="GP_address" value="<?php echo $gpData[0]['GP_address'] ?>" >
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 2" name="GP_address2" value="<?php echo $gpData[0]['GP_address2'] ?>"/>
      <div class="clearboth"></div>
      <!-- <input type="text" class="form-control address-feilds" id="inputrecNum3" placeholder="Address Line 2" name="GP_address2" value="<?php echo $gpData[0]['GP_address2'] ?>"> -->
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="GP_city" value="<?php echo $gpData[0]['GP_city'] ?>">
      <select class="form-control address-half address-feilds" id="inputorg1" name="GP_state"><option value="0">Please select your country</option>
        <? foreach ($gpState as $gpState) { ?>
         <option <? if($gpState['id'] == $gpData[0]['GP_state']) {echo 'selected';} ?> value="<? echo $gpState['id'] ?>"><? echo $gpState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" placeholder="Postcode" name="GP_postcode" value="<?php echo $gpData[0]['GP_postcode'] ?>">
       <div class="clearboth"></div>
       <select class="form-control address-feilds" id="inputorg2" name="GP_country">
        <option value="0">Please select your country</option>
        <option value="13">Australia</option>
        <option value="158">New Zealand</option>
        <option disabled="disabled">----</option>
        <? foreach ($country as $gpCountry) { ?>
         <option <? if($gpCountry['id'] == $gpData[0]['GP_country']) {echo 'selected';} ?> value="<? echo $gpCountry['id'] ?>"><? echo $gpCountry['country_name'] ?></option>
         <? } ?>
       </select>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone" name="GP_phone" value="<?php echo $gpData[0]['GP_phone'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile" name="GP_mobile" value="<?php echo $gpData[0]['GP_mobile'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Work Phone Number:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Work" name="GP_workphone" value="<?php echo $gpData[0]['GP_workphone'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax" name="GP_fax" value="<?php echo $gpData[0]['GP_fax'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Email 1" name="GP_email" value="<?php echo $gpData[0]['GP_email'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Email 2" name="GP_email2" value="<?php echo $gpData[0]['GP_email2'] ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

<!--     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance" name="GP_pi_insurance" value="<?php echo $gpData[0]['GP_pi_insurance'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance Number" name="GP_pi_insurance_num" value="<?php echo $gpData[0]['GP_pi_insurance_num'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div> -->

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Practice Manager:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Practice Manager" name="GP_responsibleperson" value="<?php echo $gpData[0]['GP_responsibleperson'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Instructions:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Instructions" name="GP_instructions"><?php echo $gpData[0]['GP_instructions'] ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Comments:</label>
      <div class="col-sm-9">
        <textarea class="form-control" placeholder="Comments" name="GP_comments"><?php echo $gpData[0]['GP_comments'] ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>     

    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->