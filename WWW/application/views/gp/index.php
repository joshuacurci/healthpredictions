<h1 class="page-header col-xs-11">General Practitioners</h1> <i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/genprac/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Individual General Practitioner</a>
  <a href="<? echo base_url(); ?>index.php/genprac/clinic_add/" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New General Practitioner Clinic</a>
</div>  
<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/genprac/search/">
      <div class="col-sm-4 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search for Individual Practitioner's Name" name="GP_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['GP_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By Individual GP's City" name="GP_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['GP_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-3 search-box-item">
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="GP_state">
          <option value="">Select Individual GP's State</option>
          <option value="">All</option>
          <? foreach ($gpState as $gpState) { ?>
           <option value="<? echo $gpState['id'] ?>"><? echo $gpState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

     <div class="col-sm-2">
      <button type="submit" class="btn btn-primary">Search</button>
    </div>
    <div style="clear:both"></div>

    <div class="advanced-search-link"><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/genprac/search_all">Advanced search</a></div>
  </form>
</div>
<div style="clear:both"></div>
<div class="col-sm-12 project-info">
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#gp">Individual General Practitioner</a></li>
    <li><a data-toggle="tab" href="#clinic">Clinic List</a></li>
  </ul>
  <div class="col-sm-12 ">

    <div class="tab-content">
      <!-- Individual General Practitioner -->
      <div id="gp" class="tab-pane fade in active">
      <div style="overflow-x:auto;">
        <table class="table table-striped table-responsive" id="advisor-data">
          <thead>
            <tr>
              <th>Name</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Phone</th>
              <th>Fax</th>
              <th>Email</th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody id="myTable">
            <? foreach ($gpData as $gpData) { ?>
              <tr>
                <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" ><? echo $gpData['GP_name']; ?></a></td>
                <td><? echo $gpData['GP_address']; ?></td>
                <td><? echo $gpData['GP_city']; ?></td>
                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

            // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_au_states WHERE id = ".$gpData['GP_state']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['state_name']; ?></td>
                    <? }
                  } else { ?>
                    <td>No Assigned State</td>
                    <? } ?>
                    <td><? echo $gpData['GP_phone']; ?></td>
                    <td><? echo $gpData['GP_fax']; ?></td>
                    <td><? echo $gpData['GP_email']; ?></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                    <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/edit/<? echo $gpData['GPID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                    <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/delete/<? echo $gpData['GPID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                    <?php } ?>
                  </tr>
                  <? } ?>
                </tbody>
              </table>
                    </div>
            </div>

            <!-- General Practitioner Clinic List-->
            <div id="clinic" class="tab-pane fade">
            <div style="overflow-x:auto;">
              <table class="table table-striped table-responsive" id="advisor-data">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Phone</th>
                    <th>Fax</th>
                    <th>Email</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="myTable">
                  <? foreach ($gpClinic as $gpData) { ?>
                    <tr>
                      <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/genprac/clinic_view/<? echo $gpData['clinicID']; ?>/" ><? echo $gpData['clinic_name']; ?></a></td>
                      <td><? echo $gpData['clinic_address']; ?></td>
                      <td><? echo $gpData['clinic_city']; ?></td>
                      <?
                      $servername = $this->db->hostname;
                      $username = $this->db->username;
                      $password = $this->db->password;
                      $dbname = $this->db->database;

            // Create connection
                      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                      if ($conn->connect_error) {
                        die("Connection failed: " . $conn->connect_error);
                      }

                      $sql = "SELECT * FROM tbl_au_states WHERE id = ".$gpData['clinic_state']."";
                      $result = $conn->query($sql);

                      $num_rec = $result->num_rows;

                      if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { ?>
                          <td><? echo $row['state_name']; ?></td>
                          <? }
                        } else { ?>
                          <td>No Assigned State</td>
                          <? } ?>
                          <td><? echo $gpData['clinic_phone']; ?></td>
                          <td><? echo $gpData['clinic_fax']; ?></td>
                          <td><? echo $gpData['clinic_email']; ?></td>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/clinic_view/<? echo $gpData['clinicID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                          <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/clinic_edit/<? echo $gpData['clinicID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                          <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/clinic_delete/<? echo $gpData['clinicID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                          <?php } ?>
                        </tr>
                        <? } ?>
                      </tbody>
                    </table>
                          </div>
                  </div>
                </div>

                <div class="col-md-12 text-center">
                  <ul class="pagination pagination-lg" id="myPager"></ul>
                </div>


<!-- Last </div> will be in the footer -->