<h1 class="page-header col-xs-11">Search General Practitioner</h1><i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div class="col-xs-12">
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
} else {
?>
<div class="search-div-searches">
    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/genprac/search_result/">
        <div class="col-sm-12 ">
          <center><h3>General Practitioner Advanced Search</h3>
               <p>Fill in one or more criteria to search for a General Practitioner.</p></center>
        </div>
        <br/><br/>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Name:</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="capital-text" placeholder="Practitioner Name" name="GP_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Type:</label>
      <div class="col-sm-7">
       <select class="form-control" id="GP_type" name="GP_type">
         <option value="">Please select General Practitioner Type</option>
         <option value="0">Clinic</option>
         <option value="1">Individual</option>
       </select>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="search-box-item col-md-4 form-group">
     <label for="inputrecNum1" class="col-sm-5 control-label">Clinic:</label>
     <div class="col-sm-7">
       <select class="form-control" id="" name="GP_clinic">
         <option value="">Select Clinic</option>
         <? foreach ($gpClinic as $clinicdata) { ?>
          <option value="<? echo $clinicdata['clinicID'] ?>"><? echo $clinicdata['clinic_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 1:</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="capital-text" placeholder="Practitioner Address Line 1" name="GP_address" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Address Line 2:</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="inputrecNum2" placeholder="Practitioner Address Line 2" name="GP_address2" />
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">City/Suburb:</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="capital-text" placeholder="City/Suburb" name="GP_city" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">State:</label>
      <div class="col-sm-7">
        <select class="form-control" id="inputorg1" name="GP_state">
          <option selected value="">Please select your state/territory</option>
          <? foreach ($gpState as $gpState) { ?>
           <option value="<? echo $gpState['id'] ?>"><? echo $gpState['state_code'] ?></option>
           <? } ?>
         </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Postcode:</label>
      <div class="col-sm-7">
        <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="GP_postcode" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="search-box-item col-md-4 form-group">
      <label for="inputrecNum1" class="col-sm-5 control-label">Country:</label>
      <div class="col-sm-7">
        <select class="form-control" id="inputorg2" name="GP_country">
          <option value="">Please select your country</option>
          <? foreach ($country as $countrydata) { ?>
            <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
            <? } ?>
          </select>
      </div>
      <div style="clear:both"></div>
    </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Phone Number:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="GP_phone" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Mobile Number:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="GP_mobile" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Work Phone:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Phone" name="GP_workphone" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Fax Number:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="GP_fax" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Email Address 1:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 1" name="GP_email" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Email Address 2:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 2" name="GP_email2" >
        </div>
        <div style="clear:both"></div>
      </div>

 <!--     <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Personal Insurance:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance" name="GP_pi_insurance" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Personal Insurance Number:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance Number" name="GP_pi_insurance_num" >
        </div>
        <div style="clear:both"></div>
      </div> -->

      <div class="search-box-item col-md-4 form-group">
        <label for="inputrecNum1" class="col-sm-5 control-label">Person Responsible:</label>
        <div class="col-sm-7">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Person Responsible" name="GP_responsibleperson" >
        </div>
        <div style="clear:both"></div>
      </div>

  <!-- Commented for now, No Date and Active field on tbl_gp -->

<!--   <div class="search-box-item col-md-4 form-group">
    <div class="col-sm-12 search-box-item col-md-4 form-group">
      <div class="search-title col-sm-5">Active/Inactive:</div>
      <div class="search-field col-sm-7">
        <input type="radio" name="GP_active" value="1">&nbsp Active &nbsp&nbsp
        <input type="radio" name="GP_active" value="0">&nbsp Inactive
      </div>
    </div>
    <div style="clear:both"></div>
  </div> -->

<!--   <div class="search-box-item col-md-4 form-group">
    <label for="inputrecNum1" class="col-sm-5 control-label">Last Active Date Range:</label>
      <div class="col-sm-7">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="spec_date_of_active"/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
    <div style="clear:both"></div>
  </div> -->


<div style="clear:both"></div>

    <div class="col-sm-12">
      <button type="submit" class="btn btn-primary">Search</button>
    </div>

    <div style="clear:both"></div>

</form>
</div>
</div>
<?php } ?>

<? if(isset($searchresults)) { echo '<div style="display: block;">';
} else { echo '<div style="display: none;">'; }
 ?>
 <a href="<? echo base_url(); ?>index.php/genprac/search_all/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <div class="col-sm-12 ">

    <div class="col-sm-12 top-buttons">  
    <div style="overflow-x:auto;">
      <table class="table table-striped table-responsive" id="advisor-data">
        <thead>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Phone</th>
            <th>Fax</th>
            <th>Email</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody id="myTable">
          <? foreach ($gpData as $gpData) { ?>
            <tr>
              <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" ><? echo $gpData['GP_name']; ?></a></td>
              <td><? echo $gpData['GP_address']; ?></td>
              <td><? echo $gpData['GP_city']; ?></td>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

            // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_au_states WHERE id = ".$gpData['GP_state']."";
              $result = $conn->query($sql);

              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><? echo $row['state_name']; ?></td>
                  <? }
                } else { ?>
                  <td>No Assigned State</td>
                  <? } ?>
                  <td><? echo $gpData['GP_phone']; ?></td>
                  <td><? echo $gpData['GP_fax']; ?></td>
                  <td><? echo $gpData['GP_email']; ?></td>
                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/view/<? echo $gpData['GPID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                  <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/edit/<? echo $gpData['GPID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                  <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/genprac/delete/<? echo $gpData['GPID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                  <?php } ?>
                </tr>
                <? } ?>
              </tbody>
            </table>
                  </div>
          </div>

          <div class="col-md-12 text-center">
            <ul class="pagination pagination-lg" id="myPager"></ul>
          </div>
         </div>

<!-- Last </div> will be in the footer -->