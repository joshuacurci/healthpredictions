<h1 class="page-header col-xs-11">Individual General Practitioner Information</h1><i class="fa fa-medkit col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-sm-8 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/genprac/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Individual General Practitioner</a>
  <?php if ($_SESSION['usertype'] != 'F' && $_SESSION['usertype'] != 'G' && $_SESSION['usertype'] != 'H') { ?>
  <a href="<? echo base_url(); ?>index.php/genprac/edit/<? echo $gpData[0]['GPID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Individual General Practitioner</a>
  <?php } ?>
</div> 
<div class="col-md-4 top-buttons">
  <div class="name-header"><i class="fa fa-medkit" aria-hidden="true"></i> <?php echo $gpData[0]['GP_name']; ?></div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $gpData[0]['dateadded']);?></div>
</div> 

<div class="col-xs-12 main-data-content">
  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $gpData[0]['GP_name']; ?></td>
      </tr>


<!--       <tr>
<td class="view-title"><strong>Personal Insurance:</strong></td>
<td><?php echo $gpData[0]['GP_pi_insurance']; ?></td>
</tr>

<tr>
<td class="view-title"><strong>Personal Insurance Number:</strong></td>
<td><?php echo $gpData[0]['GP_pi_insurance_num']; ?></td>
</tr> -->

<tr>
  <td class="view-title"><strong>Practice Manager:</strong></td>
  <td><?php echo $gpData[0]['GP_responsibleperson']; ?></td>
</tr>
<tr>
  <td class="view-title"><strong>Clinic:</strong></td>
  <!-- CLinic 1 -->
  <?
  $servername = $this->db->hostname;
  $username = $this->db->username;
  $password = $this->db->password;
  $dbname = $this->db->database;

        // Create connection
  $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic']."";
  $result = $conn->query($sql);

  $num_rec = $result->num_rows;

  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) { ?>
      <td><? echo $row['clinic_name']; ?></td>
      <? }
    } else { ?>      <td>No Assigned Clinic</td>
      <? } ?>
    </tr>
    <!-- Clinic 2 -->
    <?php if ($gpData[0]['GP_clinic2'] != 0) { ?>
      <tr>
        <td class="view-title"><strong></strong></td>
        <? $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

    // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic2']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <td><? echo $row['clinic_name']; ?></td>
            <? }
          } else { ?>  <td>No Assigned Clinic</td><? } ?>

        </tr>
        <? } ?>
        <!-- Clinic 3 -->
        <?php if ($gpData[0]['GP_clinic3'] != 0) { ?>
          <tr>
            <td class="view-title"><strong></strong></td>
            <? $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

  // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic3']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['clinic_name']; ?></td>
                <? }
              } else { ?> <td>No Assigned Clinic</td> <? } ?>
            </tr>
            <? } ?>
            <!-- Clinic 4 -->
            <?php if ($gpData[0]['GP_clinic4'] != 0) { ?>
              <tr>
                <td class="view-title"><strong></strong></td>
                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

  // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic4']."";
                $result = $conn->query($sql);

                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['clinic_name']; ?></td>
                    <? }
                  } else { ?> <td>No Assigned Clinic</td> <? } ?>


                </tr>
                <? } ?>

                <!-- Clinic 5 -->
                <?php if ($gpData[0]['GP_clinic5'] != 0) { ?>
                  <tr>
                    <td class="view-title"><strong></strong></td>

                    <? $servername = $this->db->hostname;
                    $username = $this->db->username;
                    $password = $this->db->password;
                    $dbname = $this->db->database;

  // Create connection
                    $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                    if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                    }

                    $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic5']."";
                    $result = $conn->query($sql);

                    $num_rec = $result->num_rows;

                    if ($result->num_rows > 0) {
                      while($row = $result->fetch_assoc()) { ?>
                        <td><? echo $row['clinic_name']; ?></td>
                        <? }
                      } else { ?> <td>No Assigned Clinic</td> <? } ?>

                    </tr>
                    <? } ?>
                    <!-- Clinic 6 -->
                    <?php if ($gpData[0]['GP_clinic6'] != 0) { ?>
                      <tr>
                        <td class="view-title"><strong></strong></td>

                        <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

  // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                        if ($conn->connect_error) {
                          die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic6']."";
                        $result = $conn->query($sql);

                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                          while($row = $result->fetch_assoc()) { ?>
                            <td><? echo $row['clinic_name']; ?></td>
                            <? }
                          } else { ?> <td>No Assigned Clinic</td><? } ?>

                        </tr>
                        <? } ?>
                        <!-- Clinic 7 -->
                        <?php if ($gpData[0]['GP_clinic7'] != 0) { ?>
                          <tr>
                            <td class="view-title"><strong></strong></td>
                            <?
                            $servername = $this->db->hostname;
                            $username = $this->db->username;
                            $password = $this->db->password;
                            $dbname = $this->db->database;

  // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                            if ($conn->connect_error) {
                              die("Connection failed: " . $conn->connect_error);
                            }

                            $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic7']."";
                            $result = $conn->query($sql);

                            $num_rec = $result->num_rows;

                            if ($result->num_rows > 0) {
                              while($row = $result->fetch_assoc()) { ?>
                                <td><? echo $row['clinic_name']; ?></td>
                                <? }
                              } else { ?>                                          <td>No Assigned Clinic</td>
                                <? } ?>
                              </tr>
                              <? } ?>
                              <!-- Clinic 8 -->
                              <?php if ($gpData[0]['GP_clinic8'] != 0) { ?>
                                <tr>
                                  <td class="view-title"><strong></strong></td>


                                  <?
                                  $servername = $this->db->hostname;
                                  $username = $this->db->username;
                                  $password = $this->db->password;
                                  $dbname = $this->db->database;

  // Create connection
                                  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
                                  if ($conn->connect_error) {
                                    die("Connection failed: " . $conn->connect_error);
                                  }

                                  $sql = "SELECT * FROM tbl_gp_clinic WHERE clinicID = ".$gpData[0]['GP_clinic8']."";
                                  $result = $conn->query($sql);

                                  $num_rec = $result->num_rows;

                                  if ($result->num_rows > 0) {
                                    while($row = $result->fetch_assoc()) { ?>
                                      <td><? echo $row['clinic_name']; ?></td>
                                      <? }
                                    } else { ?>                                                <td>No Assigned Clinic</td>
                                      <? }
                                    } ?>
                                  </tr>


                                </table>
                              </div>

                              <div class="col-md-4">
                                <table class="view-table">
                                  <tr>
                                    <td class="view-title"><strong>Phone:</strong></td>
                                    <td><?php echo $gpData[0]['GP_phone']; ?></td>
                                  </tr>

                                  <tr>
                                    <td class="view-title"><strong>Mobile:</strong></td>
                                    <td><?php echo $gpData[0]['GP_mobile']; ?></td>
                                  </tr>

                                  <tr>
                                    <td class="view-title"><strong>Fax:</strong></td>
                                    <td><?php echo $gpData[0]['GP_fax']; ?></td>
                                  </tr>

                                  <tr>
                                    <td class="view-title"><strong>Email 1:</strong></td>
                                    <td><?php echo $gpData[0]['GP_email']; ?></td>
                                  </tr>

                                  <tr>
                                    <td class="view-title"><strong>Email 2:</strong></td>
                                    <td><?php echo $gpData[0]['GP_email2']; ?></td>
                                  </tr>

                                </table>
                              </div>

                              <div class="col-md-4">
                                <table class="view-table">
                                  <tr>
                                    <td class="view-title"><strong>Address:</strong></td>
                                    <td>
                                      <?php echo $gpData[0]['GP_address']; ?><br/>
                                      <?php echo $gpData[0]['GP_address2']; ?><br/>
                                      <?php echo $gpData[0]['GP_city']; ?>, <?
                                      $servername = $this->db->hostname;
                                      $username = $this->db->username;
                                      $password = $this->db->password;
                                      $dbname = $this->db->database;

        // Create connection
                                      $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
                                      if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                      }

                                      $sql = "SELECT * FROM tbl_au_states WHERE id = ".$gpData[0]['GP_state']."";
                                      $result = $conn->query($sql);

                                      $num_rec = $result->num_rows;

                                      if ($result->num_rows > 0) {
                                        while($row = $result->fetch_assoc()) { ?>
                                          <? echo $row['state_name']; ?>
                                          <? }
                                        } else { } ?>
                                        , <?php echo $gpData[0]['GP_postcode']; ?><br/>
                                        <?
                                        $servername = $this->db->hostname;
                                        $username = $this->db->username;
                                        $password = $this->db->password;
                                        $dbname = $this->db->database;

    // Create connection
                                        $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
                                        if ($conn->connect_error) {
                                          die("Connection failed: " . $conn->connect_error);
                                        }

                                        $sql = "SELECT * FROM tbl_country WHERE id = ".$gpData[0]['GP_country']."";
                                        $result = $conn->query($sql);

                                        $num_rec = $result->num_rows;

                                        if ($result->num_rows > 0) {
                                          while($row = $result->fetch_assoc()) { ?>
                                            <? echo $row['country_name']; ?>
                                            <? }
                                          } else { } ?>
                                        </td>
                                      </tr>

                                    </table>
                                  </div>

                                  <div style="clear:both"></div>
                                  <hr/>
                                  <div class="col-xs-6">
                                    <strong>Instructions: </strong><br/>
                                    <?php echo $gpData[0]['GP_instructions']; ?>
                                  </div>
                                  <div class="col-xs-6">
                                    <strong>Comments: </strong><br/>
                                    <?php echo $gpData[0]['GP_comments']; ?>
                                  </div>

                                </div>      

                                                  <!-- Last </div> will be in the footer -->