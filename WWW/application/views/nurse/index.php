<h1 class="page-header col-xs-11">Nurse</h1> <i class="fa fa-plus-square col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12 top-buttons">
  <a href="<? echo base_url(); ?>index.php/nurse/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Nurse</a>
</div>  
<div class="col-sm-12">
  <div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/nurse/search/">

      <div class="col-sm-4 search-box-item">
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Nurse's Name" name="nurse_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['nurse_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By City" name="nurse_city" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['nurse_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-3 search-box-item">
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="nurse_state">
          <option value="">Please select State/Territory</option>
          <? foreach ($nurseState as $nurseState) { ?>
           <option value="<? echo $nurseState['id'] ?>"><? echo $nurseState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

      <div class="col-sm-2">
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>

      <div class="advanced-search-link"><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/nurse/search_all">Advanced search</a></div>
  </form>
</div>
<div style="clear:both"></div>
<div class="col-sm-12 ">

  <div class="col-sm-12 top-buttons">  
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Name</th>
          <th>Service</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Phone</th>
          <th>Fax</th>
          <th>Email</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($nurseData as $nurseData) { ?>
          <tr>
            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/nurse/view/<? echo $nurseData['nurseID']; ?>/" ><? echo $nurseData['nurse_name']; ?></a></td>
            <td style="width: 150px"><? echo $nurseData['nurse_service']; ?></td>
            <td><? echo $nurseData['nurse_address']; ?></td>
            <td><? echo $nurseData['nurse_city']; ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_au_states WHERE id = ".$nurseData['nurse_state']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['state_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>
                <td><? echo $nurseData['nurse_phone']; ?></td>
                <td><? echo $nurseData['nurse_fax']; ?></td>
                <td><? echo $nurseData['nurse_email']; ?></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/view/<? echo $nurseData['nurseID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <?php if ($_SESSION['usertype'] != 'F') { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/edit/<? echo $nurseData['nurseID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/nurse/delete/<? echo $nurseData['nurseID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td>
                <?php } ?>
              </tr>
              <? } ?>
            </tbody>
          </table>
                </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>


<!-- Last </div> will be in the footer -->