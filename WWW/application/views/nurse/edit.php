<h1 class="page-header col-xs-11">Edit Nurse</h1><i class="fa fa-plus-square col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/nurse/save/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <input type="hidden" name="nurseID" value="<? echo $nurseData[0]['nurseID']; ?>" >

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="nurse_name" value="<?php echo $nurseData[0]['nurse_name'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Nurse Specialist Service:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Service" name="nurse_service" value="<?php echo $nurseData[0]['nurse_service'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Registration Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Registration Number" name="nurse_regnumber" value="<?php echo $nurseData[0]['nurse_regnumber'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="nurse_address" value="<?php echo $nurseData[0]['nurse_address'] ?>" >
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds" id="inputrecNum2" placeholder="Address Line 2" name="nurse_address2" value="<?php echo $nurseData[0]['nurse_address2'] ?>">
        <div class="clearboth"></div>
        <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="nurse_city" value="<?php echo $nurseData[0]['nurse_city'] ?>">
        <select class="form-control address-half address-feilds" id="inputorg1" name="nurse_state">
          <option value="0">Please select your state/territory</option>
          <? foreach ($nurseState as $nurseState) { ?>
           <option <? if($nurseState['id'] == $nurseData[0]['nurse_state']) {echo 'selected';} ?> value="<? echo $nurseState['id'] ?>"><? echo $nurseState['state_code'] ?></option>
           <? } ?>
         </select>
         <input type="text" class="form-control address-half2 address-feilds" placeholder="Postcode" name="nurse_postcode" value="<?php echo $nurseData[0]['nurse_postcode'] ?>">
         <div class="clearboth"></div>
         <select class="form-control address-feilds" id="inputorg2" name="nurse_country">
          <option value="0">Please select your country</option>
          <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
          <? foreach ($country as $gpCountry) { ?>
           <option <? if($gpCountry['id'] == $nurseData[0]['nurse_country']) {echo 'selected';} ?> value="<? echo $gpCountry['id'] ?>"><? echo $gpCountry['country_name'] ?></option>
           <? } ?>
         </select> 
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone" name="nurse_phone" value="<?php echo $nurseData[0]['nurse_phone'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="nurse_mobile" value="<?php echo $nurseData[0]['nurse_mobile'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="nurse_fax" value="<?php echo $nurseData[0]['nurse_fax'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 1:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 1" name="nurse_email" value="<?php echo $nurseData[0]['nurse_email'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address 2:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address 2" name="nurse_email2" value="<?php echo $nurseData[0]['nurse_email2'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance Type:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance" name="nurse_pi_insurance" value="<?php echo $nurseData[0]['nurse_pi_insurance'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Personal Insurance Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Personal Insurance Number" name="nurse_pi_insurance_num" value="<?php echo $nurseData[0]['nurse_pi_insurance_num'] ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Active/Inactive:</label>
      <div class="col-sm-9">
       <select class="form-control" id="inputorg1" name="nurse_active">
         <option value="">Please select if Active or Inactive</option>
         <option <? if($nurseData[0]['nurse_active'] == '1') {echo 'selected';} ?> value="1">Active</option>
         <option <? if($nurseData[0]['nurse_active'] == '0') {echo 'selected';} ?> value="0">Inactive</option>

       </select>
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
     <label for="inputrecNum1" class="col-sm-3 control-label">Last Active Date:</label>
     <div class="col-sm-9">
      <div class="controls">
        <div class="input-group">
          <input id="date-picker-1" type="text" class="date-picker form-control" value="<? echo $nurseData[0]['nurse_date_of_active']; ?>" name="nurse_date_of_active"/>
          <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
        </div>
      </div>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Last Inactive Date:</label>
    <div class="col-sm-9">
      <div class="controls">
        <div class="input-group">
          <input id="date-picker-1" type="text" class="date-picker form-control" value="<? echo $nurseData[0]['nurse_date_of_inactive']; ?>" name="nurse_date_of_inactive"/>
          <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
        </div>
      </div>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Add Availability Comments:</label>
    <div class="col-sm-9">
      <textarea class="form-control" id="inputrecNum1" placeholder="Availability Comments" name="nurse_availcomments"><?php echo $nurseData[0]['nurse_availcomments'] ?></textarea>
    </div>
    <div style="clear:both"></div>
    </div>     

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Add Comments:</label>
    <div class="col-sm-9">
      <textarea class="form-control" placeholder="Comments" name="nurse_comments" ><?php echo $nurseData[0]['nurse_comments'] ?></textarea>
    </div>
    <div style="clear:both"></div>
  </div>   


  <div class="form-group">
    <div class="col-sm-offset-3 col-sm-9">
      <button type="submit" class="btn btn-primary">Submit</button>
      <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
    </div>
  </div>

</form>

</div>      

<!-- Last </div> will be in the footer -->