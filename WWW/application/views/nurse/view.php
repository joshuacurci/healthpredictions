<h1 class="page-header col-xs-11">Nurse Information</h1><i class="fa fa-plus-square col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-6 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
  <a href="<? echo base_url(); ?>index.php/nurse/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Nurse</a>
  <?php if ($_SESSION['usertype'] != 'F') { ?>
  <a href="<? echo base_url(); ?>index.php/nurse/edit/<? echo $nurseData[0]['nurseID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Nurse</a>
  <?php } ?>
</div>  

<div class="col-xs-6 top-buttons">
<div class="name-header"><i class="fa fa-plus-square" aria-hidden="true"></i> <?php echo $nurseData[0]['nurse_name']; ?></div> 
<div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $nurseData[0]['dateadded']);?></div>
</div> 


<div class="col-xs-12 main-data-content">
  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Name:</strong></td>
        <td><?php echo $nurseData[0]['nurse_name']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Service Type:</strong></td>
        <td><?php echo $nurseData[0]['nurse_service']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Reg. Number:</strong></td>
        <td><?php echo $nurseData[0]['nurse_regnumber']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Personal Ins. Type:</strong></td>
        <td><?php echo $nurseData[0]['nurse_pi_insurance']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Personal Ins. #:</strong></td>
        <td><?php echo $nurseData[0]['nurse_pi_insurance_num']; ?></td>
      </tr>

    </table>
  </div>

  <div class="col-md-4">
    <table class="view-table">

      <tr>
        <td class="view-title"><strong>Email 1:</strong></td>
        <td><?php echo $nurseData[0]['nurse_email']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Email 2:</strong></td>
        <td><?php echo $nurseData[0]['nurse_email2']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Phone:</strong></td>
        <td><?php echo $nurseData[0]['nurse_phone']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Mobile:</strong></td>
        <td><?php echo $nurseData[0]['nurse_mobile']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Fax:</strong></td>
        <td><?php echo $nurseData[0]['nurse_fax']; ?></td>
      </tr>
    </table>
  </div>

  <div class="col-md-4">
    <table class="view-table">
      <tr>
        <td class="view-title"><strong>Address:</strong></td>
        <td><?php echo $nurseData[0]['nurse_address']; ?><br/>
          <?php echo $nurseData[0]['nurse_address2']; ?><br/>
          <?php echo $nurseData[0]['nurse_city']; ?>, <?
          $servername = $this->db->hostname;
          $username = $this->db->username;
          $password = $this->db->password;
          $dbname = $this->db->database;

            // Create connection
          $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
          if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
          }

          $sql = "SELECT * FROM tbl_au_states WHERE id = ".$nurseData[0]['nurse_state']."";
          $result = $conn->query($sql);

          $num_rec = $result->num_rows;

          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) { ?>
              <? echo '&nbsp;'; echo $row['state_name']; ?>
              <? }
            } else { ?>

              <? } ?>, <?php echo $nurseData[0]['nurse_postcode']; ?><br/>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

            // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_country WHERE id = ".$nurseData[0]['nurse_country']."";
              $result = $conn->query($sql);

              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo '&nbsp;'; echo $row['country_name']; ?>
                  <? }
                } else { ?>

                  <? } ?>
                </td>
              </tr>

              <tr>
        <td class="view-title"><strong>Active/Inactive:</strong></td>
        <td><? if($nurseData[0]['nurse_active'] == "1") {echo 'Active';} ?><? if($nurseData[0]['nurse_active'] == "0") {echo 'Inactive';} ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Last Active Date:</strong></td>
        <td><?php echo $nurseData[0]['nurse_date_of_active']; ?></td>
      </tr>

      <tr>
        <td class="view-title"><strong>Inactive Date:</strong></td>
        <td><?php echo $nurseData[0]['nurse_date_of_inactive']; ?></td>
      </tr>

              
            </table>
          </div>


          <div style="clear:both"></div>
          <hr/>

          <div class="col-xs-6">
            <strong>Comments: </strong><br/><?php echo $nurseData[0]['nurse_comments']; ?>
          </div>

          <div class="col-xs-6">
            <strong>Availability Comments: </strong><br/><?php echo $nurseData[0]['nurse_availcomments']; ?>
          </div>

        </div>   

<!-- Last </div> will be in the footer -->