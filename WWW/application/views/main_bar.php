<div class="mobile-header visible-xs text-right">
            <p class="pull-left visible-xs">
                <button type="button" class="btn btn-primary" data-toggle="offcanvas"><span class="glyphicon glyphicon-menu-hamburger" style="top: 2px;"></span></button>
            </p>
          <img class="logoPrimary" src="//<?php echo $_SERVER['SERVER_NAME']; ?>/uploads/logos/<? echo $sitedetails[0]['site_logo'] ?>" style="width:100%; max-width:280px;">
          </div>
          <nav class="navbar navbar-inverse ">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                  <? echo $icon; ?>
                </a>
                  <? foreach ($breadcrumbs as $crumb) { ?>
                    <a class="navbar-brand" href="<?php echo base_url(); ?><? echo $crumb['link']; ?>"><? echo $crumb['title']; ?> > </a>
                  <? } ?>
                
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="//<?php echo $_SERVER['SERVER_NAME']; ?>/admin/edituser/<? echo $_SESSION['userID'];?>">Hello <? echo $_SESSION['usersname']; ?></a></li>
                  <li><a href="<?php echo base_url(); ?>login/logout">Log out</a></li>
                </ul>
                
              </div>
            </div>
          </nav>