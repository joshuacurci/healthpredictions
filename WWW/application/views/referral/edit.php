<h1 class="page-header col-xs-11">Edit Referral</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referral/update/">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID']; ?>"/>
    <input type="hidden" name="referralID" value="<? echo $referralData[0]['referralID']; ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Name" name="client_name" value="<? echo $referralData[0]['client_name']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Insurance Company:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="insurance_company">
        <option selected value="0">Please select client insurance company</option>
        <? foreach ($insuranceComp as $insuranceData) { ?>
         <option <? if ($referralData[0]['insurance_company'] == $insuranceData['insID']) { echo 'selected'; } ?> value="<? echo $insuranceData['insID'] ?>"><? echo $insuranceData['ins_name'] ?></option>
        <? } ?>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Assigned Advisor:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="assigned_advisor">
        <option selected value="0">Please select submitting advisor</option>
        <? foreach ($advisorDetails as $assignAdvisor) { ?>
         <option <? if ($referralData[0]['assigned_advisor'] == $assignAdvisor['advisorID']) { echo 'selected'; } ?> value="<? echo $assignAdvisor['advisorID'] ?>"><? echo $assignAdvisor['advisor_name'] ?></option>
        <? } ?>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Assigned Staff:</label>
    <div class="col-sm-9">
      <select class="form-control" id="inputorg1" name="assigned_staff">
        <option selected value="0">Please assign staff member</option>
        <? foreach ($staffDetails as $assignStaff) { ?>
         <option <? if ($referralData[0]['assigned_staff'] == $assignStaff['staffID']) { echo 'selected'; } ?> value="<? echo $assignStaff['staffID'] ?>"><? echo $assignStaff['staff_name'] ?></option>
        <? } ?>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Application/Policy Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Application/Policy Number" name="insurance_number" value="<? echo $referralData[0]['insurance_number']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Date Of Birth:</label>
      <div class="col-sm-9">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="client_DOB" required value="<? echo $referralData[0]['client_DOB']; ?>" />
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Sex:</label>
      <div class="col-sm-9">
       <input <? if ($referralData[0]['client_sex'] == 'M') { echo 'checked'; } ?> type="radio" name="client_sex" value="M">&nbsp Male &nbsp&nbsp
       <input <? if ($referralData[0]['client_sex'] == 'F') { echo 'checked'; } ?> type="radio" name="client_sex" value="F">&nbsp Female
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Company/Organization:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Company/Organization" name="client_organization" value="<? echo $referralData[0]['client_organization']; ?>" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Address:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 1" name="client_address" value="<? echo $referralData[0]['client_address']; ?>" >
      <input type="text" class="form-control address-feilds" id="capital-text" placeholder="Address Line 2" name="client_address2" value="<? echo $referralData[0]['client_address2']; ?>" >
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="capital-text" placeholder="City/Suburb" name="client_city" value="<? echo $referralData[0]['client_city']; ?>" >
      <select class="form-control address-half address-feilds" id="inputorg1" name="client_state">
        <option selected value="0">Please select your state</option>
        <? foreach ($clientState as $clientState) { ?>
         <option <? if ($referralData[0]['client_state'] == $clientState['id']) { echo 'selected'; } ?> value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" id="inputrecNum5" placeholder="Postcode" name="client_postcode" value="<? echo $referralData[0]['client_postcode']; ?>" >
       <div class="clearboth"></div>
       <select class="form-control address-feilds" id="inputorg2" name="client_country">
        <option value="0">Please select your country</option>
        <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
        <? foreach ($country as $countrydata) { ?>
          <option <? if ($referralData[0]['client_country'] == $countrydata['id']) { echo 'selected'; } ?> value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="client_phone" value="<? echo $referralData[0]['client_phone']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile Number" name="client_mobile" value="<? echo $referralData[0]['client_mobile']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Work Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Work Number" name="client_workphone" value="<? echo $referralData[0]['client_workphone']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="client_fax" value="<? echo $referralData[0]['client_fax']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Email Address" name="client_email" value="<? echo $referralData[0]['client_email']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

        <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Second Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="client_email2" value="<? echo $referralData[0]['client_email2']; ?>" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Advisor Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="client_notes" ><? echo $referralData[0]['client_notes']; ?></textarea>
      </div>
      <div style="clear:both"></div>
    </div>  

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Attach Files:</label>
      <div class="col-sm-9">
        <input type="file" multiple name="userfile[]" size="20" />
      </div>
      <div style="clear:both"></div>
    </div>     


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="javascript:history.back()" class="btn btn-info">Cancel</a>
      </div> 
    </div>
  </form>
</div>      

<!-- Last </div> will be in the footer -->