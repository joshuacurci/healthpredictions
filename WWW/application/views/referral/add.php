<script src="//<?php echo $_SERVER['SERVER_NAME']; ?>/js/referal-validation.js"></script>
<h1 class="page-header col-xs-11">Lodge a Referral</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12 main-data-content">
  <?php echo $this->session->flashdata('msg'); ?>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referral/lodge2/" onsubmit="return validateFormClient()" name="referal-form" id="referal-form">
    <input type="hidden" name="siteID" value="<? echo $_SESSION['siteID'] ?>"/>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Name<span class="required">*</span>:</label>
      <div class="col-sm-9" id="client_name">
        <input type="text" class="form-control" id="client_name_value" placeholder="Name" name="client_name" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Insurance Company<span class="required">*</span>:</label>
    <div class="col-sm-9" id="insurance_company">
      <select class="form-control" id="insurance_company_value" name="insurance_company">
        <option selected value="0">Please select client insurance company</option>
        <? foreach ($insuranceComp as $insuranceData) { ?>
         <option value="<? echo $insuranceData['insID'] ?>"><? echo $insuranceData['ins_name'] ?></option>
        <? } ?>
      </select>
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Application/Policy Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="capital-text" placeholder="Application/Policy Number" name="insurance_number" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Date Of Birth<span class="required">*</span>:</label>
      <div class="col-sm-9" id="client_DOB">
        <div class="controls">
          <div class="input-group">
            <input id="date-picker-1" type="text" class="date-picker form-control" name="client_DOB" required/>
            <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
          </div>
        </div>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Sex<span class="required">*</span>:</label>
      <div class="col-sm-9" id="client_sex">
       <input type="radio" name="client_sex"  checked value="M">&nbsp Male &nbsp&nbsp
       <input type="radio" name="client_sex" value="F">&nbsp Female
     </div>
     <div style="clear:both"></div>
   </div>

   <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Company/Organization:</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="inputrecNum1" placeholder="Company/Organization" name="client_organization" >
    </div>
    <div style="clear:both"></div>
  </div>

  <div class="form-group">
    <label for="inputrecNum1" class="col-sm-3 control-label">Address<span class="required">*</span>:</label>
    <div class="col-sm-9" id="client_address">
      <input type="text" class="form-control address-feilds" id="client_address_value" placeholder="Address Line 1" name="client_address" >
      <input type="text" class="form-control address-feilds" id="client_address2_value" placeholder="Address Line 2" name="client_address2" >
      <div class="clearboth"></div>
      <input type="text" class="form-control address-feilds address-largehalf" id="client_city_value" placeholder="City/Suburb" name="client_city" >
      <select class="form-control address-half address-feilds" id="client_state_value" name="client_state">
        <option selected value="0">Please select your state</option>
        <? foreach ($clientState as $clientState) { ?>
         <option value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
         <? } ?>
       </select>
       <input type="text" class="form-control address-half2 address-feilds" id="client_postcode_value" placeholder="Postcode" name="client_postcode" >
       <div class="clearboth"></div>
       <select class="form-control address-feilds" id="client_country_value" name="client_country">
        <option value="0">Please select your country</option>
        <option value="13">Australia</option>
              <option value="158">New Zealand</option>
              <option disabled="disabled">----</option>
        <? foreach ($country as $countrydata) { ?>
          <option value="<? echo $countrydata['id'] ?>"><? echo $countrydata['country_name'] ?></option>
          <? } ?>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Phone Number<span class="required">*</span>:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="client_phone_value" placeholder="Phone Number" name="client_phone" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Mobile Number<span class="required">*</span>:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="client_mobile_value" placeholder="Mobile Number" name="client_mobile" >
      </div>
      <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Work Number<span class="required">*</span>:</label>
      <div class="col-sm-9" id="client_phone">
        <input type="text" class="form-control" id="client_workphone_value" placeholder="Work Number" name="client_workphone" >
        * ONE PHONE NUMBER IS REQUIRED
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Fax Number:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="client_fax" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="client_email_value" placeholder="Email Address" name="client_email" >
      </div>
      <div style="clear:both"></div>
    </div>

        <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Second Email Address:</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Second Email Address" name="client_email2" >
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Advisor Notes:</label>
      <div class="col-sm-9">
        <textarea class="form-control" id="inputrecNum1" placeholder="Notes" name="client_notes" ></textarea>
      </div>
      <div style="clear:both"></div>
    </div>  

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Attach Files:</label>
      <div class="col-sm-9">
        <input type="file" multiple name="userfile[]" size="20" />
      </div>
      <div style="clear:both"></div>
    </div>     


    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit & Assign Tests</button>
        <a onclick="history.go(-1);" class="btn btn-info">Cancel</a>
      </div>
    </div>

  </form>

</div>      

<!-- Last </div> will be in the footer -->