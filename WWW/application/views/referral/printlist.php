<script>
  function updateFields(selectObject,projectID){
    //alert("ProjectID = "+projectID+" Value = "+selectObject.value);
    window.location.href = "<?php echo base_url(); ?>index.php/referral/quickchange/"+projectID+"/"+selectObject.value+"/";
  };
  </script>

<h1 class="page-header col-xs-11">Bulk Referral Print</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12">
<a href="<? echo base_url(); ?>index.php/referral/index/" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
</div>

<div class="col-sm-12 ">
<br/>
<p class="bg-success" style="padding:25px 10px;">
<b>Success!</b> All selected referrals have been approved and converted to clients. 
</p>

  <div class="col-sm-12 top-buttons"> 
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
        <th><input type="checkbox" onClick="selectalltoggle(this)"></th>
          <th>Date</th>
          <th>JV</th>
          <th>Advisor</th>
          <th>Name</th>          
          <th>Phone</th>
          <th>Email</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($referralData as $clientData) { ?>
          <tr>
            <td><input type="checkbox" class="selectallind" name="bulkacknowlage[]" value="<? echo $clientData['referralID']; ?>"></td>
            <td><? echo date( 'd/m/Y', $clientData['dateadded']); ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$clientData['siteID']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['site_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>

            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData['assigned_advisor']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['advisor_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned Advisor</td>
                <? } ?>

            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $clientData['referralID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
            <td><? echo $clientData['client_phone']; ?></td>
            <td><? echo $clientData['client_email']; ?></td>

              <td>
              <? if ($clientData['client_active'] == 'Y') { ?>
                Acknowledged
              <? } else { ?>
                Unacknowledged
              <? } ?>
              </td>
                
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/justprint/<? echo $clientData['referralID']; ?>/" target="_blank" class="btn btn-warning">Print Notification</a></td>
              </tr>
              <? } ?>
            </tbody>
          </table>
              </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>

       
<!-- Last </div> will be in the footer -->