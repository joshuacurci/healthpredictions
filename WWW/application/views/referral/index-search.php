<script>
  function updateFields(selectObject,projectID){
    //alert("ProjectID = "+projectID+" Value = "+selectObject.value);
    window.location.href = "<?php echo base_url(); ?>index.php/referral/quickchange/"+projectID+"/"+selectObject.value+"/";
  };
  </script>

<h1 class="page-header col-xs-11">Referral Search Results</h1> <i class="fa fa-users col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-12">
  <a href="<? echo base_url(); ?>index.php/referral/adminadd/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Referral</a>
</div>

<div class="col-sm-12 ">
<div class="search-box">
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referral/search/">
    <div class="col-sm-12 ">
      <h3>Search</h3>
      </div>
      <div class="col-sm-3 search-box-item">
        <div class="search-title">Name:</div>
        <div class="search-field">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Search by Name" name="client_name" <? if(isset($searchData)) { echo 'value = "'.$searchData['client_name'].'"';} ?> >
        </div>
      </div>

      <div class="col-sm-3 search-box-item">
        <div class="search-title">City:</div>
        <div class="search-field">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Search By City" name="client_city" <? if(isset($searchData)) { echo 'value = "'.$searchData['client_city'].'"';} ?> >
       </div>
     </div>

     <div class="col-sm-2 search-box-item">
      <div class="search-title">State:</div>
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="client_state">
          <option value="">All</option>
          <? foreach ($clientState as $clientState) { ?>
           <option <? if($searchData['client_state'] == $clientState['id']){ echo 'selected'; } ?> value="<? echo $clientState['id'] ?>"><? echo $clientState['state_code'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

     <div class="col-sm-2 search-box-item">
      <div class="search-title">Joint Venture:</div>
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="siteID">
          <option value="">All</option>
          <? foreach ($sitesDetails as $siteData) { ?>
           <option <? if($searchData['siteID'] == $siteData['siteID']){ echo 'selected'; } ?> value="<? echo $siteData['siteID'] ?>"><? echo $siteData['site_name'] ?></option>
           <? } ?>
         </select>
       </div>
     </div>

     <div class="col-sm-2 search-box-item">
      <div class="search-title">Status:</div>
      <div class="search-field">
        <select class="form-control" id="inputorg1" name="client_active">
          <option value="">All</option>
           <option <? if($searchData['client_active'] == 'Y'){ echo 'selected'; } ?> value="Y">Acknowledged</option>
           <option <? if($searchData['client_active'] == 'N'){ echo 'selected'; } ?> value="N">Unacknowledged</option>
         </select>
       </div>
     </div>


     <div class="form-group search-box-button">
      <div class="col-sm-12">
        <br/>
        <button type="submit" class="btn btn-primary">Search</button>
      </div>
      <div style="clear:both"></div>
    </div>
  </form>
</div>


  <div class="col-sm-12 top-buttons">  
  <div style="overflow-x:auto;">
    <table class="table table-striped table-responsive" id="advisor-data">
      <thead>
        <tr>
          <th>Date</th>
          <th>JV</th>
          <th>Advisor</th>
          <th>Name</th>          
          <th>Phone</th>
          <th>Email</th>
          <th>Staff</th>
          <th>Status</th>
          <th></th>
          <th></th>
          <th></th>
          <? if ($_SESSION['usertype'] != 'I') { ?><th></th><? } ?>
        </tr>
      </thead>
      <tbody id="myTable">
        <? foreach ($referralData as $clientData) { ?>
          <tr>
            <td><? echo date( 'd/m/Y', $clientData['dateadded']); ?></td>
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$clientData['siteID']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['site_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned State</td>
                <? } ?>

            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData['assigned_advisor']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <td><? echo $row['advisor_name']; ?></td>
                <? }
              } else { ?>
                <td>No Assigned Advisor</td>
                <? } ?>

            <td style="width: 150px"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $clientData['referralID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
            <td><? echo $clientData['client_phone']; ?></td>
            <td><? echo $clientData['client_email']; ?></td>

            <td>
            <select name="assigned_staff" class="form-control" id="" onchange="updateFields(this,<? echo $clientData['referralID'] ?>)" >
            <option selected value="0">Please assign staff member</option>
            <? foreach ($staffDetails as $assignStaff) { ?>
              <option <? if ($clientData['assigned_staff'] == $assignStaff['staffID']) { echo 'selected'; } ?> value="<? echo $assignStaff['staffID'] ?>"><? echo $assignStaff['staff_name'] ?></option>
            <? } ?>
          </select>
            </td>

              <td>
              <? if ($clientData['client_active'] == 'Y') { ?>
                Acknowledged
              <? } else { ?>
                Unacknowledged
              <? } ?>
              </td>
              <?php if ($clientData['client_active'] == 'Y'){  ?>
                  <td class="tableButtons"><a class="btn btn-warning" disabled>A&P</a></td>
                <?php }
                else 
                { ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/acknowlageprint/<? echo $clientData['referralID']; ?>/" target="_blank" class="btn btn-warning">A&P</a></td>
                <?php } ?>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $clientData['referralID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/edit/<? echo $clientData['referralID']; ?>/" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                <? if ($_SESSION['usertype'] != 'I') { ?><td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/delete/<? echo $clientData['referralID']; ?>/" onclick="return confirm('Are you sure you want to deactivate this item?');" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Deactivate"><span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></td><? } ?>
              </tr>
              <? } ?>
            </tbody>
          </table>
                </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>

<!-- Last </div> will be in the footer -->