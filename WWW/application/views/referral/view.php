<h1 class="page-header col-xs-11">Referral Information</h1><i class="fa fa-lock col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>

<div class="col-xs-8 top-buttons">
<button onclick="history.go(-1);" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back </button>
    <?php     if ($_SESSION['usertype'] != 'I') { ?>
      <?php 
      if ($clientData[0]['client_active'] == 'N' || $clientData[0]['client_active'] == null){  ?>
    <a href="<? echo base_url(); ?>index.php/referral/convert/<? echo $clientData[0]['referralID']; ?>/" class="btn btn-warning"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Acknowledge & Print</a>
    <?php } }?>
    <a href="<? echo base_url(); ?>index.php/referral/edit/<? echo $clientData[0]['referralID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>Edit Client Information</a>
    <a href="<? echo base_url(); ?>index.php/referral/edittests/<? echo $clientData[0]['referralID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Tests Ordered</a>
</div> 

<div class="col-md-4 top-buttons">
  <div class="name-header"><i class="fa fa-lock" aria-hidden="true"></i> <?php echo $clientData[0]['client_name']; ?></div>
  <div class="date-header"><b>Date Created:</b> <? echo date('d/m/Y', $clientData[0]['dateadded']);?></div>
</div>  



<div class="col-xs-12 main-data-content">
        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Name:</strong></td>
              <td><?php echo $clientData[0]['client_name']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Company/organization:</strong></td>
              <td><?php echo $clientData[0]['client_organization']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Date of Birth:</strong></td>
              <td><?php echo $clientData[0]['client_DOB']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Sex:</strong></td>
              <td>
                <? if ($clientData[0]['client_sex'] == 'M') { echo 'Male'; }  ?>
                <? if ($clientData[0]['client_sex'] == 'F') { echo 'Female'; }  ?>
              </td>
            </tr>

            <tr>
              <td class="view-title"><strong>Insurance Company:</strong></td>
              <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_insurance WHERE insID = ".$clientData[0]['insurance_company']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        if ($_SESSION['usertype'] == 'I') {
                          echo $row['ins_name']; 
                        } else {
                          echo "<a href='//".$_SERVER['SERVER_NAME']."/insurance/view/".$row['insID']."/' target='_blank'>".$row['ins_name']."</a>"; 
                        }
                       } } else { } ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Application/Policy Number:</strong></td>
              <td><?php echo $clientData[0]['insurance_number']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Submitted Advisor:</strong></td>
              <td><? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                     $conn = new mysqli($servername, $username, $password, $dbname);
                     if ($conn->connect_error) {
                      die("Connection failed: " . $conn->connect_error);
                     }

                     $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$clientData[0]['assigned_advisor']."";
                     $result = $conn->query($sql);
                     if ($result->num_rows > 0) {
                       while($row = $result->fetch_assoc()) { 
                        
                        if ($_SESSION['usertype'] == 'I') {
                          echo $row['advisor_name']; 
                        } else {
                          echo "<a href='//".$_SERVER['SERVER_NAME']."/advisor/view/".$row['advisorID']."/' target='_blank'>".$row['advisor_name']."</a>"; 
                        }
                       } } else { } ?></td>
            </tr>

            
          </table>
        </div>

        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Address:</strong></td>
              <td>
                <?php echo $clientData[0]['client_address']; ?><br/>
                <?php echo $clientData[0]['client_address2']; ?><br/>
                <?php echo $clientData[0]['client_city']; ?>, <?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_au_states WHERE id = ".$clientData[0]['client_state']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <? echo $row['state_name']; ?>
            <? }
          } else { } ?>, <?php echo $clientData[0]['client_postcode']; ?><br/>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_country WHERE id = ".$clientData[0]['client_country']."";
            $result = $conn->query($sql);

            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                <? echo $row['country_name']; ?>
                <? }
              } else { ?>
                
                <? } ?>
              </td>
            </tr>
          </table>
        </div>

        <div class="col-md-4">
          <table class="view-table">
            <tr>
              <td class="view-title"><strong>Email:</strong></td>
              <td><?php echo $clientData[0]['client_email']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Secondary Email:</strong></td>
              <td><?php echo $clientData[0]['client_email2']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Phone:</strong></td>
              <td><?php echo $clientData[0]['client_phone']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Work Phone:</strong></td>
              <td><?php echo $clientData[0]['client_workphone']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Mobile:</strong></td>
              <td><?php echo $clientData[0]['client_mobile']; ?></td>
            </tr>

            <tr>
              <td class="view-title"><strong>Fax:</strong></td>
              <td><?php echo $clientData[0]['client_fax']; ?></td>
            </tr>

            <tr>
              <td colspan="2">
              <? $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$clientData[0]['siteID']."";
                $result = $conn->query($sql);
                  if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { 
                      echo '<img class="logoPrimary" src="//'. $_SERVER['SERVER_NAME'] .'/uploads/logos/'. $row['site_logo'] .'" style="width:100%; max-width:280px; text-align:right;">'; 
                  } } else { } ?>
                </td>
            </tr>
          </table>
        </div>
        <div style="clear:both"></div>
        <hr>
        <strong>Advisor Notes: </strong><br/>
        <?php echo $clientData[0]['client_notes']; ?>
    </div>

<div style="clear:both"></div>
<hr/>

<div class="col-xs-12 main-data-content">
      <div>
        <? $indTicked =[];
        for($i=0; $i<count($testTicked); $i++)
        {
          array_push($indTicked, $testTicked[$i]['testtypeID']);
        } ?>
        <div class="col-sm-4">
        <b>Medicals</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 1) { ?>
          
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>
        <div class="col-sm-4">
        <b>Pathology</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 2) { ?>
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>
        <div class="col-sm-4">
        <b>Other</b><br/>
        <? foreach($testTypes as $finalTest) { ?>
        <? if ($finalTest['groupID'] == 3) { ?>
            <? if (in_array($finalTest['testtypeID'], $indTicked)) { ?>
              <div class="tick-boxes">
              <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
              </div>
            <? } else { ?>
              <div class="tick-boxes-empty">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
              </div>
            <? } ?>
          <? echo $finalTest['type_name']; ?><br/>
           <? } ?>
        <? } ?>
        </div>

        <div style="clear:both"></div><br/><br/>

        <? if ($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B') { ?>
        <div class="rightaligned"><a href="<? echo base_url(); ?>index.php/referral/edittests/<? echo $clientData[0]['referralID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Tests Ordered</a></div>
        <? } ?>

        <strong>Test Notes: </strong><br/>
        <?php echo $testNotes[0]['testNote']; ?>

        </div>

</div>

<!-- Last </div> will be in the footer -->