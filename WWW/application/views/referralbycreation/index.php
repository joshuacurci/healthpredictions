<h1 class="page-header col-xs-11">Referrals by creation</h1> <i class="fa fa-calendar col-xs-1" aria-hidden="true"></i>
<div style="clear:both"></div>
<? 
if (isset($searchresults)) { 
  echo '<h3>Search Results</h3>'; 
}
?>
<div class="col-sm-12">
  <div class="col-sm-4 ">
    <div class="search-box">
      <h3>Search Specific Date</h3>
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referralbycreation/search_date/">
        <div class="col-sm-12 search-box-item">
          <div class="search-title">Search Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-1" type="text" class="date-picker form-control" name="referral_date"/>
              <label for="date-picker-1" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Date</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>


  <div class="col-sm-8 ">
    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/referralbycreation/search_between_date/">
        <h3>Search Between Dates</h3>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Start Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-2" type="text" class="date-picker form-control" name="startdate"/>
              <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Last Date:</div>
          <div class="search-field">
            <div class="input-group">
              <input id="date-picker-3" type="text" class="date-picker form-control" name="lastdate"/>
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span></label>
            </div>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <br/>
            <button type="submit" class="btn btn-primary">Search Between Dates</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
  </div>
  <div style="clear:both"></div>
</div>
  <div class="col-sm-12 ">

    <div class="col-sm-12 top-buttons">  
    <div style="overflow-x:auto;">
      <table class="table table-striped table-responsive" id="advisor-data">
        <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>JV</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Postcode</th>
            <th>Phone</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody id="myTable">
          <? foreach ($referralClient as $referralClient) { ?>

            <tr>
            <td class="tableButtons"><a href="<? echo base_url(); ?>index.php/referral/view/<? echo $referralClient['referralID']; ?>/" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $referralClient['client_name']; ?></td>

              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;
            // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql2 = "SELECT * FROM tbl_au_states WHERE id = ".$referralClient['client_state']."";
              $result2 = $conn->query($sql2);
              $num_rec = $result2->num_rows;
              $stateData = $result2->fetch_assoc(); 

              $sql = "SELECT * FROM tbl_sites WHERE siteID = ".$referralClient['siteID']."";
              $result = $conn->query($sql);
              $num_rec = $result->num_rows;
              $siteData = $result->fetch_assoc(); 
              ?>
              <td><? echo $siteData['site_name']; ?></td>

              <td><? echo $referralClient['client_address']; ?></td>
              <td><? echo $referralClient['client_city']; ?></td>
              <td><? echo $stateData['state_code']; ?></td>
              <td><? echo $referralClient['client_postcode']; ?></td>
              <td><? echo $referralClient['client_phone']; ?></td>
              <td><? echo $referralClient['client_email']; ?></td>
              
              <? } ?>
            </tbody>
          </table>
            </div>
        </div>

        <div class="col-md-12 text-center">
          <ul class="pagination pagination-lg" id="myPager"></ul>
        </div>


<!-- Last </div> will be in the footer -->