<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referralforms_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_referralforms_all() {
          $sql = "SELECT * FROM tbl_referralforms_content WHERE referralformsID = '1'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Update General Practitioner info
     function update_referralforms_info ($postdata) {
          $sql = "UPDATE tbl_referralforms_content SET 
          HIV = '".mysql_real_escape_string($postdata['HIV'])."',
          instruct_taking_blood = '".mysql_real_escape_string($postdata['instruct_taking_blood'])."',
          client_consent_ID = '".mysql_real_escape_string($postdata['client_consent_ID'])."',
          path_tests_explained = '".mysql_real_escape_string($postdata['path_tests_explained'])."',
          client_auth_to_release = '".mysql_real_escape_string($postdata['client_auth_to_release'])."',
          bombora_medical_offers = '".mysql_real_escape_string($postdata['bombora_medical_offers'])."',
          client_instruct_for_bombora = '".mysql_real_escape_string($postdata['client_instruct_for_bombora'])."'
          WHERE referralformsID = 1";
          $query = $this->db->query($sql);
          
     }
}