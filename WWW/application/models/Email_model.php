<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class email_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     public function getSMTPconfig() {
         if ($_SESSION['siteID']==1)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'referrals@healthpredictions.com', 
                'smtp_pass' => '7hLEYNuvuGNzjuKj', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@healthpredictions.com'
              );
         }
         else if($_SESSION['siteID']==2)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'BomboraReferrals@healthpredictions.com', 
                'smtp_pass' => 'E3qnKdsZKtjS8qUp', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@bomboramedical.com.au'
              );
         }
         else if($_SESSION['siteID']==6)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'm3referrals@healthpredictions.com', 
                'smtp_pass' => 'tuqQd3Yw9HskqVG8', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@m3lifewise.com.au'
              );
         }
         else if($_SESSION['siteID']==8)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'synchronreferrals@healthpredictions.com', 
                'smtp_pass' => 'RG3Z2ZxCvGrNc8f8', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@synchronmedical.com.au'
              );
         }
         else if($_SESSION['siteID']==9)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'referrals@centrepointhealth.com.au', 
                'smtp_pass' => 'jCYrgYKkC9y9BQJp', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@centrepointhealth.com.au'
              );
         }
         else if($_SESSION['siteID']==10)
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'm3referrals@healthpredictions.com', 
                'smtp_pass' => 'tuqQd3Yw9HskqVG8', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@m3lifewise.com.au'
              );
         }
         else
         {
            $config = Array(
                'protocol' => 'sendmail',
                'smtp_host' => 'smtp.office365.com',
                'smtp_port' => 587,
                'smtp_user' => 'referrals@healthpredictions.com', 
                'smtp_pass' => '7hLEYNuvuGNzjuKj', 
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE,
                'from' => 'referrals@healthpredictions.com'
              );
         }
        return $config;
     }

     public function sendReferral($clientID, $filename) {
     
        $config = $this->getSMTPconfig();

        $sql3 = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."'";
        $query3 = $this->db->query($sql3);
        $clientdata = $query3->result_array();

        $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$clientdata[0]['refferalID']."'";
        $query = $this->db->query($sql);
        $advisor = $query->result_array();

        $sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisor[0]['assigned_advisor']."'";
        $query2 = $this->db->query($sql2);
        $advisorEmail = $query2->result_array();

        $sendto = $advisorEmail[0]['advisor_email'];
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['from']);

        $this->email->to($sendto);
        $this->email->subject('Your referral has been approved');
        $this->email->message("Your referral for '".$advisor[0]['client_name']."' has been approved<br/>
        Attached you will find your Referral Acknowledgement document");
        $this->email->attach($filename);

        if($this->email->send()){
            echo 'Email send.';
        } else {
            show_error($this->email->print_debugger());
        }
     
     }

     public function sendnotiftoadvisor($clientID, $filename) {
     
        $config = $this->getSMTPconfig();

        $sql3 = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."'";
        $query3 = $this->db->query($sql3);
        $clientdata = $query3->result_array();

        $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$clientdata[0]['refferalID']."'";
        $query = $this->db->query($sql);
        $advisor = $query->result_array();

        $sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisor[0]['assigned_advisor']."'";
        $query2 = $this->db->query($sql2);
        $advisorEmail = $query2->result_array();

        $sendto = $advisorEmail[0]['advisor_email'];
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['from']);

        $this->email->to($sendto);
        $this->email->subject('Advisor Notification of Client Appointments');
        $this->email->message("Appoinment has been made for your referral for '".$advisor[0]['client_name']."' <br/>
        Attached you will find your Notification of Client Appointments");
        $this->email->attach($filename);

        if($this->email->send()){
            echo 'Email send.';
        } else {
            show_error($this->email->print_debugger());
        }
     
     }

     public function notiftoadvcompletion($clientID, $filename) {
     
        $config = $this->getSMTPconfig();

        $sql3 = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."'";
        $query3 = $this->db->query($sql3);
        $clientdata = $query3->result_array();

        $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$clientdata[0]['refferalID']."'";
        $query = $this->db->query($sql);
        $advisor = $query->result_array();

        $sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisor[0]['assigned_advisor']."'";
        $query2 = $this->db->query($sql2);
        $advisorEmail = $query2->result_array();

        $sendto = $advisorEmail[0]['advisor_email'];
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['from']);

        $this->email->to($sendto);
        $this->email->subject('Notification of Completion');
        $this->email->message("Your referral for '".$advisor[0]['client_name']."' has been completed<br/>
        Attached you will find your Notification of Completion document");
        $this->email->attach($filename);

        if($this->email->send()){
            echo 'Email send.';
        } else {
            show_error($this->email->print_debugger());
        }
     
     }

     public function sendReferralRef($clientID, $filename) {
     
        $config = $this->getSMTPconfig();

        $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$clientID."'";
        $query = $this->db->query($sql);
        $advisor = $query->result_array();

        $sql2 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisor[0]['assigned_advisor']."'";
        $query2 = $this->db->query($sql2);
        $advisorEmail = $query2->result_array();

        $sendto = $advisorEmail[0]['advisor_email'];
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from($config['from']);

        $this->email->to($sendto);
        $this->email->subject('Your referral has been approved');
        $this->email->message("Your referral for '".$advisor[0]['client_name']."' has been approved<br/>
        Attached you will find your Referral Acknowledgement document");
        $this->email->attach($filename);

        if($this->email->send()){
            echo 'Email send.';
        } else {
            show_error($this->email->print_debugger());
        }
     
     }

     public function resetpassword($sendto, $newpassword) {
     
        $config = $this->getSMTPconfig();

        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('no-reply@healthpredictions');

        $this->email->to($sendto);
        $this->email->subject('Your password has been changed');
        $this->email->message("<h2>Your password has been changed!</h2><p>You have submitted a request for your password to be changed and it has been temporarly changed.</p><p>You will need to login with your new password and change your password in the 'profile' area</p><h3>Your temporary password is: ".$newpassword."</h3><p>Kind Regards</p><p>The Health Predictions Team</p>");
        if($this->email->send()){
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">We have emailed your temporary password to your email address</div>');
            redirect('login/index');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
            redirect('login/index');
        }
     
     }
     
} ?>