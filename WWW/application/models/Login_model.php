<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_user($usr, $pwd)
     {
          $sql = "SELECT * FROM tbl_users WHERE username = '" . $usr . "' AND password = '" . md5($pwd) . "' AND usr_status = 'A'";
          $query = $this->db->query($sql);
          return array ('rownumber' => $query->num_rows(),'userdetails' => $query->result_array());
     }

     function get_forget_user($usr)
     {
          $sql = "SELECT * FROM tbl_users WHERE username = '" . $usr . "' AND usr_status = 'A'";
          $query = $this->db->query($sql);
          return array ('rownumber' => $query->num_rows(),'userdetails' => $query->result_array());
     }

     function setlastlogin($usr, $pwd)
     {
          // $timeanddate = date("F j, Y, g:i a");
      $timeanddate = date( "Y-m-d h:i:s");

          $sql = "UPDATE tbl_users SET lastlogin = '".$timeanddate."' WHERE username = '" . $usr . "'";
          $query = $this->db->query($sql);
     }

     function set_forget_user_password($usr, $pwd)
     {
          $sql = "UPDATE tbl_users SET password = '".md5($pwd)."' WHERE username = '" . $usr . "'";
          $query = $this->db->query($sql);
     }

     function generateRandomString($length = 10) {
         $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
         $charactersLength = strlen($characters);
         $randomString = '';
         for ($i = 0; $i < $length; $i++) {
             $randomString .= $characters[rand(0, $charactersLength - 1)];
         }
         return $randomString;
     }
}?>