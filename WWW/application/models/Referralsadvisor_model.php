<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referralsadvisor_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_referrals() {
          $sql = "SELECT * FROM tbl_referral WHERE client_deleted = 'N' ORDER BY dateadded";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_app_date($date) {
          $sql = "SELECT * FROM tbl_referral WHERE dateadded LIKE '%".$date."%' AND client_deleted = 'N' ORDER BY dateadded";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function search_between_date($startdate, $lastdate, $advisorID) {
          $sql = "SELECT * FROM tbl_referral WHERE client_deleted = 'N'";

        if ($advisorID != '') {
            $sql .= " AND assigned_advisor = '".$advisorID."'";
        }
        if($startdate<0)
        {
          $startdate='000000000';
        }
        if($lastdate<0)
        {
          $lastdate='000000000';
        }
        if($startdate!='000000000' && $startdate<'1000000000')
        {
              $startdate="000".$startdate;
        }
        if($lastdate!='9999999999' && $lastdate<'1000000000')
        {
              $lastdate="000".$lastdate;
        }
          $sql .= " AND dateadded between '".$startdate."' AND '".$lastdate."' AND client_deleted = 'N' ORDER BY dateadded";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
 }

 ?>