<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class advisor_model extends CI_Model
{
     public static $HOMEPAGE = "home/index/";
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_advisors($siteID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_status = 'A' AND advisor_deleted = 'N' ORDER BY advisor_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_all_advisors() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_status = 'A' AND advisor_deleted = 'N' ORDER BY advisor_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."' ORDER BY advisor_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_insurance() {
          $sql = "SELECT * FROM tbl_insurance";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_staff() {
      $sql = "SELECT * FROM tbl_staff";
      $query = $this->db->query($sql);
      return $query->result_array();
 }

     function get_all_sites() {
          $sql = "SELECT * FROM tbl_sites";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_advisor($searchData) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_name LIKE '%".$searchData['GP_name']."%' AND advisor_city LIKE '%".$searchData['GP_city']."%' AND advisor_state LIKE '%".$searchData['GP_state']."%' AND advisor_status = 'A' ORDER BY advisor_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // function delete_advisor_info($advisorID) {
     //      $sql = "UPDATE tbl_advisor SET advisor_status = 'D' WHERE advisorID = '".$advisorID."'";
     //      $query = $this->db->query($sql);
     // }

     function new_advisor_info($postdata, $userID){
          $time = time();
          $sql = "INSERT INTO tbl_advisor (
          siteID,
          userID,
          advisor_name,
          advisor_company,
          advisor_groups,
          advisor_address,
          advisor_address2,
          advisor_city,
          advisor_state,
          advisor_postcode,
          advisor_postaladdress,
          advisor_postaladdress2,
          advisor_postalcity,
          advisor_postalstate,
          advisor_postalpostcode,
          advisor_email,
          advisor_email2,
          advisor_email3,
          advisor_email4,
          advisor_email5,
          advisor_email6,
          advisor_phone,
          advisor_workphone,
          advisor_mobile,
          advisor_fax,
          advisor_enable_points,
          advisor_points,
          advisor_first_time,
          advisor_mailing_list,
          advisor_copy_results,
          advisor_comments,
          dateadded,
          advisor_status) 
          VALUES 
          ('".$postdata['siteID']."',
          '".$userID."',
          '".$postdata['advisor_name']."',
          '".$postdata['advisor_company']."',
          '".$postdata['advisor_groups']."',
          '".$postdata['advisor_address']."',
          '".$postdata['advisor_address2']."',
          '".$postdata['advisor_city']."',
          '".$postdata['advisor_state']."',
          '".$postdata['advisor_postcode']."',
          '".$postdata['advisor_postaladdress']."',
          '".$postdata['advisor_postaladdress2']."',
          '".$postdata['advisor_postalcity']."',
          '".$postdata['advisor_postalstate']."',
          '".$postdata['advisor_postalpostcode']."',
          '".$postdata['advisor_email']."',
          '".$postdata['advisor_email2']."',
          '".$postdata['advisor_email3']."',
          '".$postdata['advisor_email4']."',
          '".$postdata['advisor_email5']."',
          '".$postdata['advisor_email6']."',
          '".$postdata['advisor_phone']."',
          '".$postdata['advisor_workphone']."',
          '".$postdata['advisor_mobile']."',
          '".$postdata['advisor_fax']."',
          '".$postdata['advisor_enable_points']."',
          '".$postdata['advisor_points']."',
          '".$postdata['advisor_first_time']."',
          '".$postdata['advisor_mailing_list']."',
          '".$postdata['advisor_copy_results']."',
          '".$postdata['advisor_comments']."',
          '".$time."',
          '".$postdata['advisor_status']."')";

          $query = $this->db->query($sql);          
     }

     function update_advisor_info ($postdata, $newuserID) {
          if ($newuserID != "") {
               $extra = "insuranceID = '".$postdata['insuranceID']."',userID = '".$newuserID."',siteID = '".$postdata['siteID']."',";
          } else {
               $extra = "";
          }

          $sql = "UPDATE tbl_advisor SET 
          advisor_name = '".$postdata['advisor_name']."',
          advisor_company = '".$postdata['advisor_company']."',
          advisor_groups = '".$postdata['advisor_groups']."',
          advisor_address = '".$postdata['advisor_address']."',
          advisor_address2 = '".$postdata['advisor_address2']."',
          advisor_city = '".$postdata['advisor_city']."',
          advisor_state = '".$postdata['advisor_state']."',
          advisor_postcode = '".$postdata['advisor_postcode']."',
          advisor_postaladdress = '".$postdata['advisor_postaladdress']."',
          advisor_postaladdress2 = '".$postdata['advisor_postaladdress2']."',
          advisor_postalcity = '".$postdata['advisor_postalcity']."',
          advisor_postalstate = '".$postdata['advisor_postalstate']."',
          advisor_postalpostcode = '".$postdata['advisor_postalpostcode']."',
          advisor_email = '".$postdata['advisor_email']."',
          advisor_email2 = '".$postdata['advisor_email2']."',
          advisor_email3 = '".$postdata['advisor_email3']."',
          advisor_email4 = '".$postdata['advisor_email4']."',
          advisor_email5 = '".$postdata['advisor_email5']."',
          advisor_email6 = '".$postdata['advisor_email6']."',
          advisor_phone = '".$postdata['advisor_phone']."',
          advisor_workphone = '".$postdata['advisor_workphone']."',
          advisor_mobile = '".$postdata['advisor_mobile']."',
          advisor_fax = '".$postdata['advisor_fax']."',
          advisor_enable_points = '".$postdata['advisor_enable_points']."',
          advisor_points = '".$postdata['advisor_points']."',
          advisor_first_time = '".$postdata['advisor_first_time']."',
          advisor_mailing_list = '".$postdata['advisor_mailing_list']."',
          advisor_copy_results = '".$postdata['advisor_copy_results']."',
          advisor_comments = '".$postdata['advisor_comments']."',
          ".$extra."
          advisor_status = '".$postdata['advisor_status']."'
          WHERE advisorID = ".$postdata['advisorID']."";
          $query = $this->db->query($sql);
     }

     function search_advisor_all($searchData) { 

        if ($searchData['advisor_groups']=='')
        {
           $groups="AND (advisor_groups LIKE '%".$searchData['advisor_groups']."%' OR advisor_groups IS NULL)";
        }
        else
        {
           $groups="AND advisor_groups LIKE '%".$searchData['advisor_groups']."%'";
        }

        if ($searchData['advisor_address2']=='')
        {
           $address2="AND (advisor_address2 LIKE '%".$searchData['advisor_address2']."%' OR advisor_address2 IS NULL)";
        }
       else
       {
           $address2="AND advisor_address2 LIKE '%".$searchData['advisor_address2']."%'";
       }

       if ($searchData['advisor_email2']=='')
       {
          $email2="AND (advisor_email2 LIKE '%".$searchData['advisor_email2']."%' OR advisor_email2 IS NULL)";
       }
      else
      {
          $email2="AND advisor_email2 LIKE '%".$searchData['advisor_email2']."%'";
      }

       if ($searchData['advisor_email3']=='')
       {
          $email3="AND (advisor_email3 LIKE '%".$searchData['advisor_email3']."%' OR advisor_email3 IS NULL)";
       }
      else
      {
          $email3="AND advisor_email3 LIKE '%".$searchData['advisor_email3']."%'";
      }

      if ($searchData['advisor_email4']=='')
      {
         $email4="AND (advisor_email4 LIKE '%".$searchData['advisor_email4']."%' OR advisor_email4 IS NULL)";
      }
     else
     {
         $email4="AND advisor_email4 LIKE '%".$searchData['advisor_email4']."%'";
     }
      
     if ($searchData['advisor_email5']=='')
     {
        $email5="AND (advisor_email5 LIKE '%".$searchData['advisor_email5']."%' OR advisor_email5 IS NULL)";
     }
    else
    {
        $email5="AND advisor_email5 LIKE '%".$searchData['advisor_email5']."%'";
    }

    if ($searchData['advisor_email6']=='')
    {
       $email6="AND (advisor_email6 LIKE '%".$searchData['advisor_email6']."%' OR advisor_email6 IS NULL)";
    }
   else
   {
       $email6="AND advisor_email6 LIKE '%".$searchData['advisor_email6']."%'";
   }

   if ($searchData['advisor_workphone']=='')
   {
      $workphone="AND (advisor_workphone LIKE '%".$searchData['advisor_workphone']."%' OR advisor_workphone IS NULL)";
   }
  else
  {
      $workphone="AND advisor_workphone LIKE '%".$searchData['advisor_workphone']."%'";
  }

  if ($searchData['insuranceID']=='')
  {
     $insuranceID="AND (insuranceID LIKE '%".$searchData['insuranceID']."%' OR insuranceID IS NULL)";
  }
 else
 {
     $insuranceID="AND insuranceID LIKE '%".$searchData['insuranceID']."%'";
 }
          $sql = "SELECT * FROM tbl_advisor WHERE 
          advisor_name LIKE '%".$searchData['advisor_name']."%' 
          AND advisor_company  LIKE '%".$searchData['advisor_company']."%'".$groups."
          AND advisor_address LIKE '%".$searchData['advisor_address']."%'".$address2."
          AND advisor_city LIKE '%".$searchData['advisor_city']."%'
          AND advisor_postcode LIKE '%".$searchData['advisor_postcode']."%'
          AND advisor_state LIKE '%".$searchData['advisor_state']."%' 
          AND advisor_country LIKE '%".$searchData['advisor_country']."%' 
          AND advisor_phone LIKE '%".$searchData['advisor_phone']."%' 
          AND advisor_mobile LIKE '%".$searchData['advisor_mobile']."%' 
          AND advisor_fax LIKE '%".$searchData['advisor_fax']."%' 
          AND advisor_email LIKE '%".$searchData['advisor_email']."%'".$email2." ".$email3." ".$email4." ".$email5." ".$email6." "."
          AND advisor_points LIKE '%".$searchData['advisor_points']."%'
          AND advisor_enable_points LIKE '%".$searchData['advisor_enable_points']."%' 
          AND advisor_first_time LIKE '%".$searchData['advisor_first_time']."%' 
          AND advisor_mailing_list LIKE '%".$searchData['advisor_mailing_list']."%' 
          AND advisor_status LIKE '%".$searchData['advisor_status']."%' ".$workphone." ".$insuranceID."
          AND advisor_deleted = 'N' ORDER BY advisor_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_advisor_info($advisorID) {
          $sql = "UPDATE tbl_advisor SET 
          advisor_deleted = 'Y'
          WHERE advisorID = ".$advisorID."";
          $query = $this->db->query($sql);
     }

     function update_user_info($postdata) {
      $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$postdata['advisorID']."";
      $query = $this->db->query($sql);
      $advisorInfo = $query->result_array();


      $sql2 = "UPDATE tbl_users SET 
          name = '".$postdata['advisor_name']."',
          email = '".$postdata['advisor_email']."',
          email2 = '".$postdata['advisor_email2']."',
          siteID = '".$postdata['siteID']."'
          WHERE userID = ".$advisorInfo[0]['userID']."";
      $query2 = $this->db->query($sql2);
 }

 function create_new_user_oripw($postdata, $oripw){

     $password = $oripw;

     $sql = "INSERT INTO tbl_users (
     name,
     username,
     password,
     email,
     email2,
     usr_phone,
     usr_mobile,
     type_letter,
     siteID,
     usr_status,
     dateadded
     ) VALUES (
     '".$postdata['advisor_name']."',
     '".$postdata['advisor_email']."',
     '".$password."',
     '".$postdata['advisor_email']."',
     '".$postdata['advisor_email2']."',
     '".$postdata['advisor_phone']."',
     '".$postdata['advisor_mobile']."',
     'I',
     '".$postdata['siteID']."',
     'A',
     '".time()."'
     )";
   $query = $this->db->query($sql);

   $subject = "New profile created in Health Predictions Portal";

   $to = "".$postdata['advisor_name']." <".$postdata['advisor_email'].">";

   $cc = "";
   if ($postdata['advisor_email2'] != "") {$cc .= $postdata['advisor_email2'];};
   if ($postdata['advisor_email3'] != "") {$cc .= ", ".$postdata['advisor_email3'];};
   if ($postdata['advisor_email4'] != "") {$cc .= ", ".$postdata['advisor_email4'];};
   if ($postdata['advisor_email5'] != "") {$cc .= ", ".$postdata['advisor_email5'];};
   if ($postdata['advisor_email6'] != "") {$cc .= ", ".$postdata['advisor_email6'];};

   $message = "<html><body>Dear ".$postdata['advisor_name'].",<br/>
   <br/>
To access your secure, individual referral site, you should click on the following link.
<br/><br/>
http://portal.healthpredictions.com/
<br/><br/>
Your login details are as follows:<br/>
Username: ".$postdata['advisor_email']."<br/>
Password: ".$password."<br/>

Access through this URL will allow you the following -<br/>

1. Autofill of all your information, saving you the need to repeatedly type your name, address, phone etc.<br/>

2. Unambiguous access to our rewards system.<br/>

3. Ability to review the progress of all your referrals through our system.<br/>

4. Use of a link back to Bombora Medical where you may, if required, find any further details as well as client instructions etc.<br/><br/>

We remind you that Bombora Medical offers an Australia wide service for pathology, paramedical, general practice medicals and specialist medicals with strong emphasis on home and workplace visits. We are medically supervised and accepted by all insurance companies.
<br/><br/>
Please telephone our office if you would like to discuss this.
<br/><br/>

Yours faithfully,<br/>
Health Predictions Team</body></html>";

    $headers = "From: Health Predictions <no-reply@healthpredictions.com.au>" . "\r\n";
    $headers .= 'Cc: '.$cc." \r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";    

   mail( $to, $subject, $message, $headers );


   return $this->db->insert_id();          
}

     function create_new_user($postdata){
          $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $length = 10;
          $charactersLength = strlen($characters);
          $randomString = '';
          for ($i = 0; $i < $length; $i++) {
               $randomString .= $characters[rand(0, $charactersLength - 1)];
          }

          $password = $randomString;

          $sql = "INSERT INTO tbl_users (
          name,
          username,
          password,
          email,
          email2,
          usr_phone,
          usr_mobile,
          type_letter,
          siteID,
          usr_status,
          dateadded
          ) VALUES (
          '".$postdata['advisor_name']."',
          '".$postdata['advisor_email']."',
          '".md5($password)."',
          '".$postdata['advisor_email']."',
          '".$postdata['advisor_email2']."',
          '".$postdata['advisor_phone']."',
          '".$postdata['advisor_mobile']."',
          'I',
          '".$postdata['siteID']."',
          'A',
          '".time()."'
          )";
        $query = $this->db->query($sql);

        $subject = "New profile created in Health Predictions Portal";

        $to = "".$postdata['advisor_name']." <".$postdata['advisor_email'].">";

        $cc = "";
        if ($postdata['advisor_email2'] != "") {$cc .= $postdata['advisor_email2'];};
        if ($postdata['advisor_email3'] != "") {$cc .= ", ".$postdata['advisor_email3'];};
        if ($postdata['advisor_email4'] != "") {$cc .= ", ".$postdata['advisor_email4'];};
        if ($postdata['advisor_email5'] != "") {$cc .= ", ".$postdata['advisor_email5'];};
        if ($postdata['advisor_email6'] != "") {$cc .= ", ".$postdata['advisor_email6'];};

        $message = "<html><body>Dear ".$postdata['advisor_name'].",<br/>
        <br/>
To access your secure, individual referral site, you should click on the following link.
<br/><br/>
http://portal.healthpredictions.com/
<br/><br/>
Your login details are as follows:<br/>
     Username: ".$postdata['advisor_email']."<br/>
     Password: ".$password."<br/>

Access through this URL will allow you the following -<br/>

1. Autofill of all your information, saving you the need to repeatedly type your name, address, phone etc.<br/>

2. Unambiguous access to our rewards system.<br/>

3. Ability to review the progress of all your referrals through our system.<br/>

4. Use of a link back to Bombora Medical where you may, if required, find any further details as well as client instructions etc.<br/><br/>

We remind you that Bombora Medical offers an Australia wide service for pathology, paramedical, general practice medicals and specialist medicals with strong emphasis on home and workplace visits. We are medically supervised and accepted by all insurance companies.
<br/><br/>
Please telephone our office if you would like to discuss this.
<br/><br/>

Yours faithfully,<br/>
Health Predictions Team</body></html>";

         $headers = "From: Health Predictions <no-reply@healthpredictions.com.au>" . "\r\n";
         $headers .= 'Cc: '.$cc." \r\n";
         $headers .= "MIME-Version: 1.0\r\n";
         $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";    

        mail( $to, $subject, $message, $headers );


        return $this->db->insert_id();          
     }

     function deactivate_old_user($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$advisorID."";
          $query = $this->db->query($sql);
          $advisorInfo = $query->result_array();
          $sql2 = "UPDATE tbl_users SET 
               usr_status = 'D'
               WHERE userID = ".$advisorInfo[0]['userID']."";
          $query2 = $this->db->query($sql2);
   }
     /**
      *   Get Advisor detail and all advisor's point history
      *   @param advisorID    Identifying Advisor
      */
     function get_user_point_history($advisorID){
          
          # Get advisor name and total point
          $sql_info = "SELECT advisor_name, advisor_points FROM tbl_advisor WHERE advisorID = ".$advisorID."";
          $query1 = $this->db->query($sql_info);
          $data['info']=$query1->result_array();            # Calling it in view --> info[i]['DB_FIELD']
          
          # Get all point histories of the advisor
          $sql = "SELECT * FROM tbl_point_history WHERE advisorID=".$advisorID." ORDER BY pointID DESC";
          $query2 = $this->db->query($sql);
          $data['points'] = $query2->result_array();        # Calling it in view --> $points[i]['DB_FIELD']

          return $data;
     }  

     /**
      * Get advisor current points using its ID
      * @param advisorID Identifying advisor
      */
     function get_advisor_current_point($advisorID){
          $sql = "SELECT advisor_points FROM tbl_advisor WHERE advisorID=".$advisorID."";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     /**
      * Inserting both add and redeem record on the point history database
      * @param advisorID Identifying advisor
      * @param name      Point record Name
      * @param Points    Actual point Added/Redeemed
      * @param type      Add OR Redeem
      */
     function insert_history_points($advisorID, $name, $Points, $type){
          $sql = "INSERT INTO tbl_point_history(advisorID, pointName, pointChange, app_type, point_date) VALUES(".$advisorID.", '".$name."', ".$Points.", '".$type."', CURRENT_DATE);";
          $query = $this->db->query($sql);
     }
          
     /**
      * Updating advisor point after inserting in point history system
      */
     function update_advisor_points($id, $points){
          $sql = "UPDATE tbl_advisor SET advisor_points=".$points." WHERE advisorID=".$id."";
          $query = $this->db->query($sql);
     }

     /**
      * Error Prompt For Advisor Model
      * @param errormsg       Message print on error page
      * @param redirect_url   Next action to behave on the error page
      */
     public function error_prompt($errormsg, $redirect_url){

          $data['error_msg'] = $errormsg ;
          $data['redirect_url'] = $redirect_url;
          $header['breadcrumbs'][]    = array('title' => 'Advisors', 'link' => 'advisor/error' );
          $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
          $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
          $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
          $mainheader['title']        = "Error Page";
          $this->load->view('main_header', $mainheader);
          $this->load->view('main_bar', $header);
          $this->load->view('advisor/error', $data);
          $this->load->view('main_footer');   
        }
}