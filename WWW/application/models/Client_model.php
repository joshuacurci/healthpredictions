<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class client_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_client($clientID) {
          $sql = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."' AND client_deleted = 'N' AND client_active = 'Y'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_appointment_type($clientID){
          $sql = "SELECT appID, app_name, app_date, app_type FROM tbl_appointments WHERE clientID = '".$clientID."'";
          
          $query = $this->db->query($sql);

          return $query->result_array();
     }
     function timestamp_to_date($date){
          return date('d/m/Y', $date);
     }
     //get the username & password from tbl_usrs
     function get_path_report($clientID) {
          $sql = "SELECT * FROM tbl_client_path_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function completeClient($clientID) {
        $sqlCom = "UPDATE tbl_client SET 
        client_archived = 'Y'
        WHERE clientID = ".$clientID."";
        $this->db->query($sqlCom);

        $sql = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."'";
        $query = $this->db->query($sql);
        $values = $query->result_array();
        return array('assigned_advisor' => $values[0]['assigned_advisor'], 'insurance_company' => $values[0]['insurance_company']);
   }

     //get the username & password from tbl_usrs
     function get_referral_noti($clientID) {
          $sql = "SELECT * FROM tbl_client_referral_noti WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_to_ins_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_to_ins_completion_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_to_adv_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_to_adv_completion_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_client_consent_id_report($clientID) {
          $sql = "SELECT * FROM tbl_client_consent_id_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_apointment($clientID) {
          $sql = "SELECT * FROM tbl_appointments WHERE clientID = '".$clientID."' ORDER BY appID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_apointments($appID) {
          $sql = "SELECT * FROM tbl_appointments WHERE appID IN(".implode(",",$appID).") ORDER BY appID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_specific_apointment($appID) {
          $sql = "SELECT * FROM tbl_appointments WHERE appID = '".$appID."' ORDER BY appID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_history($clientID) {
          $sql = "SELECT * FROM tbl_client_contacthistory WHERE clientID = '".$clientID."' ORDER BY historyID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clienttest_info($clientID) {
          $sql = "SELECT * FROM tbl_client_tests WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clienttest_notes($clientID) {
          $sql = "SELECT * FROM tbl_client_testnotes WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_client_all() {
          $sql = "SELECT * FROM tbl_client WHERE client_deleted = 'N' AND client_active = 'Y' AND client_archived != 'Y' ORDER BY clientID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_archived() {
          $sql = "SELECT * FROM tbl_client WHERE client_deleted = 'N' AND client_active = 'Y' AND client_archived = 'Y' ORDER BY clientID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     function get_client_all_advisor($loginID) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE userID = ".$loginID." ORDER BY advisor_name";
          $query = $this->db->query($sql1);
          $advisorData = $query->result_array();
          $sql = "SELECT * FROM tbl_client WHERE assigned_advisor = '".$advisorData[0]['advisorID']."' AND client_deleted = 'N' ORDER BY client_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_insurance() {
          $sql = "SELECT * FROM tbl_insurance ORDER BY ins_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisors() {
          $sql = "SELECT * FROM tbl_advisor ORDER BY advisor_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_staff() {
          $sql = "SELECT * FROM tbl_staff ORDER BY staff_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_test_types() {
          $sql = "SELECT * FROM tbl_client_testtypes ORDER BY type_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_test_client($clientID) {
          $sql = "SELECT * FROM tbl_client_tests WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_path_report($clientID) {
          $sql = "SELECT clientID FROM tbl_client_path_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_noti_report($clientID) {
          $sql = "SELECT clientID FROM tbl_client_noti_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_referral_noti($clientID) {
          $sql = "SELECT clientID FROM tbl_client_referral_noti WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_client_info($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$clientdata['assigned_advisor']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();
          if($advisorData[0]['siteID']=='' || $advisorData[0]['siteID']=='0')
          {    
               $advisorData[0]['siteID']=$_SESSION['siteID'];
          }
          $time = time();
          $sql = "INSERT INTO tbl_client (client_name, client_sex, client_DOB, client_organization, siteID, client_address, client_address2, client_city, client_state, client_postcode, client_country, client_phone, client_mobile, client_fax, client_email, client_email2, client_notes, dateadded, insurance_number, insurance_company, assigned_advisor, assigned_staff) 
          VALUES 
          ('".$clientdata['client_name']."', '".$clientdata['client_sex']."', '".$clientdata['client_DOB']."', '".$clientdata['client_organization']."', '".$advisorData[0]['siteID']."', '".$clientdata['client_address']."', '".$clientdata['client_address2']."', '".$clientdata['client_city']."', '".$clientdata['client_state']."', '".$clientdata['client_postcode']."', '".$clientdata['client_country']."', '".$clientdata['client_phone']."', '".$clientdata['client_mobile']."', '".$clientdata['client_fax']."', '".$clientdata['client_email']."', '".$clientdata['client_email2']."', '".str_replace("'", "&acute;", $clientdata['client_notes'])."', '".$time."', '".$clientdata['insurance_number']."', '".$clientdata['insurance_company']."', '".$clientdata['assigned_advisor']."', '".$clientdata['assigned_staff']."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_info_referal($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$clientdata['assigned_advisor']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();
          
          $time = time();
          $sql = "INSERT INTO tbl_client (client_name, client_sex, client_DOB, client_organization, siteID, client_address, client_address2, client_city, client_state, client_postcode, client_country, client_phone, client_mobile, client_fax, client_email, client_email2, client_notes, dateadded, insurance_number, insurance_company, assigned_advisor, assigned_staff, client_active) 
          VALUES 
          ('".$clientdata['client_name']."', '".$clientdata['client_sex']."', '".$clientdata['client_DOB']."', '".$clientdata['client_organization']."', '".$advisorData[0]['siteID']."', '".$clientdata['client_address']."', '".$clientdata['client_address2']."', '".$clientdata['client_city']."', '".$clientdata['client_state']."', '".$clientdata['client_postcode']."', '".$clientdata['client_country']."', '".$clientdata['client_phone']."', '".$clientdata['client_mobile']."', '".$clientdata['client_fax']."', '".$clientdata['client_email']."', '".$clientdata['client_email2']."', '".str_replace("'", "&acute;", $clientdata['client_notes'])."', '".$time."', '".$clientdata['insurance_number']."', '".$clientdata['insurance_company']."', '".$clientdata['assigned_advisor']."', '".$clientdata['assigned_staff']."','N')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_path_report_info($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_path_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['HIV']."', '".$reportdata['instruct_taking_blood']."', '".$reportdata['client_consent_id']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_path_report_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_path_report (siteID, clientID, HIV, client_consent_id, path_tests_explained, client_authority_results)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_noti_info($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['HIV']."', '".$reportdata['instruct_taking_blood']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_ins_to_completion($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_ins_completion_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['HIV']."', '".$reportdata['instruct_taking_blood']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['letter_to_adv']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_adv_to_completion($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['HIV']."', '".$reportdata['instruct_taking_blood']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['letter_to_adv']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_id($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_consent_id_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_details']."', '".$reportdata['tests_ordered']."', '".$reportdata['client_ins_bombora']."', '".$reportdata['HIV']."', '".$reportdata['instruct_taking_blood']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_default($reportdata, $clientID, $siteID){
          $time = time();
           $sql = "INSERT INTO tbl_client_consent_id_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers, date_created)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_noti_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_referral_noti_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_referral_noti (siteID, clientID, HIV, instruct_taking_blood, client_consent_id)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_to_ins_completion_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_ins_completion_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_to_adv_completion_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_id_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, HIV, instruct_taking_blood, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     // Update General Practitioner info
     function edit_path_report_info ($reportdata) {
          $sql = "UPDATE tbl_client_path_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_noti_info ($reportdata) {
          $sql = "UPDATE tbl_client_noti_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_referral_noti ($reportdata) {
          $sql = "UPDATE tbl_client_referral_noti SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_noti_ins_to_completion ($reportdata) {
          $sql = "UPDATE tbl_client_noti_to_ins_completion_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          letter_to_adv = '".$reportdata['letter_to_adv']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_noti_adv_to_completion ($reportdata) {
          $sql = "UPDATE tbl_client_noti_to_adv_completion_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          letter_to_adv = '".$reportdata['letter_to_adv']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_consent_id ($reportdata) {
          $sql = "UPDATE tbl_client_consent_id_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          HIV = '".$reportdata['HIV']."',
          instruct_taking_blood = '".$reportdata['instruct_taking_blood']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          client_details = '".$reportdata['client_details']."',
          tests_ordered = '".$reportdata['tests_ordered']."',
          client_ins_bombora = '".$reportdata['client_ins_bombora']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     function new_notifications($clientID) {
          $sql = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '1')";
          $sql2 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '2')";
          $sql3 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '3')";
          $sql4 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '4')";
          $sql5 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '5')";
          $sql6 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '6')";
          $sql7 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '7')";
          $sql8 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '8')";

          $sql9 = "INSERT INTO tbl_client_tests (clientID) VALUES ('".$clientID."')";
          $sql10 = "INSERT INTO tbl_client_testnotes (clientID) VALUES ('".$clientID."')";
          
          $this->db->query($sql);
          $this->db->query($sql2);
          $this->db->query($sql3);
          $this->db->query($sql4);
          $this->db->query($sql5);
          $this->db->query($sql6);
          $this->db->query($sql7);
          $this->db->query($sql8);
          $this->db->query($sql9);
          $this->db->query($sql10);

     }
     
     function set_print_referalAcknowledgment ($clientID) {
        $sql7 = "UPDATE tbl_client_notifications SET print_date = '".time()."' WHERE clientID = '".$clientID."' AND typeID = '7'";
        $this->db->query($sql7);
     }

     function set_ind_print_notification ($clientID, $typeID) {
        $sql7 = "UPDATE tbl_client_notifications SET print_date = '".time()."' WHERE clientID = '".$clientID."' AND typeID = '".$typeID."'";
        $this->db->query($sql7);
     }

     function set_ind_email_notification ($clientID, $typeID) {
        $sql7 = "UPDATE tbl_client_notifications SET email_date = '".time()."' WHERE clientID = '".$clientID."' AND typeID = '".$typeID."'";
        $this->db->query($sql7);
     }

     // Update General Practitioner info
     function update_client_info ($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$clientdata['assigned_advisor']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();
          if($advisorData[0]['siteID']=='' || $advisorData[0]['siteID']=='0')
          {    
               $advisorData[0]['siteID']=$_SESSION['siteID'];
          }

          $sql = "UPDATE tbl_client SET 
          client_name = '".$clientdata['client_name']."',
          client_DOB = '".$clientdata['client_DOB']."',
          client_sex = '".$clientdata['client_sex']."',
          client_organization = '".$clientdata['client_organization']."',
          siteID = '".$advisorData[0]['siteID']."',
          client_address = '".$clientdata['client_address']."',
          client_address2 = '".$clientdata['client_address2']."',
          client_city = '".$clientdata['client_city']."',
          client_state = '".$clientdata['client_state']."',
          client_postcode = '".$clientdata['client_postcode']."',
          client_country = '".$clientdata['client_country']."',
          client_phone = '".$clientdata['client_phone']."',
          client_mobile = '".$clientdata['client_mobile']."',
          client_fax = '".$clientdata['client_fax'].    "',
          client_email = '".$clientdata['client_email']."',
          client_email2 = '".$clientdata['client_email2']."',
          insurance_company = '".$clientdata['insurance_company']."',
          insurance_number = '".$clientdata['insurance_number']."',
          assigned_advisor = '".$clientdata['assigned_advisor']."',
          assigned_staff = '".$clientdata['assigned_staff']."',
          client_notes = '".str_replace("'", "&#039;",$clientdata['client_notes'])."'
          WHERE clientID = ".$clientdata['clientID']."";
          $query = $this->db->query($sql);
     }

     function search_client($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' AND client_active = 'Y' AND client_archived != 'Y' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_archived($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' AND client_active = 'Y' AND client_archived = 'Y' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_advisor($searchData, $loginID) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$loginID."'";
          $query = $this->db->query($sql1);
          $advisorData = $query->result_array();

          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' AND assigned_advisor = '".$advisorData[0]['advisorID']."' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_all($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE 
          client_name LIKE '%".$searchData['client_name']."%' 
          AND client_organization  LIKE '%".$searchData['client_organization']."%'
          AND client_address LIKE '%".$searchData['client_address']."%'
          AND client_address2 LIKE '%".$searchData['client_address2']."%'
          AND client_city LIKE '%".$searchData['client_city']."%'
          AND client_postcode LIKE '%".$searchData['client_postcode']."%'
          AND client_state LIKE '%".$searchData['client_state']."%' 
          AND client_country LIKE '%".$searchData['client_country']."%' 
          AND client_phone LIKE '%".$searchData['client_phone']."%' 
          AND client_mobile LIKE '%".$searchData['client_mobile']."%' 
          AND client_fax LIKE '%".$searchData['client_fax']."%' 
          AND client_email LIKE '%".$searchData['client_email']."%' 
          AND client_email2 LIKE '%".$searchData['client_email2']."%' 
          AND client_sex LIKE '%".$searchData['client_sex']."%' 
          AND client_workphone LIKE '%".$searchData['client_workphone']."%' 
          AND client_deleted = 'N' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE client_city LIKE '%".$searchData['client_city']."%' ORDER BY client_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE client_state LIKE '%".$searchData['client_state']."%' ORDER BY client_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_notifications($clientID, $typeID) {
          $sql = "SELECT * FROM tbl_client_notifications WHERE clientID = '".$clientID."' AND typeID = '".$typeID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function reactivate_client_info($clientID) {
          $sql = "UPDATE tbl_client SET 
          client_archived = 'N'
          WHERE clientID = ".$clientID."";
          $query = $this->db->query($sql);
     }
     function exist_appointment_type($appType){
               $sql = "SELECT app_score FROM tbl_appointment_type WHERE app_type = '".$appType."'";
               $query = $this->db->query($sql);
          return $query->result_array();
     }
     function get_appointment_score($app_type){
          $sql = "SELECT app_score FROM tbl_appointment_type WHERE app_type='".$app_type."'";
          $query = $this->db->query($sql);
          $result = $query->result_array();
          $result = $result[0];
          #var_dump($app_type);
          if (sizeof($result)==0){
               
               $result = array('app_score'=>'1');
          }
          
          return $result;
     }

     function get_appointment_name_date_type($appID){
          
          $sql = "SELECT app_name, app_date,app_type FROM tbl_appointments WHERE appID = '".$appID."'";
          $query = $this->db->query($sql);
          $result = $query->result_array();
          $result = $result[0];
          
          #var_dump($result);
          $result['app_date'] = date('Y-m-d', $result['app_date']);


          return $result;
     }

     function get_advisor_id ($clientID){
          $sql = "SELECT assigned_advisor FROM tbl_client WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          $result = $query->result_array();
          return $result[0]['assigned_advisor'];

     }

     function insert_point_history($pointName, $clientID, $type, $advisorID, $pointChange, $result_date){
          
          $sql = "INSERT INTO tbl_point_history(pointName, clientID, advisorID, app_type, pointChange, point_date) VALUES('".$pointName."','".$clientID."','".$type."','".$advisorID."','".$pointChange."','".$result_date."')";
          #var_dump($clientID, $type);
          // var_dump($sql);
          // die();
   
          $query = $this->db->query($sql);
     }

     function update_client_notify ($clientdata) {

          if (array_key_exists('not1', $clientdata)) {
               $sql1 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not1'])) { $sql1 .= "print_date = '".strtotime($clientdata['not1']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not1'])) { $sql1 .= "email_date = '".strtotime($clientdata['not1']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not1'])) { $sql1 .= "fax_date = '".strtotime($clientdata['not1']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not1'])) { $sql1 .= "phone_date = '".strtotime($clientdata['not1']['phone_date'])."',";}; 
               $sql1 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '1'";

               $query = $this->db->query($sql1);
          }

          if (array_key_exists('not2', $clientdata)) {
               $sql2 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not2'])) { $sql2 .= "print_date = '".strtotime($clientdata['not2']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not2'])) { $sql2 .= "email_date = '".strtotime($clientdata['not2']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not2'])) { $sql2 .= "fax_date = '".strtotime($clientdata['not2']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not2'])) { $sql2 .= "phone_date = '".strtotime($clientdata['not2']['phone_date'])."',";}; 
               $sql2 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '2'";

               $query = $this->db->query($sql2);
          }

          if (array_key_exists('not3', $clientdata)) {
               $sql3 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not3'])) { $sql3 .= "print_date = '".strtotime($clientdata['not3']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not3'])) { $sql3 .= "email_date = '".strtotime($clientdata['not3']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not3'])) { $sql3 .= "fax_date = '".strtotime($clientdata['not3']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not3'])) { $sql3 .= "phone_date = '".strtotime($clientdata['not3']['phone_date'])."',";}; 
               $sql3 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '3'";

               $query = $this->db->query($sql3);
          }

          if (array_key_exists('not4', $clientdata)) {
               $sql4 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not4'])) { $sql4 .= "print_date = '".strtotime($clientdata['not4']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not4'])) { $sql4 .= "email_date = '".strtotime($clientdata['not4']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not4'])) { $sql4 .= "fax_date = '".strtotime($clientdata['not4']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not4'])) { $sql4 .= "phone_date = '".strtotime($clientdata['not4']['phone_date'])."',";}; 
               $sql4 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '4'";

               $query = $this->db->query($sql4);
          }

          if (array_key_exists('not5', $clientdata)) {
               $sql5 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not5'])) { $sql5 .= "print_date = '".strtotime($clientdata['not5']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not5'])) { $sql5 .= "email_date = '".strtotime($clientdata['not5']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not5'])) { $sql5 .= "fax_date = '".strtotime($clientdata['not5']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not5'])) { $sql5 .= "phone_date = '".strtotime($clientdata['not5']['phone_date'])."',";}; 
               $sql5 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '5'";

               $query = $this->db->query($sql5);
          }

          if (array_key_exists('not6', $clientdata)) {
               $sql6 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not6'])) { $sql6 .= "print_date = '".strtotime($clientdata['not6']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not6'])) { $sql6 .= "email_date = '".strtotime($clientdata['not6']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not6'])) { $sql6 .= "fax_date = '".strtotime($clientdata['not6']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not6'])) { $sql6 .= "phone_date = '".strtotime($clientdata['not6']['phone_date'])."',";}; 
               $sql6 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '6'";

               $query = $this->db->query($sql6);
          }

          if (array_key_exists('not7', $clientdata)) {
               $sql7 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not7'])) { $sql7 .= "print_date = '".strtotime($clientdata['not7']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not7'])) { $sql7 .= "email_date = '".strtotime($clientdata['not7']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not7'])) { $sql7 .= "fax_date = '".strtotime($clientdata['not7']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not7'])) { $sql7 .= "phone_date = '".strtotime($clientdata['not7']['phone_date'])."',";}; 
               $sql7 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '7'";

               $query = $this->db->query($sql7);
          }

          if (array_key_exists('not8', $clientdata)) {
               $sql8 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not8'])) { $sql8 .= "print_date = '".strtotime($clientdata['not8']['print_date'])."',";}; 
               if (array_key_exists('email_date', $clientdata['not8'])) { $sql8 .= "email_date = '".strtotime($clientdata['not8']['email_date'])."',";}; 
               if (array_key_exists('fax_date', $clientdata['not8'])) { $sql8 .= "fax_date = '".strtotime($clientdata['not8']['fax_date'])."',";}; 
               if (array_key_exists('phone_date', $clientdata['not8'])) { $sql8 .= "phone_date = '".strtotime($clientdata['not8']['phone_date'])."',";}; 
               $sql8 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '8'";

               $query = $this->db->query($sql8);
          }
     }

     function update_client_tests ($clientdata) {
          array_shift($clientdata["checkedid"]);
          $this->db->query("DELETE FROM tbl_client_tests WHERE clientID = '".$clientdata['clientID']."';");
          foreach($clientdata["checkedid"] as $id)
          {
            $sql = "INSERT INTO tbl_client_tests (clientID, testtypeID) VALUES ('".$clientdata['clientID']."', '".$id."')";
          $query = $this->db->query($sql);
          $sql2 = "UPDATE tbl_client_testnotes SET 
          testNote = '".str_replace("'", "&#039;",$clientdata['testNote'])."'
          WHERE clientID = ".$clientdata['clientID']."";
          $query2 = $this->db->query($sql2);
          }
          // $ids = implode(",",$clientdata["checkedid"]);
     }
          # Updating advisor point value
          function update_advisor_points($id, $points){
               $sql = "UPDATE tbl_advisor SET advisor_points=$points WHERE advisorID=".$id."";

               $query = $this->db->query($sql);
               
          }
          function get_advisor_current_point($advisorID){
               $sql = "SELECT advisor_points FROM tbl_advisor WHERE advisorID=".$advisorID."";
               $query = $this->db->query($sql);
               return $query->result_array();
          }

     
}
?>