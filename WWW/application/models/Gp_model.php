<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gp_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_gp($GPID) {
          $sql = "SELECT * FROM tbl_GP WHERE GPID = '".$GPID."' AND GP_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clinic($clinicID) {
          $sql = "SELECT * FROM tbl_GP_clinic WHERE clinicID = '".$clinicID."' AND clinic_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the all the GP
     function get_gp_all() {
          $sql = "SELECT * FROM tbl_GP WHERE GP_active = '1' AND GP_deleted = 'N' ORDER BY GP_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get all the indivial GP
     function get_gp_indigp() {
          $sql = "SELECT * FROM tbl_GP WHERE GP_active = '1' AND GP_deleted = 'N' AND GP_type = '1' ORDER BY GP_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get all the GP that belongs to a clinic
     function get_gp_clinicgp() {
          $sql = "SELECT * FROM tbl_GP WHERE GP_active = '1' AND GP_deleted = 'N' AND GP_type = '0' ORDER BY GP_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get all the GP that belongs to a clinic
     function get_gp_clinic_all() {
          $sql = "SELECT * FROM tbl_GP_clinic WHERE clinic_deleted = 'N' ORDER BY clinic_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     // function get_gp_clinic() {
     //      $sql = "SELECT * FROM tbl_gp ORDER BY GP_address ASC";
     //      $query = $this->db->query($sql);
     //      return $query->result_array();
     // }

     // Add new General Practitioner
     function new_gp_info($gpdata) {
          $sql = "INSERT INTO tbl_GP (GP_name, siteID, GP_clinic, GP_clinic2, GP_clinic3, GP_clinic4, GP_clinic5, GP_clinic6, GP_clinic7, GP_clinic8, GP_address, GP_address2, GP_city, GP_state, GP_postcode,GP_country, GP_phone, GP_workphone, GP_mobile, GP_fax, GP_email, GP_email2, GP_responsibleperson, GP_instructions, GP_comments ) 
          VALUES 
          ('".$gpdata['GP_name']."','".$gpdata['siteID']."', '".$gpdata['GP_clinic']."', '".$gpdata['GP_clinic2']."', '".$gpdata['GP_clinic3']."', '".$gpdata['GP_clinic4']."', '".$gpdata['GP_clinic5']."', '".$gpdata['GP_clinic6']."', '".$gpdata['GP_clinic7']."', '".$gpdata['GP_clinic8']."', '".$gpdata['GP_address']."', '".$gpdata['GP_address2']."', '".$gpdata['GP_city']."','".$gpdata['GP_state']."','".$gpdata['GP_postcode']."','".$gpdata['GP_country']."','".$gpdata['GP_phone']."','".$gpdata['GP_workphone']."','".$gpdata['GP_mobile']."','".$gpdata['GP_fax']."','".$gpdata['GP_email']."','".$gpdata['GP_email2']."','".$gpdata['GP_responsibleperson']."','".$gpdata['GP_instructions']."','".$gpdata['GP_comments']."')";
          $query = $this->db->query($sql);
     }

     // Add new General Practitioner Clinic Info
     function new_gpclinic_info($gpdata) {
          $sql = "INSERT INTO tbl_GP_clinic (clinic_name, siteID, clinic_address, clinic_city, clinic_state, clinic_postcode, clinic_country, clinic_phone, clinic_mobile,clinic_fax, clinic_email, clinic_email2, clinic_responsibleperson, clinic_instructions, clinic_notes ) 
          VALUES 
          ('".$gpdata['clinic_name']."','".$gpdata['siteID']."', '".$gpdata['clinic_address']."', '".$gpdata['clinic_city']."', '".$gpdata['clinic_state']."', '".$gpdata['clinic_postcode']."', '".$gpdata['clinic_country']."', '".$gpdata['clinic_phone']."', '".$gpdata['clinic_mobile']."', '".$gpdata['clinic_fax']."', '".$gpdata['clinic_email']."', '".$gpdata['clinic_email2']."', '".$gpdata['clinic_responsibleperson']."', '".$gpdata['clinic_instructions']."', '".$gpdata['clinic_notes']."')";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_gp_info ($gpdata) {
          $sql = "UPDATE tbl_GP SET 
          GP_name = '".$gpdata['GP_name']."',
          siteID = '".$gpdata['siteID']."',
          GP_address = '".$gpdata['GP_address']."',
          GP_address2 = '".$gpdata['GP_address2']."',
          GP_city = '".$gpdata['GP_city']."',
          GP_state = '".$gpdata['GP_state']."',
          GP_postcode = '".$gpdata['GP_postcode']."',
          GP_country = '".$gpdata['GP_country']."',
          GP_phone = '".$gpdata['GP_phone']."',
          GP_workphone = '".$gpdata['GP_workphone']."',
          GP_mobile = '".$gpdata['GP_mobile']."',
          GP_fax = '".$gpdata['GP_fax'].    "',
          GP_email = '".$gpdata['GP_email']."',
          GP_email2 = '".$gpdata['GP_email2']."',
          GP_responsibleperson = '".$gpdata['GP_responsibleperson']."',
          GP_instructions = '".$gpdata['GP_instructions']."',
          GP_comments = '".$gpdata['GP_comments']."',
          GP_clinic = '".$gpdata['GP_clinic']."',
          GP_clinic2 = '".$gpdata['GP_clinic2']."',
          GP_clinic3 = '".$gpdata['GP_clinic3']."',
          GP_clinic4 = '".$gpdata['GP_clinic4']."',
          GP_clinic5 = '".$gpdata['GP_clinic5']."',
          GP_clinic6 = '".$gpdata['GP_clinic6']."',
          GP_clinic7 = '".$gpdata['GP_clinic7']."',
          GP_clinic8 = '".$gpdata['GP_clinic8']."'
          WHERE GPID = ".$gpdata['GPID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner Clinic info
     function update_gpclinic_info ($gpdata) {
          $sql = "UPDATE tbl_GP_clinic SET 
          clinic_name = '".$gpdata['clinic_name']."',
          siteID = '".$gpdata['siteID']."',
          clinic_address = '".$gpdata['clinic_address']."',
          clinic_city = '".$gpdata['clinic_city']."',
          clinic_state = '".$gpdata['clinic_state']."',
          clinic_postcode = '".$gpdata['clinic_postcode']."',
          clinic_country = '".$gpdata['clinic_country']."',
          clinic_phone = '".$gpdata['clinic_phone']."',
          clinic_mobile = '".$gpdata['clinic_mobile']."',
          clinic_fax = '".$gpdata['clinic_fax'].    "',
          clinic_email = '".$gpdata['clinic_email']."',
          clinic_email2 = '".$gpdata['clinic_email2']."',
          clinic_responsibleperson = '".$gpdata['clinic_responsibleperson']."',
          clinic_instructions = '".$gpdata['clinic_instructions']."',
          clinic_notes = '".$gpdata['clinic_notes']."'
          WHERE clinicID = ".$gpdata['clinicID']."";
          $query = $this->db->query($sql);
     }

     function search_gp($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE GP_name LIKE '%".$searchData['GP_name']."%' AND GP_city LIKE '%".$searchData['GP_city']."%' AND GP_state LIKE '%".$searchData['GP_state']."%' AND GP_deleted = 'N' ORDER BY GP_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_gp_all($searchData) {
         if ($searchData['GP_workphone']=='')
         {
            $workphone="AND (GP_workphone LIKE '%".$searchData['GP_workphone']."%' OR GP_workphone IS NULL)";
         }
         else
         {
            $workphone="AND GP_workphone LIKE '%".$searchData['GP_workphone']."%'";
         }
         if ($searchData['GP_email2']=='')
         {
            $email2="AND (GP_email2 LIKE '%".$searchData['GP_email2']."%' OR GP_email2 IS NULL)";
         }
        else
        {
            $email2="AND GP_email2 LIKE '%".$searchData['GP_email2']."%'";
        }
          $sql = "SELECT * FROM tbl_GP WHERE GP_name LIKE '%".$searchData['GP_name']."%' 
          AND GP_type LIKE '%".$searchData['GP_type']."%'
          AND GP_clinic LIKE '%".$searchData['GP_clinic']."%'
          AND GP_address LIKE '%".$searchData['GP_address']."%'
          AND GP_city LIKE '%".$searchData['GP_city']."%'
          AND GP_postcode LIKE '%".$searchData['GP_postcode']."%'
          AND GP_state LIKE '%".$searchData['GP_state']."%' 
          AND GP_country LIKE '%".$searchData['GP_country']."%' 
          AND GP_phone LIKE '%".$searchData['GP_phone']."%' 
          AND GP_mobile LIKE '%".$searchData['GP_mobile']."%'".$workphone."
          AND GP_fax LIKE '%".$searchData['GP_fax']."%' 
          AND GP_email LIKE '%".$searchData['GP_email']."%'".$email2."
          AND GP_responsibleperson LIKE '%".$searchData['GP_responsibleperson']."%' 
          AND GP_deleted = 'N' ORDER BY GP_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_gp_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE GP_city LIKE '%".$searchData['GP_city']."%' ORDER BY GP_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_gp_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE GP_state LIKE '%".$searchData['GP_state']."%' ORDER BY GP_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_gp_clinicall() {
          $sql = "SELECT * FROM tbl_GP_clinic ORDER BY clinicID ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the states from tbl_states
     function get_gp_clinic($clinicID) {
          $sql = "SELECT * FROM tbl_GP_clinic WHERE clinicID = '".$clinicID."' ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_gp_info($GPID) {
          $sql = "UPDATE tbl_GP SET
          GP_deleted = 'Y'
          WHERE GPID = '".$GPID."'";
          $query = $this->db->query($sql);
     }

     function delete_gpclinic_info($clinicID) {
          $sql = "UPDATE tbl_GP_clinic SET
          clinic_deleted = 'Y'
          WHERE clinicID = '".$clinicID."'";
          $query = $this->db->query($sql);
     }

     
}?>