<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class insurance_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_insurance($insID) {
          $sql = "SELECT * FROM tbl_insurance WHERE insID = '".$insID."' AND ins_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_insurance_all() {
          $sql = "SELECT * FROM tbl_insurance WHERE ins_deleted = 'N' ORDER BY ins_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_insurance_info($insurancedata) {
          $time = time();
          $sql = "INSERT INTO tbl_insurance (ins_name, siteID, ins_address, ins_address2, ins_city, ins_state, ins_postcode, ins_country, ins_phone, ins_fax, ins_email, ins_email2, ins_responsibleperson, ins_notes, ins_ins4results, ins_contactname, ins_contactemail, ins_contactphone, ins_underwritingcontract, dateadded) 
          VALUES 
          ('".$insurancedata['ins_name']."', '".$insurancedata['siteID']."', '".$insurancedata['ins_address']."', '".$insurancedata['ins_address2']."', '".$insurancedata['ins_city']."', '".$insurancedata['ins_state']."', '".$insurancedata['ins_postcode']."', '".$insurancedata['ins_country']."', '".$insurancedata['ins_phone']."', '".$insurancedata['ins_fax']."', '".$insurancedata['ins_email']."', '".$insurancedata['ins_email2']."', '".$insurancedata['ins_responsibleperson']."', '".$insurancedata['ins_notes']."', '".$insurancedata['ins_ins4results']."', '".$insurancedata['ins_contactname']."', '".$insurancedata['ins_contactemail']."', '".$insurancedata['ins_contactphone']."', '".$insurancedata['ins_underwritingcontract']."', '".$time."')";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_insurance_info ($insurancedata) {
          $sql = "UPDATE tbl_insurance SET 
          ins_name = '".$insurancedata['ins_name']."',
          siteID = '".$insurancedata['siteID']."',
          ins_address = '".$insurancedata['ins_address']."',
          ins_address2 = '".$insurancedata['ins_address2']."',
          ins_city = '".$insurancedata['ins_city']."',
          ins_state = '".$insurancedata['ins_state']."',
          ins_postcode = '".$insurancedata['ins_postcode']."',
          ins_country = '".$insurancedata['ins_country']."',
          ins_phone = '".$insurancedata['ins_phone']."',
          ins_fax = '".$insurancedata['ins_fax'].    "',
          ins_email = '".$insurancedata['ins_email']."',
          ins_responsibleperson = '".$insurancedata['ins_responsibleperson']."',
          ins_notes = '".$insurancedata['ins_notes']."',
          ins_contactname = '".$insurancedata['ins_contactname']."',
          ins_contactemail = '".$insurancedata['ins_contactemail']."',
          ins_contactphone = '".$insurancedata['ins_contactphone']."',
          ins_underwritingcontract = '".$insurancedata['ins_underwritingcontract']."',
          ins_ins4results = '".$insurancedata['ins_ins4results']."'
          WHERE insID = ".$insurancedata['insID']."";
          $query = $this->db->query($sql);
     }

     function search_insurance($searchData) {
          $sql = "SELECT * FROM tbl_insurance WHERE ins_name LIKE '%".$searchData['ins_name']."%' AND ins_city LIKE '%".$searchData['ins_city']."%' AND ins_state LIKE '%".$searchData['ins_state']."%' AND ins_deleted = 'N' ORDER BY ins_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_insurance_all($searchData) {

        if ($searchData['ins_address2']=='')
        {
           $address2="AND (ins_address2 LIKE '%".$searchData['ins_address2']."%' OR ins_address2 IS NULL)";
        }
        else
        {
           $address2="AND ins_address2 LIKE '%".$searchData['ins_address2']."%'";
        }
        if ($searchData['ins_email2']=='')
        {
           $email2="AND (ins_email2 LIKE '%".$searchData['ins_email2']."%' OR ins_email2 IS NULL)";
        }
       else
       {
           $email2="AND ins_email2 LIKE '%".$searchData['ins_email2']."%'";
       }


          $sql = "SELECT * FROM tbl_insurance WHERE 
          ins_name LIKE '%".$searchData['ins_name']."%' 
          AND ins_address LIKE '%".$searchData['ins_address']."%'".$address2."
          AND ins_city LIKE '%".$searchData['ins_city']."%'
          AND ins_postcode LIKE '%".$searchData['ins_postcode']."%'
          AND ins_state LIKE '%".$searchData['ins_state']."%' 
          AND ins_country LIKE '%".$searchData['ins_country']."%' 
          AND ins_phone LIKE '%".$searchData['ins_phone']."%' 
          AND ins_fax LIKE '%".$searchData['ins_fax']."%' 
          AND ins_email LIKE '%".$searchData['ins_email']."%'".$email2."
          AND ins_deleted = 'N' ORDER BY ins_name";

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_ins_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE ins_city LIKE '%".$searchData['ins_city']."%' ORDER BY ins_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_ins_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE ins_state LIKE '%".$searchData['ins_state']."%' ORDER BY ins_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_insurance_info($insID) {
          $sql = "UPDATE tbl_insurance SET 
          ins_deleted = 'Y'
          WHERE insID = ".$insID."";
          $query = $this->db->query($sql);
     }

     
}?>