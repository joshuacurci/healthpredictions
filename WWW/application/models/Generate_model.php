<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Generate_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_appointments() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // REFERRAL

     function check_appform_referral($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '1' ";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '1' AND appID IN(".implode(",",$appID).")";
          }
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function referral_acknowledgement($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
               $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', ".$appID.", '1')";
               $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function referral_acknowledgement_update($postdata) {
          $sql = "UPDATE tbl_forms_generated SET 
          clientID = '".$postdata['clientID']."',
          appID = '".$postdata['appID']."' WHERE (clientID = '".$postdata['clientID']."' AND appID = '".$postdata['appID']."'AND formID = '1')";
          $query = $this->db->query($sql);
     }

     // PATHOLOGY

     function check_appform_pathology($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '2'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '2' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_path($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '2')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_path_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '2' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '2')";
          $query = $this->db->query($sql);
          }
     }

     // DOCTOR

     function check_appform_doctor($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '3' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '3' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_doctor($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '3')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_doctor_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '3' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '3')";
          $query = $this->db->query($sql);
          }
     }

     // CLIENT

     function check_appform_client($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '4' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '4' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_client($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '4')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_client_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '4' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '4')";
          $query = $this->db->query($sql);
          }
     }

     // ADVISOR

     function check_appform_advisor($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '5' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '5' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_advisor($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '5')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_advisor_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '5' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '5')";
          $query = $this->db->query($sql);
          }
     }

     // ADVISOR COMPLETION

     function check_appform_adv_completion($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '6' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '6' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_adv_completion($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '6')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_adv_completion_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '6' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '6')";
          $query = $this->db->query($sql);
          }
     }

     // INSURANCE

     function check_appform_insurance($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '7' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '7' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function noti_to_insurance($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '7')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function noti_to_insurance_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '7' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '7')";
          $query = $this->db->query($sql);
          }
     }

     // Client Consent ID

     function check_appform_clientConsentID($clientID, $appID) {
          if($appID==NULL)
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '8' AND appID = '".$appID."'";
          }
          else
          {
               $sql = "SELECT * FROM tbl_forms_generated WHERE clientID = '".$clientID."' AND formID = '8' AND appID IN(".implode(",",$appID).")";
          }

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function client_consent_id($postdata) {
          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '8')";
          $query = $this->db->query($sql);
          }
          return $this->db->insert_id();
     }

     function client_consent_id_update($postdata) {
          $sql = "DELETE FROM tbl_forms_generated WHERE formID = '8' AND
          clientID = '".$postdata['clientID']."'";
          $query = $this->db->query($sql);

          foreach ($postdata['appID'] as $appID)
          {
          $sql = "INSERT INTO tbl_forms_generated (clientID, appID, formID) VALUES ('".$postdata['clientID']."', '".$appID."', '8')";
          $query = $this->db->query($sql);
          }
     }

     // EXTRA

     function search_by_test($postdata) {
         $testTypesSelected = implode(', ', $postdata['checkedid']);
         $sql = 'Select * 
                  FROM `tbl_client`
                  LEFT JOIN `tbl_client_tests`
                    ON `tbl_client`.`clientID` = `tbl_client_tests`.`clientID`
                  WHERE `tbl_client_tests`.`testtypeID` IN ('.$testTypesSelected.')';
         $query = $this->db->query($sql);
         return $query->result_array();
    }



function search_between_date($startdate, $lastdate) {
     $sql = "SELECT * FROM tbl_appointments WHERE app_date between '".$startdate."' AND '".$lastdate."' AND app_deleted = 'N' ORDER BY app_date";
     $query = $this->db->query($sql);
     return $query->result_array();
}
}

?>