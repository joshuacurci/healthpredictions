<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class global_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_site_info($siteURL) {
          $sql = "SELECT * FROM tbl_sites WHERE site_domain = '".$siteURL."'";
          //$sql = "SELECT * FROM tbl_sites";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_site_infoID($siteID) {
          $sql = "SELECT * FROM tbl_sites WHERE siteID = '".$siteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_type_data() {
          $sql = "SELECT * FROM tbl_usertypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     //get the country from tbl_country
     function get_all_country() {
          $sql = "SELECT * FROM tbl_country ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

          //get the states from tbl_states
     function get_all_austates() {
          $sql = "SELECT * FROM tbl_au_states ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }    


     
}?>