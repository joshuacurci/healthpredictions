<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_referral_num() {
          $sql = "SELECT * FROM tbl_referral WHERE client_active = 'N' and client_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->num_rows();
     }

     function get_staff_refferals() {

        $finalAdvisors = array();
        $now = time();
        $beginning_of_week = strtotime('last Monday', $now); // Gives you the time at the BEGINNING of the week
        $end_of_week = strtotime('next Sunday', $now) + 86400; // Gives you the time at the END of the last day of the week

        $beginning_of_last_week = strtotime('-7 days', $beginning_of_week); // Gives you the time at the BEGINNING of the week
        $end_of_last_week = strtotime('+7 days', $beginning_of_last_week);

        $sql = "SELECT * FROM tbl_advisor";
        $query = $this->db->query($sql);
        $advisors = $query->result_array();

        foreach ($advisors as $filterAdd) {
            $finalAdvisors[$filterAdd['advisorID']] = array('name' => $filterAdd['advisor_name'], 'number' => 0, 'lastweekNum' => 0);
        }

        $sqlReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_week."' AND '".$end_of_week."'";
        $queryReff = $this->db->query($sqlReff);
        $allThisReff = $queryReff->result_array();

        $sqllwReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_last_week."' AND '".$end_of_last_week."'";
        $querylwReff = $this->db->query($sqllwReff);
        $allLwReff = $querylwReff->result_array();

        foreach ($allThisReff as $reffData) {
            $finalAdvisors[$reffData['assigned_advisor']]['number'] = $finalAdvisors[$reffData['assigned_advisor']]['number'] + 1;
        }

        foreach ($allLwReff as $reffLwData) {
            $finalAdvisors[$reffLwData['assigned_advisor']]['lastweekNum'] = $finalAdvisors[$reffLwData['assigned_advisor']]['lastweekNum'] + 1;
        }

        foreach ($finalAdvisors as $key => $row) {
            $number[$key] = $row['number'];
        }
        
        array_multisort($number, SORT_DESC, $finalAdvisors);

        return $finalAdvisors;

     }

     function get_JV_refferals_week() {

        $finalAdvisors = array();
        $now = time();
        $beginning_of_week = strtotime('last Monday', $now); // Gives you the time at the BEGINNING of the week
        $end_of_week = strtotime('next Sunday', $now) + 86400; // Gives you the time at the END of the last day of the week

        $beginning_of_last_week = strtotime('-7 days', $beginning_of_week); // Gives you the time at the BEGINNING of the week
        $end_of_last_week = strtotime('+7 days', $beginning_of_last_week);

        $sql = "SELECT * FROM tbl_insurance";
        $query = $this->db->query($sql);
        $advisors = $query->result_array();

        foreach ($advisors as $filterAdd) {
            $finalAdvisors[$filterAdd['insID']] = array('name' => $filterAdd['ins_name'], 'number' => 0, 'lastweekNum' => 0);
        }


        $sqlReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_week."' AND '".$end_of_week."'";
        $queryReff = $this->db->query($sqlReff);
        $allThisReff = $queryReff->result_array();

        $sqllwReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_last_week."' AND '".$end_of_last_week."'";
        $querylwReff = $this->db->query($sqllwReff);
        $allLwReff = $querylwReff->result_array();

        foreach ($allThisReff as $reffData) {
            $finalAdvisors[$reffData['insurance_company']]['number'] = $finalAdvisors[$reffData['insurance_company']]['number'] + 1;
        }

        foreach ($allLwReff as $reffLwData) {
            $finalAdvisors[$reffLwData['insurance_company']]['lastweekNum'] = $finalAdvisors[$reffLwData['insurance_company']]['lastweekNum'] + 1;
        }

        foreach ($finalAdvisors as $key => $row) {
            $number[$key] = $row['number'];
        }
        
        array_multisort($number, SORT_DESC, $finalAdvisors);

        return $finalAdvisors;

     }

     function get_JV_refferals_year() {

        $finalAdvisors = array();
        $now = time();
        $beginning_of_week = strtotime('first day of January', $now); // Gives you the time at the BEGINNING of the week
        $end_of_week = strtotime('last day of December', $now) + 86400; // Gives you the time at the END of the last day of the week

        $beginning_of_last_week = strtotime('-1 year', $beginning_of_week); // Gives you the time at the BEGINNING of the week
        $end_of_last_week = strtotime('+1 year', $beginning_of_last_week);

        $sql = "SELECT * FROM tbl_insurance";
        $query = $this->db->query($sql);
        $advisors = $query->result_array();

        foreach ($advisors as $filterAdd) {
            $finalAdvisors[$filterAdd['insID']] = array('name' => $filterAdd['ins_name'], 'number' => 0, 'lastweekNum' => 0);
        }

        $sqlReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_week."' AND '".$end_of_week."'";
        $queryReff = $this->db->query($sqlReff);
        $allThisReff = $queryReff->result_array();

        $sqllwReff = "SELECT * FROM tbl_referral WHERE dateadded BETWEEN '".$beginning_of_last_week."' AND '".$end_of_last_week."'";
        $querylwReff = $this->db->query($sqllwReff);
        $allLwReff = $querylwReff->result_array();

        foreach ($allThisReff as $reffData) {
            $finalAdvisors[$reffData['insurance_company']]['number'] = $finalAdvisors[$reffData['insurance_company']]['number'] + 1;
        }

        foreach ($allLwReff as $reffLwData) {
            $finalAdvisors[$reffLwData['insurance_company']]['lastweekNum'] = $finalAdvisors[$reffLwData['insurance_company']]['lastweekNum'] + 1;
        }

        foreach ($finalAdvisors as $key => $row) {
            $number[$key] = $row['number'];
        }
        
        array_multisort($number, SORT_DESC, $finalAdvisors);

        return $finalAdvisors;

     }

     
}?>