<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class appointment_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_client_appointment($appID) {
          $sql = "SELECT * FROM tbl_appointments WHERE appID = '".$appID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     //get the username & password from tbl_usrs
     function get_all_client_appointment() {
          $sql = "SELECT * FROM tbl_appointments";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_client_appointment($appdata) {
          $time = time();
          $sql = "INSERT INTO tbl_appointments (
               siteID, 
               clientID, 
               app_name, 
               app_time,
               app_date,
               app_place,
               app_service,
               app_timestamp,
               app_type) 
          VALUES 
          ('".$appdata['siteID']."', 
               '".$appdata['clientID']."', 
               '".$appdata['app_name']."', 
               '".$appdata['app_time']."', 
               '".strtotime($appdata['app_date'])."',
               '".$appdata['app_place']."',
               '".$appdata['app_service']."',
               '".$time."',
               '".$appdata['app_type']."')";
          $query = $this->db->query($sql);

          return $appdata['clientID'];
     }

     function update_client_appointment ($appdata) {
          $sql = "UPDATE tbl_appointments SET 
          app_name = '".$appdata['app_name']."',
          app_time = '".$appdata['app_time']."',
          app_date = '".strtotime($appdata['app_date'])."',
          app_place = '".$appdata['app_place']."',
          app_service = '".$appdata['app_service']."',
          app_type = '".$appdata['app_type']."'
          WHERE appID = ".$appdata['appID']."";
          $query = $this->db->query($sql);
          return $appdata['clientID'];
     }

     function delete_client_appointment($appID) {
          $sql = "DELETE FROM tbl_appointments WHERE appID = '".$appID."'";
          $query = $this->db->query($sql);
     }

     function delete_client_app($appID) {
          $sql = "UPDATE tbl_appointments SET app_deleted = 'Y' WHERE appID = '".$appID."'";
          $query = $this->db->query($sql);
     }
     
}?>