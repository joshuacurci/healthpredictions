<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class advisorhome_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE userID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     function get_client($advisorID) {
        $sql = "SELECT * FROM tbl_client WHERE assigned_advisor = '".$advisorID."'";
        $query = $this->db->query($sql);
        return $query->result_array();
   }
     function get_referral($advisorID) {
          $sql = "SELECT * FROM tbl_referral WHERE assigned_advisor = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_staff() {
        $sql = "SELECT * FROM tbl_staff";
        $query = $this->db->query($sql);
        return $query->result_array();
   }
     

     
}?>