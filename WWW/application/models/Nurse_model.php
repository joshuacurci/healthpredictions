<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nurse_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_nurse($nurseID) {
          $sql = "SELECT * FROM tbl_nurse WHERE nurseID = '".$nurseID."' AND nurse_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_nurse_all() {
          $sql = "SELECT * FROM tbl_nurse WHERE nurse_deleted = 'N' ORDER BY nurse_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

          // Add new Nurse
     function new_nurse_info($nursedata) {
          $time = time();
          $sql = "INSERT INTO tbl_nurse (nurse_name, siteID, nurse_address, nurse_address2, nurse_city, nurse_state, nurse_postcode, nurse_country, nurse_phone, nurse_mobile, nurse_fax, nurse_email, nurse_email2, nurse_pi_insurance, nurse_pi_insurance_num, nurse_active, nurse_date_of_active, nurse_date_of_inactive, nurse_comments, nurse_service, nurse_regnumber, nurse_availcomments, dateadded) 
          VALUES 
          ('".$nursedata['nurse_name']."','".$nursedata['siteID']."','".$nursedata['nurse_address']."', '".$nursedata['nurse_address2']."', '".$nursedata['nurse_city']."','".$nursedata['nurse_state']."','".$nursedata['nurse_postcode']."', '".$nursedata['nurse_country']."', '".$nursedata['nurse_phone']."','".$nursedata['nurse_mobile']."','".$nursedata['nurse_fax']."','".$nursedata['nurse_email']."','".$nursedata['nurse_email2']."','".$nursedata['nurse_pi_insurance']."','".$nursedata['nurse_pi_insurance_num']."','".$nursedata['nurse_active']."','".$nursedata['nurse_date_of_active']."','".$nursedata['nurse_date_of_inactive']."','".$nursedata['nurse_comments']."','".$nursedata['nurse_service']."','".$nursedata['nurse_regnumber']."','".$nursedata['nurse_availcomments']."','".$time."')";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_nurse_info ($nursedata) {
          $sql = "UPDATE tbl_nurse SET 
          nurse_name = '".$nursedata['nurse_name']."',
          siteID = '".$nursedata['siteID']."',
          nurse_service = '".$nursedata['nurse_service']."',
          nurse_regnumber = '".$nursedata['nurse_regnumber']."',
          nurse_address = '".$nursedata['nurse_address']."',
          nurse_address2 = '".$nursedata['nurse_address2']."',
          nurse_city = '".$nursedata['nurse_city']."',
          nurse_state = '".$nursedata['nurse_state']."',
          nurse_postcode = '".$nursedata['nurse_postcode']."',
          nurse_country = '".$nursedata['nurse_country']."',
          nurse_phone = '".$nursedata['nurse_phone']."',
          nurse_mobile = '".$nursedata['nurse_mobile']."',
          nurse_fax = '".$nursedata['nurse_fax'].    "',
          nurse_email = '".$nursedata['nurse_email']."',
          nurse_email2 = '".$nursedata['nurse_email2']."',
          nurse_pi_insurance = '".$nursedata['nurse_pi_insurance']."',
          nurse_pi_insurance_num = '".$nursedata['nurse_pi_insurance_num']."',
          nurse_active = '".$nursedata['nurse_active']."',
          nurse_date_of_active = '".$nursedata['nurse_date_of_active']."',
          nurse_date_of_inactive = '".$nursedata['nurse_date_of_inactive']."',
          nurse_comments = '".$nursedata['nurse_comments']."',
          nurse_availcomments = '".$nursedata['nurse_availcomments']."'
          WHERE nurseID = ".$nursedata['nurseID']."";
          $query = $this->db->query($sql);
     }

     function search_nurse($searchData) {
          $sql = "SELECT * FROM tbl_nurse WHERE nurse_name LIKE '%".$searchData['nurse_name']."%' AND nurse_city LIKE '%".$searchData['nurse_city']."%' AND nurse_state LIKE '%".$searchData['nurse_state']."%' AND nurse_deleted = 'N' ORDER BY nurse_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_nurse_all($searchData) {
        if ($searchData['nurse_address2']=='')
        {
           $address2="AND (nurse_address2 LIKE '%".$searchData['nurse_address2']."%' OR nurse_address2 IS NULL)";
        }
        else
        {
           $address2="AND nurse_address2 LIKE '%".$searchData['nurse_address2']."%'";
        }
        if ($searchData['nurse_email2']=='')
        {
           $email2="AND (nurse_email2 LIKE '%".$searchData['nurse_email2']."%' OR nurse_email2 IS NULL)";
        }
       else
       {
           $email2="AND nurse_email2 LIKE '%".$searchData['nurse_email2']."%'";
       }
          $sql = "SELECT * FROM tbl_nurse WHERE nurse_name LIKE '%".$searchData['nurse_name']."%'  
          AND nurse_address LIKE '%".$searchData['nurse_address']."%'".$address2."
          AND nurse_city LIKE '%".$searchData['nurse_city']."%'
          AND nurse_postcode LIKE '%".$searchData['nurse_postcode']."%'
          AND nurse_state LIKE '%".$searchData['nurse_state']."%' 
          AND nurse_country LIKE '%".$searchData['nurse_country']."%' 
          AND nurse_phone LIKE '%".$searchData['nurse_phone']."%' 
          AND nurse_mobile LIKE '%".$searchData['nurse_mobile']."%' 
          AND nurse_fax LIKE '%".$searchData['nurse_fax']."%' 
          AND nurse_email LIKE '%".$searchData['nurse_email']."%'".$email2."
          AND nurse_pi_insurance LIKE '%".$searchData['nurse_pi_insurance']."%' 
          AND nurse_pi_insurance_num LIKE '%".$searchData['nurse_pi_insurance_num']."%' 
          AND nurse_active LIKE '%".$searchData['nurse_active']."%'
          AND nurse_regnumber LIKE '%".$searchData['nurse_regnumber']."%'
          AND nurse_deleted = 'N' ORDER BY nurse_name";

          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_nurse_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE nurse_city LIKE '%".$searchData['nurse_city']."%' ORDER BY nurse_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_nurse_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE nurse_state LIKE '%".$searchData['nurse_state']."%' ORDER BY nurse_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_nurse_info($nurseID) {
          $sql = "UPDATE tbl_nurse SET nurse_deleted = 'Y'
          WHERE nurseID = ".$nurseID."";
          $query = $this->db->query($sql);
     }

     
}?>