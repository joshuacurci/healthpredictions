<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clientcontact_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_client_contact($historyID) {
          $sql = "SELECT * FROM tbl_client_contacthistory WHERE historyID = '".$historyID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_staff_list() {
          $sql = "SELECT * FROM tbl_staff WHERE staff_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_client_contact($appdata) {
          $time = time();
          $sql = "INSERT INTO tbl_client_contacthistory (
               clientID, 
               history_timestamp, 
               history_action,
               history_staffID) 
          VALUES  
          ('".$appdata['clientID']."', 
               '".$time."', 
               '".$appdata['history_action']."', 
               '".$appdata['history_staffID']."')";
          $query = $this->db->query($sql);

          return $appdata['clientID'];
     }

     function update_client_contact ($appdata) {
          $time = time();
          $sql = "UPDATE tbl_client_contacthistory SET 
          history_action = '".$appdata['history_action']."',
          history_staffID = '".$appdata['history_staffID']."',
          history_lastedited = '".$time."'
          WHERE historyID = ".$appdata['historyID']."";
          $query = $this->db->query($sql);
          return $appdata['clientID'];
     }

     function delete_client_contact($historyID) {
          $sql = "DELETE FROM tbl_client_contacthistory WHERE historyID = '".$historyID."'";
          $query = $this->db->query($sql);
     }
     
}?>