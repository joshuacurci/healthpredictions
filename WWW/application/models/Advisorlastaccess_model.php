<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advisorlastaccess_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_appointments() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisor($result) {
          $advisorId = implode(', ', $result);
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorId."' AND advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_advisor() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_test_types() {
          $sql = "SELECT * FROM tbl_client_testtypes ORDER BY type_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_1month() {
          $month = date("Y-m-d h:i:s", strtotime("-1 month"));
            $sql = 'Select * 
                  FROM `tbl_advisor`
                  LEFT JOIN `tbl_users`
                    ON `tbl_advisor`.`userID` = `tbl_users`.`userID`
                  WHERE `tbl_users`.`lastlogin` > "'.$month.'"';
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_3month() {
          $month3 = date("Y-m-d h:i:s", strtotime("-3 month"));
         $sql = 'Select * 
                  FROM `tbl_advisor`
                  LEFT JOIN `tbl_users`
                    ON `tbl_advisor`.`userID` = `tbl_users`.`userID`
                  WHERE `tbl_users`.`lastlogin` > "'.$month3.'" ';
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_6month() {
      $month6 = date("Y-m-d h:i:s", strtotime("-6 month"));
          $sql = 'Select * 
                  FROM `tbl_advisor`
                  LEFT JOIN `tbl_users`
                    ON `tbl_advisor`.`userID` = `tbl_users`.`userID`
                  WHERE `tbl_users`.`lastlogin` > "'.$month6.'"';
          $query = $this->db->query($sql);
          return $query->result_array();
     }


function search_between_date($startdate, $lastdate) {
     $sql = "SELECT * FROM tbl_appointments WHERE app_date between '".$startdate."' AND '".$lastdate."' AND app_deleted = 'N' ORDER BY app_date";
     $query = $this->db->query($sql);
     return $query->result_array();
}
}

?>