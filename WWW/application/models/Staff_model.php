<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class staff_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_staff($staffID) {
          $sql = "SELECT * FROM tbl_staff WHERE staffID = '".$staffID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_staff_all() {
          $sql = "SELECT * FROM tbl_staff WHERE staff_deleted = 'N' ORDER BY staff_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_staff_info($staffdata) {
          $time = time();
          $sql = "INSERT INTO tbl_staff (staff_name, siteID, staff_service, staff_address, staff_address2, staff_city, staff_state, staff_postcode, staff_country, staff_phone, staff_mobile, staff_fax, staff_email, staff_responsibleperson, staff_location, staff_notes, dateadded) 
          VALUES 
          ('".$staffdata['staff_name']."', '".$staffdata['siteID']."', '".$staffdata['staff_service']."', '".$staffdata['staff_address']."', '".$staffdata['staff_address2']."', '".$staffdata['staff_city']."', '".$staffdata['staff_state']."', '".$staffdata['staff_postcode']."', '".$staffdata['staff_country']."', '".$staffdata['staff_phone']."', '".$staffdata['staff_mobile']."', '".$staffdata['staff_fax']."', '".$staffdata['staff_email']."', '".$staffdata['staff_responsibleperson']."', '".$staffdata['staff_location']."', '".$staffdata['staff_notes']."', '".$time."')";
          $query = $this->db->query($sql);
          return $this->db->insert_id();
     }

     function new_user_info($staffID, $staffdata) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
          $length = 10;
          $charactersLength = strlen($characters);
          $randomString = '';
          for ($i = 0; $i < $length; $i++) {
               $randomString .= $characters[rand(0, $charactersLength - 1)];
          }

          $password = $randomString;

          $sql = "INSERT INTO tbl_users (
          name,
          username,
          password,
          email,
          linkedID,
          usr_phone,
          usr_mobile,
          type_letter,
          siteID,
          usr_status,
          dateadded
          ) VALUES (
          '".$staffdata['staff_name']."',
          '".$staffdata['staff_email']."',
          '".md5($password)."',
          '".$staffdata['staff_email']."',
          '".$staffID."',
          '".$staffdata['staff_phone']."',
          '".$staffdata['staff_mobile']."',
          '".$staffdata['staff_service']."',
          '".$staffdata['siteID']."',
          'A',
          '".time()."'
          )";
        $query = $this->db->query($sql);

        $subject = "New profile created in Health Predictions Portal";

        $to = "".$staffdata['staff_name']." <".$staffdata['staff_email'].">";

        $message = "<html><body>Dear ".$staffdata['staff_name'].",<br/>
        <br/>
To access the Health Predictions portal, click on the following link.
<br/><br/>
http://portal.healthpredictions.com/
<br/><br/>
Your login details are as follows:<br/>
     Username: ".$staffdata['staff_email']."<br/>
     Password: ".$password."<br/>

Yours faithfully,<br/>
Health Predictions Team</body></html>";

         $headers = "From: Health Predictions <no-reply@healthpredictions.com.au>" . "\r\n";
         $headers .= "MIME-Version: 1.0\r\n";
         $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";    

        mail( $to, $subject, $message, $headers );


        return $this->db->insert_id(); 
   }

     // Update General Practitioner info
     function update_staff_info ($staffdata) {
          $sql = "UPDATE tbl_staff SET 
          staff_name = '".$staffdata['staff_name']."',
          siteID = '".$staffdata['siteID']."',
          staff_service = '".$staffdata['staff_service']."',
          staff_address = '".$staffdata['staff_address']."',
          staff_address2 = '".$staffdata['staff_address2']."',
          staff_city = '".$staffdata['staff_city']."',
          staff_state = '".$staffdata['staff_state']."',
          staff_postcode = '".$staffdata['staff_postcode']."',
          staff_country = '".$staffdata['staff_country']."',
          staff_phone = '".$staffdata['staff_phone']."',
          staff_mobile = '".$staffdata['staff_mobile']."',
          staff_fax = '".$staffdata['staff_fax']."',
          staff_email = '".$staffdata['staff_email']."',
          staff_responsibleperson = '".$staffdata['staff_responsibleperson']."',
          staff_location = '".$staffdata['staff_location']."',
          staff_notes = '".$staffdata['staff_notes']."'
          WHERE staffID = ".$staffdata['staffID']."";
          $query = $this->db->query($sql);
     }

     function update_user_info ($staffdata) {
        $sql = "UPDATE tbl_users SET 
        name = '".$staffdata['staff_name']."',
        siteID = '".$staffdata['siteID']."',
        usr_phone = '".$staffdata['staff_phone']."',
        usr_mobile = '".$staffdata['staff_mobile']."',
        email = '".$staffdata['staff_email']."',
        username = '".$staffdata['staff_email']."'
        WHERE linkedID = ".$staffdata['staffID']." AND type_letter != 'I'";
        $query = $this->db->query($sql);
   }

     function search_staff($searchData) {

          if($searchData['staff_city']=="")
          {
            $citystring = " ";
          }
          else
          {
            $citystring = " staff_city LIKE '%".$searchData['staff_city']."%' AND";
          }
          $sql = "SELECT * FROM tbl_staff WHERE staff_name LIKE '%".$searchData['staff_name']."%' AND".$citystring." staff_state LIKE '%".$searchData['staff_state']."%' AND staff_deleted = 'N' ORDER BY staff_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_staff_all($searchData) {
          if ($searchData['staff_name']=="" && $searchData['staff_address']=="" && $searchData['staff_address2']=="" 
          && $searchData['staff_city']=="" && $searchData['staff_postcode']=="" && $searchData['staff_state']=="" && 
          $searchData['staff_country']=="" && $searchData['staff_phone']=="" && $searchData['staff_mobile']=="" &&
          $searchData['staff_fax']=="" && $searchData['staff_email']=="" && $searchData['staff_responsibleperson']=="" && $searchData['staff_active']=="")
          {
            $sql = "SELECT * FROM tbl_staff ORDER BY staff_name";
            $query = $this->db->query($sql);
            return $query->result_array();
          }
          else
          {
            if($searchData['staff_name']=="")
            {
              $nameSQL="(staff_name LIKE '%%' OR staff_name IS NULL)";
            }
            else
            {
              $nameSQL="staff_name LIKE '%".$searchData['staff_name']."%'";
            }
            if($searchData['staff_address']=="")
            {
              $addressSQL=" (staff_address LIKE '%%' OR staff_address IS NULL)";
            }
            else
            {
              $addressSQL=" staff_address LIKE '%".$searchData['staff_address']."%'";
            }
            if($searchData['staff_address2']=="")
            {
              $address2SQL=" (staff_address2 LIKE '%%' OR staff_address2 IS NULL)";
            }
            else
            {
              $address2SQL=" staff_address2 LIKE '%".$searchData['staff_address2']."%'";
            }
            if($searchData['staff_city']=="")
            {
              $citySQL=" (staff_city LIKE '%%' OR staff_city IS NULL)";
            }
            else
            {
              $citySQL=" staff_city LIKE '%".$searchData['staff_city']."%'";
            }
            if($searchData['staff_postcode']=="")
            {
              $postcodeSQL=" (staff_postcode LIKE '%%' OR staff_postcode IS NULL)";
            }
            else
            {
              $postcodeSQL=" staff_postcode LIKE '%".$searchData['staff_postcode']."%'";
            }
            if($searchData['staff_state']=="")
            {
              $stateSQL=" (staff_state LIKE '%%' OR staff_state IS NULL)";
            }
            else
            {
              $stateSQL=" staff_state LIKE '%".$searchData['staff_state']."%'";
            }
            if($searchData['staff_country']=="")
            {
              $countrySQL=" (staff_country LIKE '%%' OR staff_country IS NULL)";
            }
            else
            {
              $countrySQL=" staff_country LIKE '%".$searchData['staff_country']."%'";
            }
            if($searchData['staff_phone']=="")
            {
              $phoneSQL=" (staff_phone LIKE '%%' OR staff_phone IS NULL)";
            }
            else
            {
              $phoneSQL=" staff_phone LIKE '%".$searchData['staff_phone']."%'";
            }
            if($searchData['staff_mobile']=="")
            {
              $mobileSQL=" (staff_mobile LIKE '%%' OR staff_mobile IS NULL)";
            }
            else
            {
              $mobileSQL=" staff_mobile LIKE '%".$searchData['staff_mobile']."%'";
            }
            if($searchData['staff_fax']=="")
            {
              $faxSQL=" (staff_fax LIKE '%%' OR staff_fax IS NULL)";
            }
            else
            {
              $faxSQL=" staff_fax LIKE '%".$searchData['staff_fax']."%'";
            }
            if($searchData['staff_email']=="")
            {
              $emailSQL=" (staff_email LIKE '%%' OR staff_email IS NULL)";
            }
            else
            {
              $emailSQL=" staff_email LIKE '%".$searchData['staff_email']."%'";
            }
            if($searchData['staff_responsibleperson']=="")
            {
              $responsiblepersonSQL=" (staff_responsibleperson LIKE '%%' OR staff_responsibleperson IS NULL)";
            }
            else
            {
              $responsiblepersonSQL=" staff_responsibleperson LIKE '%".$searchData['staff_responsibleperson']."%'";
            }
            if($searchData['staff_active']=="")
            {
              $activeSQL=" (staff_deleted LIKE '%%' OR staff_deleted IS NULL)";
            }
            else
            {
              if($searchData['staff_active']=="1")
              {
                $activeSQL=" staff_deleted='N'";
              }
              if($searchData['staff_active']=="0")
              {
                $activeSQL=" staff_deleted='Y'";
              }
            }
            $sql = "SELECT * FROM tbl_staff WHERE ".$nameSQL." AND".$addressSQL." AND".$address2SQL." AND".$citySQL." AND".$postcodeSQL." AND".$stateSQL." AND".
            $countrySQL." AND".$phoneSQL." AND".$mobileSQL." AND".$faxSQL." AND".$emailSQL." AND".$responsiblepersonSQL." AND".$activeSQL." ORDER BY staff_name";
            // var_dump($sql);
            // die();
            $query = $this->db->query($sql);
            return $query->result_array();
          }  
     }

     function search_staff_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE staff_city LIKE '%".$searchData['staff_city']."%' ORDER BY staff_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_staff_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE staff_state LIKE '%".$searchData['staff_state']."%' ORDER BY staff_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_staff_info($staffID) {
          $sql = "UPDATE tbl_staff SET 
          staff_deleted = 'Y'
          WHERE staffID = ".$staffID."";
          $query = $this->db->query($sql);

          $sql2 = "UPDATE tbl_users SET 
          usr_status = 'D'
          WHERE linkedID = ".$staffID." AND type_letter != 'I'";
          $query2 = $this->db->query($sql2);
     }

     
}?>