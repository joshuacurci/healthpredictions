<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientsbytest_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_appointments() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_test_types() {
          $sql = "SELECT * FROM tbl_client_testtypes ORDER BY type_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


      function search_between_date($postdata, $testTypesSelected) {
        if($testTypesSelected=="")
        {
                $testTypesSelected="-1";
        }
        if ($postdata['startdate'] == '') {
                $startdate = '';
        } else {
                $datestart_date = date("d-m-Y H:i:s", strtotime($postdata['startdate'])); 
                $startdate = strtotime($datestart_date);  
        }

        if ($postdata['lastdate'] == '') {
                $lastdate = '';
        } else {
                $lastdate_date = date("d-m-Y 23:59:59", strtotime($postdata['lastdate']));
                $lastdate = strtotime($lastdate_date); 
        }
//        var_dump($lastdate);
//        var_dump($testTypesSelected);
//        die();
       if ($startdate == '' && $lastdate == '') {
         $sql = "Select DISTINCT  tbl_client.clientID, client_name, client_sex, client_address, client_city, client_state, client_phone, client_fax, client_email    
                  FROM tbl_client
                  LEFT JOIN tbl_client_tests ON tbl_client.clientID=tbl_client_tests.clientID
                  LEFT JOIN tbl_appointments ON tbl_client_tests.clientID=tbl_appointments.clientID
                  WHERE tbl_client_tests.testtypeID IN (".$testTypesSelected.") AND tbl_client.client_deleted = 'N'";
          } else {
          $sql = "Select DISTINCT tbl_client.clientID, client_name, client_sex, client_address, client_city, client_state, client_phone, client_fax, client_email    
                  FROM tbl_client
                  LEFT JOIN tbl_client_tests ON tbl_client.clientID=tbl_client_tests.clientID
                  LEFT JOIN tbl_appointments ON tbl_client_tests.clientID=tbl_appointments.clientID
                  WHERE tbl_client_tests.testtypeID IN (".$testTypesSelected.") AND tbl_appointments.app_timestamp BETWEEN '".$startdate."' AND '".$lastdate."' AND tbl_client.client_deleted = 'N'";
          } 
          $query = $this->db->query($sql);
          return $query->result_array();


          
    }
}

?>