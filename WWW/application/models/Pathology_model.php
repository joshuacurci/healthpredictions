<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pathology_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_pathology($pathID) {
          $sql = "SELECT * FROM tbl_pathologist WHERE pathID = '".$pathID."' AND path_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_pathology_all() {
          $sql = "SELECT * FROM tbl_pathologist WHERE path_deleted = 'N' ORDER BY path_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_pathology_info($pathologydata) {
          $time = time();
          $sql = "INSERT INTO tbl_pathologist (path_name, siteID, path_service, path_address, path_address2, path_city, path_state, path_postcode, path_country, path_phone, path_mobile, path_fax, path_email, path_responsibleperson, path_company, path_accnumber, path_contactname, path_contactemail, path_contactphone, dateadded) 
          VALUES 
          ('".$pathologydata['path_name']."', '".$pathologydata['siteID']."', '".$pathologydata['path_service']."', '".$pathologydata['path_address']."', '".$pathologydata['path_address2']."', '".$pathologydata['path_city']."', '".$pathologydata['path_state']."', '".$pathologydata['path_postcode']."', '".$pathologydata['path_country']."', '".$pathologydata['path_phone']."', '".$pathologydata['path_mobile']."', '".$pathologydata['path_fax']."', '".$pathologydata['path_email']."', '".$pathologydata['path_responsibleperson']."', '".$pathologydata['path_company']."', '".$pathologydata['path_accnumber']."', '".$pathologydata['path_contactname']."', '".$pathologydata['path_contactemail']."', '".$pathologydata['path_contactphone']."','".$time."')";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_pathology_info ($pathologydata) {
          $sql = "UPDATE tbl_pathologist SET 
          path_name = '".$pathologydata['path_name']."',
          siteID = '".$pathologydata['siteID']."',
          path_service = '".$pathologydata['path_service']."',
          path_address = '".$pathologydata['path_address']."',
          path_address2 = '".$pathologydata['path_address2']."',
          path_city = '".$pathologydata['path_city']."',
          path_state = '".$pathologydata['path_state']."',
          path_postcode = '".$pathologydata['path_postcode']."',
          path_country = '".$pathologydata['path_country']."',
          path_phone = '".$pathologydata['path_phone']."',
          path_mobile = '".$pathologydata['path_mobile']."',
          path_fax = '".$pathologydata['path_fax'].    "',
          path_email = '".$pathologydata['path_email']."',
          path_accnumber = '".$pathologydata['path_accnumber']."',
          path_contactname = '".$pathologydata['path_contactname']."',
          path_contactemail = '".$pathologydata['path_contactemail']."',
          path_contactphone = '".$pathologydata['path_contactphone']."',
          path_responsibleperson = '".$pathologydata['path_responsibleperson']."',
          path_company = '".$pathologydata['path_company']."',
          path_instructions = '".$pathologydata['path_instructions']."'
          WHERE pathID = ".$pathologydata['pathID']."";
          $query = $this->db->query($sql);
     }

     function search_pathology($searchData) {
          $sql = "SELECT * FROM tbl_pathologist WHERE path_name LIKE '%".$searchData['path_name']."%' AND path_city LIKE '%".$searchData['path_city']."%' AND path_state LIKE '%".$searchData['path_state']."%' AND path_deleted = 'N' ORDER BY path_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_pathology_all($searchData) {
          $sql = "SELECT * FROM tbl_pathologist WHERE
          path_name LIKE '%".$searchData['path_name']."%'
          AND path_service LIKE '%".$searchData['path_service']."%' 
          AND path_company LIKE '%".$searchData['path_company']."%'
          AND path_address LIKE '%".$searchData['path_address']."%'
          AND path_address2 LIKE '%".$searchData['path_address2']."%'
          AND path_city LIKE '%".$searchData['path_city']."%'
          AND path_postcode LIKE '%".$searchData['path_postcode']."%'
          AND path_state LIKE '%".$searchData['path_state']."%' 
          AND path_country LIKE '%".$searchData['path_country']."%' 
          AND path_phone LIKE '%".$searchData['path_phone']."%' 
          AND path_mobile LIKE '%".$searchData['path_mobile']."%' 
          AND path_fax LIKE '%".$searchData['path_fax']."%' 
          AND path_email LIKE '%".$searchData['path_email']."%'  
          AND path_accnumber LIKE '%".$searchData['path_accnumber']."%'  
          AND path_contactname LIKE '%".$searchData['path_contactname']."%'  
          AND path_contactemail LIKE '%".$searchData['path_contactemail']."%'  
          AND path_contactphone LIKE '%".$searchData['path_contactphone']."%'  
          AND path_responsibleperson LIKE '%".$searchData['path_responsibleperson']."%' 
          AND path_deleted = 'N' ORDER BY path_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_path_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE path_city LIKE '%".$searchData['path_city']."%' ORDER BY path_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_path_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE path_state LIKE '%".$searchData['path_state']."%' ORDER BY path_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_pathology_info($pathID) {
          $sql = "UPDATE tbl_pathologist SET 
          path_deleted = 'Y'
          WHERE pathID = ".$pathID."";
          $query = $this->db->query($sql);
     }

     
}?>