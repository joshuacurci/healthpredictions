<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class referral_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_referral($clientID) {
          $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$clientID."' ORDER BY dateadded DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_referrals() {
          $sql = "SELECT * FROM tbl_referral WHERE client_deleted = 'N' ORDER BY dateadded DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_id ($refferalID) {
      $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$refferalID."'";
      $query = $this->db->query($sql);
      return $query->result_array();
     }

     function get_selected_referrals($submitedNumbers) {
      $finalIDs = '';
      $totalNumbers = count($submitedNumbers);
      $i = 0;
      foreach ($submitedNumbers['bulkacknowlage'] as $referralID) {
        if ($i++ == $totalNumbers) {
          $finalIDs .= $referralID;
        } else {
          $finalIDs .= $referralID.",";
        }
      }

      $sql = "SELECT * FROM tbl_referral WHERE referralID IN (".$finalIDs.") ORDER BY dateadded DESC";
      $query = $this->db->query($sql);
      return $query->result_array();
     }

     function get_sites() {
      $sql = "SELECT * FROM tbl_sites";
      $query = $this->db->query($sql);
      return $query->result_array();
 }

     function get_finalreferral($refferalID) {
          $sql = "SELECT * FROM tbl_referral WHERE referralID = '".$refferalID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_referrals($postdata) {
      $sql = "SELECT * FROM tbl_referral WHERE client_deleted = 'N'";

      if ($postdata['client_name'] != ''){
        $sql .= "AND client_name LIKE '%".$postdata['client_name']."%'";
      }

      if ($postdata['client_city'] != ''){
        $sql .= "AND client_city LIKE '%".$postdata['client_city']."%'";
      }

      if ($postdata['client_state'] != ''){
        $sql .= "AND client_state = '".$postdata['client_state']."'";
      }

      if ($postdata['siteID'] != ''){
        $sql .= "AND siteID = '".$postdata['siteID']."'";
      }

      if ($postdata['client_active'] != ''){
        $sql .= "AND client_active = '".$postdata['client_active']."'";
      }

      $sql .= " ORDER BY dateadded DESC;";

      $query = $this->db->query($sql);
      return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_path_report($clientID) {
          $sql = "SELECT * FROM tbl_client_path_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_referral_noti($clientID) {
          $sql = "SELECT * FROM tbl_client_referral_noti WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_to_ins_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_to_ins_completion_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_clientNoti_to_adv_report($clientID) {
          $sql = "SELECT * FROM tbl_client_noti_to_adv_completion_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_client_consent_id_report($clientID) {
          $sql = "SELECT * FROM tbl_client_consent_id_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_apointment($clientID) {
          $sql = "SELECT * FROM tbl_appointments WHERE clientID = '".$clientID."' ORDER BY appID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_specific_apointment($appID) {
          $sql = "SELECT * FROM tbl_appointments WHERE appID = '".$appID."' ORDER BY appID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_history($clientID) {
          $sql = "SELECT * FROM tbl_client_contacthistory WHERE clientID = '".$clientID."' ORDER BY historyID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clienttest_info($clientID) {
          $sql = "SELECT * FROM tbl_referral_tests WHERE referralID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clienttest_notes($clientID) {
          $sql = "SELECT * FROM tbl_referral_testnotes WHERE referralID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_client_all() {
          $sql = "SELECT * FROM tbl_client WHERE client_deleted = 'N' AND client_active = 'Y' ORDER BY client_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_all_advisor($loginID) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$loginID."'";
          $query = $this->db->query($sql1);
          $advisorData = $query->result_array();

          $sql = "SELECT * FROM tbl_client WHERE assigned_advisor = '".$advisorData[0]['advisorID']."' AND client_deleted = 'N' ORDER BY client_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_insurance() {
          $sql = "SELECT * FROM tbl_insurance ORDER BY ins_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisors() {
          $sql = "SELECT * FROM tbl_advisor ORDER BY advisor_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_staff() {
          $sql = "SELECT * FROM tbl_staff ORDER BY staff_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_test_types() {
          $sql = "SELECT * FROM tbl_client_testtypes ORDER BY type_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
 
     function get_test_client($clientID) {
          $sql = "SELECT * FROM tbl_referral_tests WHERE referralID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_path_report($clientID) {
          $sql = "SELECT clientID FROM tbl_client_path_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_noti_report($clientID) {
          $sql = "SELECT clientID FROM tbl_client_noti_report WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function check_client_referral_noti($clientID) {
          $sql = "SELECT clientID FROM tbl_client_referral_noti WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     function search_referral_advisor($searchData, $loginID) {
      $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$loginID."'";
      $query = $this->db->query($sql1);
      $advisorData = $query->result_array();
      $sql = "SELECT * FROM tbl_referral WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' AND assigned_advisor = '".$advisorData[0]['advisorID']."' ORDER BY client_name";
      $query = $this->db->query($sql);
      return $query->result_array();
 }

      function new_tests($referralID,$clientID) {
          $sql = "SELECT * FROM tbl_referral_tests WHERE referralID = '".$referralID."'";
          $query = $this->db->query($sql);
          $testData = $query->result_array();

          $sql2 = "SELECT * FROM tbl_referral_testnotes WHERE referralID = '".$referralID."'";
          $query2 = $this->db->query($sql2);
          $testnoteData = $query2->result_array();

          foreach($testData as $data)
          {
          $sql9 = "INSERT INTO tbl_client_tests (clientID,testtypeID) VALUES ('".$clientID."','".$data["testtypeID"]."')";
          $sql10 = "INSERT INTO tbl_client_testnotes (clientID,testNote) VALUES ('".$clientID."','".$data["testtypeID"]."')";
          $query9 = $this->db->query($sql9);
          $query10 = $this->db->query($sql10);
          }
     }

     // Add new General Practitioner
     function new_client_info($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$clientdata['assigned_advisor']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();

          $time = time();
          $sql = "INSERT INTO tbl_client (client_name, client_sex, client_DOB, client_organization, siteID, client_address, client_address2, client_city, client_state, client_postcode, client_country, client_phone, client_mobile, client_fax, client_email, client_email2, client_notes, dateadded, insurance_number, insurance_company, assigned_advisor, assigned_staff, refferalID) 
          VALUES 
          ('".$clientdata['client_name']."', '".$clientdata['client_sex']."', '".$clientdata['client_DOB']."', '".$clientdata['client_organization']."', '".$advisorData[0]['siteID']."', '".$clientdata['client_address']."', '".$clientdata['client_address2']."', '".$clientdata['client_city']."', '".$clientdata['client_state']."', '".$clientdata['client_postcode']."', '".$clientdata['client_country']."', '".$clientdata['client_phone']."', '".$clientdata['client_mobile']."', '".$clientdata['client_fax']."', '".$clientdata['client_email']."', '".$clientdata['client_email2']."', '".str_replace("'", "&acute;", $clientdata['client_notes'])."', '".$time."', '".$clientdata['insurance_number']."', '".$clientdata['insurance_company']."', '".$clientdata['assigned_advisor']."', '".$clientdata['assigned_staff']."', '".$clientdata['referralID']."')";
          $query = $this->db->query($sql);

          $clientID = $this->db->insert_id();

          $sqlF = "UPDATE tbl_referral SET 
          clientID = '".$clientID."'
          WHERE referralID = ".$clientdata['referralID']."";
          $this->db->query($sqlF);

          //$sql9 = "INSERT INTO tbl_referral_tests (clientID) VALUES ('".$clientID."')";
          //$sql10 = "INSERT INTO tbl_referral_testnotes (clientID) VALUES ('".$clientID."')";
          //$query9 = $this->db->query($sql9);
          //$query10 = $this->db->query($sql10);

          return $clientID;
     }

     function new_client_info_referal($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$_SESSION['userID']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();
          
          $time = time();
          $sql = "INSERT INTO tbl_referral (client_name, client_sex, client_DOB, client_organization, siteID, client_address, client_address2, client_city, client_state, client_postcode, client_country, client_phone, client_mobile, client_fax, client_email, client_email2, client_notes, dateadded, insurance_number, insurance_company, assigned_advisor, client_active) 
          VALUES 
          ('".$clientdata['client_name']."', '".$clientdata['client_sex']."', '".$clientdata['client_DOB']."', '".$clientdata['client_organization']."', '".$advisorData[0]['siteID']."', '".$clientdata['client_address']."', '".$clientdata['client_address2']."', '".$clientdata['client_city']."', '".$clientdata['client_state']."', '".$clientdata['client_postcode']."', '".$clientdata['client_country']."', '".$clientdata['client_phone']."', '".$clientdata['client_mobile']."', '".$clientdata['client_fax']."', '".$clientdata['client_email']."', '".$clientdata['client_email2']."', '".str_replace("'", "&acute;", $clientdata['client_notes'])."', '".$time."', '".$clientdata['insurance_number']."', '".$clientdata['insurance_company']."', '".$advisorData[0]['advisorID']."','N')";
          $query = $this->db->query($sql);
          $clientID = $this->db->insert_id();

          $sql9 = "INSERT INTO tbl_referral_tests (referralID) VALUES ('".$clientID."')";
          $sql10 = "INSERT INTO tbl_referral_testnotes (referralID) VALUES ('".$clientID."')";
          $query9 = $this->db->query($sql9);
          $query10 = $this->db->query($sql10);

          return $clientID;
     }

     function new_client_info_referal_admin($clientdata) {
      $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$_SESSION['userID']."'";
      $query1 = $this->db->query($sql1);
      $advisorData = $query1->result_array();
      
      $time = time();
      $sql = "INSERT INTO tbl_referral (client_name, client_sex, client_DOB, client_organization, siteID, client_address, client_address2, client_city, client_state, client_postcode, client_country, client_phone, client_mobile, client_fax, client_email, client_email2, client_notes, dateadded, insurance_number, insurance_company, assigned_advisor, assigned_staff, client_active) 
      VALUES 
      ('".$clientdata['client_name']."', '".$clientdata['client_sex']."', '".$clientdata['client_DOB']."', '".$clientdata['client_organization']."', '".$clientdata['siteID']."', '".$clientdata['client_address']."', '".$clientdata['client_address2']."', '".$clientdata['client_city']."', '".$clientdata['client_state']."', '".$clientdata['client_postcode']."', '".$clientdata['client_country']."', '".$clientdata['client_phone']."', '".$clientdata['client_mobile']."', '".$clientdata['client_fax']."', '".$clientdata['client_email']."', '".$clientdata['client_email2']."', '".str_replace("'", "&acute;", $clientdata['client_notes'])."', '".$time."', '".$clientdata['insurance_number']."', '".$clientdata['insurance_company']."', '".$clientdata['advisorID']."', '".$clientdata['staffID']."','N')";
      $query = $this->db->query($sql);

      $clientID = $this->db->insert_id();

      $sql9 = "INSERT INTO tbl_referral_tests (referralID) VALUES ('".$clientID."')";
      $sql10 = "INSERT INTO tbl_referral_testnotes (referralID) VALUES ('".$clientID."')";
      $query9 = $this->db->query($sql9);
      $query10 = $this->db->query($sql10);

      return $clientID;
 }

     function new_path_report_info($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_path_report (siteID, clientID, client_information, client_consent_id, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_information']."', '".$reportdata['client_consent_id']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_path_report_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_path_report (siteID, clientID, client_information, client_consent_id, path_tests_explained, client_authority_results)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_noti_info($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_information']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_ins_to_completion($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_ins_completion_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_information']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['letter_to_adv']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_adv_to_completion($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_information']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['letter_to_adv']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_id($reportdata){
          $time = time();
          $sql = "INSERT INTO tbl_client_consent_id_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers, date_created)
          VALUES
          ('".$reportdata['siteID']."', '".$reportdata['clientID']."', '".$reportdata['client_details']."', '".$reportdata['tests_ordered']."', '".$reportdata['client_ins_bombora']."', '".$reportdata['client_information']."', '".$reportdata['client_consent_id']."', '".$reportdata['client_instructions']."', '".$reportdata['path_tests_explained']."', '".$reportdata['client_authority_results']."', '".$reportdata['medical_offers']."', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_default($reportdata, $clientID, $siteID){
          $time = time();
           $sql = "INSERT INTO tbl_client_consent_id_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers, date_created)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_noti_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, date_created)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', '".$time."')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_referral_noti_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_referral_noti (siteID, clientID, client_information, client_consent_id)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_to_ins_completion_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_ins_completion_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_noti_to_adv_completion_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, letter_to_adv, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     function new_client_consent_id_default($reportdata, $clientID, $siteID){
          $time = time();
          $sql = "INSERT INTO tbl_client_noti_to_adv_completion_report (siteID, clientID, client_details, tests_ordered, client_ins_bombora, client_information, client_consent_id, client_instructions, path_tests_explained, client_authority_results, medical_offers)
          VALUES
          ('".$siteID."', '".$clientID."', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y')";
          $query = $this->db->query($sql);

          return $this->db->insert_id();
     }

     // Update General Practitioner info
     function edit_path_report_info ($reportdata) {
          $sql = "UPDATE tbl_client_path_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_noti_info ($reportdata) {
          $sql = "UPDATE tbl_client_noti_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_referral_noti ($reportdata) {
          $sql = "UPDATE tbl_client_noti_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_noti_ins_to_completion ($reportdata) {
          $sql = "UPDATE tbl_client_noti_to_ins_completion_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          letter_to_adv = '".$reportdata['letter_to_adv']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_noti_adv_to_completion ($reportdata) {
          $sql = "UPDATE tbl_client_noti_to_adv_completion_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          letter_to_adv = '".$reportdata['letter_to_adv']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function edit_client_consent_id ($reportdata) {
          $sql = "UPDATE tbl_client_consent_id_report SET 
          siteID = '".$reportdata['siteID']."',
          clientID = '".$reportdata['clientID']."',
          client_information = '".$reportdata['client_information']."',
          client_consent_id = '".$reportdata['client_consent_id']."',
          client_instructions = '".$reportdata['client_instructions']."',
          path_tests_explained = '".$reportdata['path_tests_explained']."',
          client_authority_results = '".$reportdata['client_authority_results']."',
          client_details = '".$reportdata['client_details']."',
          tests_ordered = '".$reportdata['tests_ordered']."',
          client_ins_bombora = '".$reportdata['client_ins_bombora']."',
          medical_offers = '".$reportdata['medical_offers']."'
          WHERE clientID = ".$reportdata['clientID']."";
          $query = $this->db->query($sql);
     }

     function new_notifications($clientID) {
          $sql = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '1')";
          $sql2 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '2')";
          $sql3 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '3')";
          $sql4 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '4')";
          $sql5 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '5')";
          $sql6 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '6')";
          $sql7 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '7')";
          $sql8 = "INSERT INTO tbl_client_notifications (clientID, typeID) VALUES ('".$clientID."', '8')";

          //$sql9 = "INSERT INTO tbl_client_tests (clientID) VALUES ('".$clientID."')";
          //$sql10 = "INSERT INTO tbl_client_testnotes (clientID) VALUES ('".$clientID."')";
          
          $this->db->query($sql);
          $this->db->query($sql2);
          $this->db->query($sql3);
          $this->db->query($sql4);
          $this->db->query($sql5);
          $this->db->query($sql6);
          $this->db->query($sql7);
          $this->db->query($sql8);
          //$this->db->query($sql9);
          //$this->db->query($sql10);

     }

     // Update General Practitioner info
     function update_client_info ($clientdata) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE advisorID = '".$clientdata['assigned_advisor']."'";
          $query1 = $this->db->query($sql1);
          $advisorData = $query1->result_array();


          $sql = "UPDATE tbl_client SET 
          client_name = '".$clientdata['client_name']."',
          client_DOB = '".$clientdata['client_DOB']."',
          client_sex = '".$clientdata['client_sex']."',
          client_organization = '".$clientdata['client_organization']."',
          siteID = '".$advisorData[0]['siteID']."',
          client_address = '".$clientdata['client_address']."',
          client_address2 = '".$clientdata['client_address2']."',
          client_city = '".$clientdata['client_city']."',
          client_state = '".$clientdata['client_state']."',
          client_postcode = '".$clientdata['client_postcode']."',
          client_country = '".$clientdata['client_country']."',
          client_phone = '".$clientdata['client_phone']."',
          client_mobile = '".$clientdata['client_mobile']."',
          client_fax = '".$clientdata['client_fax'].    "',
          client_email = '".$clientdata['client_email']."',
          client_email2 = '".$clientdata['client_email2']."',
          insurance_company = '".$clientdata['insurance_company']."',
          insurance_number = '".$clientdata['insurance_number']."',
          assigned_advisor = '".$clientdata['assigned_advisor']."',
          assigned_staff = '".$clientdata['assigned_staff']."',
          client_notes = '".str_replace("'", "&#039;",$clientdata['client_notes'])."'
          WHERE clientID = ".$clientdata['clientID']."";
          $query = $this->db->query($sql);
     }

     function update_referral_info ($clientdata) {
      $sql = "UPDATE tbl_referral SET 
      client_name = '".$clientdata['client_name']."',
      client_DOB = '".$clientdata['client_DOB']."',
      client_sex = '".$clientdata['client_sex']."',
      client_organization = '".$clientdata['client_organization']."',
      siteID = '".$clientdata['siteID']."',
      client_address = '".$clientdata['client_address']."',
      client_address2 = '".$clientdata['client_address2']."',
      client_city = '".$clientdata['client_city']."',
      client_state = '".$clientdata['client_state']."',
      client_postcode = '".$clientdata['client_postcode']."',
      client_country = '".$clientdata['client_country']."',
      client_phone = '".$clientdata['client_phone']."',
      client_mobile = '".$clientdata['client_mobile']."',
      client_fax = '".$clientdata['client_fax'].    "',
      client_email = '".$clientdata['client_email']."',
      client_email2 = '".$clientdata['client_email2']."',
      insurance_company = '".$clientdata['insurance_company']."',
      insurance_number = '".$clientdata['insurance_number']."',
      assigned_advisor = '".$clientdata['assigned_advisor']."',
      assigned_staff = '".$clientdata['assigned_staff']."',
      client_notes = '".str_replace("'", "&#039;",$clientdata['client_notes'])."'
      WHERE referralID = ".$clientdata['referralID']."";
      $query = $this->db->query($sql);
 }

 function quick_update_referral_info ($staffID, $referralID) {
  $sql = "UPDATE tbl_referral SET 
  assigned_staff = '".$staffID."'
  WHERE referralID = ".$referralID."";
  $query = $this->db->query($sql);
}

     function search_client($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_advisor($searchData, $loginID) {
          $sql1 = "SELECT * FROM tbl_advisor WHERE userID = '".$loginID."'";
          $query = $this->db->query($sql1);
          $advisorData = $query->result_array();

          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' AND client_city LIKE '%".$searchData['client_city']."%' AND client_state LIKE '%".$searchData['client_state']."%' AND client_deleted = 'N' AND assigned_advisor = '".$advisorData[0]['advisorID']."' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_all($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE 
          client_name LIKE '%".$searchData['client_name']."%' 
          AND client_organization  LIKE '%".$searchData['client_organization']."%'
          AND client_address LIKE '%".$searchData['client_address']."%'
          AND client_address2 LIKE '%".$searchData['client_address2']."%'
          AND client_city LIKE '%".$searchData['client_city']."%'
          AND client_postcode LIKE '%".$searchData['client_postcode']."%'
          AND client_state LIKE '%".$searchData['client_state']."%' 
          AND client_country LIKE '%".$searchData['client_country']."%' 
          AND client_phone LIKE '%".$searchData['client_phone']."%' 
          AND client_mobile LIKE '%".$searchData['client_mobile']."%' 
          AND client_fax LIKE '%".$searchData['client_fax']."%' 
          AND client_email LIKE '%".$searchData['client_email']."%' 
          AND client_email2 LIKE '%".$searchData['client_email2']."%' 
          AND client_sex LIKE '%".$searchData['client_sex']."%' 
          AND client_workphone LIKE '%".$searchData['client_workphone']."%' 
          AND client_deleted = 'N' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE client_city LIKE '%".$searchData['client_city']."%' ORDER BY client_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE client_state LIKE '%".$searchData['client_state']."%' ORDER BY client_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_notifications($clientID, $typeID) {
          $sql = "SELECT * FROM tbl_client_notifications WHERE clientID = '".$clientID."' AND typeID = '".$typeID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_referral_info($clientID) {
          $sql = "UPDATE tbl_referral SET 
          client_deleted = 'Y'
          WHERE referralID = ".$clientID."";
          $query = $this->db->query($sql);
     }

     function change_to_active($clientID) {
          $sql = "UPDATE tbl_referral SET 
          client_active = 'Y'
          WHERE referralID = ".$clientID."";
          $query = $this->db->query($sql);
     }

     function update_client_notify ($clientdata) {

          if (array_key_exists('not1', $clientdata)) {
               $sql1 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not1'])) { $sql1 .= "print_date = '".$clientdata['not1']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not1'])) { $sql1 .= "email_date = '".$clientdata['not1']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not1'])) { $sql1 .= "fax_date = '".$clientdata['not1']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not1'])) { $sql1 .= "phone_date = '".$clientdata['not1']['phone_date']."',";}; 
               $sql1 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '1'";

               $query = $this->db->query($sql1);
          }

          if (array_key_exists('not2', $clientdata)) {
               $sql2 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not2'])) { $sql2 .= "print_date = '".$clientdata['not2']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not2'])) { $sql2 .= "email_date = '".$clientdata['not2']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not2'])) { $sql2 .= "fax_date = '".$clientdata['not2']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not2'])) { $sql2 .= "phone_date = '".$clientdata['not2']['phone_date']."',";}; 
               $sql2 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '2'";

               $query = $this->db->query($sql2);
          }

          if (array_key_exists('not3', $clientdata)) {
               $sql3 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not3'])) { $sql3 .= "print_date = '".$clientdata['not3']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not3'])) { $sql3 .= "email_date = '".$clientdata['not3']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not3'])) { $sql3 .= "fax_date = '".$clientdata['not3']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not3'])) { $sql3 .= "phone_date = '".$clientdata['not3']['phone_date']."',";}; 
               $sql3 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '3'";

               $query = $this->db->query($sql3);
          }

          if (array_key_exists('not4', $clientdata)) {
               $sql4 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not4'])) { $sql4 .= "print_date = '".$clientdata['not4']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not4'])) { $sql4 .= "email_date = '".$clientdata['not4']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not4'])) { $sql4 .= "fax_date = '".$clientdata['not4']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not4'])) { $sql4 .= "phone_date = '".$clientdata['not4']['phone_date']."',";}; 
               $sql4 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '4'";

               $query = $this->db->query($sql4);
          }

          if (array_key_exists('not5', $clientdata)) {
               $sql5 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not5'])) { $sql5 .= "print_date = '".$clientdata['not5']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not5'])) { $sql5 .= "email_date = '".$clientdata['not5']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not5'])) { $sql5 .= "fax_date = '".$clientdata['not5']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not5'])) { $sql5 .= "phone_date = '".$clientdata['not5']['phone_date']."',";}; 
               $sql5 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '5'";

               $query = $this->db->query($sql5);
          }

          if (array_key_exists('not6', $clientdata)) {
               $sql6 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not6'])) { $sql6 .= "print_date = '".$clientdata['not6']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not6'])) { $sql6 .= "email_date = '".$clientdata['not6']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not6'])) { $sql6 .= "fax_date = '".$clientdata['not6']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not6'])) { $sql6 .= "phone_date = '".$clientdata['not6']['phone_date']."',";}; 
               $sql6 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '6'";

               $query = $this->db->query($sql6);
          }

          if (array_key_exists('not7', $clientdata)) {
               $sql7 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not7'])) { $sql7 .= "print_date = '".$clientdata['not7']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not7'])) { $sql7 .= "email_date = '".$clientdata['not7']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not7'])) { $sql7 .= "fax_date = '".$clientdata['not7']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not7'])) { $sql7 .= "phone_date = '".$clientdata['not7']['phone_date']."',";}; 
               $sql7 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '7'";

               $query = $this->db->query($sql7);
          }

          if (array_key_exists('not8', $clientdata)) {
               $sql8 = "UPDATE tbl_client_notifications SET ";
               if (array_key_exists('print_date', $clientdata['not8'])) { $sql8 .= "print_date = '".$clientdata['not8']['print_date']."',";}; 
               if (array_key_exists('email_date', $clientdata['not8'])) { $sql8 .= "email_date = '".$clientdata['not8']['email_date']."',";}; 
               if (array_key_exists('fax_date', $clientdata['not8'])) { $sql8 .= "fax_date = '".$clientdata['not8']['fax_date']."',";}; 
               if (array_key_exists('phone_date', $clientdata['not8'])) { $sql8 .= "phone_date = '".$clientdata['not8']['phone_date']."',";}; 
               $sql8 .= "completed = 'Y' WHERE clientID = ".$clientdata['clientID']." AND typeID = '8'";

               $query = $this->db->query($sql8);
          }
     }
 
     
     function update_client_tests_adv ($clientdata) {
          $this->db->query("DELETE FROM tbl_referral_tests WHERE referralID = '".$clientdata['clientID']."';");
          foreach($clientdata["checkedid"] as $id)
          {
          $sql = "INSERT INTO tbl_referral_tests (referralID, testtypeID) VALUES ('".$clientdata['clientID']."', '".$id."')";
          $query = $this->db->query($sql);
          $sql2 = "UPDATE tbl_referral_testnotes SET 
          testNote = '".str_replace("'", "&#039;",$clientdata['testNote'])."'
          WHERE referralID = ".$clientdata['referralID']."";
          $query2 = $this->db->query($sql2);
          }
     }

     function update_client_tests ($clientdata) {
      $this->db->query("DELETE FROM tbl_referral_tests WHERE referralID = '".$clientdata['referralID']."';");
      foreach($clientdata["checkedid"] as $id)
      {
      $sql = "INSERT INTO tbl_referral_tests (referralID, testtypeID) VALUES ('".$clientdata['referralID']."', '".$id."')";
      $query = $this->db->query($sql);
      $sql2 = "UPDATE tbl_referral_testnotes SET 
      testNote = '".str_replace("'", "&#039;",$clientdata['testNote'])."'
      WHERE referralID = ".$clientdata['referralID']."";
      $query2 = $this->db->query($sql2);
      }
 }

     function referal_upload_files($postdata, $clientID) {
          $this->load->library('upload');
          $files = $postdata;
          $cpt = count($postdata['userfile']['name']);

               $config = array(
                 'upload_path'   => './uploads/temp/',
                 'allowed_types' => 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx|zip',
                 'overwrite'     => 1,                       
             );

          $this->upload->initialize($config);
          $this->load->library('upload', $config);
          $posteddate = time();
               
          for($i=0; $i<$cpt; $i++) {           
               $_FILES['userfile']['name']= $files['userfile']['name'][$i];
               $_FILES['userfile']['type']= $files['userfile']['type'][$i];
               $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
               $_FILES['userfile']['error']= $files['userfile']['error'][$i];
               $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                    
                    $originalname = $_FILES['userfile']['name'];
                    $prefix = md5(time());
                    $new_name = $prefix.$_FILES['userfile']['name'];
                    $new_name = str_replace(" ", "_", $new_name);
                    $_FILES['userfile']['name'] = $new_name;   

               if ($this->upload->do_upload('userfile')) {
                    //$this->_uploaded[$i] = $this->upload->data();
                    } else {
                         //$this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
                         echo $this->upload->display_errors();
                    //return FALSE;
                    }
                    
                    if ($prefix != "d41d8cd98f00b204e9800998ecf8427e") {
                         $sql = "INSERT INTO tbl_upload (filedata,filename,areaconected,date_uploaded,atachID,original_filename) VALUES ('".$prefix."','".$_FILES['userfile']['name']."','R','".$posteddate."','".$clientID."','".$originalname."')";
                         $query = $this->db->query($sql);
                    }
          }
     }

     function send_referal_email($clientID) {

          if ($_SESSION['siteID']==1)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'referrals@healthpredictions.com', 
                 'smtp_pass' => '7hLEYNuvuGNzjuKj', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@healthpredictions.com'
               );
          }
          else if($_SESSION['siteID']==2)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'BomboraReferrals@healthpredictions.com', 
                 'smtp_pass' => 'E3qnKdsZKtjS8qUp', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@bomboramedical.com.au'
               );
          }
          else if($_SESSION['siteID']==6)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'm3referrals@healthpredictions.com', 
                 'smtp_pass' => 'tuqQd3Yw9HskqVG8', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@m3lifewise.com.au'
               );
          }
          else if($_SESSION['siteID']==8)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'synchronreferrals@healthpredictions.com', 
                 'smtp_pass' => 'RG3Z2ZxCvGrNc8f8', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@synchronmedical.com.au'
               );
          }
          else if($_SESSION['siteID']==9)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'referrals@centrepointhealth.com.au', 
                 'smtp_pass' => 'jCYrgYKkC9y9BQJp', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@centrepointhealth.com.au'
               );
          }
          else if($_SESSION['siteID']==10)
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'm3referrals@healthpredictions.com', 
                 'smtp_pass' => 'tuqQd3Yw9HskqVG8', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@m3lifewise.com.au'
               );
          }
          else
          {
             $config = Array(
                 'protocol' => 'sendmail',
                 'smtp_host' => 'smtp.office365.com',
                 'smtp_port' => 587,
                 'smtp_user' => 'referrals@healthpredictions.com', 
                 'smtp_pass' => '7hLEYNuvuGNzjuKj', 
                 'mailtype' => 'html',
                 'charset' => 'iso-8859-1',
                 'wordwrap' => TRUE,
                 'from' => 'referrals@healthpredictions.com'
               );
          }

          $sql1 = "SELECT * FROM tbl_referral WHERE referralID = '".$clientID."'";
          $query1 = $this->db->query($sql1);
          $referralData = $query1->result_array();

          $sql2 = "SELECT * FROM tbl_referral_tests WHERE referralID = '".$clientID."'";
          $query2 = $this->db->query($sql2);
          $testData = $query2->result_array();
          $indTest = explode(",",$testData[0]['testtypeID']);
          $finalTests = "";

          foreach($indTest as $test) {
               $sql7 = "SELECT * FROM tbl_client_testtypes WHERE testtypeID = '".$test."'";
               $query7 = $this->db->query($sql7);
               $finalData = $query7->result_array();
               $finalTests .= $finalData[0]['type_name']."<br/>";
          }

          $sql3 = "SELECT * FROM tbl_referral_testnotes WHERE referralID = '".$clientID."'";
          $query3 = $this->db->query($sql3);
          $testnoteData = $query3->result_array();

          $sql4 = "SELECT * FROM tbl_upload WHERE atachID = '".$clientID."'";
          $query4 = $this->db->query($sql4);
          $uploadData = $query4->result_array();

          $sql5 = "SELECT * FROM tbl_insurance WHERE insID = '".$referralData[0]['insurance_company']."'";
          $query5 = $this->db->query($sql5);
          $insuranceData = $query5->result_array();

          $sql6 = "SELECT * FROM tbl_au_states WHERE id = '".$referralData[0]['client_state']."'";
          $query6 = $this->db->query($sql6);
          $stateData = $query6->result_array();

          $address = "";
          $address .= $referralData[0]['client_address'];
          if ($referralData[0]['client_address2'] != "") {$address .= ", ".$referralData[0]['client_address2'];}
          $address .= ", ".$referralData[0]['client_city'];
          $address .= ", ".$stateData[0]['state_code'];
          $address .= ", ".$referralData[0]['client_postcode'];

          $message = "<html>
<head>
  <title></title>
</head>
<body>
<b>Submitted by:</b> ".$_SESSION['usersname']."<br/>
<hr/>
<b>Name:</b> ".$referralData[0]['client_name']."<br/>
<b>Company/organization:</b> ".$referralData[0]['client_organization']."<br/>
<b>Date of Birth:</b> ".$referralData[0]['client_DOB']."<br/>
<b>Sex:</b> ".$referralData[0]['client_sex']."<br/>
<b>Insurance Company:</b> ".$insuranceData[0]['ins_name']."<br/>
<b>Application/Policy Number:</b> ".$referralData[0]['insurance_number']."<br/>
<b>Address:</b> ".$address."<br/>
<b>Email:</b> ".$referralData[0]['client_email']."<br/>
<b>Secondary Email:</b> ".$referralData[0]['client_email2']."<br/>
<b>Phone:</b> ".$referralData[0]['client_phone']."<br/>
<b>Work Phone:</b> ".$referralData[0]['client_workphone']."<br/>
<b>Mobile:</b> ".$referralData[0]['client_mobile']."<br/>
<b>Fax:</b> ".$referralData[0]['client_fax']."<br/>
<hr/>
<b>Notes:</b><br/>".$referralData[0]['client_notes']."<br/>
<hr/>
<b>Tests Selected:</b><br/>".$finalTests."<br/>
<b>Test Notes:</b>".$testnoteData[0]['testNote']."<br/>";
foreach ($uploadData as $file) {
               //$email->AddAttachment($_SERVER['DOCUMENT_ROOT']."/uploads/temp/".$file['filename']);
               $message .= $file['filename']."<br/>";
               $message .= $file['original_filename']."<br/>";
               //$email->addStringAttachment(file_get_contents("http://healthpredictions-com-au.swim.net.au/uploads/temp/0834f0a3d142338301b555cd3a5cd349logo-cscart.png"));
               }
$message .= "</body>
</html>";
          $this->load->library('email', $config);
          $this->email->set_newline("\r\n");
          $this->email->from($config['from']);
          $this->email->to("melinda@healthpredictions.com");
          $this->email->subject('A new referral has been submitted');
          $this->email->message($message);
          // $email->AddAddress( 'melinda@healthpredictions.com' );

          if (array_filter($uploadData)) { 
               foreach ($uploadData as $file) {
               $this->email->attach($_SERVER['DOCUMENT_ROOT']."/uploads/temp/".$file['filename'],$file['original_filename']);
               //$email->addStringAttachment(file_get_contents("http://healthpredictions-com-au.swim.net.au/uploads/temp/".$file['filename'],$file['original_filename']));
               //$email->addStringAttachment(file_get_contents("http://healthpredictions-com-au.swim.net.au/uploads/temp/0834f0a3d142338301b555cd3a5cd349logo-cscart.png"));
               }
          } else {
               
          }

          return $this->email->send();






          /*$message = "<html>
<head>
  <title></title>
</head>
<body>
</body>
</html>";

          $to      = $originallist;
          $subject = $emailsubject;
          // To send HTML mail, the Content-type header must be set
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= 'Cc: ' . $cc . "\r\n";

          // Additional headers
          $headers .= 'From: Risktech Interactive Portal <no-reply@risktech.com.au>' . "\r\n";

          mail($to, $subject, $message, $headers);/**/
     }

     
}?>