<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Workloadreport_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_appointments() {
          $sql = "SELECT * FROM tbl_advisor WHERE advisor_deleted = 'N' ORDER BY advisor_company";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_date($postdata) {
          $appDate = strtotime($postdata['app_date']);
          // var_dump($appDate);
          // die();
          $sql = "SELECT * FROM tbl_appointments WHERE app_date LIKE '%".$appDate."%' AND  app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_date_count($postdata) {
          $appDate = strtotime($postdata['app_date']);
          // var_dump($appDate);
          // die();
          $sql = "SELECT app_name FROM tbl_appointments WHERE app_date LIKE '%".$appDate."%' AND  app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function search_between_date($startdate, $lastdate, $postdata) {
        if($startdate<0)
        {
          $startdate='000000000';
        }
        if($lastdate<0)
        {
          $lastdate='000000000';
        }
        if($startdate!='000000000' && $startdate<'1000000000')
        {
              $startdate="000".$startdate;
        }
        if($lastdate!='9999999999' && $lastdate<'1000000000')
        {
              $lastdate="000".$lastdate;
        }
          $sql = "SELECT * FROM tbl_appointments WHERE app_date between '".$startdate."' AND '".$lastdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

      function search_between_date_count($startdate, $lastdate, $postdata) {
          // var_dump($appDate);
          // die();
          if($startdate<0)
          {
            $startdate='000000000';
          }
          if($lastdate<0)
          {
            $lastdate='000000000';
          }
          if($startdate!='000000000' && $startdate<'1000000000')
          {
                $startdate="000".$startdate;
          }
          if($lastdate!='9999999999' && $lastdate<'1000000000')
          {
                $lastdate="000".$lastdate;
          }
          $sql = "SELECT app_name FROM tbl_appointments WHERE app_date between '".$startdate."' AND '".$lastdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function search_no_date($startdate, $lastdate, $postdata) {
        if($startdate<0)
        {
          $startdate='000000000';
        }
        if($lastdate<0)
        {
          $lastdate='000000000';
        }
        if($startdate!='000000000' && $startdate<'1000000000')
        {
              $startdate="000".$startdate;
        }
        if($lastdate!='9999999999' && $lastdate<'1000000000')
        {
              $lastdate="000".$lastdate;
        }
          $sql = "SELECT * FROM tbl_appointments WHERE app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

      function search_no_date_count($startdate, $lastdate, $postdata) {
          // var_dump($appDate);
          // die();
          if($startdate<0)
          {
            $startdate='000000000';
          }
          if($lastdate<0)
          {
            $lastdate='000000000';
          }
          if($startdate!='000000000' && $startdate<'1000000000')
          {
                $startdate="000".$startdate;
          }
          if($lastdate!='9999999999' && $lastdate<'1000000000')
          {
                $lastdate="000".$lastdate;
          }
          $sql = "SELECT app_name FROM tbl_appointments WHERE app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function search_date1($startdate, $lastdate, $postdata) {
        if($startdate<0)
        {
          $startdate='000000000';
        }
        if($lastdate<0)
        {
          $lastdate='000000000';
        }
        if($startdate!='000000000' && $startdate<'1000000000')
        {
              $startdate="000".$startdate;
        }
        if($lastdate!='9999999999' && $lastdate<'1000000000')
        {
              $lastdate="000".$lastdate;
        }
          $sql = "SELECT * FROM tbl_appointments WHERE app_date > '".$startdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

      function search_date1_count($startdate, $lastdate, $postdata) {
          // var_dump($appDate);
          // die();
          if($startdate<0)
          {
            $startdate='000000000';
          }
          if($lastdate<0)
          {
            $lastdate='000000000';
          }
          if($startdate!='000000000' && $startdate<'1000000000')
          {
                $startdate="000".$startdate;
          }
          if($lastdate!='9999999999' && $lastdate<'1000000000')
          {
                $lastdate="000".$lastdate;
          }
          $sql = "SELECT app_name FROM tbl_appointments WHERE app_date > '".$startdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }


     function search_date2($startdate, $lastdate, $postdata) {
        if($startdate<0)
        {
          $startdate='000000000';
        }
        if($lastdate<0)
        {
          $lastdate='000000000';
        }
        if($startdate!='000000000' && $startdate<'1000000000')
        {
              $startdate="000".$startdate;
        }
        if($lastdate!='9999999999' && $lastdate<'1000000000')
        {
              $lastdate="000".$lastdate;
        }
          $sql = "SELECT * FROM tbl_appointments WHERE app_date < '".$lastdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

      function search_date2_count($startdate, $lastdate, $postdata) {
          // var_dump($appDate);
          // die();
          if($startdate<0)
          {
            $startdate='000000000';
          }
          if($lastdate<0)
          {
            $lastdate='000000000';
          }
          if($startdate!='000000000' && $startdate<'1000000000')
          {
                $startdate="000".$startdate;
          }
          if($lastdate!='9999999999' && $lastdate<'1000000000')
          {
                $lastdate="000".$lastdate;
          }
          $sql = "SELECT app_name FROM tbl_appointments WHERE app_date < '".$lastdate."' AND app_name LIKE '%".$postdata['name']."%' AND app_deleted = 'N' ORDER BY app_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

    
 }

 ?>