<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class specialist_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_specialist($specID) {
          $sql = "SELECT * FROM tbl_specialist WHERE specID = '".$specID."' AND spec_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_specialist_clinic($clinicID) {
          $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinicID = '".$clinicID."' AND clinic_deleted = 'N'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_specialist_all() {
          $sql = "SELECT * FROM tbl_specialist WHERE spec_deleted = 'N' ORDER BY spec_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     //get the username & password from tbl_usrs
     function get_specialistclinic_all() {
          $sql = "SELECT * FROM tbl_specialist_clinic WHERE clinic_deleted = 'N' ORDER BY clinic_name ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     // Add new General Practitioner
     function new_specialist_info($specialistdata) {
          $time = time();
          $sql = "INSERT INTO tbl_specialist (spec_name,
          siteID, 
          spec_profession, 
          spec_clinic, 
          spec_clinic2, 
          spec_clinic3, 
          spec_clinic4, 
          spec_clinic5, 
          spec_clinic6, 
          spec_clinic7, 
          spec_clinic8, 
          spec_address, 
          spec_address2, 
          spec_city, 
          spec_state, 
          spec_postcode, 
          spec_country, 
          spec_phone, 
          spec_mobile, 
          spec_fax, 
          spec_email, 
          spec_email2, 
          spec_responsibleperson, 
          spec_active, 
          spec_date_of_active, 
          spec_date_of_inactive, 
          spec_instructions,
          dateadded,
          spec_notes) 
          VALUES 
          ('".$specialistdata['spec_name']."', 
          '".$specialistdata['siteID']."', 
          '".$specialistdata['spec_profession']."', 
          '".$specialistdata['spec_clinic']."', 
          '".$specialistdata['spec_clinic2']."', 
          '".$specialistdata['spec_clinic3']."', 
          '".$specialistdata['spec_clinic4']."', 
          '".$specialistdata['spec_clinic5']."', 
          '".$specialistdata['spec_clinic6']."', 
          '".$specialistdata['spec_clinic7']."', 
          '".$specialistdata['spec_clinic8']."', 
          '".$specialistdata['spec_address']."', 
          '".$specialistdata['spec_address2']."', 
          '".$specialistdata['spec_city']."', 
          '".$specialistdata['spec_state']."', 
          '".$specialistdata['spec_postcode']."', 
          '".$specialistdata['spec_country']."', 
          '".$specialistdata['spec_phone']."', 
          '".$specialistdata['spec_mobile']."', 
          '".$specialistdata['spec_fax']."', 
          '".$specialistdata['spec_email']."', 
          '".$specialistdata['spec_email2']."', 
          '".$specialistdata['spec_responsibleperson']."', 
          '".$specialistdata['spec_active']."', 
          '".$specialistdata['spec_date_of_active']."', 
          '".$specialistdata['spec_date_of_inactive']."', 
          '".$specialistdata['spec_instructions']."',
          '".$time."',
          '".$specialistdata['spec_notes']."')";
          $query = $this->db->query($sql);
     }


     // Add new General Practitioner
     function new_specclinic_info($specialistdata) {
          $sql = "INSERT INTO tbl_specialist (clinic_name,
          siteID, 
          clinic_profession, 
          clinic_address, 
          clinic_city, 
          clinic_state, 
          clinic_postcode, 
          clinic_country, 
          clinic_phone, 
          clinic_mobile, 
          clinic_fax, 
          clinic_email, 
          clinic_email2, 
          clinic_responsibleperson, 
          clinic_active, 
          clinic_instructions,
          spec_notes) 
          VALUES 
          ('".$specialistdata['clinic_name']."', 
          '".$specialistdata['siteID']."', 
          '".$specialistdata['clinic_profession']."', 
          '".$specialistdata['clinic_address']."', 
          '".$specialistdata['clinic_city']."', 
          '".$specialistdata['clinic_state']."', 
          '".$specialistdata['clinic_postcode']."', 
          '".$specialistdata['clinic_country']."', 
          '".$specialistdata['clinic_phone']."', 
          '".$specialistdata['clinic_mobile']."', 
          '".$specialistdata['clinic_fax']."', 
          '".$specialistdata['clinic_email']."', 
          '".$specialistdata['clinic_email2']."', 
          '".$specialistdata['clinic_responsibleperson']."', 
          '".$specialistdata['clinic_active']."', 
          '".$specialistdata['clinic_instructions']."',
          '".$specialistdata['clinic_notes']."')";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_specialist_info ($specialistdata) {
          $sql = "UPDATE tbl_specialist SET 
          spec_name = '".$specialistdata['spec_name']."',
          siteID = '".$specialistdata['siteID']."',
          spec_clinic = '".$specialistdata['spec_clinic']."',
          spec_clinic2 = '".$specialistdata['spec_clinic2']."',
          spec_clinic3 = '".$specialistdata['spec_clinic3']."',
          spec_clinic4 = '".$specialistdata['spec_clinic4']."',
          spec_clinic5 = '".$specialistdata['spec_clinic5']."',
          spec_clinic6 = '".$specialistdata['spec_clinic6']."',
          spec_clinic7 = '".$specialistdata['spec_clinic7']."',
          spec_clinic8 = '".$specialistdata['spec_clinic8']."',
          spec_profession = '".$specialistdata['spec_profession']."',
          spec_address = '".$specialistdata['spec_address']."',
          spec_address2 = '".$specialistdata['spec_address2']."',
          spec_city = '".$specialistdata['spec_city']."',
          spec_state = '".$specialistdata['spec_state']."',
          spec_postcode = '".$specialistdata['spec_postcode']."',
          spec_country = '".$specialistdata['spec_country']."',
          spec_phone = '".$specialistdata['spec_phone']."',
          spec_mobile = '".$specialistdata['spec_mobile']."',
          spec_fax = '".$specialistdata['spec_fax'].    "',
          spec_email = '".$specialistdata['spec_email']."',
          spec_email2 = '".$specialistdata['spec_email2']."',
          spec_active = '".$specialistdata['spec_active']."',
          spec_date_of_active = '".$specialistdata['spec_date_of_active']."',
          spec_date_of_inactive = '".$specialistdata['spec_date_of_inactive']."',
          spec_responsibleperson = '".$specialistdata['spec_responsibleperson']."',
          spec_instructions = '".$specialistdata['spec_instructions']."',
          spec_notes = '".$specialistdata['spec_notes']."'
          WHERE specID = ".$specialistdata['specID']."";
          $query = $this->db->query($sql);
     }

     // Update General Practitioner info
     function update_specclinic_info ($specialistdata) {
          $sql = "UPDATE tbl_specialist_clinic SET 
          clinic_name = '".$specialistdata['clinic_name']."',
          siteID = '".$specialistdata['siteID']."',
          clinic_profession = '".$specialistdata['clinic_profession']."',
          clinic_address = '".$specialistdata['clinic_address']."',
          clinic_city = '".$specialistdata['clinic_city']."',
          clinic_state = '".$specialistdata['clinic_state']."',
          clinic_postcode = '".$specialistdata['clinic_postcode']."',
          clinic_country = '".$specialistdata['clinic_country']."',
          clinic_phone = '".$specialistdata['clinic_phone']."',
          clinic_mobile = '".$specialistdata['clinic_mobile']."',
          clinic_fax = '".$specialistdata['clinic_fax'].    "',
          clinic_email = '".$specialistdata['clinic_email']."',
          clinic_email2 = '".$specialistdata['clinic_email2']."',
          clinic_active = '".$specialistdata['clinic_active']."',
          clinic_responsibleperson = '".$specialistdata['clinic_responsibleperson']."',
          clinic_instructions = '".$specialistdata['clinic_instructions']."',
          clinic_notes = '".$specialistdata['clinic_notes']."'
          WHERE clinicID = ".$specialistdata['clinicID']."";
          $query = $this->db->query($sql);
     }

     function search_specialist($searchData) {
          $sql = "SELECT * FROM tbl_specialist WHERE spec_name LIKE '%".$searchData['spec_name']."%' AND spec_city LIKE '%".$searchData['spec_city']."%' AND spec_state LIKE '%".$searchData['spec_state']."%' AND spec_profession LIKE '%".$searchData['spec_profession']."%' AND spec_deleted = 'N' ORDER BY spec_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_specialist_all($searchData) {

        if ($searchData['spec_address2']=='')
        {
           $address2="AND (spec_address2 LIKE '%".$searchData['spec_address2']."%' OR spec_address2 IS NULL)";
        }
        else
        {
           $address2="AND spec_address2 LIKE '%".$searchData['spec_address2']."%'";
        }
        if ($searchData['spec_email2']=='')
        {
           $email2="AND (spec_email2 LIKE '%".$searchData['spec_email2']."%' OR spec_email2 IS NULL)";
        }
       else
       {
           $email2="AND spec_email2 LIKE '%".$searchData['spec_email2']."%'";
       }

          $sql = "SELECT * FROM tbl_specialist WHERE 
          spec_name LIKE '%".$searchData['spec_name']."%' 
          AND spec_profession LIKE '%".$searchData['spec_profession']."%'
          AND spec_type LIKE '%".$searchData['spec_type']."%'
          AND spec_address LIKE '%".$searchData['spec_address']."%'".$address2."
          AND spec_city LIKE '%".$searchData['spec_city']."%'
          AND spec_postcode LIKE '%".$searchData['spec_postcode']."%'
          AND spec_state LIKE '%".$searchData['spec_state']."%' 
          AND spec_country LIKE '%".$searchData['spec_country']."%' 
          AND spec_phone LIKE '%".$searchData['spec_phone']."%' 
          AND spec_mobile LIKE '%".$searchData['spec_mobile']."%' 
          AND spec_fax LIKE '%".$searchData['spec_fax']."%' 
          AND spec_email LIKE '%".$searchData['spec_email']."%'".$email2."
          AND spec_active LIKE '%".$searchData['spec_active']."%'
          AND spec_deleted = 'N' ORDER BY spec_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_spec_city($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE spec_city LIKE '%".$searchData['spec_city']."%' ORDER BY spec_city";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_spec_state($searchData) {
          $sql = "SELECT * FROM tbl_GP WHERE spec_state LIKE '%".$searchData['spec_state']."%' ORDER BY spec_state";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_spec_clinicall() {
          $sql = "SELECT * FROM tbl_specialist_clinic ORDER BY clinicID ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_advisor($advisorID) {
          $sql = "SELECT * FROM tbl_advisor WHERE advisorID = '".$advisorID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

               //get the states from tbl_states
     function get_au_states() {
          $sql = "SELECT * FROM tbl_au_states WHERE id =  ORDER BY id ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_specialist_info($specID) {
          $sql = "UPDATE tbl_specialist SET 
          spec_deleted = 'Y'
          WHERE specID = ".$specID."";
          $query = $this->db->query($sql);
     }

     function delete_specialistclinic_info($clinicID) {
          $sql = "UPDATE tbl_specialist_clinic SET 
          clinic_deleted = 'Y'
          WHERE clinicID = ".$clinicID."";
          $query = $this->db->query($sql);
     }

     
}?>