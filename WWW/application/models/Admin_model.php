<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
    function get_users($siteID) {
          $sql = "SELECT * FROM tbl_users WHERE usr_status = 'A' AND userID > '1'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_users() {
          $sql = "SELECT * FROM tbl_users WHERE usr_status = 'A' AND userID > '1'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_search_users($searchData) {
      $sql = "SELECT * FROM tbl_users WHERE userID > '1'";
      if($searchData['user_name'] != '') {
        $sql .= "AND name LIKE '%".$searchData['user_name']."%'";
      }

      if($searchData['email_name'] != '') {
        $sql .= "AND email LIKE '%".$searchData['email_name']."%'";
      }

      if($searchData['usr_status'] != '' && $searchData['usr_status'] != 'ALL') {
        $sql .= "AND usr_status = '".$searchData['usr_status']."'";
      }

      $query = $this->db->query($sql);
      return $query->result_array();
 }

     function get_type_data() {
          $sql = "SELECT * FROM tbl_usertypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_all_sites() {
          $sql = "SELECT * FROM tbl_sites";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_site_data($siteID) {
          $sql = "SELECT * FROM tbl_sites WHERE siteID = '".$siteID."'";;
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_data($userID) {
          $sql = "SELECT * FROM tbl_users WHERE userID = '".$userID."'";;
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_site_info ($siteData, $image) {
      $script = "";

      if($image != "0") {
        $script = "site_logo = '".$image."',";
      }

          $sql = "UPDATE tbl_sites SET 
          site_name = '".$siteData['site_name']."',
          site_domain = '".$siteData['site_domain']."',
          ".$script."
          site_maincolour = '".$siteData['site_maincolour']."',
          site_maincolour_text = '".$siteData['site_maincolour_text']."',
          site_seccolour = '".$siteData['site_seccolour']."',
          site_seccolour_text = '".$siteData['site_seccolour_text']."',
          site_thrcolour = '".$siteData['site_thrcolour']."',
          site_thrcolour_text = '".$siteData['site_thrcolour_text']."',
          site_address = '".$siteData['site_address']."',
          site_address2 = '".$siteData['site_address2']."',
          site_city = '".$siteData['site_city']."',
          site_state = '".$siteData['site_state']."',
          site_country = '".$siteData['site_country']."',
          site_postcode = '".$siteData['site_postcode']."',
          site_phone = '".$siteData['site_phone']."',
          site_fax = '".$siteData['site_fax']."',
          site_email = '".$siteData['site_email']."',
          site_website = '".$siteData['site_website']."'
          WHERE siteID = ".$siteData['siteID']."";
          $query = $this->db->query($sql);
     }

     function new_site_info ($siteData, $image) {
      if($image != "0") {
        $finalimage = $image;
      } else {
        $finalimage = 'sample-logo.jpg';
      }

        $sql = "INSERT INTO tbl_sites (
          site_name,
          site_domain,
          site_logo,
          site_maincolour,
          site_maincolour_text,
          site_seccolour,
          site_seccolour_text,
          site_thrcolour,
          site_thrcolour_text
          ) VALUES (
          '".$siteData['site_name']."',
          '".$siteData['site_domain']."',
          '".$finalimage."',
          '".$siteData['site_maincolour']."',
          '".$siteData['site_maincolour_text']."',
          '".$siteData['site_seccolour']."',
          '".$siteData['site_seccolour_text']."',
          '".$siteData['site_thrcolour']."',
          '".$siteData['site_thrcolour_text']."'
          )";
        $query = $this->db->query($sql); 
     }

     function delete_site_info ($siteID) {
     $sql = "DELETE FROM tbl_sites WHERE siteID = '".$siteID."'";
      $query = $this->db->query($sql);
   }

   function new_user_info ($siteData) {
        $sql = "INSERT INTO tbl_users (
          name,
          username,
          password,
          email,
          email2,
          usr_phone,
          usr_mobile,
          type_letter,
          siteID,
          usr_status
          ) VALUES (
          '".$siteData['name']."',
          '".$siteData['username']."',
          '".md5($siteData['password'])."',
          '".$siteData['email']."',
          '".$siteData['email2']."',
          '".$siteData['usr_phone']."',
          '".$siteData['usr_mobile']."',
          '".$siteData['type_letter']."',
          '".$siteData['siteID']."',
          'A'
          )";
        $query = $this->db->query($sql); 
     }

     function update_user_info ($siteData) {
      $script = "";

      if($siteData['password'] != "") {
        $script = "password = '".md5($siteData['password'])."',";
      }

          $sql = "UPDATE tbl_users SET 
          name = '".$siteData['name']."',
          username = '".$siteData['username']."',
          ".$script."
          email = '".$siteData['email']."',
          email2 = '".$siteData['email2']."',
          usr_phone = '".$siteData['usr_phone']."',
          usr_mobile = '".$siteData['usr_mobile']."',
          type_letter = '".$siteData['type_letter']."',
          usr_status = '".$siteData['usr_status']."'
          WHERE userID = ".$siteData['userID']."";
          $query = $this->db->query($sql);
     }

     function delete_user_info($userID) {
      $sql = "SELECT * FROM tbl_users WHERE userID = ".$userID."";
    $query = $this->db->query($sql);
    $userInfo = $query->result_array();

    if ($advisorInfo[0]['type_letter'] == 'I'){
      $sql2 = "UPDATE tbl_users SET 
        usr_status = 'D'
        WHERE userID = ".$userInfo[0]['userID']."";
      $query2 = $this->db->query($sql2);

      $sql3 = "UPDATE tbl_advisor SET 
        advisor_status = 'D',
        advisor_deleted = 'Y'
        WHERE userID = ".$userInfo[0]['userID']."";
      $query3 = $this->db->query($sql3);
    } else {
      $sql2 = "UPDATE tbl_users SET 
        usr_status = 'D'
        WHERE userID = ".$userInfo[0]['userID']."";
      $query2 = $this->db->query($sql2);

      $sql3 = "UPDATE tbl_staff SET 
        staff_deleted = 'Y'
        WHERE staffID = ".$userInfo[0]['linkedID']."";
      $query3 = $this->db->query($sql3);
    }

 }

     

     
}?>