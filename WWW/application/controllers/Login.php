<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          $this->load->model('login_model');
          $this->load->model('email_model');
          $this->load->model('global_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  $this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  //if ( ! $this->session->userdata('loginuser'))
		   //     { 
        	//	    redirect('login/index');
		   //     }
    }

	public function index()
	{
    $siteURL = $_SERVER['HTTP_HOST'];
    $data['sitedetails'] = $this->global_model->get_site_info($siteURL);
		$this->load->view('login/index', $data);
	}

  public function login() {
          //get the posted values
          $username = $this->input->post("txt_username");
          $password = $this->input->post("txt_password");

          //set validations
          $this->form_validation->set_rules("txt_username", "Username", "trim|required");
          $this->form_validation->set_rules("txt_password", "Password", "trim|required");

          if ($this->form_validation->run() == FALSE) {

            //validation fails
            $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Please ensure you have entered your username and password</div>');
            redirect('login/index');
          } else {
               //validation succeeds
              if ($this->input->post('btn_login') == "Sign In") {
                    //check if the master is logging in
                    if ($username == 'swimcommunications' && $password == 'swimcommunications') {
                      //set the session variables
                      $sessiondata['username'] = $username;
                      $sessiondata['loginuser'] = TRUE;
                      $sessiondata['userID'] = 0;
                      $sessiondata['usertype'] = 'A';
                      $sessiondata['usersname'] = 'The Creator';
                      $sessiondata['useremail'] = 'master@swim.com.au';
                      $sessiondata['siteID'] = 1;
                      //$this->login_model->setlastlogin($username, $password);
                      $this->session->set_userdata($sessiondata);
                      redirect('home/index');
                    } else {
                      //check if username and password is correct
                      $usr_result = $this->login_model->get_user($username, $password);
                      //active user record is present
                      if ($usr_result['rownumber'] > 0) {
                        //set the session variables
                        $sessiondata['username'] = $username;
                        $sessiondata['loginuser'] = TRUE;
                       
                        foreach ($usr_result['userdetails'] as $userdata) {
                          $sessiondata['userID'] = $userdata['userID'];
                          $sessiondata['usertype'] = $userdata['type_letter'];
                          $sessiondata['usersname'] = $userdata['name'];
                          $sessiondata['useremail'] =  $userdata['email'];
                          $sessiondata['siteID'] = $userdata['siteID'];
                        }
                        $this->login_model->setlastlogin($username, $password);  
                        $this->session->set_userdata($sessiondata);
                        if ($userdata['type_letter'] == 'I') {
                          redirect('advisorhome/index');
                        } else {
                          //print_r($sessiondata);
                          redirect('home/index');
                        }
                      } else {
                        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
                        redirect('login/index');
                      }
                    }
              } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
                redirect('login/index');
              }
          }
}

public function forgotpassword() {
  $siteURL = $_SERVER['SERVER_NAME'];
    $data['sitedetails'] = $this->global_model->get_site_info($siteURL);
    $this->load->view('login/forgotpassword', $data);
}

public function temppassword() {
  //get the posted values
          $username = $this->input->post("txt_username");
          //set validations
          $this->form_validation->set_rules("txt_username", "Username", "trim|required");

          if ($this->form_validation->run() == FALSE) {
            //validation fails
            $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Please ensure you have entered your username</div>');
            $this->load->view('login/forgotpassword');
          } else {
               //validation succeeds
              if ($this->input->post('btn_login') == "Sign In") {
                    //check if the master is logging in
                    $usr_result = $this->login_model->get_forget_user($username);
                      //active user record is present
                      if ($usr_result['rownumber'] > 0) {
                        $newpassword = $this->login_model->generateRandomString();
                        $this->login_model->set_forget_user_password($username, $newpassword); 

                        $to = $usr_result['userdetails'][0]['email'];
                        // To send HTML mail, the Content-type header must be set
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                        // Additional headers
                        $headers .= 'From: Health Predictions <no-reply@healthpredictions>' . "\r\n";

                        $this->email_model->resetpassword($to, $newpassword);
                      } 
              } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password!</div>');
                redirect('login/index');
              }
          }
}
   
public function logout() {
  session_destroy();
  redirect('login/index');
}

}
