<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Staff extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('staff_model');
    $this->load->model('staff_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Staff', 'link' => 'staff/index' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Staff";
    $mainheader['menuitem']     = 9;
    $mainheader['menuitem_toggle']     = '';

    $data['staffData']        = $this->staff_model->get_staff_all();
    $data['staffState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/index', $data);
    $this->load->view('main_footer');
  }

  public function view($staffID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Staff', 'link' => 'staff/index' );
    $header['breadcrumbs'][]    = array('title' => 'View', 'link' => '' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Staff";
    $mainheader['menuitem']     = 9;
    $mainheader['menuitem_toggle']     = '';
    $data['staffData']        = $this->staff_model->get_staff($staffID);
    $data['typeData']           = $this->global_model->get_type_data();
    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Staff', 'link' => 'staff/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Pathology";
    $mainheader['menuitem']     = 9;
    $mainheader['menuitem_toggle']     = '';
    $data['typeData']           = $this->global_model->get_type_data();

    $data['country']            = $this->global_model->get_all_country();
    $data['staffState']            = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/add', $data);
    $this->load->view('main_footer');
  }

  public function edit($staffID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Staff', 'link' => 'staff/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Staff', 'link' => '' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $data['typeData']           = $this->global_model->get_type_data();

    $mainheader['title']        = "Edit Staff";
    $mainheader['menuitem']     = 9;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['staffData']      = $this->staff_model->get_staff($staffID);
    $data['staffState']     = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('staff/edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Staff', 'link' => 'staff/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Pathology Search Result";
    $mainheader['menuitem']     = 9;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['staffData'] = $this->staff_model->search_staff($postdata);
    $data['searchresults'] = $postdata;
    $data['staffState']       = $this->global_model->get_all_austates();
    $data['pathologyState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Staff', 'link' => 'staff/search_all' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Staff";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 20;
    $data['location']         = "SearchAll";
    $data['staffData']        = $this->staff_model->get_staff_all();
    $data['staffState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/search_all', $data);
    $this->load->view('main_footer');
  }

  // public function services_stats()
  // {
  //   $header['breadcrumbs'][]    = array('title' => 'Search Staff', 'link' => 'staff/services_stats' );
  //   $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
  //   $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

  //   $mainheader['title']        = "Staff";
  //   $mainheader['menuitem']     = 11;
  //   $mainheader['menuitem_toggle']     = 28;
  //   $data['location']         = "ServicesStats";
  //   $data['staffData']        = $this->staff_model->get_staff_all();
  //   $data['staffState']       = $this->global_model->get_all_austates();
  //   $data['country']            = $this->global_model->get_all_country();

  //   $this->load->view('main_header', $mainheader);
  //   $this->load->view('main_bar', $header);
  //   $this->load->view('staff/search_all', $data);
  //   $this->load->view('main_footer');
  // }

  // public function services_stats_result()
  // {
  //   $header['breadcrumbs'][]    = array('title' => 'Search Staff', 'link' => 'staff/services_stats' );
  //   $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
  //   $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
  //   $data['location']         = "ServicesStats";
  //   $mainheader['title']        = "Staff";
  //   $mainheader['menuitem']     = 11;
  //   $mainheader['menuitem_toggle']     = 28;
     
  //   $postdata = $this->input->post();

  //   $data['staffData'] = $this->staff_model->search_staff_all($postdata);
  //   // var_dump($postdata); 
  //   $data['searchresults'] = $postdata;

  //   $data['staffState']       = $this->global_model->get_all_austates();
  //   $data['country']            = $this->global_model->get_all_country();

  //   $this->load->view('main_header', $mainheader);
  //   $this->load->view('main_bar', $header);
  //   $this->load->view('staff/search_all', $data);
  //   $this->load->view('main_footer');
  // }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Staff', 'link' => 'staff/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-h-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $data['location']         = "SearchAll";
    $mainheader['title']        = "Staff";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 20;

    $postdata = $this->input->post();

    $data['staffData'] = $this->staff_model->search_staff_all($postdata);
    // var_dump($postdata); 
    $data['searchresults'] = $postdata;

    $data['staffState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('staff/search_all', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $staffID = $this->input->post('staffID');

    $this->staff_model->update_staff_info($postdata);
    $this->staff_model->update_user_info($postdata);

    redirect('/staff/view/'.$staffID);
  }

  public function newstaff()
  {
    $postdata = $this->input->post();
    $email = $this->input->post('staff_email');
    $sql = "SELECT * FROM tbl_staff WHERE staff_email = '".$email."'" ;
    $query = $this->db->query($sql);
    $sql2 = "SELECT * FROM tbl_users WHERE email = '".$email."'" ;
    $query2 = $this->db->query($sql2);
    if($email!='')
    {
    if ($query->num_rows()==0 && $query2->num_rows()==0)
    {
      $staffID = $this->staff_model->new_staff_info($postdata);
      $this->staff_model->new_user_info($staffID, $postdata);
      redirect('/staff/index/');
    }
    else
    {
        $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Staff email exist. Please try again later.</div>');
        redirect('/staff/add/');
    }
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Email cannot be empty.</div>');
      redirect('/staff/add/');

    }
  }

  public function delete($staffID)
  {
    $this->staff_model->delete_staff_info($staffID);

    redirect('/staff/index/');
  }


}
