<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Workloadreport extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('appointment_model');
        $this->load->model('advisor_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('clientdiary_model');
        $this->load->model('referralsadvisor_model');
        $this->load->model('referralbycreation_model');
        $this->load->model('groupbyinsurance_model');
        $this->load->model('workloadreport_model');
        $this->load->model('pathology_model');
        $this->load->model('specialist_model');
        $this->load->model('nurse_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Workload Report', 'link' => 'workloadreport/index' );
    $header['icon']             = '<i class="fa fa-briefcase" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Workload Report";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 25;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['advData']           = $this->appointment_model->get_all_client_appointment();
    $data['pathologyData']        = $this->pathology_model->get_pathology_all();
    $data['pathologyState']       = $this->global_model->get_all_austates();
    $data['gpData']        = $this->gp_model->get_gp_all();
    $data['gpData_indigp']        = $this->gp_model->get_gp_indigp();
    $data['specialistData']        = $this->specialist_model->get_specialist_all();
    $data['specialistState']       = $this->global_model->get_all_austates();
    $data['nurseData']        = $this->nurse_model->get_nurse_all();
    $data['nurseState']       = $this->global_model->get_all_austates();
    // $data['appData']   = $this->client_model->get_client_apointment($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('workloadreport/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_app_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Workload Report', 'link' => 'workloadreport/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-briefcase" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 25;

    $postdata = $this->input->post();
    // $date = strtotime($postdata['app_date']);
    

    // var_dump($postdata);
    // die();
    $data['appData'] = $this->workloadreport_model->search_date($postdata);
    $data['countData'] = $this->workloadreport_model->search_date_count($postdata);

    // $date = $postdata['app_date'];
    // var_dump($date);
    // die();
    
    $data['searchresults'] = $postdata;

    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('workloadreport/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_between_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Workload Report', 'link' => 'workloadreport/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-briefcase" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 25;

    $postdata = $this->input->post();
    $startdate = strtotime($postdata['startdate']);
    $lastdate = strtotime($postdata['lastdate']);

    // var_dump($postdata['startdate']);
    // var_dump($lastdate);
    // die();

    $data['searchresults'] = $postdata;
    // var_dump($postdata); 
    if ($startdate == '' && $lastdate == '') {
        $data['appData'] = $this->workloadreport_model->search_no_date($startdate, $lastdate, $postdata);
    $data['countData'] = $this->workloadreport_model->search_no_date_count($startdate, $lastdate, $postdata);
} else if ($startdate != '' && $lastdate == '') {
    $data['appData'] = $this->workloadreport_model->search_date1($startdate, $lastdate, $postdata);
    $data['countData'] = $this->workloadreport_model->search_date1_count($startdate, $lastdate, $postdata);
} else if ($startdate == '' && $lastdate != '') {
    $data['appData'] = $this->workloadreport_model->search_date2($startdate, $lastdate, $postdata);
    $data['countData'] = $this->workloadreport_model->search_date2_count($startdate, $lastdate, $postdata);
} else {
    $data['appData'] = $this->workloadreport_model->search_between_date($startdate, $lastdate, $postdata);
    $data['countData'] = $this->workloadreport_model->search_between_date_count($startdate, $lastdate, $postdata);
}
    

    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('workloadreport/index', $data);
    $this->load->view('main_footer');
}

}
?>