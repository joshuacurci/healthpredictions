<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advisorhome extends CI_Controller {

	public function __construct()
    {
      parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
          $this->load->model('global_model');
          $this->load->model('advisorhome_model');
          $this->load->model('client_model');
          $this->load->library('PHPmailer');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  if ( ! $this->session->userdata('loginuser')) { 
        redirect('login/index');
      }
    }

	public function index()
	{
    $header['breadcrumbs'][] = array('title' => 'Dashboard', 'link' => 'advisorhome/index' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_referral($mainheader['advisorData'][0]['advisorID']);
    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle']     = '';

		$this->load->view('advisor_header', $mainheader);
		$this->load->view('advisor_bar', $header);
    $this->load->view('advisorhome/index');
		$this->load->view('advisor_footer');
	}

}
