<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientdiary extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('advisor_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('clientdiary_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Diary', 'link' => 'clientdiary/index' );
    $header['icon']             = '<i class="fa fa-book" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Client Diary";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 21;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientdiary/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_app_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Client Diary', 'link' => 'clientdiary/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Specific Date', 'link' => '' );
    $header['icon']             = '<i class="fa fa-book" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 21;

    $postdata = $this->input->post();
    $date = strtotime($postdata['app_date']);

    $data['appData'] = $this->clientdiary_model->search_app_date($date);
    $data['searchresults'] = $postdata;


    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientdiary/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_between_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Client Diary', 'link' => 'clientdiary/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-book" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 21;

    $postdata = $this->input->post();
    $startdate = strtotime($postdata['startdate']);
    $lastdate = strtotime($postdata['lastdate']);
    // var_dump($postdata); 
    $data['appData'] = $this->clientdiary_model->search_between_date($startdate, $lastdate);
    $data['searchresults'] = $postdata;


    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientdiary/index', $data);
    $this->load->view('main_footer');
}

}
?>