<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Specialists extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('pathology_model');
    $this->load->model('specialist_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Specialists";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    $data['specialistData']        = $this->specialist_model->get_specialist_all();
    $data['specialistState']       = $this->global_model->get_all_austates();
    $data['specialistClinic']        = $this->specialist_model->get_specialistclinic_all();
    
    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/index', $data);
    $this->load->view('main_footer');
  }

  public function view($specID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Individual Specialist Information', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Specialist";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';
    $data['specialistData']     = $this->specialist_model->get_specialist($specID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/view', $data);
    $this->load->view('main_footer');
  }

  public function clinic_view($clinicID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Specialist Clinic Information', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Clinic";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';
    $data['clinicData']     = $this->specialist_model->get_specialist_clinic($clinicID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/clinic_view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Individual Specialist', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Specialist";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['specialistState']     = $this->global_model->get_all_austates();
    $data['specClinic']           = $this->specialist_model->get_spec_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/add', $data);
    $this->load->view('main_footer');
  }

  public function clinic_add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Specialist Clinic', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Specialist Clinic";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['specialistState']     = $this->global_model->get_all_austates();
    $data['specClinic']           = $this->specialist_model->get_spec_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/clinic_add', $data);
    $this->load->view('main_footer');
  }

  public function edit($specID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Individual Specialist', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Specialist";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['specialistData']      = $this->specialist_model->get_specialist($specID);
    $data['specialistState']     = $this->global_model->get_all_austates();
    $data['specClinic']           = $this->specialist_model->get_spec_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('specialists/edit', $data);
    $this->load->view('main_footer');
  }

  public function clinic_edit($clinicID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'specialists/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Specialist Clinic', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Clinic";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['clinicData']      = $this->specialist_model->get_specialist_clinic($clinicID);
    $data['specialistState']     = $this->global_model->get_all_austates();
    $data['specClinic']           = $this->specialist_model->get_spec_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('specialists/clinic_edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'gp/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-user-md" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Specialist Search Result";
    $mainheader['menuitem']     = 5;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['specialistData'] = $this->specialist_model->search_specialist($postdata);
    $data['searchresults'] = $postdata;

    $data['specialistState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Specialists', 'link' => 'specialists/search_all' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Specialist";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 16;

    $data['specialistData']        = $this->specialist_model->get_specialist_all();
    $data['specialistState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/search_all', $data);
    $this->load->view('main_footer');
  }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Specialists', 'link' => 'specialists/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Specialists";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 16;

    $postdata = $this->input->post();

    $data['specialistData'] = $this->specialist_model->search_specialist_all($postdata);
    // var_dump($postdata); 
    $data['searchresults'] = $postdata;

    $data['specialistState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('specialists/search_all', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $specID = $this->input->post('specID');


    $this->specialist_model->update_specialist_info($postdata);
    redirect('/specialists/view/'.$specID);
  
}

public function newspecialist()
{  
  $header['breadcrumbs'][]    = array('title' => 'Specialists', 'link' => 'advisor/index' );
  $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
  $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
  $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

  $mainheader['title']        = "Add New General Practitioners";
  $mainheader['menuitem']     = 5;
  $mainheader['menuitem_toggle']     = '';

  $data['country']            = $this->global_model->get_all_country();
  $data['specialistState']    = $this->global_model->get_all_austates();
  $data['specClinic']           = $this->specialist_model->get_spec_clinicall();
  $postdata = $this->input->post();

  $this->load->view('main_header', $mainheader);
  $this->load->view('main_bar', $header);

  if (!$postdata["spec_active"]) {
    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">"Please Please select if this Specialist is Active or Inactive".</div>');
    $this->load->view('specialists/add', $data);

  } else {
    $this->specialist_model->new_specialist_info($postdata);
    redirect('/specialists/index');
  }

  $this->load->view('main_footer');


}

public function save_clinic()
{ 
  $postdata = $this->input->post();
  $clinicID = $this->input->post('clinicID');


  $this->specialist_model->update_specclinic_info($postdata);
  redirect('/specialists/clinic_view/'.$clinicID);
  
}

public function new_clinic()
{ 
  $postdata = $this->input->post();

  $this->specialist_model->new_specclinic_info($postdata);
  redirect('/specialists/index/');
  
}

public function delete($specID)
{
  $this->specialist_model->delete_specialist_info($specID);

  redirect('/specialists/index/');
}

public function clinic_delete($clinicID)
{
  $this->specialist_model->delete_specialistclinic_info($clinicID);

  redirect('/specialists/index/');
}


}
