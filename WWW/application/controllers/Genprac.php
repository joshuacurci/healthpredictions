<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Genprac extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
      //$this->load->model('news_model');
      //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';


    $data['gpData']        = $this->gp_model->get_gp_all();
    $data['gpData_indigp']        = $this->gp_model->get_gp_indigp();
    $data['gpData_clinicgp']        = $this->gp_model->get_gp_clinicgp();
    $data['gpClinic']        = $this->gp_model->get_gp_clinic_all();
    $data['gpState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/index', $data);
    $this->load->view('main_footer');
  }

  public function view($GPID)
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Individual General Practitioner Information', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View General Practitioner";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';
    $data['gpData']             = $this->gp_model->get_gp($GPID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/view', $data);
    $this->load->view('main_footer');
  }


  public function clinic_view($clinicID)
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners Clinic', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'General Practitioner Clinic Information', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioner Clinic Information";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';
    $data['clinicData']             = $this->gp_model->get_clinic($clinicID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/clinic_view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Individual General Practitioner', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Add New General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['gpState']            = $this->global_model->get_all_austates();
    $data['gpClinic']           = $this->gp_model->get_gp_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/add', $data);
    $this->load->view('main_footer');
  }

  public function clinic_add()
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Add New General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['gpState']            = $this->global_model->get_all_austates();
    $data['gpClinic']           = $this->gp_model->get_gp_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/clinic_add', $data);
    $this->load->view('main_footer');
  }

  public function edit($GPID)
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Individual General Practitioner', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['gpData']             = $this->gp_model->get_gp($GPID);
    $data['gpState']            = $this->global_model->get_all_austates();
    $data['gpClinic']            = $this->gp_model->get_gp_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('gp/edit', $data);
    $this->load->view('main_footer');
  }

  public function clinic_edit($clinicID)
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners Clinc', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit General Practitioner Clinic', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['clinicData']             = $this->gp_model->get_clinic($clinicID);
    $data['gpState']            = $this->global_model->get_all_austates();
    $data['gpClinic']            = $this->gp_model->get_gp_clinicall();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('gp/clinic_edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    
    $data['gpData'] = $this->gp_model->search_gp($postdata);
    $data['searchresults'] = $postdata;

    $data['gpState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search General Practitioners', 'link' => 'genprac/search_all' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 13;


    $data['gpData']        = $this->gp_model->get_gp_all();
    $data['gpState']       = $this->global_model->get_all_austates();
    $data['gpClinic']           = $this->gp_model->get_gp_clinicall();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/search_all', $data);
    $this->load->view('main_footer');
  }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search  General Practitioners', 'link' => 'genprac/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 13;

    $postdata = $this->input->post();
    $data['gpData'] = $this->gp_model->search_gp_all($postdata);
    $data['searchresults'] = $postdata;

    $data['gpState']       = $this->global_model->get_all_austates();
    $data['gpClinic']           = $this->gp_model->get_gp_clinicall();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/search_all', $data);
    $this->load->view('main_footer');
  }

  public function citysearch()
  {

    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 2;

    $postdata = $this->input->post();
    $data['gpData'] = $this->gp_model->search_gp_city($postdata);
    $data['searchresults'] = $postdata;
    $data['searchresults']['GP_name'] = "";
    $data['searchresults']['GP_city'] = "";
    $data['searchresults']['GP_state'] = "";

    $data['gpState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/index', $data);
    $this->load->view('main_footer');
  }

  public function statesearch()
  {

    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'genprac/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "General Practitioners";
    $mainheader['menuitem']     = 2;

    $postdata = $this->input->post();
    $data['gpData'] = $this->gp_model->search_gp_state($postdata);
    $data['searchresults'] = $postdata;
    $data['searchresults']['GP_name'] = "";
    $data['searchresults']['GP_city'] = "";
    $data['searchresults']['GP_state'] = "";

    $data['gpState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('gp/index', $data);
    $this->load->view('main_footer');
  }

  public function save()
  { 
    $postdata = $this->input->post();
    $GPID = $this->input->post('GPID');
    
    $this->gp_model->update_gp_info($postdata); 
    redirect('/genprac/view/'.$GPID);

  }

  public function save_clinic()
  { 
    $postdata = $this->input->post();
    $clinicID = $this->input->post('clinicID');


    $this->gp_model->update_gpclinic_info($postdata);
    redirect('/genprac/clinic_view/'.$clinicID);

  }

  public function new_clinic()
  { 
    $postdata = $this->input->post();

    $this->gp_model->new_gpclinic_info($postdata);
    redirect('/genprac/index/');

  }

  public function newgp()
  {
    $header['breadcrumbs'][]    = array('title' => 'General Practitioners', 'link' => 'advisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
    $header['icon']             = '<i class="fa fa-medkit" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Add New General Practitioners";
    $mainheader['menuitem']     = 2;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['gpState']            = $this->global_model->get_all_austates();
    $data['gpClinic']            = $this->gp_model->get_gp_clinicall();
    $postdata = $this->input->post();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    if (!empty($postdata['GP_name']) && !empty($postdata['GP_state']) && !empty($postdata['GP_city'])) {
     $this->gp_model->new_gp_info($postdata);
     redirect('/genprac/index');
   } else {
    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">"Name, State and City are required fields" .</div>');
    $this->load->view('gp/add', $data);
  }

  $this->load->view('main_footer');
}

public function delete($GPID)
{
  $this->gp_model->delete_gp_info($GPID);

  redirect('/genprac/index/');
}

public function clinic_delete($clinicID)
{
  $this->gp_model->delete_gpclinic_info($clinicID);

  redirect('/genprac/index/');
}


}
