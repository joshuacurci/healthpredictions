<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('advisor_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('email_model');
        $this->load->model('clientdiary_model');
        $this->load->model('clientsbytest_model');
        $this->load->model('generate_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Clients by Tests', 'link' => 'clientsbytest/index' );
    $header['icon']             = '<i class="fa fa-object-group" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Clients by Tests";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 26;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['advData']           = $this->referralsadvisor_model->get_client_appointments();
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTypes']   = $this->clientsbytest_model->get_test_types();
    // $data['testTicked']  = $this->client_model->get_test_client($clientID);
    // $data['testNotes']  = $this->client_model->get_clienttest_notes($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientsbytest/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function generate_form()
{
    $postdata = $this->input->post();
    // $date = strtotime($postdata['app_date']);
    $clientID = $postdata['clientID'];
    $insuranceID = $postdata['insuranceID'];
    if (isset($postdata['appID'])) {
    $appID = $postdata['appID'];
    }

    if (isset($_POST['referral_acknowledgement'])) {
        $this->referral_acknowledgement_dl($clientID, $insuranceID,$appID);
        $this->client_model->set_ind_print_notification ($clientID, '7');
        redirect('/client/pre_generate/'.$clientID.'/'.$insuranceID.'/');

    } 
    else if (isset($_POST['referral_acknowledgement_tab'])) {
        $this->referral_acknowledgement_tab($clientID, $insuranceID,$appID);
        $this->client_model->set_ind_print_notification ($clientID, '7');
        redirect('/client/pre_generate/'.$clientID.'/'.$insuranceID.'/');
    }
    else if (isset($_POST['referral_acknowledgement_email'])) {

        $this->client_model->set_ind_email_notification ($clientID, '7');

        $filename = $this->referral_acknowledgement_up($clientID, $insurance_company,$appID);
        $this->email_model->sendReferral($clientID, $filename);
        redirect('/client/pre_generate/'.$clientID.'/'.$insuranceID.'/');

    }
    else if (isset($_POST['notif_to_advisor_email'])) {

        $this->client_model->set_ind_email_notification ($clientID, '7');

        $filename = $this->advisor_notification_up($clientID, $insurance_company, $appID);
        $this->email_model->sendnotiftoadvisor($clientID, $filename);
        redirect('/client/pre_generate/'.$clientID.'/'.$insuranceID.'/');

    }
    else if (isset($_POST['notif_to_adv_completion_email'])) {

        $this->client_model->set_ind_email_notification ($clientID, '7');

        $filename = $this->advisor_notification_of_completion_up($clientID, $insurance_company, $appID);
        $this->email_model->notiftoadvcompletion($clientID, $filename);
        redirect('/client/pre_generate/'.$clientID.'/'.$insuranceID.'/');

    }
     else if (isset($_POST['notif_to_path'])) {
        $checkAppForm = $this->generate_model->check_appform_pathology($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_path($postdata);
        } else {
            $data = $this->generate_model->noti_to_path_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/path_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/path_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }

    } else if(isset($_POST['notif_to_doctor'])) {
        $checkAppForm = $this->generate_model->check_appform_doctor($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_doctor($postdata);
        } else {
            $data = $this->generate_model->noti_to_doctor_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/path_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/path_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }

    } else if(isset($_POST['notif_to_client'])) {
        $checkAppForm = $this->generate_model->check_appform_client($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_client($postdata);
        } else {
            $data = $this->generate_model->noti_to_client_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/client_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/client_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }
        

    } else if(isset($_POST['notif_to_advisor'])) {
        $checkAppForm = $this->generate_model->check_appform_advisor($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_advisor($postdata);
        } else {
            $data = $this->generate_model->noti_to_advisor_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/advisor_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/advisor_notification_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }

    } else if(isset($_POST['notif_to_adv_completion'])) {
        $checkAppForm = $this->generate_model->check_appform_adv_completion($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_adv_completion($postdata);
        } else {
            $data = $this->generate_model->noti_to_adv_completion_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }
        
    } else if(isset($_POST['notif_to_ins_company'])) {
        $checkAppForm = $this->generate_model->check_appform_insurance($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->noti_to_insurance($postdata);
        } else {
            $data = $this->generate_model->noti_to_insurance_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/noti_to_ins_company_completion_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/noti_to_ins_company_completion_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }
        
    } else if(isset($_POST['client_consent_id'])) {
        $checkAppForm = $this->generate_model->check_appform_clientConsentID($clientID, $appID);
        if ($checkAppForm == null) {
            $data = $this->generate_model->client_consent_id($postdata);
        } else {
            $data = $this->generate_model->client_consent_id_update($postdata);
        }
        if ($appID==NULL)
        {
            $clientApointment=$this->client_model->get_client_apointment($clientID);
            $apparray=[];
            foreach($clientApointment as $app)
            {
                array_push($apparray,$app['appID']);
            }
            redirect('/client/client_consent_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$apparray).'/');
        }
        else
        {
            redirect('/client/client_consent_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
        }
        
    }

    
}

// Search specific appointment Date
public function search_between_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Clients by Tests', 'link' => 'clientsbytest/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-object-group" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 26;

    $postdata = $this->input->post();
    $startdate = strtotime($postdata['startdate']);
    $lastdate = strtotime($postdata['lastdate']);
    // var_dump($postdata); 
    $data['appData'] = $this->clientdiary_model->search_between_date($startdate, $lastdate);
    $data['searchresults'] = $postdata;


    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientdiary/index', $data);
    $this->load->view('main_footer');
}

// Referral Acknowledgement Download 
public function referral_acknowledgement_dl($clientID, $insuranceID,$appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    if($appID==NULL)
    {
        $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);  
    }
    else
    {
        $data['clientApointment']   = $this->client_model->get_apointments($appID);
    }
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output("referral_acknowledgement.pdf",'D');
  ob_end_flush();

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_tab($clientID, $insuranceID,$appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    if($appID==NULL)
    {
        $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);  
    }
    else
    {
        $data['clientApointment']   = $this->client_model->get_apointments($appID);
    }
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output("referral_acknowledgement.pdf",'I');
  ob_end_flush();

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_up($clientID, $insuranceID,$appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    if($appID==NULL)
    {
        $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);  
    }
    else
    {
        $data['clientApointment']   = $this->client_model->get_apointments($appID);
    }
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output($_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".$clientID."_referral_acknowledgement.pdf",'F');

  return $_SERVER["DOCUMENT_ROOT"]."uploads/temp/".$clientID."_referral_acknowledgement.pdf";
  ob_end_flush();

}

// Advisor Notification
public function advisor_notification_up($clientID, $insuranceID, $appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID==NULL)
    {
        $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);  
    }
    else
    {
        $data['clientApointment']   = $this->client_model->get_apointments($appID);
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output($_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".$clientID."_advisor_no.pdf",'F');

  return $_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".$clientID."_advisor_no.pdf";
  ob_end_flush();
}

// Advisor Notification of Completion
public function advisor_notification_of_completion_up($clientID, $insuranceID, $appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_adv_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID==NULL)
    {
        $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);  
    }
    else
    {
        $data['clientApointment']   = $this->client_model->get_apointments($appID);
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);

  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);

  $pdf->Output($_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".$clientID."_advisor_completion.pdf",'F');

  return $_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".$clientID."_advisor_completion.pdf";
  ob_end_flush();
}
}
?>