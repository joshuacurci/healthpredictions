<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Insurance extends CI_Controller {

     public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('pathology_model');
    $this->load->model('insurance_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
            //$this->load->model('news_model');
            //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Insurance', 'link' => 'insurance/index' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Insurance";
    $mainheader['menuitem']     = 6;
    $mainheader['menuitem_toggle']     = '';

    $data['insuranceData']        = $this->insurance_model->get_insurance_all();
    $data['insuranceState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/index', $data);
    $this->load->view('main_footer');
  }

  public function view($insID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Insurance', 'link' => 'insurance/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Insurance', 'link' => '' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Insurance";
    $mainheader['menuitem']     = 6;
    $mainheader['menuitem_toggle']     = '';
    $data['insuranceData']        = $this->insurance_model->get_insurance($insID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Insurance', 'link' => 'insurance/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Insurance', 'link' => '' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Insurance";
    $mainheader['menuitem']     = 6;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['insuranceState']     = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/add', $data);
    $this->load->view('main_footer');
  }

  public function edit($insID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Insurance', 'link' => 'pathology/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Insurance', 'link' => '' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Insurance";
    $mainheader['menuitem']     = 6;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['insuranceData']      = $this->insurance_model->get_insurance($insID);
    $data['insuranceState']     = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('insurance/edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Insurance', 'link' => 'gp/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Insurance Search Result";
    $mainheader['menuitem']     = 6;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['insuranceData'] = $this->insurance_model->search_insurance($postdata);
    $data['searchresults'] = $postdata;

    $data['insuranceState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/index', $data);
    $this->load->view('main_footer');
  }

    public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Insurance', 'link' => 'insurance/search_all' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Insurance";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 17;

    $data['insuranceData']        = $this->insurance_model->get_insurance_all();
    $data['insuranceState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/search_all', $data);
    $this->load->view('main_footer');
  }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Insurance', 'link' => 'insurance/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-building" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Insurance";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 17;

    $postdata = $this->input->post();

    $data['insuranceData'] = $this->insurance_model->search_insurance_all($postdata);
    // var_dump($postdata); 
    $data['searchresults'] = $postdata;

    $data['insuranceState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('insurance/search_all', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $insID = $this->input->post('insID');

    $this->insurance_model->update_insurance_info($postdata);

    redirect('/insurance/view/'.$insID);
  }

  public function newinsurance()
  {
    $postdata = $this->input->post();

    $this->insurance_model->new_insurance_info($postdata);

    redirect('/insurance/index/');
  }

  public function delete($insID)
  {
    $this->insurance_model->delete_insurance_info($insID);

    redirect('/insurance/index/');
  }


}
