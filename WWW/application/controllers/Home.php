<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
      parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
          $this->load->model('global_model');

          $this->load->model('home_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  if ( ! $this->session->userdata('loginuser')) { 
        redirect('login/index');
      }
    }

	public function index()
	{
    $header['breadcrumbs'][] = array('title' => 'Dashboard', 'link' => 'home/index' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle']     = '';

    $data['unacnowkalgedReferrals'] = $this->home_model->get_referral_num();

    $data['staffWeek'] = $this->home_model->get_staff_refferals();
    $data['jvWeek'] = $this->home_model->get_JV_refferals_week();
    $data['jvYear'] = $this->home_model->get_JV_refferals_year();

		$this->load->view('main_header', $mainheader);
		$this->load->view('main_bar', $header);
    $this->load->view('home/index', $data);
		$this->load->view('main_footer');
  }
  
  public function dashSearch()
	{
    $postdata = $this->input->post();

    if ($postdata['searchType'] == 'GP'){
      $data = array(
        'GP_name'=>$postdata['searchedName'],
        'GP_city'=>'',
        'GP_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/genprac/search/');
    } elseif ($postdata['searchType'] == 'Pathology'){
      $data = array(
        'path_name'=>$postdata['searchedName'],
        'path_city'=>'',
        'path_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/pathology/search/');
    } elseif ($postdata['searchType'] == 'Nurse'){
      $data = array(
        'nurse_name'=>$postdata['searchedName'],
        'nurse_city'=>'',
        'nurse_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/nurse/search/');
    } elseif ($postdata['searchType'] == 'Specialist'){
      $data = array(
        'spec_name'=>$postdata['searchedName'],
        'spec_profession'=>'',
        'spec_city'=>'',
        'spec_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/specialists/search/');
    } elseif ($postdata['searchType'] == 'Insurance'){
      $data = array(
        'ins_name'=>$postdata['searchedName'],
        'ins_city'=>'',
        'ins_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/insurance/search/');
    } elseif ($postdata['searchType'] == 'Advisor'){
      $data = array(
        'GP_name'=>$postdata['searchedName'],
        'GP_city'=>'',
        'GP_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/advisor/search/');
    } elseif ($postdata['searchType'] == 'Client'){
      $data = array(
        'client_name'=>$postdata['searchedName'],
        'client_city'=>'',
        'client_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/client/search/');
    } elseif ($postdata['searchType'] == 'Staff'){
      $data = array(
        'staff_name'=>$postdata['searchedName'],
        'staff_city'=>'',
        'staff_state'=>''
      );
      $this->session->set_flashdata('dashData',$data);
      redirect('/staff/search/');
    } else {
      redirect('/home/index/');
    }

    
	}
}
