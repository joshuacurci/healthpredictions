<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referralforms extends CI_Controller {

  public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('client_model');
    $this->load->model('referralforms_model');
    $this->load->library('Pdf');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
      //$this->load->model('news_model');
      //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Edit Referral Content', 'link' => 'referralbycreation/index' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Edit Referral/Notifications Forms Content";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['referralformsData']        = $this->referralforms_model->get_referralforms_all();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referralforms/index', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();

    $this->referralforms_model->update_referralforms_info($postdata);

    redirect('/referralforms/index/');
  }
}