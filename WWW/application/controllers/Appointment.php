<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('client_model');
    $this->load->model('appointment_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function add($redirect, $clientID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Appointments', 'link' => '' );
    $header['breadcrumbs'][]    = array('title' => 'Add a new Appointment', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add a new Appointment";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientID'] = $clientID;

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('appointment/add', $data);
    $this->load->view('main_footer');
  }

  public function newclientapp()
  {
    $postdata = $this->input->post();

    $clientID = $this->appointment_model->new_client_appointment($postdata);

    redirect('/client/view/'.$clientID);
  }

  public function edit($clientID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Appointments', 'link' => '' );
    $header['breadcrumbs'][]    = array('title' => 'Edit a Client Appointment', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Edit a Client Appointment";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientID'] = $clientID;
    $data['appData'] = $this->appointment_model->get_client_appointment($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('appointment/edit', $data);
    $this->load->view('main_footer');
  }

  public function view($clientID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Appointments', 'link' => '' );
    $header['breadcrumbs'][]    = array('title' => 'Client Appointment View', 'link' => '' );
    $header['icon']             = '<i class="fa fa-book" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Client Appointment View";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 21;
    $data['clientID'] = $clientID;
    $data['appData'] = $this->appointment_model->get_client_appointment($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('appointment/view', $data);
    $this->load->view('main_footer');
  }

  public function saveclientapp()
  {
    $postdata = $this->input->post();

    $clientID = $this->appointment_model->update_client_appointment($postdata);

    redirect('/client/view/'.$clientID);
  }

  public function delete($appID, $clientID)
  {
    $this->appointment_model->delete_client_appointment($appID);

    redirect('/client/view/'.$clientID);
  }

  public function delete_app($appID)
  {
    $this->appointment_model->delete_client_app($appID);

    redirect('/client/diary/'.$appID);
  }


}
