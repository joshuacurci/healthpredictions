<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);
class Clientsbytest extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('appointment_model');
        $this->load->model('advisor_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('clientdiary_model');
        $this->load->model('clientsbytest_model');
        $this->load->model('referralsadvisor_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Clients by Tests', 'link' => 'clientsbytest/index' );
    $header['icon']             = '<i class="fa fa-object-group" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Clients by Tests";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 26;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['advData']           = $this->appointment_model->get_all_client_appointment();
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTypes']   = $this->clientsbytest_model->get_test_types();
    // $data['testTicked']  = $this->client_model->get_test_client($clientID);
    // $data['testNotes']  = $this->client_model->get_clienttest_notes($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientsbytest/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_by_test()
{
    $header['breadcrumbs'][]    = array('title' => 'Clients by Tests', 'link' => 'clientsbytest/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-object-group" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Clients by Tests Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 26;

    $postdata = $this->input->post();
    $testTypesSelected  = $this->input->post('checkedid');
    array_shift($testTypesSelected);
    $testTypesSelected = str_replace("'","", implode(',' ,$testTypesSelected));
    // var_dump($testTypesSelected); 
    $data['clientData'] = $this->clientsbytest_model->search_between_date($postdata, $testTypesSelected);
    $data['searchresults'] = $postdata;

    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientsbytest/index', $data);
    $this->load->view('main_footer');
}

}
?>