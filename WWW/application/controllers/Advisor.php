<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Advisor extends CI_Controller {
  public static $HOMEPAGE = "home/index/";
  public static $POINTPAGE= "advisor/point_management/";
	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model', 'advisor_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisors', 'link' => 'advisor/index' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Advisors";
    $mainheader['menuitem']     = 7;
    $mainheader['menuitem_toggle']     = '';
    $data['advisorData']        = $this->advisor_model->get_advisors($_SESSION['siteID']);
    $data['advisorState']            = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/index', $data);
    $this->load->view('main_footer');
  }

  public function view($advisorID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisors', 'link' => 'advisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'View', 'link' => '' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Advisor";
    $mainheader['menuitem']     = 7;
    $mainheader['menuitem_toggle']     = '';
    $data['advisorData']        = $this->advisor_model->get_advisor($advisorID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisors', 'link' => 'advisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Advisor";
    $mainheader['menuitem']     = 7;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['advisorState']       = $this->global_model->get_all_austates();
    $data['insuranceCompany']   = $this->advisor_model->get_all_insurance();
    $data['siteDetails']        = $this->advisor_model->get_all_sites();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/add', $data);
    $this->load->view('main_footer');
  }

  public function edit($advisorID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisors', 'link' => 'advisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit', 'link' => '' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Edit Advisor";
    $mainheader['menuitem']     = 7;
    $mainheader['menuitem_toggle']     = '';
    $data['advisorData']        = $this->advisor_model->get_advisor($advisorID);

    $data['country']            = $this->global_model->get_all_country();
    $data['advisorState']            = $this->global_model->get_all_austates();
    $data['insuranceCompany']   = $this->advisor_model->get_all_insurance();
    $data['siteDetails']        = $this->advisor_model->get_all_sites();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/edit', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $advisorID = $this->input->post('advisorID');
    $siteIDorig = $this->input->post('siteIDorig');
    $siteID = $this->input->post('siteID');

    if ($siteIDorig != $siteID) {
      if ($postdata['advisorID']!='')
      {
        $sql = "SELECT * FROM tbl_advisor WHERE advisorID = ".$this->input->post('advisorID')."";
        $query = $this->db->query($sql);
        $advisorInfo = $query->result_array();

        $sql2 = "SELECT * FROM tbl_users WHERE 
          usr_status = 'A'
          AND userID = ".$advisorInfo[0]['userID']."";
        $query2 = $this->db->query($sql2);
        $userdata = $query2->result_array();
        foreach ($userdata as $user)
        {
          $originalpw = $user['password'];
        }
        $newuserID = $this->advisor_model->create_new_user_oripw($postdata, $originalpw);
        $this->advisor_model->deactivate_old_user($this->input->post('advisorID'));
      }
      else
      {
        $newuserID = $this->advisor_model->create_new_user($postdata);
        $this->advisor_model->deactivate_old_user($this->input->post('advisorID'));
      }
    } else {
      $newuserID = "";
    }

    $this->advisor_model->update_advisor_info($postdata, $newuserID);
    $this->advisor_model->update_user_info($postdata);

    redirect('/advisor/view/'.$advisorID);
  }

  public function newad()
  {
    $postdata = $this->input->post();
    $email = $this->input->post('advisor_email');
    $sql = "SELECT * FROM tbl_advisor WHERE advisor_email = '".$email."'" ;
    $query = $this->db->query($sql);
    $sql2 = "SELECT * FROM tbl_users WHERE email = '".$email."'" ;
    $query2 = $this->db->query($sql2);
    if($email!='')
    {
    if ($query->num_rows()==0 && $query2->num_rows()==0)
    {
      $newuserID = $this->advisor_model->create_new_user($postdata);
      $this->advisor_model->new_advisor_info($postdata, $newuserID);
      redirect('/advisor/index/');
    }
    else
    {
        $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Advisor email exist. Please try again later.</div>');
        redirect('/advisor/add/');
    }
  }
  else
  {
    $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Email cannot be empty.</div>');
    redirect('/advisor/add/');
  }
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisor', 'link' => 'gp/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Advisor";
    $mainheader['menuitem']     = 7;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['advisorData'] = $this->advisor_model->search_advisor($postdata);
    $data['searchresults'] = $postdata;

    $data['gpState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Advisor', 'link' => 'advisor/search_all' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Advisor";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 18;

    $data['advisorData']        = $this->advisor_model->get_advisors($_SESSION['siteID']);
    $data['advisorState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();
    $data['insuranceCompany']   = $this->advisor_model->get_all_insurance();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/search_all', $data);
    $this->load->view('main_footer');

  }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Advisor', 'link' => 'advisor/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Advisor";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 18;

    $postdata = $this->input->post();
    // var_dump($postdata);  
    $data['advisorData'] = $this->advisor_model->search_advisor_all($postdata);
    // var_dump($postdata); 
    $data['searchresults'] = $postdata;

    $data['advisorState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/search_all', $data);
    $this->load->view('main_footer');
  }

  public function delete($advisorID)
  {
    $this->advisor_model->delete_advisor_info($advisorID);

    redirect('/advisor/index/');
  }

  /**
   * Print all the Point history for the current advisor from database
   *
   * @param advisorID $Actual Primary key in DB to identify the Advisor
   * 
   * @throws Error_PAGE When Failure (NOT cover all case yet)
   * @author Adrian Bin <adrian.bin@gmail.com>
   * @return Status Tested
   */ 
  public function point_management($advisorID){
    # Redirect URL for the advisor if failure
    $advisorURL = "advisor/view/".$advisorID."";

    # Setup Header, Sidebar and footer of the page
    $header['breadcrumbs'][]    = array('title' => 'Advisor List', 'link' => 'advisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Advisor', 'link' => $advisorURL );
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']                = "Advisor Point Management";
    $mainheader['menuitem']             = 10;
    $mainheader['menuitem_toggle']      = 18;
    
    # Get Advisor's Current Point and its information to display and calculation
    $data['cur_point']                  = $this->advisor_model->get_advisor_current_point($advisorID);
    $data['advisorData']                = $this->advisor_model->get_advisors($_SESSION['siteID']);
    
    
    try{
    $result = $this->advisor_model->get_user_point_history($advisorID);
    }
    catch (Exception $e){
      $this->error_prompt($e, $advisorURL);
      return NULL;
    }

    # Load all components in the webpage
    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/point', $result);
    $this->load->view('main_footer');
  }


  public function check_auth(){
      
    $userID=$this->session->userdata('userID');
    if($userID==2||$userID==3||$userID==5){
      return TRUE;
    }
    else{      
      $this->error_prompt("You do not have access to this page!", $HOMEPAGE);
      return NULL;
    }
  }


    /**
   * Controlling Model to add in a point history & Update Advisor Point
   *
   * @param name    $Description/Reason of adding the point
   * @param points  $Actual Point adding into the history and Advisor
   * @param id      $Actual Advisor ID which must be valid
   * 
   * @throws SQLi_Exception From Model if SQL query cannot execute
   * @author Adrian Bin <adrian.bin@gmail.com>
   * @return Status Tested
   */ 
  public function addPoints(){
    # Getting from input
    try{
    $name = $this->input->post('Reason');
    $points = $this->input->post('Points');
    $id = $this->input->post('advisorID');
    }
    catch (Exception $e){
      $this->error_prompt('Cannot get input From Advisor Page', $redirectURL);
    }
    if (strlen((string)$id) == 0){
      $this->error_prompt("INTERNAL ERROR! Please Try Again!", self::$HOMEPAGE);
      return NULL;
    }
    $redirectURL=self::$POINTPAGE.$id;
    if(strlen($name)==0 || strlen((string)$points)==0){
      $this->error_prompt("Reason AND Point Field must not be empty", $redirectURL);
      return NULL;
    }
    else if($points==0){
      $this->error_prompt("Point must be some value!", $redirectURL);
      return NULL;
    }
    else{
      # Get Current Point
      $cur_point = $this->advisor_model->get_advisor_current_point($id);
      $cur_point = $cur_point[0]['advisor_points'];                     # Reusing Function
      
      # Validation Check
      if ($points<0){
        $points = -1*$points;
      }
      try{
        $this->advisor_model->insert_history_points($id, $name, $points, "A");
        $new_point = $points+$cur_point;
        $this->advisor_model->update_advisor_points($id, $new_point);
        }
      catch(Exception $e){
        $this->error_prompt($e, $HOMEPAGE);
        return NULL;
      }

      redirect($redirectURL);
    }
  }
  
  /**
   * Controlling Model to Redeem in a point history & Update Advisor Point on 
   *
   * @param name    $Description/Reason of redeeming the point
   * @param points  $Actual Point redeemed into the history and Advisor
   * @param id      $Actual advisor id
   * 
   * @throws Error_Page If SQL or loading error found
   * @author Adrian Bin <adrian.bin@gmail.com>
   * @return Status Tested
   */ 
  public function redeemPoints(){
    # Get POST input from view
    $name = $this->input->post('Reason');
    $points = $this->input->post('Points');
    $id = $this->input->post('advisorID');

    # Check advisorID first to ensure the integraty of data
    if (strlen((string)$id)==0){
      $this->error_prompt("INTERNAL ERROR! Please Try Again", self::$HOMEPAGE);
      return NULL;
    }
    # ID is checked, redirectURL is valid to construct
    $redirectURL = self::$POINTPAGE.$id;

    # All POST input must not be empty
    if(strlen($name)==0||strlen((string)$points)==0){
      $this->error_prompt("Reason OR Point Field must not be empty", $redirectURL);
      return NULL;  
    }else if ($points==0){
      $this->error_prompt("Points Add/Redeem must not be zero!", $redirectURL);
      return NULL;
    }

    # Get Current Point
    $cur_point = $this->advisor_model->get_advisor_current_point($id);                       # Was for view but we reuse this function
   
    $cur_point = $cur_point[0]['advisor_points'];

    if($points>0){        
      $points = -1*$points;                                                     #As --> Redeem --> Transform to negative
    }
    $compare_point = $cur_point + $points;

    # IF Current Point is not negative --> Insert to Point history && Update advisor Points
    if( $compare_point >= 0 ){
      $this->advisor_model->insert_history_points($id, $name, $points, "R");
      $new_point = $points + $cur_point;
      $this->advisor_model->update_advisor_points($id, $new_point);
      redirect($redirectURL);
    }
    # Prompt Error Message
    else{
      $this->error_prompt("Result Point must not less than 0", $redirectURL);
      return NULL;
    }

  }
  /**
   * Prompt an error page 
   *
   * @param errormsg      Error Message printed on the screen
   * @param redirect_url  Create a button on Error page, allow user to redirect somewhere
   * 
   * @author Adrian Bin <adrian.bin@gmail.com>
   * @return Status Tested
   */ 
  public function error_prompt($errormsg, $redirect_url){

    # Ready data to pass to error page
    $data['error_msg'] = $errormsg ;
    $data['redirect_url'] = $redirect_url;

    # Create components on error page and load them
    $header['icon']             = '<i class="fa fa-life-ring" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Error Page";
    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisor/error', $data);
    $this->load->view('main_footer');   
  }
}
