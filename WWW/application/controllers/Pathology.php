<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Pathology extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('pathology_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Pathology', 'link' => 'pathology/index' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Pathology";
    $mainheader['menuitem']     = 3;
    $mainheader['menuitem_toggle']     = '';

    $data['pathologyData']        = $this->pathology_model->get_pathology_all();
    $data['pathologyState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/index', $data);
    $this->load->view('main_footer');
  }

  public function view($pathID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Pathology', 'link' => 'pathology/index' );
    $header['breadcrumbs'][]    = array('title' => 'View', 'link' => '' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Pathologist";
    $mainheader['menuitem']     = 3;
    $mainheader['menuitem_toggle']     = '';
    $data['pathologyData']        = $this->pathology_model->get_pathology($pathID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Pathology', 'link' => 'pathology/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New', 'link' => '' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Pathology";
    $mainheader['menuitem']     = 3;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['pathologyState']            = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/add', $data);
    $this->load->view('main_footer');
  }

  public function edit($pathID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Pathology', 'link' => 'pathology/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Pathologist', 'link' => '' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Pathologist";
    $mainheader['menuitem']     = 3;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['pathologyData']      = $this->pathology_model->get_pathology($pathID);
    $data['pathologyState']     = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('pathology/edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Pathologist', 'link' => 'gp/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Pathology Search Result";
    $mainheader['menuitem']     = 3;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['pathologyData'] = $this->pathology_model->search_pathology($postdata);
    $data['searchresults'] = $postdata;

    $data['pathologyState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Pathology', 'link' => 'pathology/search_all' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Pathology";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 14;

    $data['pathologyData']        = $this->pathology_model->get_pathology_all();
    $data['pathologyState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/search_all', $data);
    $this->load->view('main_footer');
  }

    public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search  Pathology', 'link' => 'gp/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-heartbeat" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Pathology";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 14;

    $postdata = $this->input->post();
    $data['pathologyData'] = $this->pathology_model->search_pathology_all($postdata);
    $data['searchresults'] = $postdata;

    $data['pathologyState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('pathology/search_all', $data);
    $this->load->view('main_footer');
  }



  public function save()
  {
    $postdata = $this->input->post();
    $pathID = $this->input->post('pathID');

    $this->pathology_model->update_pathology_info($postdata);

    redirect('/pathology/view/'.$pathID);
  }

  public function newpathology()
  {
    $postdata = $this->input->post();

    $this->pathology_model->new_pathology_info($postdata);

    redirect('/pathology/index/');
  }

  public function delete($pathID)
  {
    $this->pathology_model->delete_pathology_info($pathID);

    redirect('/pathology/index/');
  }


}
