<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referralsadvisor extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('advisor_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('clientdiary_model');
        $this->load->model('referralsadvisor_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Referrals by Advisor', 'link' => 'referralsadvisor/index' );
    $header['icon']             = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Referrals By Advisor";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 22;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['referralClient']           = $this->referralsadvisor_model->get_client_referrals();
    $data['advData']           = $this->advisor_model->get_all_advisors();
    $data['advData2']           = $this->advisor_model->get_all_advisors();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referralsadvisor/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_app_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Referrals by Advisor', 'link' => 'referralsadvisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Specific Date', 'link' => '' );
    $header['icon']             = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Referrals by Advisor Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 22;

    $postdata = $this->input->post();
    $datestart_date = date("d-m-Y H:i:s", strtotime($postdata['referral_date']));
    $lastdate_date = date("d-m-Y 23:59:59", strtotime($datestart_date));
    $advisorID = $postdata['advisorID'];

    $startdate = strtotime($datestart_date);
    $lastdate = strtotime($lastdate_date);

    // var_dump($datestart);
    // var_dump($lastdate);
    // die();

    $data['referralClient']           = $this->referralsadvisor_model->search_between_date($startdate, $lastdate, $advisorID);
    $data['searchresults'] = $postdata;

    $data['advData']           = $this->advisor_model->get_all_advisors();
    $data['advData2']           = $this->advisor_model->get_all_advisors();
    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referralsadvisor/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_between_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Referrals by Advisor', 'link' => 'referralsadvisor/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-file-text-o" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Referrals by Advisor Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 22;

    $postdata = $this->input->post();
    if ($postdata['startdate']=='')
    {   
    $startdate = "000000000";
    }
    else
    {
        $startdate = strtotime($postdata['startdate'].' 00:00:00');
    }
    if ($postdata['lastdate']=='')
    {   
    $lastdate = "9999999999";
    }
    else
    {
        $lastdate = strtotime($postdata['lastdate'].' 23:59:59');
    }
    $advisorID = $postdata['advisorID'];
    // var_dump($postdata); 
    $data['referralClient']           = $this->referralsadvisor_model->search_between_date($startdate, $lastdate, $advisorID);
    $data['searchresults'] = $postdata;

    $data['advData']           = $this->advisor_model->get_all_advisors();
    $data['advData2']           = $this->advisor_model->get_all_advisors();
    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referralsadvisor/index', $data);
    $this->load->view('main_footer');
}

}
?>