<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Nurse extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('pathology_model');
    $this->load->model('nurse_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Nurse', 'link' => 'nurse/index' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Nurse";
    $mainheader['menuitem']     = 4;
    $mainheader['menuitem_toggle']     = '';

    $data['nurseData']        = $this->nurse_model->get_nurse_all();
    $data['nurseState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/index', $data);
    $this->load->view('main_footer');
  }

  public function view($nurseID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Nurse', 'link' => 'nurse/index' );
    $header['breadcrumbs'][]    = array('title' => 'View', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Nurse";
    $mainheader['menuitem']     = 4;
    $mainheader['menuitem_toggle']     = '';
    $data['nurseData']        = $this->nurse_model->get_nurse($nurseID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/view', $data);
    $this->load->view('main_footer');
  }

  public function add()
  {
    $header['breadcrumbs'][]    = array('title' => 'Nurse', 'link' => 'nurse/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Nurse', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Nurse";
    $mainheader['menuitem']     = 4;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['nurseState']            = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/add', $data);
    $this->load->view('main_footer');
  }

  public function edit($nurseID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Nurse', 'link' => 'nurse/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Nurse', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Nurse";
    $mainheader['menuitem']     = 4;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['nurseData']      = $this->nurse_model->get_nurse($nurseID);
    $data['nurseState']     = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('nurse/edit', $data);
    $this->load->view('main_footer');
  }

  public function search()
  {
    $header['breadcrumbs'][]    = array('title' => 'Nurse', 'link' => 'nurse/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Nurse Search Result";
    $mainheader['menuitem']     = 4;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
      $postdata = $this->session->flashdata('dashData');
    } else {
      $postdata = $this->input->post();
    }
    $data['nurseData'] = $this->nurse_model->search_nurse($postdata);
    $data['searchresults'] = $postdata;

    $data['nurseState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/index', $data);
    $this->load->view('main_footer');
  }

  public function search_all()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Nurse', 'link' => 'nurse/search_all' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Nurse";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 15;

    $data['nurseData']        = $this->nurse_model->get_nurse_all();
    $data['nurseState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/search_all', $data);
    $this->load->view('main_footer');
  }

  public function search_result()
  {
    $header['breadcrumbs'][]    = array('title' => 'Search Nurse', 'link' => 'nurse/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-plus-square" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Nurse";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 15;

    $postdata = $this->input->post();

    $data['nurseData'] = $this->nurse_model->search_nurse_all($postdata);
    // var_dump($postdata); 
    $data['searchresults'] = $postdata;

    $data['nurseState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('nurse/search_all', $data);
    $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $nurseID = $this->input->post('nurseID');

    $this->nurse_model->update_nurse_info($postdata);

    redirect('/nurse/view/'.$nurseID);
  }

  public function newnurse()
  {
    $postdata = $this->input->post();

    $this->nurse_model->new_nurse_info($postdata);

    redirect('/nurse/index/');
  }

  public function delete($nurseID)
  {
    $this->nurse_model->delete_nurse_info($nurseID);

    redirect('/nurse/index/');
  }


}
