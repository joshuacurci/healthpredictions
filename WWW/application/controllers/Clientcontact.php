<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientcontact extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('client_model');
    $this->load->model('clientcontact_model');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
    }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

    if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
    }
  }

  public function add($clientID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Contact History', 'link' => '' );
    $header['breadcrumbs'][]    = array('title' => 'Add to Client Contact History', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add to Client Contact History";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientID'] = $clientID;
    $data['staffList'] = $this->clientcontact_model->get_staff_list();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientcontact/add', $data);
    $this->load->view('main_footer');
  }

  public function newclienthistory()
  {
    $postdata = $this->input->post();

    $clientID = $this->clientcontact_model->new_client_contact($postdata);

    redirect('/client/view/'.$clientID);
  }

  public function edit($historyID, $clientID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Client Contact History', 'link' => '' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Client Contact History', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Edit Client Contact History";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientID'] = $clientID;
    $data['contactData'] = $this->clientcontact_model->get_client_contact($historyID);

    $data['staffList'] = $this->clientcontact_model->get_staff_list();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientcontact/edit', $data);
    $this->load->view('main_footer');
  }

  public function saveclientcontact()
  {
    $postdata = $this->input->post();

    $clientID = $this->clientcontact_model->update_client_contact($postdata);

    redirect('/client/view/'.$clientID);
  }

  public function delete($appID, $clientID)
  {
    $this->clientcontact_model->delete_client_contact($appID);

    redirect('/client/view/'.$clientID);
  }


}
