<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);
class Referral extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('referral_model');
    $this->load->model('advisorhome_model');
    $this->load->model('email_model');
    $this->load->library('PHPmailer');

    $this->load->model('generate_model');
    $this->load->model('client_model');
    $this->load->library('Pdf');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
  }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

  if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
  }
}

public function index()
{
    $header['breadcrumbs'][]    = array('title' => 'Referrals', 'link' => 'referral/index' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Referrals";
    $mainheader['menuitem']     = 30;
    $mainheader['menuitem_toggle']     = '';

    $data['referralData']       = $this->referral_model->get_referrals();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['sitesDetails']       = $this->referral_model->get_sites();
    $data['staffDetails']     = $this->referral_model->get_staff();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referral/index', $data);
    $this->load->view('main_footer');

}

public function search()
{
    $header['breadcrumbs'][]    = array('title' => 'Referrals', 'link' => 'referral/index' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Referrals";
    $mainheader['menuitem']     = 30;
    $mainheader['menuitem_toggle']     = '';

    $postdata = $this->input->post();

    $data['referralData']       = $this->referral_model->search_referrals($postdata);
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['sitesDetails']       = $this->referral_model->get_sites();
    $data['staffDetails']     = $this->referral_model->get_staff();
    $data['searchData']     = $postdata;


    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('referral/index-search', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('referral/index-search', $data);
        $this->load->view('main_footer');
    }
}

public function lodge1()
  {
    $header['breadcrumbs'][] = array('title' => 'Lodge a Referral', 'link' => 'referral/lodge1' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_client($mainheader['advisorData'][0]['advisorID']);
    $data['country']            = $this->global_model->get_all_country();
    $data['clientState']        = $this->global_model->get_all_austates();
    $data['insuranceComp']      = $this->referral_model->get_insurance();
    $data['advisorDetails']     = $this->referral_model->get_advisors();
    $data['staffDetails']     = $this->referral_model->get_staff();
    $data['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);


    $mainheader['title'] = "Lodge a Referral";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle'] = '';

    $this->load->view('advisor_header', $mainheader);
    $this->load->view('advisor_bar', $header);
    $this->load->view('referral/referalAdvisor', $data);
    $this->load->view('advisor_footer');
  }

  public function lodge2()
  {
    $postdata = $this->input->post();
    $client_address = $this->input->post('client_address');
    $client_city = $this->input->post('client_city');
    $client_postcode = $this->input->post('client_postcode');
    $siteID = $this->input->post('siteID');

    // Save inital referral
    $clientID = $this->referral_model->new_client_info_referal($postdata);
    //$this->referral_model->send_test_email($postdata,$_FILES);

    // Save image/files
    $this->referral_model->referal_upload_files($_FILES, $clientID);


    $header['breadcrumbs'][] = array('title' => 'Lodge a Referal', 'link' => 'referral/lodge1' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_client($mainheader['advisorData'][0]['advisorID']);
    $data['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);

    $data['testTypes']   = $this->referral_model->get_test_types();
    //$data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testNotes']  = $this->referral_model->get_clienttest_notes($clientID);
    $data['clientID']    = $clientID;

    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle']     = '';

    $this->load->view('advisor_header', $mainheader);
    $this->load->view('advisor_bar', $header);
    $this->load->view('referral/referaltestAdvisor', $data);
    $this->load->view('advisor_footer');
  }

  public function savetests()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');
    $this->referral_model->update_client_tests_adv($postdata);
    $result = $this->referral_model->send_referal_email($clientID);
    $header['breadcrumbs'][] = array('title' => 'Dashboard', 'link' => 'advisorhome/index' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_referral($mainheader['advisorData'][0]['advisorID']);
    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle']     = '';
    $data['result'] = $result;

        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('advisorhome/index_submit', $data);
        $this->load->view('advisor_footer');
}

public function savetestsAdmin()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');

    $this->referral_model->update_client_tests($postdata);
    $result = $this->referral_model->send_referal_email($clientID);

    $header['breadcrumbs'][] = array('title' => 'Dashboard', 'link' => 'advisorhome/index' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_client($mainheader['advisorData'][0]['advisorID']);
    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 30;
    $mainheader['menuitem_toggle']     = '';
    $data['result'] = $result;

    $data['referralData']       = $this->referral_model->get_referrals();
    $data['clientState']       = $this->global_model->get_all_austates();

    if (isset($postdata['print'])) {
        $this->acknowlageprint($postdata['clientID']);
    }

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referral/index', $data);
    $this->load->view('main_footer');
}



public function view($clientID)
{
    if ($_SESSION['usertype'] == 'I') {
        $header['breadcrumbs'][]    = array('title' => 'Dashboard', 'link' => 'advisorhome/index' );
        $header['breadcrumbs'][]    = array('title' => 'View Referral', 'link' => '' );
        $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
        $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
        $mainheader['title']        = "View Client";
        $mainheader['menuitem']     = 30;
        $mainheader['menuitem_toggle']     = '';
        $data['clientData']         = $this->referral_model->get_referral($clientID);
        $data['testTypes']          = $this->referral_model->get_test_types();
        $data['testTicked']         = $this->referral_model->get_test_client($clientID);
        $data['testNotes']         = $this->referral_model->get_clienttest_notes($clientID);
    } else {
        $header['breadcrumbs'][]    = array('title' => 'Referrals', 'link' => 'referral/index' );
        $header['breadcrumbs'][]    = array('title' => 'View Referral', 'link' => '' );
        $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
        $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
        $mainheader['title']        = "View Client";
        $mainheader['menuitem']     = 30;
        $mainheader['menuitem_toggle']     = '';
        $data['clientData']         = $this->referral_model->get_referral($clientID);
        $data['testTypes']          = $this->referral_model->get_test_types();
        $data['testTicked']         = $this->referral_model->get_test_client($clientID);
        $data['testNotes']         = $this->referral_model->get_clienttest_notes($clientID);
    }
    

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('referral/view', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('referral/view', $data);
        $this->load->view('main_footer');
    }
}

public function advisorsearch()
{
    $header['breadcrumbs'][]    = array('title' => 'Referrals', 'link' => 'advisorhome/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Referrals Search Result";
    $mainheader['menuitem'] = 1;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
        $postdata = $this->session->flashdata('dashData');
      } 
      else 
      {
        $postdata = $this->input->post();
      }
        $data['clientData'] = $this->referral_model->search_referral_advisor($postdata, $_SESSION['userID']);
        $data['searchresults'] = $postdata;
        $data['clientState'] = $this->global_model->get_all_austates();
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('advisorhome/index', $data);
        $this->load->view('advisor_footer');
}

public function add()
{
    $header['breadcrumbs'][]    = array('title' => 'CLient', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Client";
    $mainheader['menuitem']     = 30;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['clientState']        = $this->global_model->get_all_austates();
    $data['insuranceComp']      = $this->referral_model->get_insurance();
    $data['advisorDetails']     = $this->referral_model->get_advisors();
    $data['staffDetails']     = $this->referral_model->get_staff();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/add', $data);
    $this->load->view('main_footer');
}

public function adminadd()
{
    $header['breadcrumbs'][] = array('title' => 'Lodge a Referral', 'link' => 'referral/adminadd' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['advisorData'] = $this->advisor_model->get_all_advisors();
    $mainheader['staffData'] = $this->advisor_model->get_all_staff();
    $mainheader['clientData'] = $this->advisorhome_model->get_client($mainheader['advisorData'][0]['advisorID']);
    $data['country']            = $this->global_model->get_all_country();
    $data['clientState']        = $this->global_model->get_all_austates();
    $data['insuranceComp']      = $this->referral_model->get_insurance();
    $data['advisorDetails']     = $this->referral_model->get_advisors();
    $data['staffDetails']     = $this->referral_model->get_staff();
    //$data['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);


    $mainheader['title'] = "Lodge a Referral";
    $mainheader['menuitem'] = 30;
    $mainheader['menuitem_toggle']     = '';

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referral/referal', $data);
    $this->load->view('main_footer');
  }

  public function adminadd2()
  {
    

    $postdata = $this->input->post();
    $client_address = $this->input->post('client_address');
    $client_city = $this->input->post('client_city');
    $client_postcode = $this->input->post('client_postcode');
    $siteID = $this->input->post('siteID');

    // Save inital referral
    $clientID = $this->referral_model->new_client_info_referal_admin($postdata);
    //$this->referral_model->send_test_email($postdata,$_FILES);

    // Save image/files
    $this->referral_model->referal_upload_files($_FILES, $clientID);


    $header['breadcrumbs'][] = array('title' => 'Lodge a Referal', 'link' => 'referral/adminadd' );
    $header['icon'] = '<i class="fa fa-dashboard" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);
    $mainheader['clientData'] = $this->advisorhome_model->get_client($postdata['advisorID']);
    $data['advisorData'] = $this->advisorhome_model->get_advisor($_SESSION['userID']);

    $data['testTypes']   = $this->referral_model->get_test_types();
    //$data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testNotes']  = $this->referral_model->get_clienttest_notes($clientID);
    $data['clientID']    = $clientID;

    $mainheader['title'] = "Welcome to Health Predictions";
    $mainheader['menuitem'] = 30;
    $mainheader['menuitem_toggle']     = '';

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('referral/referaltest', $data);
    $this->load->view('main_footer');
  }

public function edit($referralID)
{   if ($_SESSION['usertype'] == 'I') 
    {
        $header['breadcrumbs'][]    = array('title' => 'Dashboard', 'link' => 'advisorhome/index' );
        $header['breadcrumbs'][]    = array('title' => 'Edit Referral', 'link' => '' );
        $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
        $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
        $mainheader['title']        = "Edit Referral";
        $mainheader['menuitem']     = 30;
        $mainheader['menuitem_toggle']     = '';
        $data['referralData']         = $this->referral_model->get_referral($referralID);
        $data['testTypes']          = $this->referral_model->get_test_types();
        $data['testTicked']         = $this->referral_model->get_test_client($referralID);
        $data['testNotes']         = $this->referral_model->get_clienttest_notes($referralID);
    
        $data['country']            = $this->global_model->get_all_country();
        $data['clientState']        = $this->global_model->get_all_austates();
        $data['insuranceComp']      = $this->referral_model->get_insurance();
        $data['advisorDetails']     = $this->referral_model->get_advisors();
        $data['staffDetails']     = $this->referral_model->get_staff();
    }
    else
    {
        $header['breadcrumbs'][]    = array('title' => 'Referral', 'link' => 'referral/index' );
        $header['breadcrumbs'][]    = array('title' => 'Edit Referral', 'link' => '' );
        $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
        $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
        $mainheader['title']        = "Edit Referral";
        $mainheader['menuitem']     = 30;
        $mainheader['menuitem_toggle']     = '';
        $data['referralData']         = $this->referral_model->get_referral($referralID);
        $data['testTypes']          = $this->referral_model->get_test_types();
        $data['testTicked']         = $this->referral_model->get_test_client($referralID);
        $data['testNotes']         = $this->referral_model->get_clienttest_notes($referralID);
    
        $data['country']            = $this->global_model->get_all_country();
        $data['clientState']        = $this->global_model->get_all_austates();
        $data['insuranceComp']      = $this->referral_model->get_insurance();
        $data['advisorDetails']     = $this->referral_model->get_advisors();
        $data['staffDetails']     = $this->referral_model->get_staff();
    }

    

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('referral/edit', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('referral/edit', $data);
        $this->load->view('main_footer');
    }
}

public function update()
{
    $postdata = $this->input->post();

    $this->referral_model->update_referral_info($postdata);
    if ($_SESSION['usertype'] == 'I') 
    {
        redirect('/advisorhome/index/');
    }
    else
    {
        redirect('/referral/index/');
    }
}

public function quickchange($staffID, $referralID)
{

    $this->referral_model->quick_update_referral_info($referralID, $staffID);

    redirect('/referral/index/');
}

public function save()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');
    $client_address = $postdata['client_address'];
    $client_city = $postdata['client_city'];
    $client_postcode = $postdata['client_postcode'];

    $this->referral_model->update_client_info($postdata, $client_address, $client_city, $client_postcode);

    redirect('/client/view/'.$clientID);
}

public function savenotify()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');

    $this->referral_model->update_client_notify($postdata);

    redirect('/client/view/'.$clientID);
}

public function convert($referralID)
{
    $postdata = $this->referral_model->get_finalreferral($referralID);
    $client_address = $postdata[0]['client_address'];
    $client_city = $postdata[0]['client_city'];
    $client_postcode = $postdata[0]['client_postcode'];
    $siteID = $postdata[0]['siteID'];

    $clientID = $this->referral_model->new_client_info($postdata[0], $client_address, $client_city, $client_postcode);
    $this->referral_model->new_notifications($clientID); 
    $this->referral_model->new_tests($referralID,$clientID);    
    // $this->referral_model->new_referral_noti($clientID);
    $this->referral_model->new_path_report_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_noti_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_noti_to_ins_completion_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_noti_to_adv_completion_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_consent_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_referral_noti_default($postdata[0], $clientID, $siteID);

    $this->referral_model->change_to_active($referralID);

    redirect('/client/view/'.$clientID);
}

public function acknowlageprint($referralID)
{
    $postdata = $this->referral_model->get_finalreferral($referralID);
    $client_address = $postdata[0]['client_address'];
    $client_city = $postdata[0]['client_city'];
    $client_postcode = $postdata[0]['client_postcode'];
    $siteID = $postdata[0]['siteID'];

    $clientID = $this->referral_model->new_client_info($postdata[0], $client_address, $client_city, $client_postcode);
    $this->referral_model->new_notifications($clientID); 
    $this->referral_model->new_tests($referralID,$clientID);    
    // $this->referral_model->new_referral_noti($clientID);
    $this->referral_model->new_path_report_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_noti_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_noti_to_ins_completion_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_noti_to_adv_completion_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_consent_default($postdata[0], $clientID, $siteID);
    $this->referral_model->new_client_referral_noti_default($postdata[0], $clientID, $siteID);

    $this->referral_model->change_to_active($referralID);

    // --- Generate form ---

        $postdata['siteID'] = $siteID;
        $postdata['clientID'] = $clientID;
        $postdata['HIV'] = 'Y';
        $postdata['instruct_taking_blood'] = 'Y';
    
        $insurance_company = $postdata[0]['insurance_company'];
        $check_client_referral_noti = $this->client_model->check_client_referral_noti($clientID);
    
        if ($check_client_referral_noti != $clientID){
        $this->client_model->edit_client_referral_noti($postdata);
       } else {
           $this->client_model->new_client_referral_noti($postdata);
       }
    
    
       $this->client_model->new_notifications($clientID);
       $this->client_model->set_print_referalAcknowledgment($clientID);

       $filename = $this->referral_acknowledgement_up($clientID, $insurance_company);

       $this->email_model->sendReferralRef($referralID, $filename); 

       $this->referral_acknowledgement_dl($clientID, $insurance_company);
}

public function justprint($referralID)
{
    $postdata = $this->referral_model->get_finalreferral($referralID);
    $client_address = $postdata[0]['client_address'];
    $client_city = $postdata[0]['client_city'];
    $client_postcode = $postdata[0]['client_postcode'];
    $siteID = $postdata[0]['siteID'];

    $finalclientID = $this->referral_model->get_client_id($referralID);
    $clientID = $finalclientID[0]['clientID'];


    // --- Generate form ---

        $postdata['siteID'] = $siteID;
        $postdata['clientID'] = $clientID;
        $postdata['HIV'] = 'Y';
        $postdata['instruct_taking_blood'] = 'Y';
    
        $insurance_company = $postdata[0]['insurance_company'];
        $check_client_referral_noti = $this->client_model->check_client_referral_noti($clientID);
    
        if ($check_client_referral_noti != $clientID){
        $this->client_model->edit_client_referral_noti($postdata);
       } else {
           $this->client_model->new_client_referral_noti($postdata);
       }
    
    
       $this->client_model->new_notifications($clientID);
       $this->client_model->set_print_referalAcknowledgment($clientID);

       $filename = $this->referral_acknowledgement_up($clientID, $insurance_company);

       $this->email_model->sendReferral($referralID, $filename);

       $this->referral_acknowledgement_dl($clientID, $insurance_company);
}

public function bulkaccept() 
{
    $bulkID=[];
    $bulkInsurance=[];
    $submitedNumbers = $this->input->post();

    if ($submitedNumbers['bulkacknowlage']!=null)
    {
        foreach ($submitedNumbers['bulkacknowlage'] as $referralID) {

            $postdata = $this->referral_model->get_finalreferral($referralID);
            $client_address = $postdata[0]['client_address'];
            $client_city = $postdata[0]['client_city'];
            $client_postcode = $postdata[0]['client_postcode'];
            $siteID = $postdata[0]['siteID'];
        
            $clientID = $this->referral_model->new_client_info($postdata[0], $client_address, $client_city, $client_postcode);
            array_push($bulkID,$clientID);
            $this->referral_model->new_notifications($clientID); 
            $this->referral_model->new_tests($referralID,$clientID);    
            // $this->referral_model->new_referral_noti($clientID);
            $this->referral_model->new_path_report_default($postdata[0], $clientID, $siteID);
            $this->referral_model->new_client_noti_default($postdata[0], $clientID, $siteID);
            $this->referral_model->new_noti_to_ins_completion_default($postdata[0], $clientID, $siteID);
            $this->referral_model->new_noti_to_adv_completion_default($postdata[0], $clientID, $siteID);
            $this->referral_model->new_client_consent_default($postdata[0], $clientID, $siteID);
            $this->referral_model->new_client_referral_noti_default($postdata[0], $clientID, $siteID);
            $this->referral_model->change_to_active($referralID);
        
            // --- Generate form ---
        
                $postdata['siteID'] = $siteID;
                $postdata['clientID'] = $clientID;
                $postdata['HIV'] = 'Y';
                $postdata['instruct_taking_blood'] = 'Y';
            
                $insurance_company = $postdata[0]['insurance_company'];
                array_push($bulkInsurance,$insurance_company);
                $check_client_referral_noti = $this->client_model->check_client_referral_noti($clientID);
            
                if ($check_client_referral_noti != $clientID){
                $this->client_model->edit_client_referral_noti($postdata);
               } else {
                   $this->client_model->new_client_referral_noti($postdata);
               }
            }
            $this->bulkexportPDF($bulkID, $bulkInsurance);
    }
    else
    {
        redirect('/referral/index/');
    }

       $header['breadcrumbs'][]    = array('title' => 'Referrals', 'link' => 'referral/index' );
       $header['breadcrumbs'][]    = array('title' => 'Bulk Referral Print', 'link' => '' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Bulk Referral Print";
    $mainheader['menuitem']     = 30;
    $mainheader['menuitem_toggle']     = '';

    $data['referralData']       = $this->referral_model->get_selected_referrals($submitedNumbers);
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['sitesDetails']       = $this->referral_model->get_sites();
    $data['staffDetails']     = $this->referral_model->get_staff();
    

        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('referral/printlist', $data);
        $this->load->view('main_footer');

}

// Pathology Appointment Request Save
public function pathology_appointment_request_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = $this->input->post('appID');
    $check_client_path_report = $this->referral_model->check_client_path_report($clientID);

    if ($check_client_path_report != 0){
       $this->referral_model->edit_path_report_info($postdata);
   } else {
       $this->referral_model->new_path_report_info($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/path_notification_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_info_consent_id/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_info_consent_id_dl/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}
}

// CLient Notification Instruction Save
public function client_notification_ins_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = $this->input->post('appID');
    $check_client_noti_report = $this->referral_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->referral_model->edit_client_noti_info($postdata);
   } else {
       $this->referral_model->new_client_noti_info($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/path_notification_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_noti_instruct_consent_id/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_noti_instruct_consent_id_dl/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}
}

// CLient Notification Instruction Save
public function client_referral_noti_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = $this->input->post('appID');
    $check_client_referral_noti = $this->referral_model->check_client_referral_noti($clientID);

    if ($check_client_referral_noti != $clientID){
       $this->referral_model->edit_client_referral_noti($postdata);
   } else {
       $this->referral_model->new_client_referral_noti($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/referral_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/referral_acknowledgement/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/referral_acknowledgement_dl/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

redirect('/client/client_notification_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

// Notification to Insurance Company of Completion
public function client_noti_ins_to_completion_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = $this->input->post('appID');
    $insuranceID = $insurance_company;
    $check_client_noti_report = $this->referral_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->referral_model->edit_noti_ins_to_completion($postdata);
   } else {
       $this->referral_model->new_noti_ins_to_completion($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/noti_to_ins_company_completion_view/'.$clientID.'/'.$insuranceID.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/noti_to_ins_company_completion/'.$clientID.'/'.$insuranceID.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/noti_to_ins_company_completion_dl/'.$clientID.'/'.$insuranceID.'/'.$appID.'/');
}

redirect('/client/client_notification_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

// Notification to Insurance Company of Completion
public function advisor_notification_of_completion_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $appID = $this->input->post('appID');
    $insurance_company = $this->input->post('insurance_company');
    $check_client_noti_report = $this->referral_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->referral_model->edit_noti_adv_to_completion($postdata);
   } else {
       $this->referral_model->new_noti_adv_to_completion($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/advisor_notification_of_completion/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/advisor_notification_of_completion_dl/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

// Notification to Insurance Company of Completion
public function client_consent_id_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $appID = $this->input->post('appID');
    $insurance_company = $this->input->post('insurance_company');
    $check_client_noti_report = $this->referral_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->referral_model->edit_client_consent_id($postdata);
   } else {
       $this->referral_model->new_client_consent_id($postdata);
   }

   $this->referral_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/client_consent_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_consent/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_consent_dl/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.$appID.'/');
}

public function delete($clientID)
{
    $this->referral_model->delete_referral_info($clientID);

    redirect('/referral/index/');
}

// PDF GENERATOR SECTION



// Referral Acknowledgement
public function referral_acknowledgement($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);

    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->referral_model->get_referral_noti($clientID);
    $client_info_field = $client_referral_noti[0]['client_information'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  if ($client_info_field == 'Y') {
     // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }


  $pdf->Output("referral_acknowledgement.pdf",'I');

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_dl($clientID, $insuranceID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    //$data['clientApointment']   = $this->client_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  ob_clean();
  $pdf->Output("referral_acknowledgement.pdf",'D');
  ob_end_flush();

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_up($clientID, $insuranceID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    //$data['clientApointment']   = $this->client_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output($_SERVER["DOCUMENT_ROOT"]. "uploads/temp/".time()."_".$clientID."_referral_acknowledgement.pdf",'F');

  return $_SERVER["DOCUMENT_ROOT"]."uploads/temp/".time()."_".$clientID."_referral_acknowledgement.pdf";
  ob_end_flush();

}



// Client Information and Consent Identification
public function client_info_consent_id($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_pathreport'] = $this->referral_model->get_clienttest_info($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");

    // Check field values
    $client_pathreport = $this->referral_model->get_path_report($clientID);
    $client_info_field = $client_pathreport[0]['client_information'];
    $client_consent_id_field = $client_pathreport[0]['client_consent_id'];
    $path_tests_explained_field = $client_pathreport[0]['client_consent_id'];
    $authority_results_field = $client_pathreport[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/common/client_info_text', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);

  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointment_preferrence', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);


    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_preferrence, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);

  if ($client_info_field == 'Y') {
     // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {

    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Pathology Tests Explained 
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  $pdf->Output("pathology_appointment.pdf",'I');

}

// Client Information and Consent Identification
public function client_info_consent_id_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_pathreport'] = $this->referral_model->get_clienttest_info($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");

    // Check field values
    $client_pathreport = $this->referral_model->get_path_report($clientID);
    $client_info_field = $client_pathreport[0]['client_information'];
    $client_consent_id_field = $client_pathreport[0]['client_consent_id'];
    $path_tests_explained_field = $client_pathreport[0]['client_consent_id'];
    $authority_results_field = $client_pathreport[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/common/client_info_text', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);

  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointment_preferrence', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);


    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_preferrence, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);

  if ($client_info_field == 'Y') {
     // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {

    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Pathology Tests Explained 
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  $pdf->Output("pathology_appointment.pdf",'D');

}


// Client Notification, instruction and consent Identification
public function client_noti_instruct_consent_id($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);


  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_notification.pdf",'I');

}

// Client Notification, instruction and consent Identification Download
public function client_noti_instruct_consent_id_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);


  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_notification.pdf",'D');

}


// Advisor Notification
public function advisor_notification($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_no.pdf",'I');

}

// Advisor Notification download
public function advisor_notification_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_notification.pdf",'D');

}





// Advisor Notification of Completion
public function advisor_notification_of_completion($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_adv_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']  = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_to_adv_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);

  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);


  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($letter_to_adv_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("advisor_completion_notification.pdf",'I');

}

// Advisor Notification of Completion download
public function advisor_notification_of_completion_dl($clientID, $insuranceID)
{
     // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_adv_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']  = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_to_adv_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);

  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);


  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($letter_to_adv_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("advisor_notification_of_completion.pdf",'D');

}


    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion($clientID, $insuranceID ,$appID)
{
   // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']  = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);



  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($letter_to_adv_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("notification_to_insurance_completion.pdf",'I');

}

    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']  = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);



  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($letter_to_adv_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }
  $pdf->Output("notification_to_insurance_completion.pdf",'D');

}


// Client Information and Consent Identification
public function client_consent($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_client_consent_id_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];
    $client_ins_bombora_field = $client_noti_report[0]['client_ins_bombora'];
    $client_details_field = $client_noti_report[0]['client_details'];
    $tests_ordered_field = $client_noti_report[0]['tests_ordered'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/bombora/client_ins_for_bombora', $data, true);
  $tests_ordered = $this->load->view('client/generate/common/tests_ordered', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

  if ($client_details_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  }


  if ($tests_ordered_field == 'Y') {
    // Client Authority to release r
      $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  }

  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }
  

  if ($client_ins_bombora_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_consent.pdf",'I');

}

// Client Information and Consent Identification Download
public function client_consent_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->referral_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->referral_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->referral_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->referral_model->get_test_client($clientID);
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['clientApointment']   = $this->referral_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->referral_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->referral_model->get_client_consent_id_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];
    $client_ins_bombora_field = $client_noti_report[0]['client_ins_bombora'];
    $client_details_field = $client_noti_report[0]['client_details'];
    $tests_ordered_field = $client_noti_report[0]['tests_ordered'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/bombora/client_ins_for_bombora', $data, true);
  $tests_ordered = $this->load->view('client/generate/common/tests_ordered', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

  if ($client_details_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  }


  if ($tests_ordered_field == 'Y') {
    // Client Authority to release r
      $pdf->writeHTMLCell(0, 0, '', '', $tests_ordered, 0, 1, 0, true, '', true);
  }

  if ($client_info_field == 'Y') {
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }
  

  if ($client_ins_bombora_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_consent.pdf",'D');

}

public function bulkexportPDF($clientID, $insuranceID)
{
    unlink('A&Ps.zip');
    // var_dump(count($clientID));
    // die();
    $i=0;
    $files=[];
while ($i<count($clientID))
{
// DECLARE VARIABLES
$data['clientData'] = $this->client_model->get_client($clientID[$i]);
// $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
$data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID[$i]);
$data['testTicked']  = $this->client_model->get_test_client($clientID[$i]);
$data['testTypes']   = $this->client_model->get_test_types();
//$data['clientApointment']   = $this->client_model->get_specific_apointment($appID);


$title = $this->client_model->get_client($clientID[$i]);
$titlename = $title[0]['client_name']; 
$this->load->library("Pdf");

// Check field values
$client_referral_noti = $this->client_model->get_referral_noti($clientID[$i]);
$HIV_field = $client_referral_noti[0]['HIV'];
$client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
$instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

// create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);
     // remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(true);
// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
$pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}   
$first_column_width = 85;
$second_column_width = 85;
$column_space = 25; 
$pdf->SetY(28);
$current_y_position = $pdf->getY();
$line_height = 4.2;
$client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
$privacy = $this->load->view('client/generate/common/privacy', $data, true);
$HIV = $this->load->view('client/generate/common/HIV', $data, true);
$instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
$client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
$reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
$client_info = $this->load->view('client/generate/client_info', $data, true);
$referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
$visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
$client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
$services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
$appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
$pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
$services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
$auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
$pdf->setFontSubsetting(true);   
$pdf->SetFont('dejavusans', '', 8, '', true);   
// // set header data
    // // Portrait     
$pdf->AddPage('P', 'A4');
$pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

// Client Info
$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);



 // Consent Info
$pdf->AddPage('P', 'A4');
$pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
$pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
if ($HIV_field == 'Y') {
  $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
}
if ($client_consent_id_field == 'Y') {
  $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
}
$i=$i+1;
$pdf->Output("referral_acknowledgement".$i.".pdf",'F');
 array_push($files, "referral_acknowledgement".$i.".pdf");
}
$zipname = 'A&Ps.zip';
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);
foreach ($files as $file) {
  $zip->addFile($file);
}
$zip->close();
header('Content-Type: application/zip');
header('Content-disposition: attachment; filename='.$zipname);
header('Content-Length: ' . filesize($zipname));
readfile($zipname);
}

public function edittests($referralID)
{
    $header['breadcrumbs'][]    = array('title' => 'Referral', 'link' => 'referral/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Tests Ordered', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Tests Ordered";
    $mainheader['menuitem']     = 30;
    $mainheader['menuitem_toggle']     = '';
    $data['testTypes']   = $this->referral_model->get_test_types();
    $data['testTicked']  = $this->referral_model->get_test_client($referralID);
    $data['testNotes']  = $this->referral_model->get_clienttest_notes($referralID);
    $data['referralID']    = $referralID;

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('referral/testsedit', $data);
    $this->load->view('main_footer');
}

public function updatetests()
{
    $postdata = $this->input->post();
    $referralID = $this->input->post('referralID');

    $this->referral_model->update_client_tests($postdata);

    redirect('/referral/view/'.$referralID);
}

}
