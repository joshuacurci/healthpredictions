<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('client_model');
    $this->load->library('Pdf');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
      die();
  }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

  if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
  }
}

// PDF GENERATOR SECTION

// Client Information and Consent Identification
public function client_info_consent_id($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_appointment_time, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);

    // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);

    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);

  $pdf->Output("pathology_appointment.pdf",'I');

}

// Client Information and Consent Identification
public function client_info_consent_id_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_appointment_time, 0, 1, 0, true, '', true);

    // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);

    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);

  $pdf->Output("pathology_appointment.pdf",'D');

}


// Referral Acknowledgement
public function referral_acknowledgement($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $visit_time_requested, 0, 1, 0, true, '', true);

  $pdf->Output("referral_acknowledgement.pdf",'I');

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $visit_time_requested, 0, 1, 0, true, '', true);

  $pdf->Output("referral_acknowledgement.pdf",'D');

}


// Client Notification, instruction and consent Identification
public function client_noti_instruct_consent_id($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);

  // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

  // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);

  // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);

  $pdf->Output("pathology_appointment.pdf",'I');

}

// Client Notification, instruction and consent Identification Download
public function client_noti_instruct_consent_id_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);

  // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

  // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);

  // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);

  $pdf->Output("pathology_appointment.pdf",'D');

}





// Advisor Notification
public function advisor_notification($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_no.pdf",'I');

}

// Advisor Notification download
public function advisor_notification_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_notification.pdf",'D');

}





// Advisor Notification of Completion
public function advisor_notification_of_completion($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora_ins_letter', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);

    // Bombora Medical Life Insurance Medical Service
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_life_ins_med_service, 0, 1, 0, true, '', true);
    // Bombora Medical Life Insurance Medical Service
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);

  $pdf->Output("pathology_appointment.pdf",'I');

}

// Advisor Notification of Completion download
public function advisor_notification_of_completion_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora_ins_letter', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);

    // Bombora Medical Life Insurance Medical Service
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_life_ins_med_service, 0, 1, 0, true, '', true);
    // Bombora Medical Life Insurance Medical Service
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_notification_of_completion.pdf",'D');

}





    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_company', $data, true);

  $bombora_ins_letter = $this->load->view('client/generate/bombora_ins_letter', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);

  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);


  $pdf->Output("notification_to_insurance_completion.pdf",'I');

}

    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_company', $data, true);
  $noti_to_ins_company_completion = $this->load->view('client/generate/noti_to_ins_company_completion', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora_ins_letter', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion, 0, 1, 0, true, '', true);

  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $bombora_ins_letter, 0, 1, 0, true, '', true);


  $pdf->Output("notification_to_insurance_completion.pdf",'D');

}




// Client Information and Consent Identification
public function client_consent($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/client_ins_for_bombora', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

    // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);

    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);


  $pdf->Output("client_consent.pdf",'I');

}

// Client Information and Consent Identification Download
public function client_consent_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/auth_to_release_test_results', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/client_ins_for_bombora', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

    // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);

    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);

    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);


  $pdf->Output("client_consent.pdf",'D');

}

}