<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
    {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          $this->load->model('admin_model');
          $this->load->model('global_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  $this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  //if ( ! $this->session->userdata('loginuser'))
		   //     { 
        	//	    redirect('login/index');
		   //     }
    }

	 public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['userData']           = $this->admin_model->get_users($_SESSION['siteID']);
    $data['fullUserData']       = $this->admin_model->get_all_users();
    $data['siteData']           = $this->admin_model->get_all_sites();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/index', $data);
    $this->load->view('main_footer');
  }

  public function userindex()
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['userData']           = $this->admin_model->get_users($_SESSION['siteID']);
    $data['fullUserData']       = $this->admin_model->get_all_users();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/userindex', $data);
    $this->load->view('main_footer');
  }

  public function usersearch()
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['searchresults'] = $this->input->post();
    $data['userData']           = $this->admin_model->get_search_users($this->input->post());

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/userindex', $data);
    $this->load->view('main_footer');
  }

  // Site Data

  public function viewsite($siteID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Site', 'link' => 'admin/viewsite' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['siteData']           = $this->admin_model->get_site_data($siteID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/viewsite', $data);
    $this->load->view('main_footer');
  }

  public function addsite()
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add Site', 'link' => 'admin/addsite' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['country']            = $this->global_model->get_all_country();
    $data['staffState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/addsite', $data);
    $this->load->view('main_footer');
  }

  public function editsite($siteID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Site', 'link' => 'admin/editsite' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['siteData']           = $this->admin_model->get_site_data($siteID);
    $data['country']            = $this->global_model->get_all_country();
    $data['staffState']       = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/editsite', $data);
    $this->load->view('main_footer');
  }

  public function savesite()
  {
    $postdata = $this->input->post();
    $advisorID = $this->input->post('siteID');

    //if ($postdata['site_logo']) {
      //Upload files
      $this->load->library('upload');
      $files = $_FILES;
      $cpt = count($_FILES['site_logo']['name']);
      
      $config = array(
        'upload_path'   => './uploads/logos/',
        'allowed_types' => 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx|zip',
        'overwrite'     => 1,                       
      );

      $this->upload->initialize($config);
      $this->load->library('upload', $config);
      
      for($i=0; $i<$cpt; $i++) {           
        $_FILES['site_logo']['name']= $files['site_logo']['name'][$i];
        $_FILES['site_logo']['type']= $files['site_logo']['type'][$i];
        $_FILES['site_logo']['tmp_name']= $files['site_logo']['tmp_name'][$i];
        $_FILES['site_logo']['error']= $files['site_logo']['error'][$i];
        $_FILES['site_logo']['size']= $files['site_logo']['size'][$i];
        
        $prefix = md5($_FILES['site_logo']['name']);
        $new_name = $prefix.$_FILES['site_logo']['name'];
        $_FILES['site_logo']['name'] = $new_name;   

        if ($this->upload->do_upload('site_logo')) {
          $this->_uploaded[$i] = $this->upload->data();
          $image = $_FILES['site_logo']['name'];
        } else {
          $this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
          $image = "0";
        }  
      }
    //}

    $this->admin_model->update_site_info($postdata, $image);
         
    redirect('/admin/viewsite/'.$advisorID);
  }

  public function newsite()
  {
    $postdata = $this->input->post();

    //Upload files
      $this->load->library('upload');
      $files = $_FILES;
      $cpt = count($_FILES['site_logo']['name']);
      
      $config = array(
        'upload_path'   => './uploads/logos/',
        'allowed_types' => 'jpg|jpeg|png|gif|pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx|zip',
        'overwrite'     => 1,                       
      );

      $this->upload->initialize($config);
      $this->load->library('upload', $config);
      
      for($i=0; $i<$cpt; $i++) {           
        $_FILES['site_logo']['name']= $files['site_logo']['name'][$i];
        $_FILES['site_logo']['type']= $files['site_logo']['type'][$i];
        $_FILES['site_logo']['tmp_name']= $files['site_logo']['tmp_name'][$i];
        $_FILES['site_logo']['error']= $files['site_logo']['error'][$i];
        $_FILES['site_logo']['size']= $files['site_logo']['size'][$i];
        
        $prefix = md5($_FILES['site_logo']['name']);
        $new_name = $prefix.$_FILES['site_logo']['name'];
        $_FILES['site_logo']['name'] = $new_name;   

        if ($this->upload->do_upload('site_logo')) {
          $this->_uploaded[$i] = $this->upload->data();
          $image = $_FILES['site_logo']['name'];
        } else {
          $this->form_validation->set_message('fileupload_check', $this->upload->display_errors());
          $image = "0";
        }  
      }

    $this->admin_model->new_site_info($postdata, $image);
         
    redirect('/admin/index/');
  }

  public function deletesite($siteID)
  {
    $this->admin_model->delete_site_info($siteID);
         
    redirect('/admin/index/');
  }

  // User Data

  public function viewuser($userID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'View User', 'link' => 'admin/viewuser' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['userData']           = $this->admin_model->get_user_data($userID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/viewuser', $data);
    $this->load->view('main_footer');
  }

  public function adduser()
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add User', 'link' => 'admin/adduser' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['typeData']           = $this->admin_model->get_type_data();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/adduser', $data);
    $this->load->view('main_footer');
  }

  public function edituser($userID)
  {
    $header['breadcrumbs'][]    = array('title' => 'Admin', 'link' => 'admin/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit User', 'link' => 'admin/editsite' );
    $header['icon']             = '<i class="fa fa-lock" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Admin";
    $mainheader['menuitem']     = 12;
    $mainheader['menuitem_toggle']     = '';
    $data['typeData']           = $this->admin_model->get_type_data();
    $data['userData']           = $this->admin_model->get_user_data($userID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('admin/edituser', $data);
    $this->load->view('main_footer');
  }

  public function newuser()
  {
    $postdata = $this->input->post();
    $email = $this->input->post('email');
    $username = $this->input->post('username');
    $sql = "SELECT * FROM tbl_users WHERE email = '".$email."' OR username = '".$username."'" ;
    $query = $this->db->query($sql);
    if($email != '' && $username != '')
    {
      if ($query->num_rows()==0)
      {
          $this->admin_model->new_user_info($postdata);
          redirect('/admin/userindex/');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">User exist.</div>');
        redirect('/admin/adduser/');
      }
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-warning text-center">Email and username cannot be empty.</div>');
      redirect('/admin/adduser/');
    }
  }

  public function saveuser()
  {
    $postdata = $this->input->post();
    $userID = $this->input->post('userID');

    $this->admin_model->update_user_info($postdata);
         
    redirect('/admin/viewuser/'.$userID);
  }

  public function deleteuser($userID)
  {
    $this->admin_model->delete_user_info($userID);
         
    redirect('/admin/userindex/');
  }
  

}
