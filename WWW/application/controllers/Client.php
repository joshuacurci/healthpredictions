<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);
class Client extends CI_Controller {

	public function __construct()
  {
    parent::__construct();

    $this->load->library('session');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('html');
    $this->load->database();
    $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
    $this->load->model('global_model');
    $this->load->model('advisor_model');
    $this->load->model('gp_model');
    $this->load->model('client_model');
    $this->load->library('Pdf');

    if($this->config->item('maintenance_mode') == TRUE) {
      $this->load->view('under_construction');
      $content = $this->load->view('under_construction', '', TRUE); 
      echo $content;
   
  }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');

  if ( ! $this->session->userdata('loginuser')) { 
      redirect('login/index');
  }
}

public function index()
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    if ($_SESSION['usertype'] == 'I') {
        $data['clientData']        = $this->client_model->get_client_all_advisor($_SESSION['userID']);
    } else {
        $data['clientData']        = $this->client_model->get_client_all();
    }
    $data['clientState']       = $this->global_model->get_all_austates();

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('client/index', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/index', $data);
        $this->load->view('main_footer');
    }
}

public function archived()
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['clientData']        = $this->client_model->get_client_archived();
    $data['clientState']       = $this->global_model->get_all_austates();

        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/archived_index', $data);
        $this->load->view('main_footer');
}


public function view($clientID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);
    $mainheader['title']        = "View Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    $data['testNotes']         = $this->client_model->get_clienttest_notes($clientID);
    $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('client/view', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/view', $data);
        $this->load->view('main_footer');
    }
    
}


public function referral_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_referral_noti($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/referral_acknowledgement/view', $data);
    $this->load->view('client/generate/common/client_info', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    $this->load->view('client/generate/common/services_specified', $data);
    $this->load->view('client/generate/referral_acknowledgement/referral_ack_text', $data);
    $this->load->view('client/generate/referral_acknowledgement/what_to_include_form', $data);
    $this->load->view('main_footer');
}

public function path_notification_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['path_reportData']         = $this->client_model->get_path_report($clientID);
    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/noti_to_pathology_consentID/view', $data);
    $this->load->view('client/generate/common/client_info', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    $this->load->view('client/generate/client_notification/appointment_preferrence', $data);
    $this->load->view('client/generate/noti_to_pathology_consentID/what_to_include_form', $data);
    $this->load->view('main_footer');
}

// Client Notification, Instructions and Consent ID

public function client_notification_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Client Notifications";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_clientNoti_report($clientID);
    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/client_notification/view', $data);
    $this->load->view('client/generate/common/client_info', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    $this->load->view('client/generate/common/services_specified', $data);
    $this->load->view('client/generate/client_notification/what_to_include_form', $data);

    $this->load->view('main_footer');
}

// Notification to Advisor of Appointments

public function advisor_notification_view($clientID,$insuranceID,$appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Advisor Notification of Appointments";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_clientNoti_report($clientID);
    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/advisor_notification/view', $data);
    $this->load->view('client/generate/common/client_info', $data);
    $this->load->view('client/generate/common/tests_ordered', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);

    $this->load->view('main_footer');
}

// Client Final Documentation

public function noti_to_ins_company_completion_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Notification to Insurance Company of Completion";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/noti_to_ins_completion/view', $data);
    $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data);
    // $this->load->view('client/generate/common/tests_ordered', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    // $this->load->view('client/generate/noti_to_ins_completion/what_to_include_form', $data);

    $this->load->view('main_footer');
}

// Advisor Notification of Completion

public function advisor_notification_of_completion_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Notification to Advisor of Completion and Summary Form";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_clientNoti_to_adv_report($clientID);
    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/advisor_noti_of_completion/view', $data);
    $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data);
    // $this->load->view('client/generate/common/tests_ordered', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    // $this->load->view('client/generate/advisor_noti_of_completion/what_to_include_form', $data);

    $this->load->view('main_footer');
}


// Advisor Notification of Completion

public function client_consent_view($clientID,$insuranceID, $appID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Client Consent and Identification";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']         = $this->client_model->get_client($clientID);
    $data['clientNoti_reportData']         = $this->client_model->get_client_consent_id_report($clientID);
    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    $data['not1']               = $this->client_model->get_notifications($clientID, 1);
    $data['not2']               = $this->client_model->get_notifications($clientID, 2);
    $data['not3']               = $this->client_model->get_notifications($clientID, 3);
    $data['not4']               = $this->client_model->get_notifications($clientID, 4);
    $data['not5']               = $this->client_model->get_notifications($clientID, 5);
    $data['not6']               = $this->client_model->get_notifications($clientID, 6);
    $data['not7']               = $this->client_model->get_notifications($clientID, 7);
    $data['not8']               = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTicked']         = $this->client_model->get_test_client($clientID);
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['clientHistory']      = $this->client_model->get_client_history($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/generate/client_consent_id/view', $data);
    $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data);
    // $this->load->view('client/generate/common/tests_ordered', $data);
    $this->load->view('client/generate/client_notification/appointments', $data);
    $this->load->view('client/generate/client_consent_id/what_to_include_form', $data);

    $this->load->view('main_footer');
}


public function add()
{
    $header['breadcrumbs'][]    = array('title' => 'CLient', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Add New Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Add New Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['clientState']        = $this->global_model->get_all_austates();
    $data['insuranceComp']      = $this->client_model->get_insurance();
    $data['advisorDetails']     = $this->client_model->get_advisors();
    $data['staffDetails']     = $this->client_model->get_staff();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/add', $data);
    $this->load->view('main_footer');
}

public function edit($clientID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['country']            = $this->global_model->get_all_country();
    $data['clientData']      = $this->client_model->get_client($clientID);
    $data['clientState']     = $this->global_model->get_all_austates();
    $data['insuranceComp']      = $this->client_model->get_insurance();
    $data['advisorDetails']     = $this->client_model->get_advisors();
    $data['staffDetails']     = $this->client_model->get_staff();

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('client/edit', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/edit', $data);
        $this->load->view('main_footer');
    }
}

public function pre_generate($clientID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'View Client', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "View Client";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';
    $data['clientData']        = $this->client_model->get_client($clientID);
    $data['not1']        = $this->client_model->get_notifications($clientID, 1);
    $data['not2']        = $this->client_model->get_notifications($clientID, 2);
    $data['not3']        = $this->client_model->get_notifications($clientID, 3);
    $data['not4']        = $this->client_model->get_notifications($clientID, 4);
    $data['not5']        = $this->client_model->get_notifications($clientID, 5);
    $data['not6']        = $this->client_model->get_notifications($clientID, 6);
    $data['not7']        = $this->client_model->get_notifications($clientID, 7);
    $data['not8']        = $this->client_model->get_notifications($clientID, 8);
    $data['testTypes']   = $this->client_model->get_test_types();
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['clientApointment']   = $this->client_model->get_client_apointment($clientID);
    
    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('client/pre_generate', $data);
    $this->load->view('main_footer');
}

public function notifyedit($clientID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Client Notifications', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Client Notifications";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['not1']        = $this->client_model->get_notifications($clientID, 1);
    $data['not2']        = $this->client_model->get_notifications($clientID, 2);
    $data['not3']        = $this->client_model->get_notifications($clientID, 3);
    $data['not4']        = $this->client_model->get_notifications($clientID, 4);
    $data['not5']        = $this->client_model->get_notifications($clientID, 5);
    $data['not6']        = $this->client_model->get_notifications($clientID, 6);
    $data['not7']        = $this->client_model->get_notifications($clientID, 7);
    $data['not8']        = $this->client_model->get_notifications($clientID, 8);
    $data['clientID']    = $clientID;

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('client/notifyedit', $data);
    $this->load->view('main_footer');
}

public function edittests($clientID)
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Edit Tests Ordered', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Edit Tests Ordered";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    $data['testTypes']   = $this->client_model->get_test_types();
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testNotes']  = $this->client_model->get_clienttest_notes($clientID);
    $data['clientID']    = $clientID;

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header) ;
    $this->load->view('client/testsedit', $data);
    $this->load->view('main_footer');
}

public function search()
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['title']        = "Client Search Result";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
        $postdata = $this->session->flashdata('dashData');
      } else {
        $postdata = $this->input->post();
      }
    if ($_SESSION['usertype'] == 'I') {
        $data['clientData'] = $this->client_model->search_client_advisor($postdata, $_SESSION['userID']);
    } else {
        $data['clientData'] = $this->client_model->search_client($postdata);
    }
    $data['searchresults'] = $postdata;

    $data['clientState'] = $this->global_model->get_all_austates();

    if ($_SESSION['usertype'] == 'I') {
        $this->load->view('advisor_header', $mainheader);
        $this->load->view('advisor_bar', $header);
        $this->load->view('client/index', $data);
        $this->load->view('advisor_footer');
    } else {
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/index', $data);
        $this->load->view('main_footer');
    }
}

public function searchachrived()
{
    $header['breadcrumbs'][]    = array('title' => 'Client', 'link' => 'client/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails'] = $this->global_model->get_site_infoID($_SESSION['siteID']);

    $mainheader['title']        = "Archived Client Search Result";
    $mainheader['menuitem']     = 8;
    $mainheader['menuitem_toggle']     = '';

    if ($this->session->flashdata('dashData')) {
        $postdata = $this->session->flashdata('dashData');
      } else {
        $postdata = $this->input->post();
      }
        $data['clientData'] = $this->client_model->search_client_archived($postdata);
        $data['searchresults'] = $postdata;

    $data['clientState'] = $this->global_model->get_all_austates();
        $this->load->view('main_header', $mainheader);
        $this->load->view('main_bar', $header);
        $this->load->view('client/archived_index', $data);
        $this->load->view('main_footer');
}


public function search_all()
{
    $header['breadcrumbs'][]    = array('title' => 'Search Client', 'link' => 'client/search_all' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Client";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 19;

    $data['clientData']        = $this->client_model->get_client_all();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/search_all', $data);
    $this->load->view('main_footer');
}

public function search_result()
{
    $header['breadcrumbs'][]    = array('title' => 'Search Client', 'link' => 'client/search_all' );
    $header['breadcrumbs'][]    = array('title' => 'Search Result', 'link' => '' );
    $header['icon']             = '<i class="fa fa-users" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Client";
    $mainheader['menuitem']     = 10;
    $mainheader['menuitem_toggle']     = 19;

    $postdata = $this->input->post();
 
    $data['clientData'] = $this->client_model->search_client_all($postdata);
 
    $data['searchresults'] = $postdata;

    $data['clientState']       = $this->global_model->get_all_austates();
    $data['country']            = $this->global_model->get_all_country();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('client/search_all', $data);
    $this->load->view('main_footer');
}


public function save()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');
    $client_address = $postdata['client_address'];
    $client_city = $postdata['client_city'];
    $client_postcode = $postdata['client_postcode'];

    $this->client_model->update_client_info($postdata, $client_address, $client_city, $client_postcode);

    redirect('/client/view/'.$clientID);
}

public function savenotify()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');

    $this->client_model->update_client_notify($postdata);

    redirect('/client/view/'.$clientID);
}

public function savetests()
{
    $postdata = $this->input->post();
    $clientID = $this->input->post('clientID');

    $this->client_model->update_client_tests($postdata);

    redirect('/client/view/'.$clientID);
}

public function newclient()
{
    $postdata = $this->input->post();
    $client_address = $this->input->post('client_address');
    $client_city = $this->input->post('client_city');
    $client_postcode = $this->input->post('client_postcode');
    $siteID = $this->input->post('siteID');

    $clientID = $this->client_model->new_client_info($postdata, $client_address, $client_city, $client_postcode);
    $this->client_model->new_notifications($clientID);    
    // $this->client_model->new_referral_noti($clientID);
    $this->client_model->new_path_report_default($postdata, $clientID, $siteID);
    $this->client_model->new_client_noti_default($postdata, $clientID, $siteID);
    $this->client_model->new_noti_to_ins_completion_default($postdata, $clientID, $siteID);
    $this->client_model->new_noti_to_adv_completion_default($postdata, $clientID, $siteID);
    $this->client_model->new_client_consent_default($postdata, $clientID, $siteID);
    $this->client_model->new_client_referral_noti_default($postdata, $clientID, $siteID);

    redirect('/client/view/'.$clientID);
}

// Pathology Appointment Request Save
public function pathology_appointment_request_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = explode(",",$this->input->post('appID'));

    $check_client_path_report = $this->client_model->check_client_path_report($clientID);

    if ($check_client_path_report != 0){
       $this->client_model->edit_path_report_info($postdata);
   } else {
       $this->client_model->new_path_report_info($postdata);
   }

   $this->client_model->new_notifications($clientID);
 
   if (isset($_POST['submit'])) {
       redirect('/client/path_notification_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_info_consent_id/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_info_consent_id_dl/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}
}

// CLient Notification Instruction Save
public function client_notification_ins_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = explode(",",$this->input->post('appID'));
    $check_client_noti_report = $this->client_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->client_model->edit_client_noti_info($postdata);
   } else {
       $this->client_model->new_client_noti_info($postdata);
   }

   $this->client_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/path_notification_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_noti_instruct_consent_id/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_noti_instruct_consent_id_dl/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}
}

// CLient Notification Instruction Save
public function client_referral_noti_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = explode(",",$this->input->post('appID'));
    $check_client_referral_noti = $this->client_model->check_client_referral_noti($clientID);

    if ($check_client_referral_noti != $clientID){
    $this->client_model->edit_client_referral_noti($postdata);
   } else {
       $this->client_model->new_client_referral_noti($postdata);
   }


   $this->client_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/referral_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/referral_acknowledgement/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
    } else if (isset($_POST['submit_download'])){
        redirect('/client/referral_acknowledgement_dl/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
    }

redirect('/client/client_notification_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

// Notification to Insurance Company of Completion
public function client_noti_ins_to_completion_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $insurance_company = $this->input->post('insurance_company');
    $appID = explode(",",$this->input->post('appID'));
    $insuranceID = $insurance_company;
    $check_client_noti_report = $this->client_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->client_model->edit_noti_ins_to_completion($postdata);
   } else {
       $this->client_model->new_noti_ins_to_completion($postdata);
   }

   $this->client_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/noti_to_ins_company_completion_view/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/noti_to_ins_company_completion/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/noti_to_ins_company_completion_dl/'.$clientID.'/'.$insuranceID.'/'.implode("-",$appID).'/');
}

redirect('/client/client_notification_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

// Notification to Insurance Company of Completion
public function advisor_notification_of_completion_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $appID = explode(",",$this->input->post('appID'));
    $insurance_company = $this->input->post('insurance_company');
    $check_client_noti_report = $this->client_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->client_model->edit_noti_adv_to_completion($postdata);
   } else {
       $this->client_model->new_noti_adv_to_completion($postdata);
   }

   $this->client_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/advisor_notification_of_completion/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/advisor_notification_of_completion_dl/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

// Notification to Insurance Company of Completion
public function client_consent_id_save()
{
    $postdata = $this->input->post();

    $clientID = $this->input->post('clientID');
    $appID = explode(",",$this->input->post('appID'));
    $insurance_company = $this->input->post('insurance_company');
    $check_client_noti_report = $this->client_model->check_client_noti_report($clientID);

    if ($check_client_noti_report != $clientID){
       $this->client_model->edit_client_consent_id($postdata);
   } else {
       $this->client_model->new_client_consent_id($postdata);
   }

   $this->client_model->new_notifications($clientID);

   if (isset($_POST['submit'])) {
       redirect('/client/client_consent_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
   } else if (isset($_POST['submit_view'])){
    redirect('/client/client_consent/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
} else if (isset($_POST['submit_download'])){
    redirect('/client/client_consent_dl/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

redirect('/client/advisor_notification_of_completion_view/'.$clientID.'/'.$insurance_company.'/'.implode("-",$appID).'/');
}

public function delete($clientID)
{
    $this->client_model->delete_client_info($clientID);

    redirect('/client/index/');
}

public function reactivate($clientID)
{
    $this->client_model->reactivate_client_info($clientID);

    redirect('/client/index/');
}
// PDF GENERATOR SECTION



// Referral Acknowledgement
public function referral_acknowledgement($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($instruct_taking_blood_field == 'Y') {
    $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output("referral_acknowledgement.pdf",'I');

}

// Referral Acknowledgement Download 
public function referral_acknowledgement_dl($clientID, $insuranceID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    //$data['clientApointment']   = $this->client_model->get_specific_apointment($appID);
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

// Check field values
    $client_referral_noti = $this->client_model->get_referral_noti($clientID);
    $HIV_field = $client_referral_noti[0]['HIV'];
    $client_consent_id_field = $client_referral_noti[0]['instruct_taking_blood'];
    $instruct_taking_blood_field = $client_referral_noti[0]['instruct_taking_blood'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $referral_acknowledgement_title = $this->load->view('client/generate/referral_acknowledgement_title', $data, true);
  $visit_time_requested = $this->load->view('client/generate/visit_time_requested', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $referral_acknowledgement_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);


  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($client_consent_id_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }

  $pdf->Output("referral_acknowledgement.pdf",'D');

}



// Client Information and Consent Identification
public function client_info_consent_id($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_pathreport'] = $this->client_model->get_clienttest_info($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");

    // Check field values
    $client_pathreport = $this->client_model->get_path_report($clientID);
    $HIV_field = $client_pathreport[0]['HIV'];
    $instruct_taking_blood_field = $client_pathreport[0]['instruct_taking_blood'];

    $client_consent_id_field = $client_pathreport[0]['client_consent_id'];
    $path_tests_explained_field = $client_pathreport[0]['client_consent_id'];
    $authority_results_field = $client_pathreport[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/common/client_info_text', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);

  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointment_preferrence', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);


    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_preferrence, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);

  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
  
    $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($instruct_taking_blood_field == 'Y') {
    $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }


if ($client_consent_id_field == 'Y') {

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
}

if ($client_consent_id_field == 'Y') {
    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
}

if ($path_tests_explained_field == 'Y') {
    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
}

// if ($authority_results_field == 'Y') {
//     // Client Authority to release results
//   $pdf->AddPage('P', 'A4');
//   $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
//   $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
// }

$pdf->Output("pathology_appointment.pdf",'I');

}

// Client Information and Consent Identification
public function client_info_consent_id_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_pathreport'] = $this->client_model->get_clienttest_info($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name'];
    $this->load->library("Pdf");

    // Check field values
    $client_pathreport = $this->client_model->get_path_report($clientID);
    $HIV_field = $client_pathreport[0]['HIV'];
    $instruct_taking_blood_field = $client_pathreport[0]['instruct_taking_blood'];

    $client_consent_id_field = $client_pathreport[0]['client_consent_id'];
    $path_tests_explained_field = $client_pathreport[0]['client_consent_id'];
    $authority_results_field = $client_pathreport[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/common/client_info_text', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);

  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_preferrence = $this->load->view('client/generate/client_notification/appointment_preferrence', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);


    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id_title, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_preferrence, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);

  
     // Consent Info
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
  if ($HIV_field == 'Y') {
  
    $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
  }
  if ($instruct_taking_blood_field == 'Y') {
    $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
  }


if ($client_consent_id_field == 'Y') {

    // Consent Form
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
}

if ($client_consent_id_field == 'Y') {
    // Pathology Tests Explained 
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
}

if ($path_tests_explained_field == 'Y') {
    // Client Authority to release results
  $pdf->AddPage('P', 'A4');
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
}

// if ($authority_results_field == 'Y') {
//     // Client Authority to release results
//   $pdf->AddPage('P', 'A4');
//   $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
//   $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
// }

  $pdf->Output("pathology_appointment.pdf",'D');

}


// Client Notification, instruction and consent Identification
public function client_noti_instruct_consent_id($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_clientNoti_report($clientID);
    $HIV_field = $client_noti_report[0]['HIV'];
    $instruct_taking_blood_field = $client_noti_report[0]['instruct_taking_blood'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
  
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      if ($HIV_field == 'Y') {
        $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      }
      if ($instruct_taking_blood_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
        }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_notification.pdf",'I');

}

// Client Notification, instruction and consent Identification Download
public function client_noti_instruct_consent_id_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_clientNoti_report($clientID);
    $HIV_field = $client_noti_report[0]['HIV'];
    $instruct_taking_blood_field = $client_noti_report[0]['instruct_taking_blood'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $client_duty_disclosure = $this->load->view('client/generate/common/client_duty_disclosure', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointment_reminder, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  // $pdf->writeHTMLCell(0, 0, '', '', $appointment_notes, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);


  
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_duty_disclosure, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      if ($HIV_field == 'Y') {
        $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      }
      if ($instruct_taking_blood_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
        }

  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_notification.pdf",'D');

}


// Advisor Notification
public function advisor_notification($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_no.pdf",'I');

}

// Advisor Notification download
public function advisor_notification_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_notification_title = $this->load->view('client/generate/advisor_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_notification_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_notification.pdf",'D');

}



// Advisor Notification of Completion
public function advisor_notification_of_completion($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['advisorData'] = $this->client_model->get_advisor($data['clientData'][0]['assigned_advisor']);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_adv_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $advisor_noti_of_completion_point_detail = $this->load->view('client/generate/advisor_noti_of_completion_point_detail', $data, true);
  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);

  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_point_detail, 0, 1, 0, true, '', true);

  $pdf->Output("advisor_completion_notification.pdf",'I');

}

// Advisor Notification of Completion download
public function advisor_notification_of_completion_dl($clientID, $insuranceID,$appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    $data['advisorData'] = $this->client_model->get_advisor($data['clientData'][0]['assigned_advisor']);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_adv_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $advisor_noti_of_completion_title = $this->load->view('client/generate/advisor_noti_of_completion_title', $data, true);
  $advisor_noti_of_completion = $this->load->view('client/generate/advisor_noti_of_completion', $data, true);
  $advisor_noti_of_completion_point_detail = $this->load->view('client/generate/advisor_noti_of_completion_point_detail', $data, true);
  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $bombora_life_ins_med_service = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);

  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_notification_title = $this->load->view('client/generate/client_notification_title', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);

  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $advisor_noti_of_completion_point_detail, 0, 1, 0, true, '', true);
  $pdf->Output("advisor_notification_of_completion.pdf",'D');
  ob_end_flush();

}


    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion($clientID, $insuranceID ,$appID)
{
   // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);

  $pdf->Output("notification_to_insurance_completion.pdf",'I');

}

    // Notification of compeletion to insurance company 
public function noti_to_ins_company_completion_dl($clientID, $insuranceID,$appID)
{
    ob_start();
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $client_info_field = $client_noti_report[0]['client_information'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $letter_to_adv_field = $client_noti_report[0]['letter_to_adv'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];

    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");
    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $noti_to_ins_company_completion_title = $this->load->view('client/generate/noti_to_ins_company_completion_title', $data, true);

  $tests_ordered = $this->load->view('client/generate/tests_ordered', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);

  $bombora_ins_letter = $this->load->view('client/generate/bombora/bombora_ins_letter', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
  $pdf->AddPage('P', 'A4');
  $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);

    // Client Info
  $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company_completion_title, 0, 1, 0, true, '', true);
  $pdf->writeHTMLCell(0, 0, '', '', $noti_to_ins_company, 0, 1, 0, true, '', true);
  
  $pdf->Output("notification_to_insurance_completion.pdf",'D');
  ob_end_flush();

}
  # =============================CHECKED======================================== #
public function client_complete($clientID)
{
 
  # Get type for each appointments, compare price and get the highest to 
  # fetch in the database.
  $appointments = $this->client_model->get_appointment_type($clientID);  
  if (sizeof($appointments)>0)
  {
    $high_id = $appointments[0]['appID'];
    $high_value = $this->client_model->get_appointment_score($appointments[0]['app_type']);
    $high_value = $high_value['app_score'];
    # =============================CHECKED======================================== #
  
    #Loop start
    foreach($appointments as $each){
      $this_value = $this->client_model->get_appointment_score($each['app_type']);
      $this_value = $this_value['app_score'];
   
      if ($this_value > $high_value){ #Trigger
        $high_id = $each['appID'];    #Point_ID Defined
        $high_value = $this_value;    #Point_value Defined
      }
      $point_change = $high_value;
    }
    # Above code is Most likely correct
    #var_dump($high_id);
    $result_namedatetype = $this->client_model->get_appointment_name_date_type($high_id);
    $result_name = $result_namedatetype['app_name'];
    $result_date = $result_namedatetype['app_date'];
    $result_type = $result_namedatetype['app_type'];
    #var_dump($result_date,$result_type);
  
    $advisor_id = $this->client_model->get_advisor_id($clientID);
  
    $this->client_model->insert_point_history($result_name, $clientID, $advisor_id, $result_type, $high_value, $result_date);
    $cur_point = $this->client_model->get_advisor_current_point($advisor_id);
    $cur_point = $cur_point[0]['advisor_points'];
    $new_point = $cur_point + $point_change;
    $this->client_model->update_advisor_points($advisor_id, $new_point);
  }

  
  $this->complete($clientID);

  $this->index();


}


// Client Information and Consent Identification
public function client_consent($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_client_consent_id_report($clientID);
    $HIV_field = $client_noti_report[0]['HIV'];
    $instruct_taking_blood_field = $client_noti_report[0]['instruct_taking_blood'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];
    $client_ins_bombora_field = $client_noti_report[0]['client_ins_bombora'];
    $client_details_field = $client_noti_report[0]['client_details'];
    $tests_ordered_field = $client_noti_report[0]['tests_ordered'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/bombora/client_ins_for_bombora', $data, true);
  $tests_ordered = $this->load->view('client/generate/common/tests_ordered', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

  if ($client_details_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  }


  if ($tests_ordered_field == 'Y') {
    // Client Authority to release r
      $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);
  }

  
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      if ($HIV_field == 'Y') {
        $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      }
      if ($instruct_taking_blood_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
        }
  

  if ($client_ins_bombora_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_consent.pdf",'I');

}

// Client Information and Consent Identification Download
public function client_consent_dl($clientID, $insuranceID, $appID)
{
    // DECLARE VARIABLES
    $data['clientData'] = $this->client_model->get_client($clientID);
    // $data['isuranceData'] = $this->insurance_model->get_insurance_info($insuranceID);
    $data['clienttestData'] = $this->client_model->get_clienttest_info($insuranceID);
    $data['client_notireport'] = $this->client_model->get_clientNoti_to_ins_report($clientID);
    $data['testTicked']  = $this->client_model->get_test_client($clientID);
    $data['testTypes']   = $this->client_model->get_test_types();
    if($appID!=NULL)
    {
      $data['clientApointment']      = $this->client_model->get_apointments(explode("-", $appID));
    }
    $data['siteDetail'] = $this->global_model->get_site_infoID($data['clientData'][0]['siteID']);
    $title = $this->client_model->get_client($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");


    // Check field values
    $client_noti_report = $this->client_model->get_client_consent_id_report($clientID);
    $HIV_field = $client_noti_report[0]['HIV'];
    $instruct_taking_blood_field = $client_noti_report[0]['instruct_taking_blood'];
    $client_consent_id_field = $client_noti_report[0]['client_consent_id'];
    $path_tests_explained_field = $client_noti_report[0]['path_tests_explained'];
    $client_instructions_field = $client_noti_report[0]['client_instructions'];
    $authority_results_field = $client_noti_report[0]['client_authority_results'];
    $medical_offers_field = $client_noti_report[0]['medical_offers'];
    $client_ins_bombora_field = $client_noti_report[0]['client_ins_bombora'];
    $client_details_field = $client_noti_report[0]['client_details'];
    $tests_ordered_field = $client_noti_report[0]['tests_ordered'];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
      require_once(dirname(__FILE__).'/lang/eng.php');
      $pdf->setLanguageArray($l);
  }   
  $first_column_width = 85;
  $second_column_width = 85;
  $column_space = 25; 
  $pdf->SetY(28);
  $current_y_position = $pdf->getY();
  $line_height = 4.2;
  $client_info_consent_id = $this->load->view('client/generate/client_info_consent_identification', $data, true);
  $privacy = $this->load->view('client/generate/common/privacy', $data, true);
  $HIV = $this->load->view('client/generate/common/HIV', $data, true);
  $instruct_taking_blood = $this->load->view('client/generate/common/instruct_taking_blood', $data, true);
  $reportheader = $this->load->view('client/generate/bombora/bombora_info', $data, true);
  $client_info = $this->load->view('client/generate/client_info', $data, true);
  $client_info_consent_id_title = $this->load->view('client/generate/common/client_info_consent_id_title', $data, true);
  $client_appointment_time = $this->load->view('client/generate/common/appointment_time', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);

  $services_specified = $this->load->view('client/generate/common/services_specified', $data, true);
  $services_specified_text = $this->load->view('client/generate/common/services_specified_text', $data, true);
  $appointments = $this->load->view('client/generate/client_notification/appointments', $data, true);
  $appointment_reminder = $this->load->view('client/generate/appointment_reminder', $data, true);
  $appointment_notes = $this->load->view('client/generate/appointment_notes', $data, true);
  $client_consent_id = $this->load->view('client/generate/common/client_consent_id', $data, true);
  $pathology_tests_explained = $this->load->view('client/generate/common/pathology_tests_explained', $data, true);
  $auth_to_release_test_results = $this->load->view('client/generate/common/auth_to_release_test_results', $data, true);
  $client_instructions = $this->load->view('client/generate/client_instructions', $data, true);
  $noti_to_ins_company = $this->load->view('client/generate/noti_to_ins_completion/noti_to_ins_company', $data, true);
  $bombora_medical_offers = $this->load->view('client/generate/bombora/bombora_life_ins_med_service', $data, true);
  $client_ins_for_bombora = $this->load->view('client/generate/bombora/client_ins_for_bombora', $data, true);
  $tests_ordered = $this->load->view('client/generate/common/tests_ordered', $data, true);
  $pdf->setFontSubsetting(true);   
  $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait

  if ($client_details_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $appointments, 0, 1, 0, true, '', true);
  }


  if ($tests_ordered_field == 'Y') {
    // Client Authority to release r
      $pdf->writeHTMLCell(0, 0, '', '', $services_specified, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $services_specified_text, 0, 1, 0, true, '', true);
  }

  
  // Consent Info
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_info_consent_id, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $privacy, 0, 1, 0, true, '', true);
      if ($HIV_field == 'Y') {
        $pdf->writeHTMLCell(0, 0, '', '', $HIV, 0, 1, 0, true, '', true);
      }
      if ($instruct_taking_blood_field == 'Y') {
      $pdf->writeHTMLCell(0, 0, '', '', $instruct_taking_blood, 0, 1, 0, true, '', true);
        }
  

  if ($client_ins_bombora_field == 'Y') {
    // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_ins_for_bombora, 0, 1, 0, true, '', true);
  }


  if ($client_consent_id_field == 'Y') {
    // Consent Form
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_consent_id, 0, 1, 0, true, '', true);
  }

  if ($client_instructions_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $client_instructions, 0, 1, 0, true, '', true);
  }

  if ($path_tests_explained_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $pathology_tests_explained, 0, 1, 0, true, '', true);
  }

  if ($authority_results_field == 'Y') {
  // Client Authority to release results
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $auth_to_release_test_results, 0, 1, 0, true, '', true);
  }

  if ($medical_offers_field == 'Y') {
      $pdf->AddPage('P', 'A4');
      $pdf->writeHTMLCell(0, 0, '', '', $reportheader, 0, 1, 0, true, '', true);
      $pdf->writeHTMLCell(0, 0, '', '', $bombora_medical_offers, 0, 1, 0, true, '', true);
  }

  $pdf->Output("client_consent.pdf",'D');

}

public function test_backIndex(){
  var_dump("WHAt?");
  $this->index();

}


public function complete($clientID) 
{

  
    $returnedValues = $this->client_model->completeClient($clientID);
    $this->client_model->set_ind_print_notification ($clientID, '6');
    $this->client_model->set_ind_print_notification ($clientID, '8');
    $this->advisor_notification_of_completion_dl($clientID, $returnedValues['insurance_company']);
    $this->noti_to_ins_company_completion_dl($clientID, $returnedValues['insurance_company']);    
}



}
