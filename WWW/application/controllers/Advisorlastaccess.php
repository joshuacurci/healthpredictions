<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Advisorlastaccess extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->database();
        $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
        $this->load->model('global_model');
        $this->load->model('advisor_model');
        $this->load->model('appointment_model');
        $this->load->model('gp_model');
        $this->load->model('client_model');
        $this->load->model('admin_model');
        $this->load->model('clientdiary_model');
        $this->load->model('clientsbytest_model');
        $this->load->model('advisorlastaccess_model');
        $this->load->model('referralsadvisor_model');
        $this->load->library('Pdf');

        if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
          //$this->load->model('news_model');
          //$this->load->model('admin_model');

      if ( ! $this->session->userdata('loginuser')) { 
          redirect('login/index');
      }
  }

  public function index()
  {
    $header['breadcrumbs'][]    = array('title' => 'Advisor Last Access', 'link' => 'advisorlastaccess/index' );
    $header['icon']             = '<i class="fa fa-low-vision" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);
    $mainheader['title']        = "Advisor Last Access";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 27;

    $data['advisorData']        = $this->advisorlastaccess_model->get_all_advisor();
    $data['clientState']       = $this->global_model->get_all_austates();
    $data['advData']           = $this->appointment_model->get_all_client_appointment();
    $data['testTypes']          = $this->client_model->get_test_types();
    $data['testTypes']   = $this->clientsbytest_model->get_test_types();
    // $data['testTicked']  = $this->client_model->get_test_client($clientID);
    // $data['testNotes']  = $this->client_model->get_clienttest_notes($clientID);

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisorlastaccess/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_last_access()
{
    $header['breadcrumbs'][]    = array('title' => 'Advisor Last Access', 'link' => 'advisorlastaccess/index' );
    $header['breadcrumbs'][]    = array('title' => 'Last Access', 'link' => '' );
    $header['icon']             = '<i class="fa fa-low-vision" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 27;

    $postdata = $this->input->post();

    // $data['advisorData']        = $this->advisorlastaccess_model->get_all_advisor();

     if (isset($_POST['1month'])) {
    // $data['advisorData']  = $this->advisorlastaccess_model->search_lastlogin();
        $data['advisorData']  = $this->advisorlastaccess_model->search_1month();
    } else if (isset($_POST['3month'])) {
        $data['advisorData']  = $this->advisorlastaccess_model->search_3month();
    } else if (isset($_POST['6month'])) {
        $data['advisorData']  = $this->advisorlastaccess_model->search_6month();
    } 

    // Turn advisor ID into single array from multidimensional
    // $result = array_map('current', $result);

// var_dump($result);
// die();
     // = $this->advisorlastaccess_model->get_advisor($result);

    $data['searchresults'] = $postdata;

    $data['clientState'] = $this->global_model->get_all_austates();

    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('advisorlastaccess/index', $data);
    $this->load->view('main_footer');
}

// Search specific appointment Date
public function search_between_date()
{
    $header['breadcrumbs'][]    = array('title' => 'Client Last Access', 'link' => 'advisorlastaccess/index' );
    $header['breadcrumbs'][]    = array('title' => 'Search Between Dates', 'link' => '' );
    $header['icon']             = '<i class="fa fa-low-vision" aria-hidden="true"></i>';
    $mainheader['sitedetails']  = $this->global_model->get_site_info($_SERVER['SERVER_NAME']);

    $mainheader['title']        = "Appointment Search Result";
    $mainheader['menuitem']     = 11;
    $mainheader['menuitem_toggle']     = 27;

    $postdata = $this->input->post();
    $startdate = strtotime($postdata['startdate']);
    $lastdate = strtotime($postdata['lastdate']);
    // var_dump($postdata); 
    $data['appData'] = $this->clientdiary_model->search_between_date($startdate, $lastdate);
    $data['searchresults'] = $postdata;


    $data['clientData']  = $this->client_model->get_client_all();
    $data['clientState'] = $this->global_model->get_all_austates();


    $this->load->view('main_header', $mainheader);
    $this->load->view('main_bar', $header);
    $this->load->view('clientdiary/index', $data);
    $this->load->view('main_footer');
}

}
?>