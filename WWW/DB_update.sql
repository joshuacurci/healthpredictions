-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 16, 2019 at 09:55 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `portalhe`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appointment_type`
--

CREATE TABLE `tbl_appointment_type` (
  `app_tname` varchar(100) DEFAULT NULL,
  `app_score` int(10) DEFAULT NULL,
  `app_type` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_appointment_type`
--

INSERT INTO `tbl_appointment_type` (`app_tname`, `app_score`, `app_type`) VALUES
('GP', 2, 'GP'),
('Specialist', 3, 'S');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_appointment_type`
--
ALTER TABLE `tbl_appointment_type`
  ADD PRIMARY KEY (`app_type`);

-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 16, 2019 at 09:56 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `portalhe`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_point_history`
--

CREATE TABLE `tbl_point_history` (
  `pointID` bigint(11) UNSIGNED NOT NULL,
  `pointName` text,
  `clientID` int(11) UNSIGNED DEFAULT NULL,
  `advisorID` int(11) UNSIGNED NOT NULL,
  `app_type` char(7) DEFAULT NULL,
  `pointChange` int(3) NOT NULL,
  `point_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_point_history`
--
--
-- Indexes for dumped tables
--
--
-- Indexes for table `tbl_point_history`
--
ALTER TABLE `tbl_point_history`
  ADD PRIMARY KEY (`pointID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_point_history`
--
ALTER TABLE `tbl_point_history`
  MODIFY `pointID` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT;
