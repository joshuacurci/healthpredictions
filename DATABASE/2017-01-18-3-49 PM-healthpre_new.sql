# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: healthpre_new
# Generation Time: 2017-01-18 04:49:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_advisor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_advisor`;

CREATE TABLE `tbl_advisor` (
  `advisorID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT '0',
  `insuranceID` int(11) DEFAULT NULL,
  `advisor_name` varchar(255) DEFAULT NULL,
  `advisor_company` varchar(255) DEFAULT NULL,
  `advisor_groups` varchar(255) DEFAULT NULL,
  `advisor_address` varchar(255) DEFAULT NULL,
  `advisor_address2` varchar(255) DEFAULT NULL,
  `advisor_city` varchar(255) DEFAULT NULL,
  `advisor_state` varchar(10) DEFAULT NULL,
  `advisor_postcode` varchar(4) DEFAULT NULL,
  `advisor_country` varchar(255) DEFAULT 'Australia',
  `advisor_email` varchar(255) DEFAULT NULL,
  `advisor_email2` varchar(255) DEFAULT NULL,
  `advisor_email3` varchar(255) DEFAULT NULL,
  `advisor_email4` varchar(255) DEFAULT NULL,
  `advisor_email5` varchar(255) DEFAULT NULL,
  `advisor_email6` varchar(255) DEFAULT NULL,
  `advisor_phone` varchar(20) DEFAULT NULL,
  `advisor_workphone` varchar(20) DEFAULT NULL,
  `advisor_mobile` varchar(20) DEFAULT NULL,
  `advisor_fax` varchar(20) DEFAULT NULL,
  `advisor_enable_points` varchar(2) DEFAULT NULL,
  `advisor_points` int(11) DEFAULT '0',
  `advisor_first_time` varchar(2) DEFAULT NULL,
  `advisor_mailing_list` varchar(2) DEFAULT NULL,
  `advisor_copy_results` varchar(2) DEFAULT NULL,
  `advisor_comments` text,
  `advisor_status` varchar(2) DEFAULT 'A',
  `advisor_deleted` varchar(1) DEFAULT 'N',
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`advisorID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_advisor` WRITE;
/*!40000 ALTER TABLE `tbl_advisor` DISABLE KEYS */;

INSERT INTO `tbl_advisor` (`advisorID`, `siteID`, `userID`, `insuranceID`, `advisor_name`, `advisor_company`, `advisor_groups`, `advisor_address`, `advisor_address2`, `advisor_city`, `advisor_state`, `advisor_postcode`, `advisor_country`, `advisor_email`, `advisor_email2`, `advisor_email3`, `advisor_email4`, `advisor_email5`, `advisor_email6`, `advisor_phone`, `advisor_workphone`, `advisor_mobile`, `advisor_fax`, `advisor_enable_points`, `advisor_points`, `advisor_first_time`, `advisor_mailing_list`, `advisor_copy_results`, `advisor_comments`, `advisor_status`, `advisor_deleted`, `dateadded`)
VALUES
	(22,3,6,3,'Sadie Kreiger ','Hermann Ltd ','Jast Group ','5364 Wyman Stream Apt. 117 ','Suite 693 ','Gislasonhaven ','4','3897','13','dhowe@example.org ','dariana.krajcik@example.net ','stanton.rhea@example.com ','virginie.gerhold@example.org ','chet.sawayn@example.net ','stacey48@example.org ','1-017-267-7121 ','192-488-7920x05585 ','1-017-267-7121 ','192-488-7920x05585 ','Y',0,'Y','Y','Y','Oh dear! I wish you wouldn\'t squeeze so.\' said the Duchess who seemed too much overcome to do it?\' \'In my youth\' said his father \'I took to the company generally \'You are old\' said the Rabbit. ','A ','N','1481762332'),
	(21,2,6,1,'Tremayne Steuber ','Torp Roob and Franecki ','Howe-Wisozk ','2826 Renner Gardens Suite 083 ','Suite 688 ','South Santiago ','3','3669','13','trisha21@example.org ','tianna.bauch@example.com ','pkris@example.net ','yrohan@example.net ','ona.reichert@example.org ','vjacobson@example.org ','212.819.8362x585 ','338-773-8158x450 ','212.819.8362x585 ','338-773-8158x450 ','Y',0,'Y','Y','Y','\"Alice did not feel encouraged to ask the question?\' said the Queen in front of the jurymen. \'No they\'re not\' said the Hatter. \'He won\'t stand beating. Now if you please! \"\"William the Conqueror. \"','A ','N','1481762226'),
	(20,2,6,4,'Gerry Lindgren ','O\'Reilly Inc ','Auer Rath and Hills ','07873 Baumbach Point Suite 302 ','Suite 808 ','South Vickyborough ','2','3679','13','jerrod.gleichner@example.net ','wuckert.rhett@example.org ','paul.hamill@example.org ','dward@example.com ','tre.bogan@example.net ','jonas.carroll@example.org ','285.027.7326 ','153.400.3732x8205 ','285.027.7326 ','153.400.3732x8205 ','Y',0,'Y','Y','Y','I\'ve often seen them so often you know.\' \'I don\'t know what to uglify is you see Miss this here ought to tell its age there was Mystery\' the Mock Turtle. \'No no! The adventures first\' said. ','A ','N','1481763443'),
	(19,4,6,2,'Lester Langworth ','Bartell-West ','Will-Bashirian ','75318 Crooks Fort ','Apt. 324 ','Lacyfort ','1','6326','13','elwin.stehr@example.net ','makayla55@example.net ','khyatt@example.net ','wkutch@example.net ','ullrich.lea@example.org ','block.lelia@example.com ','(691)522-1429x185 ','377-676-8547 ','(691)522-1429x185 ','377-676-8547 ','Y',0,'Y','Y','Y','Knave \'I didn\'t mean it!\' pleaded poor Alice. \'But you\'re so easily offended!\' \'You\'ll get used to it in large letters. It was so large in the distance screaming with passion. She had already. ','A ','N','1481762332'),
	(18,4,6,6,'Ferne Howe ','Kuhic-Armstrong ','Muller Bartell and Larkin ','13428 Haylee Valley ','Suite 914 ','New Mandyborough ','5','5591','13','jrenner@example.com ','cdietrich@example.org ','adickens@example.org ','williamson.keara@example.com ','keaton69@example.org ','lou.powlowski@example.net ','1-752-079-2765 ','4028374554','1-752-079-2765 ','4028374554','N',0,'N','N','N','Cheshire cat\' said the Queen pointing to the Gryphon. \'Of course\' the Gryphon added \'Come let\'s hear some of the gloves and was gone in a moment: she looked up and began talking to him\' said. ','A ','N','1481762226'),
	(17,4,6,5,'Darlene Jacobs ','Langosh Williamson and Dickens ','Turner LLC ','561 Vandervort Meadows ','Apt. 493 ','West Misael ','4','5702','13','jazmyne.bauch@example.org ','rolfson.rodger@example.net ','bosco.patsy@example.org ','maci.frami@example.com ','robin50@example.net ','tsmitham@example.com ','990-044-6745x273 ','464.165.0187x7355 ','990-044-6745x273 ','464.165.0187x7355 ','Y',0,'Y','Y','Y','\"Dodo could not remember ever having heard of uglifying!\' it exclaimed. \'You know what \"\"it\"\" means well enough when I got up this morning? I almost wish I\'d gone to see if she had never done such a. \"','A ','N','1481763443'),
	(16,3,6,5,'Dariana Emmerich','Will Group ','Mills Group ','701 Hal Mews Apt. 650 ','Apt. 035 ','Krajcikview ','3','8392','13','garrick.rogahn@example.com ','madisen01@example.com ','rkunde@example.net ','emelia04@example.com ','kris.justice@example.net ','ispencer@example.net ','697-813-4224x911 ','(212)056-2264x93891 ','697-813-4224x911 ','(212)056-2264x93891 ','N',0,'N','N','N','\"Alice. \'Stand up and said without even waiting to put the hookah out of court! Suppress him! Pinch him! Off with his head!\"\"\' \'How dreadfully savage!\' exclaimed Alice. \'That\'s the reason so many. \"','A ','N','1481762332'),
	(15,3,6,5,'Avis Parisian ','Murphy Group ','Gleichner-Torphy ','1355 Ziemann Ranch ','Suite 170 ','Wintheiserton ','2','7568','13','erdman.ernest@example.com ','bhackett@example.com ','neoma.auer@example.net ','bessie54@example.net ','ari69@example.com ','howell.lemke@example.com ','437-326-3548 ','5133289643','437-326-3548 ','5133289643','N',0,'N','N','N','Pigeon in a low trembling voice. \'There\'s more evidence to come out among the trees a little startled when she looked back once or twice she had put the hookah into its eyes by this time and was. ','A ','N','1481762226'),
	(14,3,6,2,'Dalton Farrell','Heaney Ltd ','Gutmann-Sipes ','293 Turner Knolls Suite 251 ','Suite 233 ','North Hardy ','1','7298','13','uschuppe@example.com ','clint.reichert@example.net ','rosamond.funk@example.net ','bbarrows@example.net ','jay.bartoletti@example.net ','rosenbaum.terrill@example.com ','1-332-057-6891 ','1-529-042-7855 ','1-332-057-6891 ','1-529-042-7855 ','N',0,'N','N','N','White Rabbit trotting slowly back again and she tried to beat them off and had to stoop to save her neck would bend about easily in any direction like a wild beast screamed \'Off with his head!\'. ','A ','N','1481763443'),
	(13,3,6,3,'Kayley Lebsack ','Ratke-Sporer ','Raynor Ltd ','41082 Gaylord Trafficway ','Apt. 645 ','Port Deltaton ','5','8772','13','filomena26@example.org ','zking@example.com ','west.elinore@example.com ','rkunde@example.com ','nhand@example.net ','nhills@example.com ','(268)536-7727x4537 ','(338)337-0009 ','(268)536-7727x4537 ','(338)337-0009 ','N',0,'N','N','N','But do cats eat bats?\' and sometimes \'Do bats eat cats?\' for you see Miss we\'re doing our best afore she comes to--\' At this moment Alice felt that she had never had fits my dear YOU must. ','A ','N','1481762332'),
	(12,2,6,5,'Andreane Grant ','Herzog-Predovic ','Kuhic and Sons ','7718 Richard Track Suite 225 ','Apt. 373 ','Stromanburgh ','4','8884','13','sheila78@example.org ','tsawayn@example.com ','enid44@example.com ','thora.sporer@example.org ','adriana44@example.net ','wilkinson.taryn@example.org ','(058)584-1359x26479 ','(910)363-7285x95214 ','(058)584-1359x26479 ','(910)363-7285x95214 ','Y',0,'Y','Y','Y','I won\'t then!--Bill\'s to go with the words came very queer to ME.\' \'You!\' said the Mock Turtle said: \'I\'m too stiff. And the Eaglet bent down its head impatiently and walked off; the Dormouse. ','A ','N','1481762226'),
	(11,2,6,4,'Gregory Beatty ','Lind-Ankunding ','Friesen Inc ','1989 Cheyenne Path ','Apt. 936 ','New Johnathonside ','3','8311','13','roxanne.klocko@example.com ','maya.beahan@example.com ','velma88@example.org ','gharris@example.org ','winona91@example.net ','msmitham@example.com ','(615)414-6848x89159 ','1473877485','(615)414-6848x89159 ','1473877485','Y',0,'Y','Y','Y','Alice remarked. \'Oh you can\'t swim can you?\' he added turning to Alice with one finger for the hot day made her so savage when they met in the last few minutes and began picking them up again as. ','A ','N','1481763443'),
	(10,2,6,9,'Citlalli Dibbert ','Willms Kub and Kihn ','Metz-Kulas ','557 Lockman Ranch Apt. 494 ','Suite 579 ','Port Lutherport ','2','3852','13','lparker@example.net ','trempel@example.org ','lgoodwin@example.net ','powlowski.damon@example.org ','mitchell.travon@example.org ','yernser@example.com ','309-946-1954x599 ','+20(6)0306866793 ','309-946-1954x599 ','+20(6)0306866793 ','Y',0,'Y','Y','Y','Will you won\'t you join the dance. So they began solemnly dancing round and look up in such a thing before and behind them a new kind of sob \'I\'ve tried every way and the White Rabbit read out. ','A ','N','1481762332'),
	(9,2,6,2,'Lempi Lubowitz ','Borer Ernser and Koch ','Ryan Daniel and Bergstrom ','92335 Mills Lane Suite 584 ','Suite 590 ','Murazikburgh ','1','7698','13','nmorissette@example.org ','kaden97@example.org ','vromaguera@example.org ','micheal20@example.org ','mturcotte@example.net ','prutherford@example.org ','(848)062-4484 ','652.005.5129 ','(848)062-4484 ','652.005.5129 ','Y',0,'Y','Y','Y','Gryphon. \'I\'ve forgotten the Duchess to play croquet.\' The Frog-Footman repeated in the back. However it was growing and very angrily. \'A knot!\' said Alice \'it\'s very interesting. I never. ','A ','N','1481762226'),
	(23,3,6,6,'Eden Reichert ','Dach Wilkinson and Rice ','Kiehn Gaylord and Bartoletti ','4500 Patricia Parks ','Suite 764 ','South Tamara ','5','8255','13','ewell05@example.net ','dena.ward@example.org ','asia.raynor@example.org ','fgulgowski@example.com ','heidenreich.thaddeus@example.com ','runolfsdottir.lora@example.com ','8783240928','3564801405','8783240928','3564801405','N',0,'N','N','N','I to get rather sleepy and went on just as well go back and see that she had never forgotten that if you don\'t know the way to fly up into hers--she could hear the very middle of her sharp little. ','A ','N','1481763443'),
	(24,4,6,3,'Ulices VonRueden ','Hilpert Block and Raynor ','Hansen Gerlach and Christiansen ','5292 Jazmyn Path ','Suite 953 ','Port Rupertstad ','1','8404','13','kaci.brekke@example.org ','hilario.jacobs@example.net ','ibrekke@example.org ','collins.ali@example.net ','hintz.glennie@example.org ','oral84@example.com ','8907184475','692.344.3608 ','8907184475','692.344.3608 ','N',0,'N','N','N','King looking round the table half hoping that the Mouse only growled in reply. \'Idiot!\' said the Cat. \'I don\'t know where Dinn may be\' said the Caterpillar. Alice folded her hands and she jumped. ','A ','N','1481762226'),
	(25,4,6,2,'Mitchell Dickens ','Oberbrunner Inc ','Christiansen-King ','05492 Jacobs Underpass Suite 296 ','Apt. 656 ','New Annalise ','2','7970','13','maynard71@example.com ','daisha26@example.net ','maye.green@example.net ','donald10@example.org ','nick94@example.com ','fern.ziemann@example.net ','1-080-110-4795 ','171.561.6116x764 ','1-080-110-4795 ','171.561.6116x764 ','N',0,'N','N','N','And Alice was too slippery; and when she found herself in a bit.\' \'Perhaps it doesn\'t understand English\' thought Alice; \'I must be kind to them\' thought Alice \'and if it had been it suddenly. ','A ','N','1481762332'),
	(26,2,6,2,'Otha Quitzon','Effertz Jones and Hansen ','Bogan PLC ','9711 Melyssa Pike ','Apt. 592 ','Ivahstad ','3','1814','13','destin.howe@example.org ','irolfson@example.net ','taurean83@example.org ','wilfrid.leffler@example.net ','casper.maude@example.org ','eleazar.mccullough@example.com ','(801)821-9368x3500 ','+21(8)9782122773 ','(801)821-9368x3500 ','+21(8)9782122773 ','N',0,'N','N','N','\"Allow me to introduce some other subject of conversation. While she was exactly the right distance--but then I wonder what you\'re at!\"\" You know the song she kept fanning herself all the rest. \"','A ','N','1481763443'),
	(27,2,6,8,'Kristoffer Koch','DuBuque-Hintz ','Schroeder-Terry ','3551 Monahan Stravenue ','Suite 665 ','Wavaside ','4','7323','13','shawna30@example.com ','emery.bauch@example.com ','fharber@example.net ','mcglynn.julie@example.com ','agnes.lind@example.net ','wallace28@example.com ','384.647.7790 ','494-134-9098 ','384.647.7790 ','494-134-9098 ','Y',0,'Y','Y','Y','They had a little bit and said to herself \'to be going messages for a few minutes she heard a little anxiously. \'Yes\' said Alice as she had plenty of time as she could. \'No\' said Alice. \'Why. ','A ','N','1481762226'),
	(28,2,6,6,'Jed Bartell ','Nolan Ltd ','Abshire Glover and Weimann ','7434 Ritchie Brook Apt. 962 ','Suite 942 ','Daughertyhaven ','5','1344','13','jose23@example.com ','kshlerin.ole@example.net ','delphine.jast@example.com ','justice.cummerata@example.com ','purdy.roxane@example.net ','jbartoletti@example.com ','406.210.6714x71631 ','(359)219-2656 ','406.210.6714x71631 ','(359)219-2656 ','N',0,'N','N','N','Alice. \'That\'s very curious.\' \'It\'s all his fancy that: he hasn\'t got no business of MINE.\' The Queen had ordered. They very soon finished it off. * * * CHAPTER II. The Pool of Tears \'Curiouser and. ','A ','N','1481762332');

/*!40000 ALTER TABLE `tbl_advisor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_appointments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_appointments`;

CREATE TABLE `tbl_appointments` (
  `appID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `clientID` int(11) NOT NULL DEFAULT '0',
  `app_name` varchar(255) DEFAULT NULL,
  `app_time` varchar(255) DEFAULT NULL,
  `app_date` varchar(255) DEFAULT NULL,
  `app_place` varchar(100) DEFAULT NULL,
  `app_service` varchar(200) DEFAULT NULL,
  `app_timestamp` varchar(255) DEFAULT NULL,
  `app_dismiss` varchar(255) DEFAULT NULL,
  `app_appointnum` varchar(11) NOT NULL DEFAULT '0',
  `app_type` varchar(2) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`appID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_appointments` WRITE;
/*!40000 ALTER TABLE `tbl_appointments` DISABLE KEYS */;

INSERT INTO `tbl_appointments` (`appID`, `siteID`, `clientID`, `app_name`, `app_time`, `app_date`, `app_place`, `app_service`, `app_timestamp`, `app_dismiss`, `app_appointnum`, `app_type`, `dateadded`)
VALUES
	(2,1,1,'Nola Kruize','11:45 am','1484658000','69 Calkwell St.Malvern','Paramedical, Full blood count','2005-12-07 13:15:30','2005-12-07 13:15:30','1','3',NULL),
	(3,1,1,'Sally Davis','11:00 am','1484658000','30 Jesica Road, Campbellfield','Paramedical & ECG','2005-12-07 13:15:30','2005-12-07 13:15:30','2','3',NULL),
	(4,1,1,'Sally Davis','10:00 am','1484658000','14 Railway Place, Preston','3 x Blood Pressure Readings','2005-12-07 13:15:30','2005-12-07 13:15:30','1','3',NULL),
	(5,1,1,'Sally Davis','2:30 pm','1484658000','56 Arundel Avenue\r\nReservoir','Paramedical Ins Assessment','2005-12-07 13:15:30','2005-12-07 13:15:30','1','3',NULL),
	(6,1,1,'Tester','02:00pm','1484658000','134 Langford Street','Blood Test','1484712088',NULL,'0','S',NULL);

/*!40000 ALTER TABLE `tbl_appointments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_au_states
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_au_states`;

CREATE TABLE `tbl_au_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(3) NOT NULL DEFAULT '',
  `state_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_au_states` WRITE;
/*!40000 ALTER TABLE `tbl_au_states` DISABLE KEYS */;

INSERT INTO `tbl_au_states` (`id`, `state_code`, `state_name`)
VALUES
	(1,'NSW','New South Wales'),
	(2,'QLD','Queensland'),
	(3,'SA','South Australia'),
	(4,'TAS','Tasmania'),
	(5,'VIC','Victoria'),
	(6,'WA','Western Australia'),
	(7,'ACT','Australian Capital Territory'),
	(8,'NT','Northern Territory');

/*!40000 ALTER TABLE `tbl_au_states` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `client_name` varchar(30) NOT NULL DEFAULT '',
  `client_organization` varchar(30) DEFAULT NULL,
  `client_address` varchar(50) DEFAULT NULL,
  `client_address2` varchar(50) DEFAULT NULL,
  `client_city` varchar(20) DEFAULT NULL,
  `client_state` varchar(10) DEFAULT '0',
  `client_postcode` varchar(4) DEFAULT NULL,
  `client_country` int(4) DEFAULT '0',
  `client_phone` varchar(20) DEFAULT NULL,
  `client_mobile` varchar(20) DEFAULT NULL,
  `client_fax` varchar(20) DEFAULT NULL,
  `client_email` varchar(100) DEFAULT NULL,
  `client_email2` varchar(100) DEFAULT '',
  `client_DOB` varchar(30) DEFAULT NULL,
  `client_sex` varchar(10) DEFAULT NULL,
  `client_workphone` varchar(50) DEFAULT '',
  `client_archived` varchar(5) DEFAULT NULL,
  `client_DateOfArchive` varchar(255) DEFAULT NULL,
  `client_StartDate` varchar(255) DEFAULT NULL,
  `client_EndDate` varchar(255) DEFAULT NULL,
  `client_deleted` varchar(2) DEFAULT 'N',
  `client_notes` mediumtext,
  `client_active` varchar(1) DEFAULT '',
  `dateadded` varchar(255) DEFAULT NULL,
  `insurance_company` int(11) DEFAULT NULL,
  `insurance_number` varchar(255) DEFAULT NULL,
  `assigned_advisor` int(11) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `siteID`, `client_name`, `client_organization`, `client_address`, `client_address2`, `client_city`, `client_state`, `client_postcode`, `client_country`, `client_phone`, `client_mobile`, `client_fax`, `client_email`, `client_email2`, `client_DOB`, `client_sex`, `client_workphone`, `client_archived`, `client_DateOfArchive`, `client_StartDate`, `client_EndDate`, `client_deleted`, `client_notes`, `client_active`, `dateadded`, `insurance_company`, `insurance_number`, `assigned_advisor`)
VALUES
	(1,1,'Joshua Curci','SWiM Communications','123 Sample Street','sub street','Sample Town','4','3058',13,'987654321','654987321','456987132','test@swim.com.au','josh@swim.com.au','07-12-2016','M','',NULL,NULL,NULL,NULL,'N','<p>Sample text goes here</p>','','1482213221',4,'ABC123',12);

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_notifications`;

CREATE TABLE `tbl_client_notifications` (
  `notID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `typeID` int(11) DEFAULT NULL,
  `print_date` varchar(255) DEFAULT NULL,
  `email_date` varchar(255) DEFAULT NULL,
  `fax_date` varchar(255) DEFAULT NULL,
  `phone_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`notID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_notifications` WRITE;
/*!40000 ALTER TABLE `tbl_client_notifications` DISABLE KEYS */;

INSERT INTO `tbl_client_notifications` (`notID`, `clientID`, `typeID`, `print_date`, `email_date`, `fax_date`, `phone_date`)
VALUES
	(1,1,1,'','15-12-2016','',''),
	(2,1,2,'','','',''),
	(3,1,3,'','','',''),
	(4,1,4,'','09-12-2016','',''),
	(5,1,5,'','','',''),
	(6,1,6,'','','',''),
	(7,1,7,'','','',''),
	(8,1,8,'','','','');

/*!40000 ALTER TABLE `tbl_client_notifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_notificationtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_notificationtypes`;

CREATE TABLE `tbl_client_notificationtypes` (
  `typeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_notificationtypes` WRITE;
/*!40000 ALTER TABLE `tbl_client_notificationtypes` DISABLE KEYS */;

INSERT INTO `tbl_client_notificationtypes` (`typeID`, `type_name`)
VALUES
	(1,'Pathology Appointment Notification'),
	(2,'Doctor Appointment Notification'),
	(3,'Client Consent Form'),
	(4,'Client Appointment Notification/Instructions'),
	(5,'Advisor Appointment Notification'),
	(6,'Advisor Completion Notification'),
	(7,'Referral Acknowledgment'),
	(8,'Insurance Completion Notification');

/*!40000 ALTER TABLE `tbl_client_notificationtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_tests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_tests`;

CREATE TABLE `tbl_client_tests` (
  `testID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `testtypeID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`testID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_tests` WRITE;
/*!40000 ALTER TABLE `tbl_client_tests` DISABLE KEYS */;

INSERT INTO `tbl_client_tests` (`testID`, `clientID`, `testtypeID`)
VALUES
	(1,1,'7,18,25,38');

/*!40000 ALTER TABLE `tbl_client_tests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_testtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_testtypes`;

CREATE TABLE `tbl_client_testtypes` (
  `testtypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`testtypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_testtypes` WRITE;
/*!40000 ALTER TABLE `tbl_client_testtypes` DISABLE KEYS */;

INSERT INTO `tbl_client_testtypes` (`testtypeID`, `type_name`)
VALUES
	(1,'Specialist Insurance Assessment'),
	(2,'GP Insurance Assessment (GP)'),
	(3,'GP Insurance Assessment (Own Doctor)'),
	(4,'Paramedical Insurance Assessment'),
	(5,'Quickcheck/rapidcheck/fastcheck'),
	(6,'AIA - Short Form Medical'),
	(7,'AMP - Express Health Check'),
	(8,'Asteron - Quickcheck'),
	(9,'Aviva - Rapid Check'),
	(10,'AXA - Mini Check'),
	(11,'Comminsure - Medilite'),
	(12,'Clearview - Mini Check'),
	(13,'BT - Quickcheck'),
	(14,'One Path - Medi Quick'),
	(15,'Macquarie - Fast Check/Quickcheck'),
	(16,'Macquarie Short-Form Medical Report'),
	(17,'MLC - Mini Check'),
	(18,'Suncorp - Quickcheck'),
	(19,'Tal - Fast Check'),
	(20,'Westpac - Quickcheck'),
	(21,'Zurich - Express Exam'),
	(22,'Three Blood Pressure readings at 5 minute intervals'),
	(23,'Teleunderwriting'),
	(24,'Client personal medical history report (P.M.A.R)'),
	(25,'Electrocardiogram (resting ECG)'),
	(26,'Exercise stress test (exercise ECG)'),
	(27,'Stress Echocardiogram'),
	(28,'Chest X-Ray'),
	(29,'Echocardiogram'),
	(30,'Lung Function Test'),
	(31,'24hr Blood Pressure Monitor'),
	(32,'24hr Electrocardiogram Monitor'),
	(33,'Breast Exam'),
	(34,'Results of current mammogram (within last 12 months)'),
	(35,'NON-Fasting MBA20 (inc HbA1c,HDL/LDL/Chol)'),
	(36,'Fasting MBA20 (inc HDL/LDL/Chol)'),
	(37,'Hep B (antigens) Hep C (antibodies)'),
	(38,'H.I.V'),
	(39,'Full blood exam/Full blood count (FBE/FBC)'),
	(40,'ESR'),
	(41,'PSA'),
	(42,'Serum Iron Studies'),
	(43,'Thyroid Function'),
	(44,'HbA1c - (Glycosylated haemoglobin)'),
	(45,'Urine Micro & Culture (MSU)'),
	(46,'Urine Micro & Culture (MSU) with red cell morphology'),
	(47,'Urine drug screen and report'),
	(48,'Cotinine'),
	(49,'Fasting Blood Glucose Test'),
	(50,'Lipids Profile'),
	(51,'Glucose Tolerance'),
	(52,'Liver Function Test'),
	(53,'Hepatitis C PCR'),
	(54,'24 Hour Urinary Protein'),
	(55,'CDT - Carbohydrate Deficient Transferrin'),
	(56,'Creatinine Clearance Blood test'),
	(57,'Creatinine Clearance Urine test'),
	(58,'C Reactive Protein'),
	(59,'PSA Free (including Free to Total Ratio)'),
	(60,'Hepatitis B e Antigen'),
	(61,'Testosterone Blood test'),
	(62,'Albumin / Creatinine Ratio urine test'),
	(63,'Limousine Service to Appointment');

/*!40000 ALTER TABLE `tbl_client_testtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_country`;

CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_country` WRITE;
/*!40000 ALTER TABLE `tbl_country` DISABLE KEYS */;

INSERT INTO `tbl_country` (`id`, `country_code`, `country_name`)
VALUES
	(1,'AF','Afghanistan'),
	(2,'AL','Albania'),
	(3,'DZ','Algeria'),
	(4,'DS','American Samoa'),
	(5,'AD','Andorra'),
	(6,'AO','Angola'),
	(7,'AI','Anguilla'),
	(8,'AQ','Antarctica'),
	(9,'AG','Antigua and Barbuda'),
	(10,'AR','Argentina'),
	(11,'AM','Armenia'),
	(12,'AW','Aruba'),
	(13,'AU','Australia'),
	(14,'AT','Austria'),
	(15,'AZ','Azerbaijan'),
	(16,'BS','Bahamas'),
	(17,'BH','Bahrain'),
	(18,'BD','Bangladesh'),
	(19,'BB','Barbados'),
	(20,'BY','Belarus'),
	(21,'BE','Belgium'),
	(22,'BZ','Belize'),
	(23,'BJ','Benin'),
	(24,'BM','Bermuda'),
	(25,'BT','Bhutan'),
	(26,'BO','Bolivia'),
	(27,'BA','Bosnia and Herzegovina'),
	(28,'BW','Botswana'),
	(29,'BV','Bouvet Island'),
	(30,'BR','Brazil'),
	(31,'IO','British Indian Ocean Territory'),
	(32,'BN','Brunei Darussalam'),
	(33,'BG','Bulgaria'),
	(34,'BF','Burkina Faso'),
	(35,'BI','Burundi'),
	(36,'KH','Cambodia'),
	(37,'CM','Cameroon'),
	(38,'CA','Canada'),
	(39,'CV','Cape Verde'),
	(40,'KY','Cayman Islands'),
	(41,'CF','Central African Republic'),
	(42,'TD','Chad'),
	(43,'CL','Chile'),
	(44,'CN','China'),
	(45,'CX','Christmas Island'),
	(46,'CC','Cocos (Keeling) Islands'),
	(47,'CO','Colombia'),
	(48,'KM','Comoros'),
	(49,'CG','Congo'),
	(50,'CK','Cook Islands'),
	(51,'CR','Costa Rica'),
	(52,'HR','Croatia (Hrvatska)'),
	(53,'CU','Cuba'),
	(54,'CY','Cyprus'),
	(55,'CZ','Czech Republic'),
	(56,'DK','Denmark'),
	(57,'DJ','Djibouti'),
	(58,'DM','Dominica'),
	(59,'DO','Dominican Republic'),
	(60,'TP','East Timor'),
	(61,'EC','Ecuador'),
	(62,'EG','Egypt'),
	(63,'SV','El Salvador'),
	(64,'GQ','Equatorial Guinea'),
	(65,'ER','Eritrea'),
	(66,'EE','Estonia'),
	(67,'ET','Ethiopia'),
	(68,'FK','Falkland Islands (Malvinas)'),
	(69,'FO','Faroe Islands'),
	(70,'FJ','Fiji'),
	(71,'FI','Finland'),
	(72,'FR','France'),
	(73,'FX','France, Metropolitan'),
	(74,'GF','French Guiana'),
	(75,'PF','French Polynesia'),
	(76,'TF','French Southern Territories'),
	(77,'GA','Gabon'),
	(78,'GM','Gambia'),
	(79,'GE','Georgia'),
	(80,'DE','Germany'),
	(81,'GH','Ghana'),
	(82,'GI','Gibraltar'),
	(83,'GK','Guernsey'),
	(84,'GR','Greece'),
	(85,'GL','Greenland'),
	(86,'GD','Grenada'),
	(87,'GP','Guadeloupe'),
	(88,'GU','Guam'),
	(89,'GT','Guatemala'),
	(90,'GN','Guinea'),
	(91,'GW','Guinea-Bissau'),
	(92,'GY','Guyana'),
	(93,'HT','Haiti'),
	(94,'HM','Heard and Mc Donald Islands'),
	(95,'HN','Honduras'),
	(96,'HK','Hong Kong'),
	(97,'HU','Hungary'),
	(98,'IS','Iceland'),
	(99,'IN','India'),
	(100,'IM','Isle of Man'),
	(101,'ID','Indonesia'),
	(102,'IR','Iran (Islamic Republic of)'),
	(103,'IQ','Iraq'),
	(104,'IE','Ireland'),
	(105,'IL','Israel'),
	(106,'IT','Italy'),
	(107,'CI','Ivory Coast'),
	(108,'JE','Jersey'),
	(109,'JM','Jamaica'),
	(110,'JP','Japan'),
	(111,'JO','Jordan'),
	(112,'KZ','Kazakhstan'),
	(113,'KE','Kenya'),
	(114,'KI','Kiribati'),
	(115,'KP','Korea, Democratic People\'s Republic of'),
	(116,'KR','Korea, Republic of'),
	(117,'XK','Kosovo'),
	(118,'KW','Kuwait'),
	(119,'KG','Kyrgyzstan'),
	(120,'LA','Lao People\'s Democratic Republic'),
	(121,'LV','Latvia'),
	(122,'LB','Lebanon'),
	(123,'LS','Lesotho'),
	(124,'LR','Liberia'),
	(125,'LY','Libyan Arab Jamahiriya'),
	(126,'LI','Liechtenstein'),
	(127,'LT','Lithuania'),
	(128,'LU','Luxembourg'),
	(129,'MO','Macau'),
	(130,'MK','Macedonia'),
	(131,'MG','Madagascar'),
	(132,'MW','Malawi'),
	(133,'MY','Malaysia'),
	(134,'MV','Maldives'),
	(135,'ML','Mali'),
	(136,'MT','Malta'),
	(137,'MH','Marshall Islands'),
	(138,'MQ','Martinique'),
	(139,'MR','Mauritania'),
	(140,'MU','Mauritius'),
	(141,'TY','Mayotte'),
	(142,'MX','Mexico'),
	(143,'FM','Micronesia, Federated States of'),
	(144,'MD','Moldova, Republic of'),
	(145,'MC','Monaco'),
	(146,'MN','Mongolia'),
	(147,'ME','Montenegro'),
	(148,'MS','Montserrat'),
	(149,'MA','Morocco'),
	(150,'MZ','Mozambique'),
	(151,'MM','Myanmar'),
	(152,'NA','Namibia'),
	(153,'NR','Nauru'),
	(154,'NP','Nepal'),
	(155,'NL','Netherlands'),
	(156,'AN','Netherlands Antilles'),
	(157,'NC','New Caledonia'),
	(158,'NZ','New Zealand'),
	(159,'NI','Nicaragua'),
	(160,'NE','Niger'),
	(161,'NG','Nigeria'),
	(162,'NU','Niue'),
	(163,'NF','Norfolk Island'),
	(164,'MP','Northern Mariana Islands'),
	(165,'NO','Norway'),
	(166,'OM','Oman'),
	(167,'PK','Pakistan'),
	(168,'PW','Palau'),
	(169,'PS','Palestine'),
	(170,'PA','Panama'),
	(171,'PG','Papua New Guinea'),
	(172,'PY','Paraguay'),
	(173,'PE','Peru'),
	(174,'PH','Philippines'),
	(175,'PN','Pitcairn'),
	(176,'PL','Poland'),
	(177,'PT','Portugal'),
	(178,'PR','Puerto Rico'),
	(179,'QA','Qatar'),
	(180,'RE','Reunion'),
	(181,'RO','Romania'),
	(182,'RU','Russian Federation'),
	(183,'RW','Rwanda'),
	(184,'KN','Saint Kitts and Nevis'),
	(185,'LC','Saint Lucia'),
	(186,'VC','Saint Vincent and the Grenadines'),
	(187,'WS','Samoa'),
	(188,'SM','San Marino'),
	(189,'ST','Sao Tome and Principe'),
	(190,'SA','Saudi Arabia'),
	(191,'SN','Senegal'),
	(192,'RS','Serbia'),
	(193,'SC','Seychelles'),
	(194,'SL','Sierra Leone'),
	(195,'SG','Singapore'),
	(196,'SK','Slovakia'),
	(197,'SI','Slovenia'),
	(198,'SB','Solomon Islands'),
	(199,'SO','Somalia'),
	(200,'ZA','South Africa'),
	(201,'GS','South Georgia South Sandwich Islands'),
	(202,'ES','Spain'),
	(203,'LK','Sri Lanka'),
	(204,'SH','St. Helena'),
	(205,'PM','St. Pierre and Miquelon'),
	(206,'SD','Sudan'),
	(207,'SR','Suriname'),
	(208,'SJ','Svalbard and Jan Mayen Islands'),
	(209,'SZ','Swaziland'),
	(210,'SE','Sweden'),
	(211,'CH','Switzerland'),
	(212,'SY','Syrian Arab Republic'),
	(213,'TW','Taiwan'),
	(214,'TJ','Tajikistan'),
	(215,'TZ','Tanzania, United Republic of'),
	(216,'TH','Thailand'),
	(217,'TG','Togo'),
	(218,'TK','Tokelau'),
	(219,'TO','Tonga'),
	(220,'TT','Trinidad and Tobago'),
	(221,'TN','Tunisia'),
	(222,'TR','Turkey'),
	(223,'TM','Turkmenistan'),
	(224,'TC','Turks and Caicos Islands'),
	(225,'TV','Tuvalu'),
	(226,'UG','Uganda'),
	(227,'UA','Ukraine'),
	(228,'AE','United Arab Emirates'),
	(229,'GB','United Kingdom'),
	(230,'US','United States'),
	(231,'UM','United States minor outlying islands'),
	(232,'UY','Uruguay'),
	(233,'UZ','Uzbekistan'),
	(234,'VU','Vanuatu'),
	(235,'VA','Vatican City State'),
	(236,'VE','Venezuela'),
	(237,'VN','Vietnam'),
	(238,'VG','Virgin Islands (British)'),
	(239,'VI','Virgin Islands (U.S.)'),
	(240,'WF','Wallis and Futuna Islands'),
	(241,'EH','Western Sahara'),
	(242,'YE','Yemen'),
	(243,'ZR','Zaire'),
	(244,'ZM','Zambia'),
	(245,'ZW','Zimbabwe');

/*!40000 ALTER TABLE `tbl_country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_GP
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_GP`;

CREATE TABLE `tbl_GP` (
  `GPID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `GP_name` varchar(50) DEFAULT NULL,
  `GP_address` varchar(100) DEFAULT NULL,
  `GP_address2` varchar(100) DEFAULT NULL,
  `GP_phone` varchar(20) DEFAULT NULL,
  `GP_mobile` varchar(20) DEFAULT NULL,
  `GP_fax` varchar(20) DEFAULT NULL,
  `GP_email` varchar(50) DEFAULT NULL,
  `GP_email2` varchar(50) DEFAULT NULL,
  `GP_type` tinyint(4) DEFAULT NULL,
  `GP_extra` varchar(30) DEFAULT NULL,
  `GP_instructions` varchar(100) DEFAULT NULL,
  `GP_active` tinyint(4) NOT NULL DEFAULT '1',
  `GP_date_of_active` date DEFAULT NULL,
  `GP_date_of_inactive` date DEFAULT NULL,
  `GP_registration` varchar(20) NOT NULL,
  `GP_pi_insurance` varchar(20) NOT NULL,
  `GP_pi_insurance_num` varchar(20) NOT NULL,
  `GP_comments` varchar(300) NOT NULL,
  `GP_country` int(11) DEFAULT '0',
  `GP_workphone` varchar(20) DEFAULT NULL,
  `GP_state` varchar(20) DEFAULT '0',
  `GP_city` varchar(100) DEFAULT NULL,
  `GP_postcode` int(4) DEFAULT NULL,
  `GP_responsibleperson` varchar(100) DEFAULT NULL,
  `GP_deleted` varchar(1) DEFAULT 'N',
  `GP_clinic` int(20) DEFAULT '0',
  `dateadded` varchar(255) DEFAULT NULL,
  `GP_clinic2` int(20) DEFAULT '0',
  `GP_clinic3` int(20) DEFAULT '0',
  `GP_clinic4` int(20) DEFAULT '0',
  `GP_clinic5` int(20) DEFAULT '0',
  `GP_clinic6` int(20) DEFAULT '0',
  `GP_clinic7` int(20) DEFAULT '0',
  `GP_clinic8` int(20) DEFAULT '0',
  PRIMARY KEY (`GPID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_GP` WRITE;
/*!40000 ALTER TABLE `tbl_GP` DISABLE KEYS */;

INSERT INTO `tbl_GP` (`GPID`, `siteID`, `GP_name`, `GP_address`, `GP_address2`, `GP_phone`, `GP_mobile`, `GP_fax`, `GP_email`, `GP_email2`, `GP_type`, `GP_extra`, `GP_instructions`, `GP_active`, `GP_date_of_active`, `GP_date_of_inactive`, `GP_registration`, `GP_pi_insurance`, `GP_pi_insurance_num`, `GP_comments`, `GP_country`, `GP_workphone`, `GP_state`, `GP_city`, `GP_postcode`, `GP_responsibleperson`, `GP_deleted`, `GP_clinic`, `dateadded`, `GP_clinic2`, `GP_clinic3`, `GP_clinic4`, `GP_clinic5`, `GP_clinic6`, `GP_clinic7`, `GP_clinic8`)
VALUES
	(7,NULL,'Dr Granek Abe','Southland Medical Centre 50 Chesterville Road  ',NULL,'(03) 9584 9504','','(03) 9583 7403','','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'','1','Cheltenham',3192,'','N',0,NULL,0,0,0,0,0,0,0),
	(11,NULL,'Dr Aboud','46 Gap Rd,',NULL,'9740 4429','','9740 8827','','',2,NULL,NULL,1,NULL,NULL,'','','','',0,'','5','Sunbury',0,'','N',0,NULL,0,0,0,0,0,0,0),
	(9,NULL,'Dr Sami Abed','Charles Street',NULL,'5871 1433','','5871 1517','','',0,NULL,NULL,1,NULL,NULL,'','','','',0,'','5','Cobram',0,'','N',1,NULL,0,0,0,0,0,0,0),
	(10,1,'Dr Abou-Seif','63 Hogans Road','test','(03) 9749 6777','000','00000','Test email 1','Test email 2',0,NULL,'<p>Test Instructions</p>',1,NULL,NULL,'','insurance 1','insurance 2','<p>Test Comments</p>\r\n<p>&nbsp;</p>',13,'0000','5','Hoppers Crossing',3029,'Person Responsible','N',1,NULL,2,0,0,0,0,0,0),
	(12,NULL,'Dr Keith Abraham','Gardens Medical Centre, Level 3, 470 Wodonga Place',NULL,'02 6021 3555','','','kazzi@thegardensmedical.com.au','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'','1','ALBURY',2640,'','N',0,NULL,0,0,0,0,0,0,0),
	(13,NULL,'Dr Atalla Abraham','Bulleen Plaza, 103 Manningham Road',NULL,'9852 2234','','','','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'','5','Bulleen',3105,'','N',0,NULL,0,0,0,0,0,0,0),
	(14,NULL,'Dr Jim Abrahams','189 Ashmore Rd',NULL,' (07) 5527 8880','','','','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'','2','BENOWA',4217,'','N',0,NULL,0,0,0,0,0,0,0),
	(15,NULL,'Dr P Acharya','72 Maude Streete',NULL,'03 58212355','','','','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'03 58312556','5','Shepparton',3630,'','N',0,NULL,0,0,0,0,0,0,0),
	(16,NULL,'Dr Penny Adams','393 Miltary Rd',NULL,'02 99600655','','','','',1,NULL,NULL,1,NULL,NULL,'','','','',0,'','1','Mosman',2088,'','N',0,NULL,0,0,0,0,0,0,0);

/*!40000 ALTER TABLE `tbl_GP` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_GP_clinic
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_GP_clinic`;

CREATE TABLE `tbl_GP_clinic` (
  `clinicID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `clinic_name` varchar(150) DEFAULT NULL,
  `clinic_responsibleperson` varchar(100) DEFAULT NULL,
  `clinic_address` varchar(100) DEFAULT NULL,
  `clinic_city` varchar(100) DEFAULT NULL,
  `clinic_state` varchar(100) DEFAULT NULL,
  `clinic_postcode` int(4) DEFAULT NULL,
  `clinic_country` varchar(100) DEFAULT NULL,
  `clinic_phone` varchar(30) DEFAULT NULL,
  `clinic_mobile` varchar(30) DEFAULT NULL,
  `clinic_fax` varchar(30) DEFAULT NULL,
  `clinic_email` varchar(50) DEFAULT NULL,
  `clinic_email2` varchar(50) DEFAULT NULL,
  `clinic_instructions` mediumtext,
  `clinic_notes` mediumtext,
  `clinic_active` varchar(1) DEFAULT NULL,
  `clinic_date_inactive` varchar(50) DEFAULT NULL,
  `clinic_date_active` varchar(50) DEFAULT NULL,
  `clinic_phone2` varchar(30) DEFAULT NULL,
  `clinic_deleted` varchar(1) DEFAULT 'N',
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clinicID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_GP_clinic` WRITE;
/*!40000 ALTER TABLE `tbl_GP_clinic` DISABLE KEYS */;

INSERT INTO `tbl_GP_clinic` (`clinicID`, `siteID`, `clinic_name`, `clinic_responsibleperson`, `clinic_address`, `clinic_city`, `clinic_state`, `clinic_postcode`, `clinic_country`, `clinic_phone`, `clinic_mobile`, `clinic_fax`, `clinic_email`, `clinic_email2`, `clinic_instructions`, `clinic_notes`, `clinic_active`, `clinic_date_inactive`, `clinic_date_active`, `clinic_phone2`, `clinic_deleted`, `dateadded`)
VALUES
	(1,2,'Borer Ernser and Koch ','Lempi Lubowitz ','92335 Mills Lane Suite 584 ','Murazikburgh ','1',7698,'13','(848)062-4484 ','652.005.5129 ','(848)062-4484 ','nmorissette@example.org ','kaden97@example.org ','Gryphon. \'I\'ve forgotten the Duchess to play croquet.\' The Frog-Footman repeated in the back. However it was growing and very angrily. \'A knot!\' said Alice \'it\'s very interesting. I never. ','Gryphon. \'I\'ve forgotten the Duchess to play croquet.\' The Frog-Footman repeated in the back. However it was growing and very angrily. \'A knot!\' said Alice \'it\'s very interesting. I never. ','1','1481762226','1481762226','(848)062-4484 ','N','1481762226'),
	(2,2,'Willms Kub and Kihn ','Citlalli Dibbert ','557 Lockman Ranch Apt. 494 ','Port Lutherport ','2',3852,'13','309-946-1954x599 ','+20(6)0306866793 ','309-946-1954x599 ','lparker@example.net ','trempel@example.org ','Will you won\'t you join the dance. So they began solemnly dancing round and look up in such a thing before and behind them a new kind of sob \'I\'ve tried every way and the White Rabbit read out. ','Will you won\'t you join the dance. So they began solemnly dancing round and look up in such a thing before and behind them a new kind of sob \'I\'ve tried every way and the White Rabbit read out. ','1','1481762332','1481762332','309-946-1954x599 ','N','1481762332'),
	(3,2,'Lind-Ankunding ','Gregory Beatty ','1989 Cheyenne Path ','New Johnathonside ','3',8311,'13','(615)414-6848x89159 ','1473877485','(615)414-6848x89159 ','roxanne.klocko@example.com ','maya.beahan@example.com ','Alice remarked. \'Oh you can\'t swim can you?\' he added turning to Alice with one finger for the hot day made her so savage when they met in the last few minutes and began picking them up again as. ','Alice remarked. \'Oh you can\'t swim can you?\' he added turning to Alice with one finger for the hot day made her so savage when they met in the last few minutes and began picking them up again as. ','1','1481763443','1481763443','(615)414-6848x89159 ','N','1481763443'),
	(4,2,'Herzog-Predovic ','Andreane Grant ','7718 Richard Track Suite 225 ','Stromanburgh ','4',8884,'13','(058)584-1359x26479 ','(910)363-7285x95214 ','(058)584-1359x26479 ','sheila78@example.org ','tsawayn@example.com ','I won\'t then!--Bill\'s to go with the words came very queer to ME.\' \'You!\' said the Mock Turtle said: \'I\'m too stiff. And the Eaglet bent down its head impatiently and walked off; the Dormouse. ','I won\'t then!--Bill\'s to go with the words came very queer to ME.\' \'You!\' said the Mock Turtle said: \'I\'m too stiff. And the Eaglet bent down its head impatiently and walked off; the Dormouse. ','1','1481762226','1481762226','(058)584-1359x26479 ','N','1481762226'),
	(5,3,'Ratke-Sporer ','Kayley Lebsack ','41082 Gaylord Trafficway ','Port Deltaton ','5',8772,'13','(268)536-7727x4537 ','(338)337-0009 ','(268)536-7727x4537 ','filomena26@example.org ','zking@example.com ','But do cats eat bats?\' and sometimes \'Do bats eat cats?\' for you see Miss we\'re doing our best afore she comes to--\' At this moment Alice felt that she had never had fits my dear YOU must. ','But do cats eat bats?\' and sometimes \'Do bats eat cats?\' for you see Miss we\'re doing our best afore she comes to--\' At this moment Alice felt that she had never had fits my dear YOU must. ','1','1481762332','1481762332','(268)536-7727x4537 ','N','1481762332'),
	(6,3,'Heaney Ltd ','Dalton Farrell','293 Turner Knolls Suite 251 ','North Hardy ','1',7298,'13','1-332-057-6891 ','1-529-042-7855 ','1-332-057-6891 ','uschuppe@example.com ','clint.reichert@example.net ','White Rabbit trotting slowly back again and she tried to beat them off and had to stoop to save her neck would bend about easily in any direction like a wild beast screamed \'Off with his head!\'. ','White Rabbit trotting slowly back again and she tried to beat them off and had to stoop to save her neck would bend about easily in any direction like a wild beast screamed \'Off with his head!\'. ','1','1481763443','1481763443','1-332-057-6891 ','N','1481763443'),
	(7,3,'Murphy Group ','Avis Parisian ','1355 Ziemann Ranch ','Wintheiserton ','2',7568,'13','437-326-3548 ','5133289643','437-326-3548 ','erdman.ernest@example.com ','bhackett@example.com ','Pigeon in a low trembling voice. \'There\'s more evidence to come out among the trees a little startled when she looked back once or twice she had put the hookah into its eyes by this time and was. ','Pigeon in a low trembling voice. \'There\'s more evidence to come out among the trees a little startled when she looked back once or twice she had put the hookah into its eyes by this time and was. ','1','1481762226','1481762226','437-326-3548 ','N','1481762226'),
	(8,3,'Will Group ','Dariana Emmerich','701 Hal Mews Apt. 650 ','Krajcikview ','3',8392,'13','697-813-4224x911 ','(212)056-2264x93891 ','697-813-4224x911 ','garrick.rogahn@example.com ','madisen01@example.com ','\"Alice. \'Stand up and said without even waiting to put the hookah out of court! Suppress him! Pinch him! Off with his head!\"\"\' \'How dreadfully savage!\' exclaimed Alice. \'That\'s the reason so many. \"','\"Alice. \'Stand up and said without even waiting to put the hookah out of court! Suppress him! Pinch him! Off with his head!\"\"\' \'How dreadfully savage!\' exclaimed Alice. \'That\'s the reason so many. \"','1','1481762332','1481762332','697-813-4224x911 ','N','1481762332'),
	(9,4,'Langosh Williamson and Dickens ','Darlene Jacobs ','561 Vandervort Meadows ','West Misael ','4',5702,'13','990-044-6745x273 ','464.165.0187x7355 ','990-044-6745x273 ','jazmyne.bauch@example.org ','rolfson.rodger@example.net ','\"Dodo could not remember ever having heard of uglifying!\' it exclaimed. \'You know what \"\"it\"\" means well enough when I got up this morning? I almost wish I\'d gone to see if she had never done such a. \"','\"Dodo could not remember ever having heard of uglifying!\' it exclaimed. \'You know what \"\"it\"\" means well enough when I got up this morning? I almost wish I\'d gone to see if she had never done such a. \"','1','1481763443','1481763443','990-044-6745x273 ','N','1481763443'),
	(10,4,'Kuhic-Armstrong ','Ferne Howe ','13428 Haylee Valley ','New Mandyborough ','5',5591,'13','1-752-079-2765 ','4028374554','1-752-079-2765 ','jrenner@example.com ','cdietrich@example.org ','Cheshire cat\' said the Queen pointing to the Gryphon. \'Of course\' the Gryphon added \'Come let\'s hear some of the gloves and was gone in a moment: she looked up and began talking to him\' said. ','Cheshire cat\' said the Queen pointing to the Gryphon. \'Of course\' the Gryphon added \'Come let\'s hear some of the gloves and was gone in a moment: she looked up and began talking to him\' said. ','1','1481762226','1481762226','1-752-079-2765 ','N','1481762226'),
	(11,4,'Bartell-West ','Lester Langworth ','75318 Crooks Fort ','Lacyfort ','1',6326,'13','(691)522-1429x185 ','377-676-8547 ','(691)522-1429x185 ','elwin.stehr@example.net ','makayla55@example.net ','Knave \'I didn\'t mean it!\' pleaded poor Alice. \'But you\'re so easily offended!\' \'You\'ll get used to it in large letters. It was so large in the distance screaming with passion. She had already. ','Knave \'I didn\'t mean it!\' pleaded poor Alice. \'But you\'re so easily offended!\' \'You\'ll get used to it in large letters. It was so large in the distance screaming with passion. She had already. ','1','1481762332','1481762332','(691)522-1429x185 ','N','1481762332'),
	(12,2,'O\'Reilly Inc ','Gerry Lindgren ','07873 Baumbach Point Suite 302 ','South Vickyborough ','2',3679,'13','285.027.7326 ','153.400.3732x8205 ','285.027.7326 ','jerrod.gleichner@example.net ','wuckert.rhett@example.org ','I\'ve often seen them so often you know.\' \'I don\'t know what to uglify is you see Miss this here ought to tell its age there was Mystery\' the Mock Turtle. \'No no! The adventures first\' said. ','I\'ve often seen them so often you know.\' \'I don\'t know what to uglify is you see Miss this here ought to tell its age there was Mystery\' the Mock Turtle. \'No no! The adventures first\' said. ','1','1481763443','1481763443','285.027.7326 ','N','1481763443'),
	(13,2,'Torp Roob and Franecki ','Tremayne Steuber ','2826 Renner Gardens Suite 083 ','South Santiago ','3',3669,'13','212.819.8362x585 ','338-773-8158x450 ','212.819.8362x585 ','trisha21@example.org ','tianna.bauch@example.com ','\"Alice did not feel encouraged to ask the question?\' said the Queen in front of the jurymen. \'No they\'re not\' said the Hatter. \'He won\'t stand beating. Now if you please! \"\"William the Conqueror. \"','\"Alice did not feel encouraged to ask the question?\' said the Queen in front of the jurymen. \'No they\'re not\' said the Hatter. \'He won\'t stand beating. Now if you please! \"\"William the Conqueror. \"','1','1481762226','1481762226','212.819.8362x585 ','N','1481762226'),
	(14,3,'Hermann Ltd ','Sadie Kreiger ','5364 Wyman Stream Apt. 117 ','Gislasonhaven ','4',3897,'13','1-017-267-7121 ','192-488-7920x05585 ','1-017-267-7121 ','dhowe@example.org ','dariana.krajcik@example.net ','Oh dear! I wish you wouldn\'t squeeze so.\' said the Duchess who seemed too much overcome to do it?\' \'In my youth\' said his father \'I took to the company generally \'You are old\' said the Rabbit. ','Oh dear! I wish you wouldn\'t squeeze so.\' said the Duchess who seemed too much overcome to do it?\' \'In my youth\' said his father \'I took to the company generally \'You are old\' said the Rabbit. ','1','1481762332','1481762332','1-017-267-7121 ','N','1481762332'),
	(15,3,'Dach Wilkinson and Rice ','Eden Reichert ','4500 Patricia Parks ','South Tamara ','5',8255,'13','8783240928','3564801405','8783240928','ewell05@example.net ','dena.ward@example.org ','I to get rather sleepy and went on just as well go back and see that she had never forgotten that if you don\'t know the way to fly up into hers--she could hear the very middle of her sharp little. ','I to get rather sleepy and went on just as well go back and see that she had never forgotten that if you don\'t know the way to fly up into hers--she could hear the very middle of her sharp little. ','1','1481763443','1481763443','8783240928','N','1481763443'),
	(16,4,'Hilpert Block and Raynor ','Ulices VonRueden ','5292 Jazmyn Path ','Port Rupertstad ','1',8404,'13','8907184475','692.344.3608 ','8907184475','kaci.brekke@example.org ','hilario.jacobs@example.net ','King looking round the table half hoping that the Mouse only growled in reply. \'Idiot!\' said the Cat. \'I don\'t know where Dinn may be\' said the Caterpillar. Alice folded her hands and she jumped. ','King looking round the table half hoping that the Mouse only growled in reply. \'Idiot!\' said the Cat. \'I don\'t know where Dinn may be\' said the Caterpillar. Alice folded her hands and she jumped. ','1','1481762226','1481762226','8907184475','N','1481762226'),
	(17,4,'Oberbrunner Inc ','Mitchell Dickens ','05492 Jacobs Underpass Suite 296 ','New Annalise ','2',7970,'13','1-080-110-4795 ','171.561.6116x764 ','1-080-110-4795 ','maynard71@example.com ','daisha26@example.net ','And Alice was too slippery; and when she found herself in a bit.\' \'Perhaps it doesn\'t understand English\' thought Alice; \'I must be kind to them\' thought Alice \'and if it had been it suddenly. ','And Alice was too slippery; and when she found herself in a bit.\' \'Perhaps it doesn\'t understand English\' thought Alice; \'I must be kind to them\' thought Alice \'and if it had been it suddenly. ','1','1481762332','1481762332','1-080-110-4795 ','N','1481762332'),
	(18,2,'Effertz Jones and Hansen ','Otha Quitzon','9711 Melyssa Pike ','Ivahstad ','3',1814,'13','(801)821-9368x3500 ','+21(8)9782122773 ','(801)821-9368x3500 ','destin.howe@example.org ','irolfson@example.net ','\"Allow me to introduce some other subject of conversation. While she was exactly the right distance--but then I wonder what you\'re at!\"\" You know the song she kept fanning herself all the rest. \"','\"Allow me to introduce some other subject of conversation. While she was exactly the right distance--but then I wonder what you\'re at!\"\" You know the song she kept fanning herself all the rest. \"','1','1481763443','1481763443','(801)821-9368x3500 ','N','1481763443'),
	(19,2,'DuBuque-Hintz ','Kristoffer Koch','3551 Monahan Stravenue ','Wavaside ','4',7323,'13','384.647.7790 ','494-134-9098 ','384.647.7790 ','shawna30@example.com ','emery.bauch@example.com ','They had a little bit and said to herself \'to be going messages for a few minutes she heard a little anxiously. \'Yes\' said Alice as she had plenty of time as she could. \'No\' said Alice. \'Why. ','They had a little bit and said to herself \'to be going messages for a few minutes she heard a little anxiously. \'Yes\' said Alice as she had plenty of time as she could. \'No\' said Alice. \'Why. ','1','1481762226','1481762226','384.647.7790 ','N','1481762226'),
	(20,2,'Nolan Ltd ','Jed Bartell ','7434 Ritchie Brook Apt. 962 ','Daughertyhaven ','5',1344,'13','406.210.6714x71631 ','(359)219-2656 ','406.210.6714x71631 ','jose23@example.com ','kshlerin.ole@example.net ','Alice. \'That\'s very curious.\' \'It\'s all his fancy that: he hasn\'t got no business of MINE.\' The Queen had ordered. They very soon finished it off. * * * CHAPTER II. The Pool of Tears \'Curiouser and. ','Alice. \'That\'s very curious.\' \'It\'s all his fancy that: he hasn\'t got no business of MINE.\' The Queen had ordered. They very soon finished it off. * * * CHAPTER II. The Pool of Tears \'Curiouser and. ','1','1481762332','1481762332','406.210.6714x71631 ','N','1481762332');

/*!40000 ALTER TABLE `tbl_GP_clinic` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_insurance
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_insurance`;

CREATE TABLE `tbl_insurance` (
  `insID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `ins_name` varchar(60) DEFAULT NULL,
  `ins_responsibleperson` varchar(30) DEFAULT NULL,
  `ins_address` varchar(50) DEFAULT NULL,
  `ins_address2` varchar(50) DEFAULT NULL,
  `ins_city` varchar(20) DEFAULT NULL,
  `ins_state` varchar(10) DEFAULT NULL,
  `ins_postcode` varchar(4) DEFAULT NULL,
  `ins_country` int(11) DEFAULT '0',
  `ins_phone` varchar(20) DEFAULT NULL,
  `ins_fax` varchar(20) DEFAULT NULL,
  `ins_email` varchar(50) DEFAULT NULL,
  `ins_email2` varchar(50) DEFAULT NULL,
  `ins_comments` varchar(255) DEFAULT NULL,
  `ins_deleted` varchar(1) DEFAULT 'N',
  `ins_notes` mediumtext,
  `dateadded` varchar(255) DEFAULT NULL,
  `ins_ins4results` mediumtext,
  `ins_contactname` varchar(50) DEFAULT NULL,
  `ins_contactemail` varchar(50) DEFAULT NULL,
  `ins_contactphone` varchar(50) DEFAULT NULL,
  `ins_underwritingcontract` mediumtext,
  PRIMARY KEY (`insID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_insurance` WRITE;
/*!40000 ALTER TABLE `tbl_insurance` DISABLE KEYS */;

INSERT INTO `tbl_insurance` (`insID`, `siteID`, `ins_name`, `ins_responsibleperson`, `ins_address`, `ins_address2`, `ins_city`, `ins_state`, `ins_postcode`, `ins_country`, `ins_phone`, `ins_fax`, `ins_email`, `ins_email2`, `ins_comments`, `ins_deleted`, `ins_notes`, `dateadded`, `ins_ins4results`, `ins_contactname`, `ins_contactemail`, `ins_contactphone`, `ins_underwritingcontract`)
VALUES
	(1,1,'Test Insurance Test 2','Test responsible person','test address','test address 2','test city','1','4000',13,'5555555','','test email','test email2',NULL,'Y','',NULL,NULL,NULL,NULL,NULL,NULL),
	(4,1,'Test Insurance','Test responsible person','test address','test address 2','test city','1','4000',1,'5555555','','test email','test email2',NULL,'N','',NULL,'','','','','');

/*!40000 ALTER TABLE `tbl_insurance` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_nurse
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_nurse`;

CREATE TABLE `tbl_nurse` (
  `nurseID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `nurse_type` tinyint(4) DEFAULT '0',
  `nurse_extra` varchar(30) DEFAULT NULL,
  `nurse_regnumber` varchar(30) DEFAULT NULL,
  `nurse_instructions` varchar(100) DEFAULT NULL,
  `nurse_active` tinyint(4) NOT NULL DEFAULT '1',
  `nurse_date_of_active` varchar(50) DEFAULT NULL,
  `nurse_date_of_inactive` varchar(50) DEFAULT NULL,
  `nurse_registration` varchar(20) NOT NULL,
  `nurse_pi_insurance` varchar(20) NOT NULL,
  `nurse_pi_insurance_num` varchar(20) NOT NULL,
  `nurse_comments` varchar(300) NOT NULL,
  `nurse_responsibleperson` varchar(30) DEFAULT NULL,
  `nurse_fax` varchar(20) DEFAULT NULL,
  `nurse_mobile` varchar(20) DEFAULT NULL,
  `nurse_phone` varchar(20) DEFAULT NULL,
  `nurse_postcode` varchar(4) DEFAULT NULL,
  `nurse_state` varchar(10) DEFAULT '0',
  `nurse_city` varchar(20) DEFAULT NULL,
  `nurse_address` varchar(50) DEFAULT NULL,
  `nurse_address2` varchar(50) DEFAULT NULL,
  `nurse_name` varchar(30) NOT NULL DEFAULT '',
  `nurse_email` varchar(50) DEFAULT NULL,
  `nurse_email2` varchar(50) DEFAULT NULL,
  `nurse_service` varchar(50) DEFAULT NULL,
  `nurse_service_company` varchar(100) DEFAULT NULL,
  `nurse_country` varchar(10) DEFAULT '0',
  `nurse_deleted` varchar(1) DEFAULT 'N',
  `dateadded` varchar(255) DEFAULT NULL,
  `nurse_availcomments` varchar(300) NOT NULL DEFAULT '',
  PRIMARY KEY (`nurseID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_nurse` WRITE;
/*!40000 ALTER TABLE `tbl_nurse` DISABLE KEYS */;

INSERT INTO `tbl_nurse` (`nurseID`, `siteID`, `nurse_type`, `nurse_extra`, `nurse_regnumber`, `nurse_instructions`, `nurse_active`, `nurse_date_of_active`, `nurse_date_of_inactive`, `nurse_registration`, `nurse_pi_insurance`, `nurse_pi_insurance_num`, `nurse_comments`, `nurse_responsibleperson`, `nurse_fax`, `nurse_mobile`, `nurse_phone`, `nurse_postcode`, `nurse_state`, `nurse_city`, `nurse_address`, `nurse_address2`, `nurse_name`, `nurse_email`, `nurse_email2`, `nurse_service`, `nurse_service_company`, `nurse_country`, `nurse_deleted`, `dateadded`, `nurse_availcomments`)
VALUES
	(1,1,0,NULL,NULL,NULL,1,'25-10-2016','31-10-2016','','','','<p>test</p>','','','0402 295 765','','4655','2','Hervey Bay','14 Renee CourtTorquay QLD 4655 ','','Tracey Aherne','','','NO LONGER WORKS FOR US',NULL,'0','N',NULL,''),
	(3,NULL,0,NULL,NULL,NULL,1,'00-00-0000','00-00-0000','','','','<p>&nbsp;PO Box 1935 Armidale NSW &nbsp;</p>','','','0439 471 977','02 6775 1334','2350','1','ARMIDALE','132 Handel St',NULL,'Tanya Alcorn','tanya@alcorn2350.com.au','talcorn@une.edu.au ','',NULL,'0','Y',NULL,''),
	(6,1,0,NULL,NULL,NULL,0,'21-10-2016','21-10-2016','','','','<p>Test Comments</p>','Person Responsible','(02) 9686 9277','0418 453 650','(02) 9639 1246','NLWF','1','SYDNEY','9 Hera Place, Winston Hills','test address line 2','Evan Alexandrou','donnaevan@optusnet.com.au','Nurse email 2','Pathology Service',NULL,'0','N',NULL,''),
	(7,1,0,NULL,NULL,NULL,0,'00-00-0000','00-00-0000','','Nurse Personal Insur','Insurance Number','<p>Test Comments</p>','Person Responsible','(02) 9686 9277','0402 295 765','02 6775 1334','0000','5','Test City','Address Line 1','Address Line 2','Test Name','tanya@alcorn2350.com.au','talcorn@une.edu.au ',NULL,NULL,'13','Y',NULL,''),
	(8,1,0,NULL,NULL,NULL,0,'00-00-0000','00-00-0000','','Nurse Personal Insur','Insurance Number','<p>Test Comments</p>','Person Responsible','(02) 9686 9277','0402 295 765','02 6775 1334','0000','5','Test City','Address Line 1','Address Line 2','Test Name','tanya@alcorn2350.com.au','talcorn@une.edu.au ',NULL,NULL,'13','Y',NULL,''),
	(9,1,0,NULL,NULL,NULL,0,'00-00-0000','00-00-0000','','Nurse Personal Insur','Insurance Number','<p>Test Comments</p>','Person Responsible','(02) 9686 9277','0402 295 765','02 6775 1334','0000','5','Test City','Address Line 1','Address Line 2','Test Name','tanya@alcorn2350.com.au','talcorn@une.edu.au ',NULL,NULL,'13','N',NULL,'');

/*!40000 ALTER TABLE `tbl_nurse` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_pathologist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_pathologist`;

CREATE TABLE `tbl_pathologist` (
  `pathID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `path_instructions` varchar(100) DEFAULT NULL,
  `path_responsibleperson` varchar(30) DEFAULT NULL,
  `path_email` varchar(50) DEFAULT NULL,
  `path_fax` varchar(20) DEFAULT NULL,
  `path_mobile` varchar(20) DEFAULT NULL,
  `path_phone` varchar(20) DEFAULT NULL,
  `path_postcode` varchar(4) DEFAULT NULL,
  `path_state` varchar(10) DEFAULT NULL,
  `path_city` varchar(20) DEFAULT NULL,
  `path_address` varchar(50) DEFAULT NULL,
  `path_company` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `path_service` varchar(20) DEFAULT NULL,
  `path_name` varchar(50) DEFAULT NULL,
  `path_address2` varchar(50) DEFAULT NULL,
  `path_country` varchar(10) DEFAULT '0',
  `path_deleted` varchar(1) DEFAULT 'N',
  `path_accnumber` varchar(20) DEFAULT NULL,
  `path_contactname` varchar(50) DEFAULT NULL,
  `path_contactemail` varchar(50) DEFAULT NULL,
  `path_contactphone` varchar(50) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pathID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_pathologist` WRITE;
/*!40000 ALTER TABLE `tbl_pathologist` DISABLE KEYS */;

INSERT INTO `tbl_pathologist` (`pathID`, `siteID`, `path_instructions`, `path_responsibleperson`, `path_email`, `path_fax`, `path_mobile`, `path_phone`, `path_postcode`, `path_state`, `path_city`, `path_address`, `path_company`, `path_service`, `path_name`, `path_address2`, `path_country`, `path_deleted`, `path_accnumber`, `path_contactname`, `path_contactemail`, `path_contactphone`, `dateadded`)
VALUES
	(1,1,'','Person Responsible','email Address ','0000','000','02 6244 2816','2605','1','Garran','Building 10/ Canberra HospGilmore Cres',X'74657374','Pathology','ACT Pathology','Address Line 2','0','N',NULL,NULL,NULL,NULL,NULL),
	(5,1,'<p>Instructions</p>','Person Responsible','test email','(08) 8443 3146  ','000','(08) 8159 7900  ','','1','Mile End','Lvl1/46 Sir Donald Bradman Drv',X'5365727669636520436F6D70616E79','Test pathology Servi','Abbotts Pathology','Address Line 2','13','N',NULL,NULL,NULL,NULL,NULL),
	(6,1,NULL,'Person Responsible','email','00000','0000','000','0000','1','City','line 1',X'5465737420436F6D70616E79','Test Service','Test Name','line 2','15','N',NULL,NULL,NULL,NULL,NULL),
	(8,1,NULL,'Person Responsible','email','3','2','1','0000','5','City','Line 1',X'436F6D70616E79','Service','Name','Line 2','13','N',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_pathologist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_registration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_registration`;

CREATE TABLE `tbl_registration` (
  `regID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `advisorID` int(11) NOT NULL DEFAULT '0',
  `clientID` int(11) NOT NULL DEFAULT '0',
  `reg_company` varchar(50) DEFAULT NULL,
  `reg_clientrefno` varchar(20) DEFAULT NULL,
  `reg_placeaddress` varchar(200) DEFAULT NULL,
  `reg_phone` varchar(20) DEFAULT NULL,
  `reg_appointtime` varchar(200) DEFAULT NULL,
  `reg_appointdate` varchar(200) DEFAULT NULL,
  `reg_appointplace` varchar(100) DEFAULT NULL,
  `reg_pref1` varchar(20) DEFAULT NULL,
  `reg_pref2` varchar(20) DEFAULT NULL,
  `reg_pref3` varchar(20) DEFAULT NULL,
  `reg_pref4` varchar(20) DEFAULT NULL,
  `reg_services` varchar(100) DEFAULT NULL,
  `reg_othertests` text,
  `reg_timestamp` varchar(200) DEFAULT NULL,
  `reg_companyaddress` varchar(50) DEFAULT NULL,
  `reg_companyfax` varchar(50) DEFAULT NULL,
  `reg_clientcontact` varchar(50) DEFAULT NULL,
  `reg_creation` varchar(200) DEFAULT NULL,
  `reg_assigned_to` varchar(100) DEFAULT NULL,
  `reg_questionnaire` varchar(200) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`regID`),
  KEY `advisorID` (`advisorID`,`clientID`),
  KEY `clientID` (`clientID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tbl_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_service`;

CREATE TABLE `tbl_service` (
  `serID` int(11) NOT NULL DEFAULT '0',
  `siteID` int(11) DEFAULT NULL,
  `ser_service` varchar(100) DEFAULT NULL,
  `ser_ordering` int(11) DEFAULT '0',
  `ser_options` varchar(50) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`serID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table tbl_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sites`;

CREATE TABLE `tbl_sites` (
  `siteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_name` varchar(100) DEFAULT NULL,
  `site_domain` varchar(255) DEFAULT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `site_maincolour` varchar(255) DEFAULT NULL,
  `site_maincolour_text` varchar(255) DEFAULT NULL,
  `site_seccolour` varchar(255) DEFAULT NULL,
  `site_seccolour_text` varchar(255) DEFAULT NULL,
  `site_thrcolour` varchar(255) DEFAULT NULL,
  `site_thrcolour_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`siteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sites` WRITE;
/*!40000 ALTER TABLE `tbl_sites` DISABLE KEYS */;

INSERT INTO `tbl_sites` (`siteID`, `site_name`, `site_domain`, `site_logo`, `site_maincolour`, `site_maincolour_text`, `site_seccolour`, `site_seccolour_text`, `site_thrcolour`, `site_thrcolour_text`)
VALUES
	(1,'Health Predictions','healthpredictions-com-au.swim.net.au','health-predictions.png','8bbef4','ffffff','1d6ab1','ffffff','17447e','ffffff'),
	(2,'Bombora Medical','bombora-com-au.swim.net.au','sample-logo.jpg','41d93d','ffffff','1b6119','ffffff','70946f','ffffff'),
	(6,'M3lifewise','m3life-com-au.swim.net.au','sample-logo.jpg','41d93d','ffffff','1b6123','ffffff','70946f','ffffff'),
	(8,'Synchron','synchron-com-au.swim.net.au','sample-logo.jpg','41d93d','ffffff','1b6125','ffffff','70946f','ffffff'),
	(9,'Centrepoint Health Screen','chs-com-au.swim.net.au','sample-logo.jpg','8bbef4','ffffff','1d6ab1','ffffff','17447e','ffffff');

/*!40000 ALTER TABLE `tbl_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_specialist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_specialist`;

CREATE TABLE `tbl_specialist` (
  `specID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `spec_name` varchar(30) NOT NULL DEFAULT '',
  `spec_address` varchar(50) DEFAULT NULL,
  `spec_city` varchar(20) DEFAULT NULL,
  `spec_state` varchar(10) DEFAULT NULL,
  `spec_postcode` varchar(4) DEFAULT NULL,
  `spec_profession` varchar(20) DEFAULT NULL,
  `spec_phone` varchar(20) DEFAULT NULL,
  `spec_mobile` varchar(20) DEFAULT NULL,
  `spec_fax` varchar(20) DEFAULT NULL,
  `spec_email` varchar(50) DEFAULT NULL,
  `spec_email2` varchar(50) DEFAULT NULL,
  `spec_responsibleperson` varchar(30) DEFAULT NULL,
  `spec_country` varchar(50) DEFAULT NULL,
  `spec_deleted` varchar(1) DEFAULT 'N',
  `spec_active` tinyint(4) DEFAULT NULL,
  `spec_date_of_active` varchar(50) DEFAULT NULL,
  `spec_date_of_inactive` varchar(50) DEFAULT NULL,
  `spec_address2` varchar(50) DEFAULT NULL,
  `spec_instructions` mediumtext,
  `spec_clinic` int(20) DEFAULT NULL,
  `spec_type` int(1) DEFAULT '1',
  `spec_notes` mediumtext,
  `dateadded` varchar(255) DEFAULT NULL,
  `spec_clinic2` int(20) DEFAULT NULL,
  `spec_clinic3` int(20) DEFAULT NULL,
  `spec_clinic4` int(20) DEFAULT NULL,
  `spec_clinic5` int(20) DEFAULT NULL,
  `spec_clinic6` int(20) DEFAULT NULL,
  `spec_clinic7` int(20) DEFAULT NULL,
  `spec_clinic8` int(20) DEFAULT NULL,
  PRIMARY KEY (`specID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_specialist` WRITE;
/*!40000 ALTER TABLE `tbl_specialist` DISABLE KEYS */;

INSERT INTO `tbl_specialist` (`specID`, `siteID`, `spec_name`, `spec_address`, `spec_city`, `spec_state`, `spec_postcode`, `spec_profession`, `spec_phone`, `spec_mobile`, `spec_fax`, `spec_email`, `spec_email2`, `spec_responsibleperson`, `spec_country`, `spec_deleted`, `spec_active`, `spec_date_of_active`, `spec_date_of_inactive`, `spec_address2`, `spec_instructions`, `spec_clinic`, `spec_type`, `spec_notes`, `dateadded`, `spec_clinic2`, `spec_clinic3`, `spec_clinic4`, `spec_clinic5`, `spec_clinic6`, `spec_clinic7`, `spec_clinic8`)
VALUES
	(1,1,'Dr Walter Abhayaratna','Suite 5, National Capital Private Hospital, Cnr Gi','GARRAN','0','2605','Cardiologist','(02) 6222 6614','','(02) 6222 6660','DO NOT USE!','','DO NOT USE! Expensive','13','N',1,'07-11-2016','07-11-2016','','<p>DO NOT USE! Expensive</p>',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,1,'Dr Adera','Specialist Medical Centre 6-8 Sydney St, Gateshea','Newcastle','1','2290','Stress ECG, Stress E','02 4943 7877','','','','','','0','N',1,'','','','',1,1,'',NULL,2,0,0,0,0,0,0),
	(4,1,'Dr Mohammed Alam','Suite 3, 2900 Logan Road Underwood','','2','4119','Skin Cancer Speciali','07 3341 7033','','','','','','13','N',1,'08-11-2016','08-11-2016','Brisbane','',0,1,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,1,'Dr Mohammed Alam','Suite 3, 2900 Logan Road Underwood','','2','4119','Skin Cancer Speciali','07 3341 7033','','','',NULL,'','13','Y',0,'','','Brisbane','',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,1,'Dr Mohammed Alam','Suite 3, 2900 Logan Road Underwood','','2','4119','Skin Cancer Speciali','07 3341 7033','','','',NULL,'','13','Y',0,'07-11-2016','07-11-2016','Brisbane','',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_specialist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_specialist_clinic
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_specialist_clinic`;

CREATE TABLE `tbl_specialist_clinic` (
  `clinicID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(20) DEFAULT NULL,
  `clinic_name` varchar(150) DEFAULT NULL,
  `clinic_responsibleperson` varchar(100) DEFAULT NULL,
  `clinic_address` varchar(100) DEFAULT NULL,
  `clinic_city` varchar(100) DEFAULT NULL,
  `clinic_state` varchar(100) DEFAULT NULL,
  `clinic_postcode` int(4) DEFAULT NULL,
  `clinic_country` varchar(100) DEFAULT NULL,
  `clinic_phone` varchar(30) DEFAULT NULL,
  `clinic_mobile` varchar(30) DEFAULT NULL,
  `clinic_fax` varchar(30) DEFAULT NULL,
  `clinic_email` varchar(50) DEFAULT NULL,
  `clinic_email2` varchar(50) DEFAULT NULL,
  `clinic_instructions` mediumtext,
  `clinic_active` varchar(1) DEFAULT NULL,
  `clinic_date_inactive` varchar(50) DEFAULT '00/00/00',
  `clinic_date_active` varchar(50) DEFAULT '00/00/00',
  `clinic_phone2` varchar(30) DEFAULT NULL,
  `clinic_profession` varchar(50) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  `clinic_deleted` varchar(1) DEFAULT 'N',
  `clinic_notes` mediumtext,
  PRIMARY KEY (`clinicID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_specialist_clinic` WRITE;
/*!40000 ALTER TABLE `tbl_specialist_clinic` DISABLE KEYS */;

INSERT INTO `tbl_specialist_clinic` (`clinicID`, `siteID`, `clinic_name`, `clinic_responsibleperson`, `clinic_address`, `clinic_city`, `clinic_state`, `clinic_postcode`, `clinic_country`, `clinic_phone`, `clinic_mobile`, `clinic_fax`, `clinic_email`, `clinic_email2`, `clinic_instructions`, `clinic_active`, `clinic_date_inactive`, `clinic_date_active`, `clinic_phone2`, `clinic_profession`, `dateadded`, `clinic_deleted`, `clinic_notes`)
VALUES
	(1,1,'ACDS','','Sunshine Clinic. 141 Durham Rd','Sunshine','0',3020,'0','','','','','','','','00/00/00','00/00/00',NULL,'Echo',NULL,'N',''),
	(2,1,'ACDS','','Sheehans Medical Clinic, 81 Canterbury Road','Blackburn South','0',4306,'0','9877 1200','9877 1722','','','','','','00/00/00','00/00/00',NULL,'',NULL,'N','');

/*!40000 ALTER TABLE `tbl_specialist_clinic` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_staff
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_staff`;

CREATE TABLE `tbl_staff` (
  `staffID` int(11) NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `staff_name` varchar(50) DEFAULT NULL,
  `staff_email` varchar(50) DEFAULT NULL,
  `staff_notes` varchar(100) DEFAULT NULL,
  `staff_responsibleperson` varchar(30) DEFAULT NULL,
  `staff_fax` varchar(20) DEFAULT NULL,
  `staff_mobile` varchar(20) DEFAULT NULL,
  `staff_phone` varchar(20) DEFAULT NULL,
  `staff_postcode` varchar(4) DEFAULT NULL,
  `staff_state` varchar(10) DEFAULT '0',
  `staff_city` varchar(20) DEFAULT NULL,
  `staff_address` varchar(50) DEFAULT NULL,
  `staff_location` varchar(20) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `staff_service` varchar(20) DEFAULT NULL,
  `staff_address2` varchar(50) DEFAULT NULL,
  `staff_country` varchar(10) DEFAULT '0',
  `staff_deleted` varchar(1) DEFAULT 'N',
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`staffID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_staff` WRITE;
/*!40000 ALTER TABLE `tbl_staff` DISABLE KEYS */;

INSERT INTO `tbl_staff` (`staffID`, `siteID`, `staff_name`, `staff_email`, `staff_notes`, `staff_responsibleperson`, `staff_fax`, `staff_mobile`, `staff_phone`, `staff_postcode`, `staff_state`, `staff_city`, `staff_address`, `staff_location`, `staff_service`, `staff_address2`, `staff_country`, `staff_deleted`, `dateadded`)
VALUES
	(1,1,'Danielle','danielle@advisermed.com.au',NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,'0','N',NULL),
	(2,1,'Tabitha','tabitha@advisermed.com.au',NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,'0','N',NULL),
	(3,1,'George','george@healthpredictions.com',NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,'0','N',NULL),
	(4,1,'Adam','adam@healthpredictions.com','','','','','','','0','','',X'','','','0','N',NULL),
	(6,1,'Test Staff','test@swim.com.au','<p>test notes</p>','test','0000','00000','000000','3000','5','Test City','test adress',X'74657374206C6F636174696F6E','test service','','13','N',NULL);

/*!40000 ALTER TABLE `tbl_staff` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `siteID` int(11) DEFAULT NULL,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email2` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `usr_client` int(5) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  `usr_phone` varchar(50) DEFAULT NULL,
  `usr_mobile` varchar(200) DEFAULT NULL,
  `dateadded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `siteID`, `type_letter`, `username`, `password`, `name`, `email`, `email2`, `usr_status`, `usr_client`, `lastlogin`, `usr_phone`, `usr_mobile`, `dateadded`)
VALUES
	(1,1,'A','joshua','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au',NULL,'A',NULL,'January 18, 2017, 2:14 pm',NULL,NULL,NULL),
	(2,2,'A','sample','8b46f9154241ea7b458e6ce6e711d6da','Joshua 2','josh@swim.com.au',NULL,'A',NULL,NULL,NULL,NULL,NULL),
	(6,1,'B','test@healthpredictions.com.au','7348730ce7dac77c79a8807e9d77c792','Health Predictions Test','test@healthpredictions.com.au','test@healthpredictions.com.au','A',NULL,NULL,'9876 5432','0412 345 678',NULL);

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Developer','A'),
	(2,'Super Admin','B'),
	(3,'Administrator','C'),
	(4,'Supervisor','D'),
	(5,'Case File Manager','E'),
	(6,'Nurses/GP (on staff)','F'),
	(7,'Nurses/GP (contractors)','G'),
	(8,'Nurses/GP (contractors - own apointments)','H'),
	(9,'Advisors','I');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
