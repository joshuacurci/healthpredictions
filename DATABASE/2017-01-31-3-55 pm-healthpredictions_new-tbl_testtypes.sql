# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: healthpredictions_new
# Generation Time: 2017-01-31 04:55:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_client_testtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_testtypes`;

CREATE TABLE `tbl_client_testtypes` (
  `testtypeID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `groupID` int(11) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`testtypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_testtypes` WRITE;
/*!40000 ALTER TABLE `tbl_client_testtypes` DISABLE KEYS */;

INSERT INTO `tbl_client_testtypes` (`testtypeID`, `groupID`, `type_name`)
VALUES
	(1,1,'Specialist Insurance Assessment'),
	(2,1,'GP Insurance Assessment (GP)'),
	(3,1,'GP Insurance Assessment (Own Doctor)'),
	(4,1,'Paramedical Insurance Assessment'),
	(5,1,'Quickcheck/rapidcheck/fastcheck'),
	(6,1,'AIA - Short Form Medical'),
	(7,1,'AMP - Express Health Check'),
	(8,1,'Asteron - Quickcheck'),
	(9,1,'Aviva - Rapid Check'),
	(10,1,'AXA - Mini Check'),
	(11,1,'Comminsure - Medilite'),
	(12,1,'Clearview - Mini Check'),
	(13,1,'BT - Quickcheck'),
	(14,1,'One Path - Medi Quick'),
	(15,1,'Macquarie - Fast Check/Quickcheck'),
	(16,1,'Macquarie Short-Form Medical Report'),
	(17,1,'MLC - Mini Check'),
	(18,1,'Suncorp - Quickcheck'),
	(19,1,'Tal - Fast Check'),
	(20,1,'Westpac - Quickcheck'),
	(21,1,'Zurich - Express Exam'),
	(22,3,'Three Blood Pressure readings at 5 minute intervals'),
	(23,1,'Teleunderwriting'),
	(24,1,'Client personal medical history report (P.M.A.R)'),
	(25,3,'Electrocardiogram (resting ECG)'),
	(26,1,'Exercise stress test (exercise ECG)'),
	(27,3,'Stress Echocardiogram'),
	(28,3,'Chest X-Ray'),
	(29,3,'Echocardiogram'),
	(30,3,'Lung Function Test'),
	(31,3,'24hr Blood Pressure Monitor'),
	(32,3,'24hr Electrocardiogram Monitor'),
	(33,3,'Breast Exam'),
	(34,3,'Results of current mammogram (within last 12 months)'),
	(35,2,'NON-Fasting MBA20 (inc HbA1c,HDL/LDL/Chol)'),
	(36,2,'Fasting MBA20 (inc HDL/LDL/Chol)'),
	(37,2,'Hep B (antigens) Hep C (antibodies)'),
	(38,2,'H.I.V'),
	(39,2,'Full blood exam/Full blood count (FBE/FBC)'),
	(40,2,'ESR'),
	(41,2,'PSA'),
	(42,2,'Serum Iron Studies'),
	(43,2,'Thyroid Function'),
	(44,2,'HbA1c - (Glycosylated haemoglobin)'),
	(45,2,'Urine Micro & Culture (MSU)'),
	(46,2,'Urine Micro & Culture (MSU) with red cell morphology'),
	(47,2,'Urine drug screen and report'),
	(48,2,'Cotinine'),
	(49,2,'Fasting Blood Glucose Test'),
	(50,2,'Lipids Profile'),
	(51,2,'Glucose Tolerance'),
	(52,2,'Liver Function Test'),
	(53,2,'Hepatitis C PCR'),
	(54,2,'24 Hour Urinary Protein'),
	(55,2,'CDT - Carbohydrate Deficient Transferrin'),
	(56,2,'Creatinine Clearance Blood test'),
	(57,2,'Creatinine Clearance Urine test'),
	(58,2,'C Reactive Protein'),
	(59,2,'PSA Free (including Free to Total Ratio)'),
	(60,2,'Hepatitis B e Antigen'),
	(61,2,'Testosterone Blood test'),
	(62,2,'Albumin / Creatinine Ratio urine test'),
	(63,3,'Limousine Service to Appointment');

/*!40000 ALTER TABLE `tbl_client_testtypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
