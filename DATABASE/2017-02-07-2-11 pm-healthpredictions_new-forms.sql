# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: healthpredictions_new
# Generation Time: 2017-02-07 03:11:16 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_forms`;

CREATE TABLE `tbl_forms` (
  `formID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  PRIMARY KEY (`formID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_forms` WRITE;
/*!40000 ALTER TABLE `tbl_forms` DISABLE KEYS */;

INSERT INTO `tbl_forms` (`formID`, `name`)
VALUES
	(1,'Referral Acknowledgement'),
	(2,'Notification to Pathology of Appointment and Consent ID'),
	(3,'Notification to Doctor of Appointment and Consent ID'),
	(4,'Notification to Client of Appointment, Client Instructions and Consent ID'),
	(5,'Notification to Advisor of Appointments'),
	(6,'Notification to Advisor of Completion and Summary Form'),
	(7,'Notification to Insurance Company of Completion'),
	(8,'Client Consent and Identification');

/*!40000 ALTER TABLE `tbl_forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_forms_generated
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_forms_generated`;

CREATE TABLE `tbl_forms_generated` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` text,
  `formID` varchar(100) DEFAULT NULL,
  `appID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
